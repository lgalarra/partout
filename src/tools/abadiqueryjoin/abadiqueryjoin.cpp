#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/utsname.h>
#include "cts/semana/SemanticAnalysis.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DPlanGen.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DSemanticAnalysis.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DPlanSearch.hpp"
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DOperatorCostFunction.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"
#include <sstream>
#include <boost/thread.hpp>
#include "drdf/RemoteOperators.hpp"
#include "partitioning/Utilities.hpp"
#include "ParallelUnion.hpp"


#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
using namespace drdf;
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace drdf;

vector<SAStatistics> statistics;

struct Job{
    DOperator *dOperator;
    DRuntime *druntime;
    Runtime *runtime;
    RemoteFetch *remoteFetch;
};

struct SubqueryJob{
    Union *op;
    DRuntime *druntime;
    Runtime *runtime;
    Job *subjobs;
};


class ResultsWrapper : public Operator{
private:
    vector<Register*> outputRegisters;
    vector<vector<unsigned> > *rows;
    vector<vector<unsigned> >::iterator cursor;
public:
    
    ResultsWrapper(vector<Register*> &registers, vector<vector<unsigned> > *rows): Operator(0), outputRegisters(registers), rows(rows){}
    
    ~ResultsWrapper(){}
    
    unsigned first(){
        if(rows->empty()) return 0;
        
        cursor = rows->begin();
        vector<unsigned>::iterator icur = cursor->begin();
        for(vector<Register*>::iterator rit = outputRegisters.begin(); rit != outputRegisters.end(); ++rit){
            (*rit)->value = *icur;
            ++icur;
        }
        
        ++cursor;
        observedOutputCardinality += *icur;
        return *icur;
    }
    
    unsigned next(){
        if(cursor == rows->end()){
            return 0;
        }
        
        vector<unsigned>::iterator icur = cursor->begin();
        for(vector<Register*>::iterator rit = outputRegisters.begin(); rit != outputRegisters.end(); ++rit){
            (*rit)->value = *icur;
            ++icur;
        }
        
        ++cursor;
        observedOutputCardinality += *icur;        
        return *icur;
    }
    /// Print the operator
    void print(PlanPrinter& out){
    }
    /// Handle a merge hint
    void addMergeHint(Register* l,Register* r){
    }
    /// Register parts of the tree that can be executed asynchronous
    void getAsyncInputCandidates(Scheduler& scheduler){

    }
};

//---------------------------------------------------------------------------
bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}
//---------------------------------------------------------------------------
static void showHelp()
   // Show internal commands
{
    cerr << "usage: abadiqueryjoin <database> <originalDatabase> <queryfile>" << endl;
    cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
    cerr << "sa = simulated annealing; ls = local search" << endl;
}

static void printExecutionStats(){   
    unsigned i = 0;
    for(vector<SAStatistics>::iterator sit = statistics.begin(); sit != statistics.end(); ++sit, ++i){
       cout << "Q" << i << "," << (sit->endTime - sit->startTime) << "," << (sit->prepareForExecStartStamp - sit->startTime) << 
               "," << (sit->execEndStamp - sit->prepareForExecStartStamp) << "," << (sit->execEndStamp - sit->startTime) << "," << sit->cardinality  << endl;
    }
}

//---------------------------------------------------------------------------
static void buildSubquery(DDatabase *db, Database *dummy, const string& query, SubqueryJob *subquery)
   // Evaluate a query
{
    
   QueryGraph queryGraph;
   {
      // Parse the query
      SPARQLLexer lexer(query);
      SPARQLParser parser(lexer);
      try {
         parser.parse();
      } catch (const SPARQLParser::ParserException& e) {
         cerr << "parse error: " << e.message << endl;
         return;
      }

      // And perform the semantic anaylsis
      try {
         SemanticAnalysis semana(*dummy);
         semana.transform(parser,queryGraph);
      } catch (const SemanticAnalysis::SemanticException& e) {
         cerr << "semantic error: " << e.message << endl;
         return;
      }
      if (queryGraph.knownEmpty()) {
         cerr << "static analysis determined that the query result will be empty" << endl;
         cout << "<empty result>" << endl;
         return;
      }
   }
   
   PlanGen plangen;
   Plan* plan=plangen.translate(*dummy,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return;
   }

   // Build a physical plan
   vector<drdf::Host> allHosts;
   db->getFragmentsDefinitionSegment().getAllHosts(allHosts);
   vector<DOperator*> operators;
   DOperatorTransformer transformer(db);   
   Job *jobs = new Job[allHosts.size()];
   
   unsigned h = 0;
   for(vector<drdf::Host>::iterator hit = allHosts.begin(); hit != allHosts.end(); ++hit, ++h){
       Runtime *baseRuntime = new Runtime(*dummy, NULL, NULL);
       jobs[h].runtime = baseRuntime;
       jobs[h].druntime = new DRuntime(*db, *baseRuntime);       
       vector<Register*> outputRegisters;       
       Operator *op = DCodeGen().translate(*jobs[h].druntime, queryGraph, plan, outputRegisters, true);
       DOperator *dop = transformer.buildDOperator(op);
       operators.push_back(dop);
	   dop->setOutputRegisters(outputRegisters);
	   cout << "[";
	   for(vector<Register*>::iterator rit = outputRegisters.begin(); rit != outputRegisters.end(); ++rit){
			cout << jobs[h].druntime->registerOffset(*rit) << ", ";
	   }
	   cout << "]" << endl;
       //Implement a method which assigns the same home host to all operators
       transformer.forceHomeHost(dop, hit->uri);
       //Then retrieve everything in parallel
       jobs[h].dOperator = dop;
       jobs[h].remoteFetch = new RemoteFetch(jobs[h].dOperator->getOperator(), jobs[h].dOperator, string("") , hit->uri, &jobs[h].druntime->getRuntime());   
       DebugPlanPrinter out(*baseRuntime, false);
       jobs[h].remoteFetch->print(out);	
       string errorMsg;
       jobs[h].remoteFetch->prepareRequest(errorMsg);
       if(!errorMsg.empty()){
              cerr << errorMsg << endl;
			  assert(0);
		}
   }

   vector<Operator*> parts;
   vector<vector<Register*> > mappings, initial;   
   Runtime *aRuntime = new Runtime(*dummy, NULL, NULL);
   subquery->runtime = aRuntime;
   subquery->druntime = new DRuntime(*db, *aRuntime);
   for(h = 0; h < allHosts.size(); ++h){
       vector<Register*> &outRegs = jobs[h].dOperator->getOutputRegisters();
       //The first time, request extra allocation
       if(h == 0)
            subquery->druntime->allocateRegisters(outRegs.size());
       
       vector<Register*> maps, init;
       unsigned r = 0;
       for(vector<Register*>::iterator rit = outRegs.begin(); rit != outRegs.end(); ++rit, ++r){
           maps.push_back(*rit);
           maps.push_back(subquery->druntime->getRegister(r));
           init.push_back(subquery->druntime->getRegister(r));
       }
       mappings.push_back(maps);
       initial.push_back(init);
       parts.push_back(jobs[h].remoteFetch);
   }
   
   subquery->op = new Union(parts, mappings, initial, 0);
   cout << initial.size() << endl;
   subquery->subjobs = jobs;
}

static Operator * joinSubqueryJobs(SubqueryJob *jobs, DRuntime &runtime, unsigned nJobs, vector<unsigned> &joinCols, unsigned &card, vector<unsigned> &distinctCols, bool silent){
    //Create a join chain
    HashJoin *lastJoin, *currentJoin;
    Operator *topOperator;
    unsigned jcols = 0;
    for(unsigned i = 1; i < nJobs; ++i){
        Register *left = jobs[i - 1].op->initializations[0].at(joinCols[jcols]);
        ++jcols;            
        Register *right = jobs[i].op->initializations[0].at(joinCols[jcols]);
        ++jcols;        
        if(i == 1){
            currentJoin = new HashJoin(dynamic_cast<Operator*>(jobs[i - 1].op), left, jobs[i - 1].op->initializations[0], dynamic_cast<Operator*>(jobs[i].op), right, jobs[i].op->initializations[0], 0, 0, 0);
        }else{
            vector<Register*> leftTail;
            leftTail.insert(leftTail.begin(), lastJoin->leftTail.begin(), lastJoin->leftTail.end());
            leftTail.insert(leftTail.begin(), lastJoin->rightTail.begin(), lastJoin->rightTail.end());            
            currentJoin = new HashJoin(dynamic_cast<Operator*>(lastJoin), left, leftTail, dynamic_cast<Operator*>(jobs[i].op), right, jobs[i].op->initializations[0], 0, 0, 0);
        }
        
        lastJoin = currentJoin;
    }
    
    vector<Register*> values;    
    if(!distinctCols.empty()){
        unsigned k = 0;
        for(vector<unsigned>::iterator colit = distinctCols.begin(); colit != distinctCols.end(); ++colit, ++k){
            values.push_back(jobs[k].op->initializations[0].at(*colit));
        }
        
        HashGroupify *hg = new HashGroupify(lastJoin, values, 0);
        topOperator = hg;
    }else{
        topOperator = lastJoin;
    }

    unsigned realCard = 0;
    unsigned rowCard = 0;
    if(topOperator->first()){
		realCard++;
		rowCard++;
        unsigned count;
        while((count = topOperator->next())){
            if(!silent){
                for(unsigned j = 0; j < nJobs; ++j){
                    unsigned sz = jobs[j].op->initializations[0].size();                    
                    for(unsigned i = 0; i < sz; ++i){
                        string result1;
                        unsigned stype1;
                        Type::ID type1;
                        lookupById(jobs->druntime->getRuntime().getDatabase(), jobs[j].op->initializations[0].at(i)->value, type1, stype1, result1);
                        cout << result1 << "(" << jobs[j].op->initializations[0].at(i)->value << ")" << "||";
                    }
                }                    
                
                cout << "||" << count << "||" << endl;
            }
            ++rowCard;
            //For query 10
//            if(jobs[0].op->initializations[0].at(1)->value != jobs[1].op->initializations[0].at(1)->value)
                realCard += count;
                
//            if(realCard >= 2000)
//                break;
            
        }
    }

    card = topOperator->getObservedOutputCardinality();
    cout << "Real card: " << realCard << endl;
    cout << "Row card: " << rowCard << endl;
    return topOperator;
}
//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    // Warn first
    SAStatistics stats;
    bool silent = true;
    std::vector<unsigned> joinColumns, distinctColumns;
    if (smallAddressSpace())
       cerr << "Warning: Running DRDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "DRDF-3X query interface" << endl
         << "(c) 2011 Luis Galárraga, based on RDF-3X query interface by 2008 Thomas Neumann." << endl;

    // Check the arguments
    if (argc < 4) {
        showHelp();
        return 1;
    }

    //Open the dummy database
    Database db;
    if(!db.open(argv[2], true)){
       cerr << "An original RDF-3x database has not been provided " << endl;
       return 1;
    }

    // Open the database
    DDatabase ddb;
    if (!ddb.open(argv[1], &db.getDictionary(), false)) {
        cerr << "unable to open distributed database " << argv[1] << endl;
        return 1;
    }

    ifstream in(argv[3]);
    if (!in.is_open()) {
        cerr << "unable to open " << argv[3] << endl;
        return 1;
    }
    
    if(argc > 4){
        string silentStr(argv[4]);
        if(silentStr.compare("no") == 0){
            silent = false;
        }
    }

    char registers[1024];
    in.getline(registers, 1024);

    std::stringstream ss(registers);
    int i;
    while (ss >> i) {
        joinColumns.push_back(i);
        ss.ignore(1);
    }
    
    char distinctClause[1024];
    in.getline(distinctClause, 1024);
    string distinct(distinctClause);
    cout << distinct << endl;
    if(!distinct.empty()){
        std::stringstream ss1(distinct);
        int j;
        while (ss1 >> j) {
            distinctColumns.push_back(j);
            cout << j << endl;
            ss1.ignore(1);
        }
    }

    vector<string> subqueries;      
    while(!in.eof()){
        char queryStr[8192];
        in.getline(queryStr, 8192);
        string query = string(queryStr);
        if(query.empty()) continue;      
        subqueries.push_back(query);
    }        

    stats.startTime = Timestamp();
    boost::thread::thread ** threads = new boost::thread::thread*[subqueries.size()];
    SubqueryJob *jobs = new SubqueryJob[subqueries.size()];
    unsigned s = 0;

    // Build a physical plan
    Runtime baseRuntime(db, NULL, NULL);
    DRuntime runtime(ddb, baseRuntime);

    for(vector<string>::iterator qit = subqueries.begin(); qit != subqueries.end(); ++qit, ++s){
        threads[s] = new boost::thread(buildSubquery, &ddb, &db, *qit, &jobs[s]);
    }

    for(s = 0; s < subqueries.size(); ++s){
        threads[s]->join();
    }

    unsigned card;
    Operator *join = joinSubqueryJobs(jobs, runtime, subqueries.size(), joinColumns, card, distinctColumns, silent);

    for(s = 0; s < subqueries.size(); ++s){
        for(unsigned k = 0; k < jobs[s].op->parts.size(); ++k){
            delete jobs[s].subjobs[k].dOperator->getOperator();
            delete jobs[s].subjobs[k].dOperator;
            delete jobs[s].subjobs[k].druntime;
            delete jobs[s].subjobs[k].runtime;
        }
        delete jobs[s].runtime;
        delete jobs[s].druntime;
        delete[] jobs[s].subjobs;
    }

    //Free memory      
    delete[] threads;
    delete join;
    delete[] jobs;
    
    stats.endTime = Timestamp();
    stats.cardinality = card;
    statistics.push_back(stats);    
    printExecutionStats();
}
//---------------------------------------------------------------------------
