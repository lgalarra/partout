/**
 * RDF-3x sampler program. Takes a big dataset and generates a smaller RDF-3x 
 * file containing a uniform and random sample of its data.
 * @param argc
 * @param argv
 * @return 
 */

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <fstream>
#include <string>
#include "rts/segment/FactsSegment.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/database/Database.hpp"
#include "partitioning/Utilities.hpp"
#include <iostream>
#include <libgen.h>

string _tmpPrefix, _outputFile, _binPrefix;
using namespace std;

// Is the address space too small?
bool smallAddressSpace()
{
   return sizeof(void*)<8;
}

void sample(Database *db, unsigned subSample, int tid){
    FactsSegment &facts = db->getFacts(Database::Order_Subject_Predicate_Object);
    FactsSegment::Scan scan;
    srand(time(NULL));
    ostringstream fileName;
    fileName << _tmpPrefix << "rdf3xsampler_" << tid;
    fstream tmpStm(fileName.str().c_str(), ios_base::in | ios_base::out | ios_base::trunc);

    for(unsigned i = 0; i < subSample; ++i){
        unsigned pos1, pos2, pos3;
        string rawText, subjectText, predicateText, objectText;
        unsigned subType;
        Type::ID type;
        
        while(true){
            //Just choose a random page
            pos1 = rand() % (db->getDictionary().getNextId() - 1);
            pos2 = 0;
            pos3 = 0;        
            if(scan.first(facts, pos1, pos2, pos3))
                break;
        }
        
        bool resultSub = lookupById(*db, scan.getValue1(), type, subType, rawText, subjectText);
        bool resultPred = lookupById(*db, scan.getValue2(), type, subType, rawText, predicateText);
        bool resultObj = lookupById(*db, scan.getValue3(), type, subType, rawText, objectText);
        if(resultSub && resultPred && resultObj){
            tmpStm.write(subjectText.c_str(), subjectText.size());
            tmpStm.write(" ", 1);
            tmpStm.write(predicateText.c_str(), predicateText.size());
            tmpStm.write(" ", 1);
            tmpStm.write(objectText.c_str(), objectText.size());
            tmpStm.write(" .\n", 3);             
        }
    }
    
    tmpStm.close();
}

void sampleDatabase(Database &db, float factor){
    unsigned sampleSize = (unsigned)(factor * (float)db.getExactStatistics().getCardinality(~0u, ~0u, ~0u));	
    unsigned nThreads = boost::thread::hardware_concurrency();    
    boost::thread **threads = new boost::thread*[nThreads];

    unsigned subSample = sampleSize / nThreads;
    for(unsigned i = 0; i < nThreads; ++i){
        threads[i] = new boost::thread(boost::bind(sample, &db, subSample, i));
    }
	
    for(unsigned i = 0; i < nThreads; ++i){
        threads[i]->join();
    }

    for(unsigned i = 0; i < nThreads; ++i){
        delete threads[i];
    }
    
    //Time to merge the results
    ostringstream command;
    command << _binPrefix << "/rdf3xload " << _outputFile;
    for(unsigned i = 0; i < nThreads; ++i){
        command << " " << _tmpPrefix << "rdf3xsampler_" << i;
    }
    
    string commandText = command.str();
    cout << "Running command: " << commandText << endl;
    unsigned int syrt = system(commandText.c_str());
    if(syrt != 0){
        cerr << "The command " + commandText + " failed" << endl;
    }
    

    for(unsigned i = 0; i < nThreads; ++i){
        ostringstream removeStm;
        removeStm << _tmpPrefix << "rdf3xsampler_" << i;
        remove(removeStm.str().c_str());
    }
    

    delete[] threads;     
}

//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    Database db;
    float factor;

    // Warn first
    if (smallAddressSpace())
      cerr << "Warning: Running RDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "RDF-3X database sampler" << endl
            << "(c) 2011 Luis Galarraga" << endl;

    // Check the arguments
    if (argc < 4) {
      cerr <<  "usage: " << argv[0] << " <database> <factor> <outputFile> [temporalLocation]" << endl
               << "fraction is a number between 0 and 1 and determines the fraction of tuples taken in the sample" << endl
               << "temporalLocation determines the location in the filesystem for temporary files";
      return 1;
    }

    if(!db.open(argv[1], true)){
        cerr << "The database file " << argv[1] << " could not be opened" << endl;
        return 1;
    }

    try{
        factor = boost::lexical_cast<float>(string(argv[2]));
    }catch(std::exception &e){
        cerr << e.what() << endl; 
    }
    
   char *dirName = dirname(argv[0]);
   _binPrefix = string(dirName);

   _outputFile = string(argv[3]);
   
  //Location for temporal files
   if(argc > 4){
       _tmpPrefix = string(argv[4]);
       if(_tmpPrefix[_tmpPrefix.length() - 1] != '/'){
           _tmpPrefix += "/";
       }
   }else{
       _tmpPrefix = "/tmp/";
   }
 
   sampleDatabase(db, factor);
   cout << "Done." << endl;

    return 0;
}