/* 
 * File:   fragmentstats.cpp
 * Author: luis
 * Created on June 21, 2011, 5:51 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */
/*
 * This program takes a database and a raw query load file (HTTP logs for SPARQL queries)
 * and outputs a new file with all the queries normalized. 
 */

#include "rts/database/Database.hpp"
#include "partitioning/CommonTypes.hpp"
#include "qload/QueryLoadDecodedInputStream.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;
using namespace qload;
using namespace stdpartitioning;

bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}

struct QueryStatsDescriptor{    
public:
   string query;
   vector<pair<string, vector<QueryGraph::Node> > > assignment;
   
   QueryStatsDescriptor(): remoteOperations(-1), localOperations(-1){};

   int getRemoteOperations(){
       if(remoteOperations == -1){
           remoteOperations = 0;           
           vector<pair<string, vector<QueryGraph::Node> > >::const_iterator it, it2;           
           vector<QueryGraph::Node>::const_iterator nit, nit2;           
           for(it = this->assignment.begin(); it != this->assignment.end(); ++it){
               for(nit = it->second.begin(); nit != it->second.end(); ++nit ){
                   for(it2 = it + 1; it2 != this->assignment.end(); ++it2){
                       for(nit2 = it2->second.begin(); nit2 != it2->second.end(); ++nit2){
                           if(nit2->canJoin(*nit)){
                               ++remoteOperations;
                           }
                       }
                   }
               }
           }
       }
       
       return remoteOperations;
   }; 
   
   
   int getLocalOperations(){
       if(localOperations == -1){
           localOperations = 0;
           vector<pair<string, vector<QueryGraph::Node> > >::const_iterator it;           
           vector<QueryGraph::Node>::const_iterator nit, nit2;           
           for(it = this->assignment.begin(); it != this->assignment.end(); ++it){
               for(nit = it->second.begin(); nit != it->second.end(); ++nit ){
                   for(nit2 = nit + 1; nit2 != it->second.end(); ++nit2){
                       if(nit2->canJoin(*nit)){
                           ++localOperations;
                       }
                   }                   
               }
           }            
       }
       
       return localOperations;
   };
   
private:
   int remoteOperations;
   int localOperations;

};

typedef struct{
    Database *db;
    string name;    
}FragmentDescriptor;

void extractQueryGraph(Database &db, string query, QueryGraph &queryGraph){
     SPARQLLexer lexer(query);
     SPARQLParser parser(lexer);
     parser.parse();

     SemanticAnalysis semana(db);
     semana.transform(parser,queryGraph);
}

string buildQuery(Database &db, QueryGraph::Node &node){
    string s, p, o;
    Type::ID type;
    unsigned int subtype;
    
    if(node.constObject){
        if(!lookupById(db, node.object, type, subtype, o))
            o = "?o";
    }else{
        o = "?o";
    }
    
    if(node.constPredicate){
        if(!lookupById(db, node.predicate, type, subtype, p))
            p = "?p";
    }else{
        p = "?p";
    }
    
    if(node.constSubject){
        if(!lookupById(db, node.subject, type, subtype, s))
            s = "?s";
    }else{
        s = "?s";
    }
    
    return "SELECT count * WHERE { " + s + " " + p + " " + o + " . }";
}

inline ostream& operator<<(ostream& str, QueryStatsDescriptor& descriptor){
    str << "query=" << descriptor.query << endl;
    str << "djoins=" << descriptor.getRemoteOperations() << endl;
    str << "ljoins=" << descriptor.getLocalOperations() << endl;
    str << endl;
    
    return str;
}

void calculateStatistics(char *queryFile, vector<FragmentDescriptor>& fragments){
    string inputStreamFile(queryFile);
    QueryLoadDecodedInputStream inputStream(inputStreamFile);
    unsigned int nFrags = fragments.size();
    inputStream.open();
    vector<QueryStatsDescriptor> descriptors;
    
    while(!inputStream.eof()){
        string query;
        
        inputStream >> query;
                
        if(query.empty())
            continue;
        
        cout << query << endl;

        QueryStatsDescriptor stats;
        stats.query = query;
        //Now run the query against every database
        for(unsigned int i = 0; i < nFrags; ++i){
            QueryGraph queryGraph;
            vector<QueryGraph::SubQuery> subqueries;
            cout << "Fragment " << fragments[i].name << endl;
            extractQueryGraph(*fragments[i].db, query, queryGraph);
            cout << "This query graph has " << queryGraph.getQuery().nodes.size() << " nodes and " << queryGraph.getQuery().unions.size() << " unions " << endl;
            extractIndependentSubqueries(queryGraph.getQuery(), subqueries);            
            stats.assignment.push_back(pair<string, vector<QueryGraph::Node> >(fragments[i].name, vector<QueryGraph::Node>()));
            for(vector<QueryGraph::SubQuery>::iterator it = subqueries.begin(); it != subqueries.end(); ++it){
                for(vector<QueryGraph::Node>::iterator nit = it->nodes.begin(); nit != it->nodes.end(); ++nit){
                    string queryPattern = buildQuery(*fragments[i].db, *nit);
                    cout << "Query pattern: " << queryPattern << endl; 
					unsigned int card;
                    if(queryHasResults(*fragments[i].db, queryPattern, card)){
                        stats.assignment.back().second.push_back(*nit);
                    }
                }
            }
        }
        descriptors.push_back(stats);
    }
    
    for(vector<QueryStatsDescriptor>::iterator qsit = descriptors.begin(); qsit != descriptors.end(); ++qsit){
        cout << *qsit << endl;
    }
    
}

int main(int argc, char** argv) {
    Database* fragmentDbs;
    unsigned int nFragments;
    
   // Warn first
   if (smallAddressSpace()){
      cerr << "Warning: Running RDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;
   }
   // Greeting
   cerr << "RDF-3X load-aware fragments statistics extractor" << endl << "(c) 2011 Luis Galarraga." << endl;

   // Check the arguments
   if (argc < 3) {
      cerr <<  "usage: " << argv[0] << " <fragmentsDescriptor> <queries>" << endl;
      cerr << "At least 2 database fragments are needed" << endl;
      return 1;      
   }
   
   nFragments = argc - 2;
   fragmentDbs = new Database[nFragments];
   vector<FragmentDescriptor> fdescriptors;
   for(unsigned int i = 0; i < nFragments; ++i){
       if(!fragmentDbs[i].open(argv[i + 2])){
           cerr << "unable to open database " << argv[i + 2] << endl;
           delete[] fragmentDbs;
           return 1;
       }
       
       FragmentDescriptor fd;
       fd.db = &fragmentDbs[i];
       fd.name = string(argv[i + 2]);
       fdescriptors.push_back(fd);       
   }
  
   calculateStatistics(argv[1], fdescriptors);
   
   delete[] fragmentDbs;
   return 0;
}
