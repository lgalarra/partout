/* 
 * File:   DictionaryServerHandler.hpp
 * Author: lgalarra
 *
 * Created on September 9, 2011, 6:26 PM
 */

#ifndef src_tools_drdf3xserver_dictionaryserverhandler_hpp
#define	src_tools_drdf3xserver_dictionaryserverhandler_hpp

#include "rts/database/Database.hpp"
#include "server/RDF3xServerMessages.h"
#include "common.hpp"

/**
 * Class for processing dictionary lookups
 * @param db
 */
using namespace drdf::message;

class DictionaryServerHandler {    
public:
    DictionaryServerHandler(Database *db): db(db){}

    //Methods to process dictionary lookups
    void process(const RequestDescriptor &, const DictionaryLookupByIdRequest &request, DictionaryLookupByIdResponse &response);

    void process(const RequestDescriptor &, const DictionaryLookupByStringRequest &request, DictionaryLookupByStringResponse &response);
    
    ~DictionaryServerHandler(){}
private:    
    Database *db;

};

#endif	/* DICTIONARYSERVERHANDLER_HPP */

