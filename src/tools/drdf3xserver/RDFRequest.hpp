/* 
 * File:   RDFRequest.hpp
 * Author: lgalarra
 *
 * Created on September 8, 2011, 11:55 AM
 */

#ifndef src_tools_rdf_request_hpp
#define	src_tools_rdf_request_hpp

#include "drdf/DOperator.hpp"

/**
 * Base class for all types of requests supported by the 
 * server
 */
class RDFResponse;

using namespace drdf;

class RDFRequest {
public:
    RDFRequest(unsigned id, unsigned cid):id(id), cid(cid){}

    RDFRequest(const RDFRequest& orig); 

    virtual ~RDFRequest();

private:
    /**
     * The request id
     */
    unsigned id;
    
    /**
     * The client id
     */
    unsigned cid;

    unsigned getId(){ return id; }
    unsigned getClientId(){ return cid; }
};

class DictionaryLookupByIdRequest: public RDFRequest{
private:
    DOperator *op;
public:
    DictionaryLookupByIdRequest(unsigned id, unsigned cid, DOperator *op): RDFRequest(id, cid), op(op){}
    
    ~DictionaryLookupByIdRequest(){}

    DOperator* getOperator(){ return op; }
};

class DictionaryLookupByStringRequest: public RDFRequest{
private:
    string value;
public:
    DictionaryLookupByStringRequest(unsigned id, unsigned cid, string value): RDFRequest(id, cid), value(value){}
};


#endif	/* RDFREQUEST_HPP */

