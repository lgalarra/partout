/* 
 * File:   RDF3xServer.hpp
 * Author: luis
 *
 * Created on September 8, 2011, 10:35 PM
 */

#ifndef src_tools_rdf3xserver_hpp
#define	src_tools_rdf3xserver_hpp

#include "common.hpp"
#include "drdf/CommonTypes.hpp"
#include "RDF3xServerHandler.hpp"
#include <deque>

using namespace std;

class RDF3xServer : private boost::noncopyable{
public:
    explicit RDF3xServer(const string &address, const string& port, size_t thread_pool_size, stdpt::DatabaseDescriptor &database);
    
    virtual ~RDF3xServer();
    
    /// Run the server's io_service loop.
    void run();

    /// Stop the server.
    void stop();
    
    void start_accept();

private:

    /// Handle completion of an asynchronous accept operation.
    void handle_accept(RDF3xServerHandler::pointer new_connection, const boost::system::error_code &e);

    /// The number of threads that will call io_service::run().
    size_t thread_pool_size_;

    /// The io_service used to perform asynchronous operations.
    boost::asio::io_service io_service_;

    /// Acceptor used to listen for incoming connections.
    boost::asio::ip::tcp::acceptor acceptor_;
        
    //Server shared data
    ServerSharedData *sharedData;
    
};

#endif	/* RDF3XSERVER_HPP */

