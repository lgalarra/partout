/* 
 * File:   common.hpp
 * Author: luis
 * Created on September 8, 2011, 10:23 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#ifndef src_tools_drdf3xserver_common_hpp
#define	src_tools_drdf3xserver_common_hpp

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/regex.hpp>

#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <iomanip>

#include <iostream>
#include <string>
#include <cstdlib>
#include <map>
#include "drdf/DOperator.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/database/Database.hpp"
#include "partitioning/CommonTypes.hpp"

namespace ba=boost::asio;
namespace bs=boost::system;
namespace stdpt=stdpartitioning;

typedef boost::shared_ptr<ba::ip::tcp::socket> socket_ptr;
typedef boost::shared_ptr<ba::io_service> io_service_ptr;
typedef std::deque<io_service_ptr> ios_deque;
using namespace std;
using namespace log4cplus;

struct RequestDescriptor{
    uint64_t reqid;
    uint64_t cid;
};

enum ExecState{Received, Scheduling, ReadyToRun, UnableToRun, Running, Done, Interrupted, Forgotten};

struct ExecRequestDescriptor{
    RequestDescriptor request;
    bool readonly; //Is it a read-only operation?
    ExecState status;    
    unsigned opid; //Operator id
    drdf::DOperator *opTree; //The operator to remove. It will be null at Received and Scheduling states
    unsigned timestamp; //Use to remove old requests    
    Runtime *runtime;
    
    ExecRequestDescriptor(): runtime(0), opTree(0){}
    
    void buildRuntime(Database &db){        
        runtime = new Runtime(db);
    }
    
    ~ExecRequestDescriptor(){
        if(opTree != NULL){
            Operator *opt = opTree->getOperator();
            if(opt != NULL) delete opt;
            delete opTree;
        }
        
        if(runtime != NULL) 
            delete runtime;
    }
};

inline Logger getLogger(){
   stringstream sstr;
   sstr << boost::this_thread::get_id();
   return Logger::getInstance("server." + sstr.str());
}

struct ServerSharedData{
    //Known operations
    map<uint64_t, ExecRequestDescriptor> operations;
    //Database descriptor
    stdpt::DatabaseDescriptor dbds;    
    //reqId counter
    uint64_t reqid;
    
    //Mutual exclusion for the operations map
    //boost::shared_mutex operations_mutex;
    boost::mutex operations_mutex;
    //Mutual exclusion for the assignment of new ids
    boost::mutex reqid_mutex;
    //Server name
    string serverName;
    
    ServerSharedData(): reqid(0){}
    
    uint64_t newRequestId(){
        boost::mutex::scoped_lock lock(reqid_mutex);
        return ++reqid;
    }
    
};

#endif