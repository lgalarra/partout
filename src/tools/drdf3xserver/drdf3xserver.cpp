/* A simple server in the internet domain using TCP
   The port number is passed as an argument 
   This version runs forever, forking off a separate 
   process for each connection
*/
#include "common.hpp"
#include "RDF3xServer.hpp"
#include "rts/segment/FactsSegment.hpp"
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <iomanip>

using namespace log4cplus;

using namespace std;

int main(int argc, char* argv[]){
    try{

        // Check command line arguments.
        if (argc < 4)
        {
          cerr << "Usage: drdf3xserver <address> <port> <database> <threads>\n";
          return 1;
        }

        // Initialize server.
        size_t num_threads = boost::thread::hardware_concurrency();
        if(argc >= 5)
            num_threads = boost::lexical_cast<size_t>(argv[4]);
        
        BasicConfigurator config;
        config.configure();
        Logger logger = Logger::getInstance("server");
        LOG4CPLUS_INFO(logger, "Using a pool of " << num_threads << " threads " << endl);

        string host(const_cast<const char*>(argv[1]));
        string port(const_cast<const char*>(argv[2]));

        //Open the database
        Database theDb;

        if(!theDb.open(argv[3], false)){
            cerr << "The database file " << argv[3] << " could not be opened" << endl;
            return 2;
        }
        
        LOG4CPLUS_INFO(logger, "In charge of " << theDb.getFacts(Database::Order_Subject_Predicate_Object).getCardinality() << " triples " << endl);

        //The database exists, now time to start the server
        stdpt::DatabaseDescriptor ddbs;
        ddbs.db = &theDb;
        ddbs.filename = string(argv[3]);

        RDF3xServer server(host, port, num_threads, ddbs);

        server.start_accept();
        // Run the server until stopped.
        server.run();
        
        //If the server stops, just close the database
        theDb.close();
    }catch (std::exception& e){
        std::cerr << "exception: " << e.what() << "\n";
    }
    
    return 0;
}
