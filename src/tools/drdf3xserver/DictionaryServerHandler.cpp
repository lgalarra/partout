/* 
 * File:   DictionaryServerHandler.cpp
 * Author: lgalarra
 * 
 * Created on September 9, 2011, 6:26 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "DictionaryServerHandler.hpp"
#include "partitioning/Utilities.hpp"
#include "infra/util/Type.hpp"
#include "common.hpp"
#include "server/RDF3xServerLookupMessages.h"
#include "rts/segment/DictionarySegment.hpp"

using namespace drdf::message;

//Methods to process dictionary lookups
void DictionaryServerHandler::process(const RequestDescriptor &rd, const DictionaryLookupByIdRequest &request, DictionaryLookupByIdResponse &response){
    //Iterate over the requests
    uint32_t nRequests = request.requests_size();
    for(uint32_t i = 0; i < nRequests; ++i){
        DictionaryLookupByIdRequest::SingleRequest singleRequest = request.requests(i);
        ::Type::ID type;
        unsigned subtype;
        string result;
        uint64_t id = singleRequest.lid();
        cout << "Processing " << endl;
        singleRequest.PrintDebugString();        
        bool success = lookupById(*this->db, (unsigned)id, type, subtype, result);
        //Prepare the response
        DictionaryLookupByIdResponse::SingleResponse *singleResponse = response.add_responses();
        singleResponse->set_success(success);
        if(success){
            singleResponse->set_lid(static_cast<uint64_t>(id));
            singleResponse->set_result(result);
            singleResponse->set_type(static_cast<drdf::message::EntityType>(type));
            singleResponse->set_subtype(static_cast<uint64_t>(subtype));            
        }
    }
    
    response.set_cid(rd.cid);
    response.set_reqid(rd.reqid);
    response.set_status(DictionaryLookupByIdResponse::SUCCESS);
    
}

void DictionaryServerHandler::process(const RequestDescriptor &rd, const DictionaryLookupByStringRequest &request, DictionaryLookupByStringResponse &response){
    //Iterate over the requests
    uint32_t nRequests = request.requests_size();
    for(uint32_t i = 0; i < nRequests; ++i){
        DictionaryLookupByStringRequest::SingleRequest singleRequest = request.requests(i);
        cout << "Processing " << endl;
        singleRequest.PrintDebugString();
        Type::ID type = static_cast<Type::ID>(singleRequest.type());
        unsigned subtype = static_cast<unsigned>(singleRequest.subtype());
        string key = singleRequest.literal();
        unsigned id;
        bool success = this->db->getDictionary().lookup(key, type, subtype, id);
        //Prepare the response
        DictionaryLookupByStringResponse::SingleResponse *singleResponse = response.add_responses();
        singleResponse->set_success(success);
        singleResponse->set_literal(key);
        singleResponse->set_type(static_cast<drdf::message::EntityType>(type));
        singleResponse->set_subtype(static_cast<uint64_t>(subtype));                    
        if(success){
            singleResponse->set_result(static_cast<uint64_t>(id));
        }
    }
    response.set_cid(rd.cid);
    response.set_reqid(rd.reqid);
    response.set_status(DictionaryLookupByStringResponse::SUCCESS);
    response.PrintDebugString();
}


