/* 
 * File:   RDF3xServerHandler.h
 * Author: luis
 *
 * Created on September 8, 2011, 11:06 PM
 */

#ifndef src_tools_rdf3xserverhandler_hpp
#define	src_tools_rdf3xserverhandler_hpp

#include <map>
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <iomanip>

#include "common.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DOperator.hpp"

using namespace log4cplus;
using namespace drdf;
using namespace drdf::message;

class RDF3xServerHandler: public boost::enable_shared_from_this<RDF3xServerHandler> {
public:
    typedef boost::shared_ptr<RDF3xServerHandler> pointer;
        
    static const unsigned defaultBufferSize;
    unsigned temporalBufferSize;

    static pointer create(ba::io_service& io_service, Database *db) {
        stringstream sstr;
        sstr << boost::this_thread::get_id();        
		string threadName = sstr.str();
        return pointer(new RDF3xServerHandler(io_service, db, threadName));
    }

    ba::ip::tcp::socket& socket() {
        return bsocket_;
    }

    void start(ServerSharedData *);
    DOperator * parseOperatorTree(PrepareForExecRequest &);
	~RDF3xServerHandler();
private:
    ServerSharedData *sharedData;
    RDF3xServerHandler(ba::io_service& io_service, Database *db, string hid);
    void dispatch(const bs::error_code& err, size_t len);
    void start_connect();
    void shutdown();
    void handleDictionaryLookupById(const RequestDescriptor &rd, const DictionaryLookupByIdRequest &request);
    void handleDictionaryLookupByString(const RequestDescriptor &rd, const DictionaryLookupByStringRequest &request);
    void handlePrepareForExecRequest(const RequestDescriptor &rd, const PrepareForExecRequest &request);
    void handleExecRequest(const RequestDescriptor &rd, const ExecRequest &request);

    ba::io_service& io_service_;
    ba::ip::tcp::socket bsocket_;
    ba::ip::tcp::socket ssocket_;
    ba::ip::tcp::resolver resolver_;

    boost::array<char, 32> headerBuffer;
    Database *db;
    string hid; //Handler id
    Logger logger; //Object to log execution information
};

#endif	/* RDF3XSERVERHANDLER_H */

