/* 
 * File:   RDF3xServerHandler.cpp
 * Author: luis
 * Created on September 8, 2011, 11:06 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "RDF3xServerHandler.hpp"
#include "DictionaryServerHandler.hpp"
#include "ExecutionServerHandler.hpp"
#include <iostream>
#include <string.h>
#include <boost/thread/pthread/recursive_mutex.hpp>
#include <exception>
#include <iostream>

using namespace drdf;
using namespace drdf::message;
using namespace log4cplus;

unsigned const RDF3xServerHandler::defaultBufferSize = 5;

/** 
 * 
 * 
 * @param io_service 
 */
RDF3xServerHandler::RDF3xServerHandler(ba::io_service& io_service, Database *db, string hid) : io_service_(io_service),
                                                     bsocket_(io_service),
                                                     ssocket_(io_service),
                                                     resolver_(io_service),
                                                     db(db), hid(hid) {
    logger = Logger::getInstance("server." + hid);
    temporalBufferSize = 0;
    
    
}

RDF3xServerHandler::~RDF3xServerHandler(){
}


/** 
 * 
 * 
 */
void RDF3xServerHandler::start(ServerSharedData *data) {
    this->sharedData = data;
    try{
        //Here we are reading
        ba::async_read(bsocket_, ba::buffer(this->headerBuffer, defaultBufferSize), ba::transfer_all(),
                   boost::bind(&RDF3xServerHandler::dispatch,
                               shared_from_this(),
                               ba::placeholders::error,
                               ba::placeholders::bytes_transferred));

    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());
    }
}

void RDF3xServerHandler::shutdown() {
    ssocket_.close();
    bsocket_.close();
}

void RDF3xServerHandler::handleDictionaryLookupById(const RequestDescriptor &rd, const DictionaryLookupByIdRequest &request){
    DictionaryLookupByIdResponse response;
    DictionaryServerHandler dictHandler(this->db);
    dictHandler.process(rd, request, response);
    //Time to reply
    response.PrintDebugString();
    string output = response.SerializeAsString();
    try{
        ba::write(bsocket_, ba::buffer(output));
    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());
    }
    
}

void RDF3xServerHandler::handleDictionaryLookupByString(const RequestDescriptor &rd, const DictionaryLookupByStringRequest &request){    
    DictionaryLookupByStringResponse response;
    DictionaryServerHandler dictHandler(this->db);
    dictHandler.process(rd, request, response);
    //Time to reply
    response.PrintDebugString();
    string output = response.SerializeAsString();    
    try{
        ba::write(bsocket_, ba::buffer(output));
    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());
    }
}

/** 
 * 
 * 
 * @param err 
 * @param len 
 */
void RDF3xServerHandler::dispatch(const bs::error_code& err, size_t len) {
    if(err){
        this->shutdown();
        return;
    }
    
    LengthMessage req; 
    RequestDescriptor rd;
    req.ParseFromArray((const void*)this->headerBuffer.data(), len);    
//    LOG4CPLUS_DEBUG(getLogger(), req.DebugString());
    
    uint64_t messageLength = req.length();
    char *message = new char[messageLength];

    try{
        boost::system::error_code rerror;
        ba::read(bsocket_, ba::buffer(message, messageLength), ba::transfer_all(), rerror);
    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());
    }

    RDF3xServerRemoteRequest actualReq;
    actualReq.ParseFromArray((const void*)message, messageLength);

    rd.cid = actualReq.cid();    
    rd.reqid = this->sharedData->newRequestId();

    if(actualReq.has_lookupidrequest()){
        this->handleDictionaryLookupById(rd, actualReq.lookupidrequest());
    }else if(actualReq.has_lookupstringrequest()){
        this->handleDictionaryLookupByString(rd, actualReq.lookupstringrequest());
    }else if(actualReq.has_prepforexrequest()){
        this->handlePrepareForExecRequest(rd, actualReq.prepforexrequest());    
    }else if(actualReq.has_execrequest()){
        this->handleExecRequest(rd, actualReq.execrequest());
    }

    delete[] message;
    
    this->shutdown();
}


void RDF3xServerHandler::handlePrepareForExecRequest(const RequestDescriptor &rd, const PrepareForExecRequest &request){
    //Time to build the operator
    PrepareForExecResponse response;
    ExecutionServerHandler execHandler(this->sharedData);
    execHandler.process(rd, request, response);   
    string output = response.SerializeAsString();
    try{
        ba::write(bsocket_, ba::buffer(output, output.size()));
    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());
    }
}

void RDF3xServerHandler::handleExecRequest(const RequestDescriptor &rd, const ExecRequest &request){
     //Time to build the operator
    ExecResponse response;
    ExecutionServerHandler execHandler(this->sharedData);
    execHandler.process(rd, request, response);   
    string output = response.SerializeAsString();
    //First write the length
    LengthMessage lmess;
    lmess.set_length(output.size());
    string lengthString = lmess.SerializeAsString();
//    LOG4CPLUS_INFO(getLogger(), "Writing the output in the socket!");        
    try{
        size_t bw = ba::write(bsocket_, ba::buffer(lengthString, defaultBufferSize), ba::transfer_all());
        bw = ba::write(bsocket_, ba::buffer(output, output.size()), ba::transfer_all());
    }catch(std::exception &e){
        LOG4CPLUS_WARN(getLogger(), e.what());        
    }
//    LOG4CPLUS_INFO(getLogger(), "Output written in the socket");            
}
