/* 
 * File:   ExecutionHandler.cpp
 * Author: lgalarra
 * 
 * Created on September 12, 2011, 7:58 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <boost/thread/locks.hpp>
#include <stack>

#include "rts/operator/PlanPrinter.hpp"
#include "ExecutionServerHandler.hpp"
#include "drdf/DOperator.hpp"
#include "partitioning/CommonTypes.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "rts/runtime/Runtime.hpp"
#include "drdf/RemoteOperators.hpp"
#include "drdf/constants.hpp"
#include "infra/osdep/Timestamp.hpp"


namespace proto=google::protobuf;

unsigned const ExecutionServerHandler::registersLimit = MAX_REGISTERS_PER_REMOTE_PAGE;

struct null_deleter
{
    void operator()(void const *) const
    {
    }
};

ExecutionServerHandler::ExecutionServerHandler(ServerSharedData *sharedData): sharedData(sharedData){
    //We will figure out how to generate an id
    this->client = new DRDF3xClient<Runtime>(Hash::hash64(sharedData->serverName, 1));
}

ExecutionServerHandler::~ExecutionServerHandler(){
    delete this->client;
}

Operator* ExecutionServerHandler::parseAnyIndexScan(const OperatorMessage &op, ExecRequestDescriptor &erd, OperatorMessage::Type type){
    OperatorMessage::DataOrder dataOrder = op.dataorder();
    unsigned subjectBound = ~0u;
    unsigned predicateBound = ~0u;
    unsigned objectBound = ~0u;
    Register *sbReg, *predReg, *objReg;
    sbReg = predReg = objReg = NULL;

//    cout << "Failing operator message: " << endl;
//    cout << op.DebugString() << endl;
    
    if(op.has_regsubject()){
//        cout  << op.regsubject() << " out of " << erd.runtime->getRegisterCount() << endl;        
        sbReg = erd.runtime->getRegister(static_cast<unsigned>(op.regsubject()));
    }
    
    if(op.has_regpredicate()){
//        cout  << op.regpredicate() << " out of " << erd.runtime->getRegisterCount() << endl;
        predReg = erd.runtime->getRegister(static_cast<unsigned>(op.regpredicate()));
    }
    
    if(op.has_regobject()){      
//        cout  << op.regobject() << " out of " << erd.runtime->getRegisterCount() << endl;
        objReg = erd.runtime->getRegister(static_cast<unsigned>(op.regobject()));
    }

    if(op.has_subjectbound()){
        assert(sbReg != NULL);        
        subjectBound = static_cast<unsigned>(op.subjectbound());
        sbReg->value = subjectBound;
    }
    
    if(op.has_predicatebound()){
        assert(predReg != NULL);
        predicateBound = static_cast<unsigned>(op.predicatebound());
        predReg->value = predicateBound;
    }
    
    if(op.has_objectbound()){
        assert(objReg != NULL);
        objectBound = static_cast<unsigned>(op.objectbound());
        objReg->value = objectBound;                
    }
    
    if(type == OperatorMessage::AggregatedIndexScan)
        return AggregatedIndexScan::create(*this->sharedData->dbds.db, static_cast<Database::DataOrder>(dataOrder), sbReg, ~subjectBound, predReg, ~predicateBound, objReg, ~objectBound , op.expectedcardinality());        
    else if(type == OperatorMessage::FullyAggregatedIndexScan )
        return FullyAggregatedIndexScan::create(*this->sharedData->dbds.db, static_cast<Database::DataOrder>(dataOrder), sbReg, ~subjectBound, predReg, ~predicateBound, objReg, ~objectBound , op.expectedcardinality());        
    else if(type == OperatorMessage::IndexScan)
        return IndexScan::create(*this->sharedData->dbds.db, static_cast<Database::DataOrder>(dataOrder), sbReg, ~subjectBound, predReg, ~predicateBound, objReg, ~objectBound , op.expectedcardinality());
    else
        return NULL;
}

Operator* ExecutionServerHandler::parseAggregatedIndexScan(const OperatorMessage &op, ExecRequestDescriptor &erd){
    return parseAnyIndexScan(op, erd, OperatorMessage::AggregatedIndexScan);
}

Operator* ExecutionServerHandler::parseEmptyScan(const OperatorMessage &op){ return NULL; }
Operator* ExecutionServerHandler::parseFilter(const OperatorMessage &op){ return NULL; }

Operator* ExecutionServerHandler::parseFullyAggregatedIndexScan(const OperatorMessage &op, ExecRequestDescriptor &erd){
    return parseAnyIndexScan(op, erd, OperatorMessage::FullyAggregatedIndexScan);
}

Operator* ExecutionServerHandler::parseHashGroupify(const OperatorMessage &op, ExecRequestDescriptor &erd){ 
    const proto::RepeatedField<proto::uint64> &registers = op.regshashjoin();
    proto::RepeatedField<proto::uint64>::const_iterator rit;
    vector<Register*> values;
    for(rit = registers.begin(); rit != registers.end(); ++rit){
        values.push_back(erd.runtime->getRegister(*rit));
    }
    
    return new HashGroupify(NULL, values, op.expectedcardinality());

}

Operator* ExecutionServerHandler::parseSimpleGroupify(const OperatorMessage &op, ExecRequestDescriptor &erd){ 
    const proto::RepeatedField<proto::uint64> &registers = op.regshashjoin();
    proto::RepeatedField<proto::uint64>::const_iterator rit;
    vector<Register*> values;
    for(rit = registers.begin(); rit != registers.end(); ++rit){
        values.push_back(erd.runtime->getRegister(*rit));
    }
    
    return new SimpleGroupify(NULL, values, op.expectedcardinality());

}    

Operator* ExecutionServerHandler::parseHashJoin(const OperatorMessage &op, ExecRequestDescriptor &erd){ 
    Register *leftRegister = erd.runtime->getRegister(static_cast<unsigned>(op.regleftvalue()));
    Register *rightRegister = erd.runtime->getRegister(static_cast<unsigned>(op.regrightvalue()));
    vector<Register*> leftTail, rightTail;    
    
    for(proto::uint64 i = 0; i < op.regslefttail_size(); ++i){
        leftTail.push_back(erd.runtime->getRegister(op.regslefttail(static_cast<int>(i))));
    }
    
    for(proto::uint64 i = 0; i < op.regsrighttail_size(); ++i){
        rightTail.push_back(erd.runtime->getRegister(op.regsrighttail(static_cast<int>(i))));
    }

    return new HashJoin(NULL, leftRegister, leftTail, NULL, rightRegister, rightTail, op.hashpriority(), op.probepriority(), op.expectedcardinality()); 
}

Operator* ExecutionServerHandler::parseIndexScan(const OperatorMessage &op, ExecRequestDescriptor &erd){
    return parseAnyIndexScan(op, erd, OperatorMessage::IndexScan);    
}    

Operator* ExecutionServerHandler::parseMergeJoin(const OperatorMessage &op, ExecRequestDescriptor &erd){ 
    Register *leftRegister = erd.runtime->getRegister(static_cast<unsigned>(op.regleftvalue()));
    Register *rightRegister = erd.runtime->getRegister(static_cast<unsigned>(op.regrightvalue()));
    vector<Register*> leftTail, rightTail;
    
    for(proto::uint64 i = 0; i < op.regslefttail_size(); ++i){
        leftTail.push_back(erd.runtime->getRegister(op.regslefttail(static_cast<int>(i))));
    }
    
    for(proto::uint64 i = 0; i < op.regsrighttail_size(); ++i){
        rightTail.push_back(erd.runtime->getRegister(op.regsrighttail(static_cast<int>(i))));
    }

    return new MergeJoin(NULL, leftRegister, leftTail, NULL, rightRegister, rightTail, op.expectedcardinality()); 

}

Operator* ExecutionServerHandler::parseMultiMergeUnion(const OperatorMessage &op, ExecRequestDescriptor &erd){
    vector<Register*> result, leftReg, rightReg;
    for(int i = 0; i < op.regmmuresult_size(); ++i){
        result.push_back(erd.runtime->getRegister(op.regmmuresult(i)));
    }
    
    for(int i = 0; i < op.regmmuleft_size(); ++i){
        leftReg.push_back(erd.runtime->getRegister(op.regmmuleft(i)));
    }
    
    for(int i = 0; i < op.regmmuright_size(); ++i){
        rightReg.push_back(erd.runtime->getRegister(op.regmmuright(i)));
    }
    
    return new MultiMergeUnion(result, NULL, leftReg, NULL, rightReg, op.expectedcardinality());
}

Operator* ExecutionServerHandler::parseMergeUnion(const OperatorMessage &op, ExecRequestDescriptor &erd){
    Register *result = erd.runtime->getRegister(op.regmuresult());
    Register *left = erd.runtime->getRegister(op.regmuleft());
    Register *right = erd.runtime->getRegister(op.regmuright());
    return new MergeUnion(result, NULL, left, NULL, right, op.expectedcardinality()); 
}

Operator* ExecutionServerHandler::parseNestedLoopFilter(const OperatorMessage &){ return NULL; }    
Operator* ExecutionServerHandler::parseNestedLoopJoin(const OperatorMessage &){ return NULL; }
Operator* ExecutionServerHandler::parseResultsPrinter(const OperatorMessage &){ return NULL; }

Selection::Predicate * ExecutionServerHandler::parsePredicateTree(const OperatorMessage::PredicateTree &predTree, ExecRequestDescriptor &erd){
    unsigned size = predTree.tree_size();
    
    map<unsigned, Selection::Predicate*> tempPredicatesMap;
    map<unsigned, const OperatorMessage::PredicateMessage*> tempPredicateMessagesMap;
    Selection::Predicate *root = NULL;
    for(unsigned i = 0; i < size; ++i){
        Selection::Predicate *currentPredicate = NULL;
        const OperatorMessage::PredicateMessage &pred = predTree.tree(i);
        OperatorMessage::PredicateType predType = pred.type();
        pred.PrintDebugString();
        switch(predType){
            case OperatorMessage::Equal:
                currentPredicate = new Selection::Equal(NULL, NULL);
                break;
            case OperatorMessage::Variable:{
                google::protobuf::uint64 regPos = pred.regvar();
                Register *reg = erd.runtime->getRegister(regPos);
                currentPredicate = new Selection::Variable(reg);
            }
                break;
            case OperatorMessage::NotEqual:
                currentPredicate = new Selection::NotEqual(NULL, NULL);                
                break;
            case OperatorMessage::Not:
                currentPredicate = new Selection::Not(NULL);
                break;
            default:            
                break;
        }
        
        assert(currentPredicate != NULL);
        
        tempPredicatesMap[pred.pid()] = currentPredicate;   
        tempPredicateMessagesMap[pred.pid()] = &pred;
        if(pred.has_ppid()){
            //Link it with the parent
            Selection::Predicate *parentPred = tempPredicatesMap[pred.ppid()];
            OperatorMessage::PredicateType parentType = tempPredicateMessagesMap[pred.ppid()]->type();
            switch(parentType){
                case OperatorMessage::Equal:{
                    Selection::Equal *equalPred = static_cast<Selection::Equal*>(parentPred);
                    if(equalPred->left == NULL) equalPred->left = currentPredicate;
                    else if(equalPred->right == NULL) equalPred->right = currentPredicate;
                }
                    break;
                case OperatorMessage::NotEqual:{
                    Selection::NotEqual *nequalPred = static_cast<Selection::NotEqual*>(parentPred);
                    if(nequalPred->left == NULL) nequalPred->left = currentPredicate;
                    else if(nequalPred->right == NULL) nequalPred->right = currentPredicate;
                }                    
                    break;
				case OperatorMessage::Not:{
                    Selection::Not *noteq = static_cast<Selection::Not*>(parentPred);					
					noteq->input = currentPredicate;
				}
					break;
                default:
                    break;
            }            
        }else{
            //It is the root
            root = currentPredicate;
        }
    }
    
    return root;
}

Operator* ExecutionServerHandler::parseSelection(const OperatorMessage &op, ExecRequestDescriptor &erd){
    const OperatorMessage::PredicateTree &predTree = op.predicate();
    Selection::Predicate *predicate = parsePredicateTree(predTree, erd);
    return new Selection(NULL, *erd.runtime, predicate, op.expectedcardinality()); 
}

Operator* ExecutionServerHandler::parseSingletonScan(const OperatorMessage &){ return NULL; }

Operator* ExecutionServerHandler::parseSort(const OperatorMessage &op, ExecRequestDescriptor &erd){     
    vector<Register*> values;
    vector<pair<Register*, bool> > orders;
    const proto::RepeatedField<proto::uint64> &registers = op.regvalues();
    const proto::RepeatedField<bool> &orderFlags = op.orderflags();
    const proto::RepeatedField<proto::uint64> &orderRegisters = op.orderreqisters();
    int ors = orderRegisters.size();
    
    proto::RepeatedField<proto::uint64>::const_iterator regit;
    for(regit = registers.begin(); regit != registers.end(); ++regit){
        values.push_back(erd.runtime->getRegister(*regit));
    }
    
    for(int i = 0; i < ors; ++i){
        orders.push_back(pair<Register*, bool>(values[orderRegisters.Get(i)], orderFlags.Get(i)));
    }
    
    return new Sort(*this->sharedData->dbds.db, NULL, values, orders, op.expectedcardinality());
}

Operator* ExecutionServerHandler::parseTableFunction(const OperatorMessage &){ return NULL; }

Operator* ExecutionServerHandler::parseUnion(const OperatorMessage &op, ExecRequestDescriptor &erd){ 
    const proto::RepeatedPtrField<drdf::message::OperatorMessage::UnionInitialization> &unionInits = op.unioninits();
    proto::RepeatedPtrField<drdf::message::OperatorMessage::UnionInitialization>::const_iterator uit;
    const proto::RepeatedPtrField<drdf::message::OperatorMessage::UnionMapping> &unionMappings = op.unionmappings();
    proto::RepeatedPtrField<drdf::message::OperatorMessage::UnionMapping>::const_iterator mit;    
    vector<vector<Register*> > inits, mappings;
    vector<Operator*> parts;

    inits.resize(unionInits.size());
    unsigned initInd = 0;
    for(uit = unionInits.begin(); uit != unionInits.end(); ++uit, ++initInd){
       const proto::RepeatedField<proto::uint64> &registers = uit->registers();
        proto::RepeatedField<proto::uint64>::const_iterator rit;
        for(rit = registers.begin(); rit != registers.end(); ++rit){
            inits[initInd].push_back(erd.runtime->getRegister(*rit));
        }
    }
    
    mappings.resize(unionMappings.size());
    unsigned mapInd = 0;
    for(mit = unionMappings.begin(); mit != unionMappings.end(); ++mit, ++mapInd){
        const proto::RepeatedField<proto::uint64> &registers = mit->registers();
        proto::RepeatedField<proto::uint64>::const_iterator rit;
        for(rit = registers.begin(); rit != registers.end(); ++rit){
            mappings[mapInd].push_back(erd.runtime->getRegister(*rit));
        }
    }
        
    return new Union(parts, mappings, inits, op.expectedcardinality()); 
}
Operator* ExecutionServerHandler::parseRemoteFetch(const OperatorMessage &){ return NULL; }
Operator* ExecutionServerHandler::parseRemoteSend(const OperatorMessage &){ return NULL; }
Operator* ExecutionServerHandler::parseDistributedResultsPrinter(const OperatorMessage &){ return NULL; }

DOperator * ExecutionServerHandler::parseOperatorTree(const PrepareForExecRequest &request, ExecRequestDescriptor &erd){
    Logger logger = getLogger();
//    LOG4CPLUS_INFO(logger, "start::parseOperatorTree" << endl);
    const OperatorTree &theTree = request.tree();
    unsigned size = theTree.optree_size();
    
    map<unsigned, DOperator*> temporal;
    DOperator *root;
    for(unsigned i = 0; i < size; ++i){
        Operator *newNode;
        DOperator *wrapper;
        const OperatorMessage &op = theTree.optree(i);
        OperatorMessage::Type opType = op.type();
        switch(opType){
            case OperatorMessage::AggregatedIndexScan:
                newNode = parseAggregatedIndexScan(op, erd);
                break;
            case OperatorMessage::EmptyScan:
                newNode = parseEmptyScan(op);
                break;
            case OperatorMessage::Filter:
                newNode = parseFilter(op);
                break;
            case OperatorMessage::FullyAggregatedIndexScan:
                newNode = parseFullyAggregatedIndexScan(op, erd);
                break;
            case OperatorMessage::HashGroupify:
                newNode = parseHashGroupify(op, erd);
                break;
            case OperatorMessage::SimpleGroupify:
                newNode = parseSimpleGroupify(op, erd);
                break;
            case OperatorMessage::HashJoin:
                newNode = parseHashJoin(op, erd);
                break;
            case OperatorMessage::IndexScan:
                newNode = parseIndexScan(op, erd);
                break;
            case OperatorMessage::MergeJoin:
                newNode = parseMergeJoin(op, erd);
                break;
            case OperatorMessage::MergeUnion:
                newNode = parseMergeUnion(op, erd);
                break;
            case OperatorMessage::NestedLoopFilter:
                newNode = parseNestedLoopFilter(op);
                break;
            case OperatorMessage::RemoteFetch:
                newNode = parseRemoteFetch(op);
                break;
            case OperatorMessage::RemoteSend:
                newNode = parseRemoteSend(op);
                break;
            case OperatorMessage::ResultsPrinter:
                newNode = parseResultsPrinter(op);
                break;
            case OperatorMessage::Selection:
                newNode = parseSelection(op, erd);
                break;
            case OperatorMessage::SingletonScan:
                newNode = parseSingletonScan(op);
                break;
            case OperatorMessage::Sort:
                newNode = parseSort(op, erd);
                break;
            case OperatorMessage::TableFunction:
                newNode = parseTableFunction(op);
                break;
            case OperatorMessage::NestedLoopJoin:
                newNode = parseNestedLoopJoin(op);
                break;
            case OperatorMessage::Union:
                newNode = parseUnion(op, erd);
                break;
            case OperatorMessage::DistributedResultsPrinter:
                newNode = parseDistributedResultsPrinter(op);
                break;
            case OperatorMessage::MultiMergeUnion:
                newNode = parseMultiMergeUnion(op, erd);
                break;
        }
        wrapper = new DOperator(newNode, static_cast<unsigned>(op.opid()), op.host());
        temporal[wrapper->getId()] = wrapper;
        if(op.has_parent()){
            //Lookup parent in temporal storage
            map<unsigned, DOperator*>::iterator it = temporal.find(static_cast<unsigned>(op.parent()));
            assert(it != temporal.end()); //This cannot happen since we did breath search. The parent must be there!
            wrapper->setParent(it->second);
            it->second->getChildren().push_back(wrapper);
        }else{
            //It is the root
            root = wrapper;
        }
        //Link the underlying operators
        DOperatorTransformer transformer;
        transformer.linkWithParent(wrapper);        
    }
   // cout << "end::parseOperatorTree" << endl;
    return root;
    
}

void ExecutionServerHandler::process(const RequestDescriptor &rd, const PrepareForExecRequest &request, PrepareForExecResponse &response){        
    Logger logger = getLogger();
    LOG4CPLUS_INFO(logger, this->sharedData->serverName << endl);
    //Write to the map
    //boost::upgrade_lock<boost::shared_mutex> lockObj(this->sharedData->operations_mutex);
//    LOG4CPLUS_INFO(logger, "prepareForExec process, attempting to acquire lock for writing ");                                    
    boost::unique_lock<boost::mutex> lockObj(this->sharedData->operations_mutex);
//    LOG4CPLUS_INFO(logger, "prepareForExec process, releasing the lock for writing");                                        
//    if(lockObj){        
    ExecRequestDescriptor &newDesc = this->sharedData->operations[rd.reqid];   
    lockObj.unlock();
//       {
//           boost::upgrade_to_unique_lock<boost::shared_mutex> write_lock(lockObj);
//           if(write_lock){
                //Under the assumption that reqids are unique, this will be always a modification
                //to the map structure
                newDesc.timestamp = time(NULL);
                newDesc.opid = request.tree().optree(0).opid();
                newDesc.readonly = true;
                newDesc.status = Received;
                newDesc.buildRuntime(*this->sharedData->dbds.db);     
//                LOG4CPLUS_DEBUG(logger, request.DebugString());
                assert(request.tree().has_runtimesize() && request.tree().runtimesize() > 0);
                newDesc.runtime->allocateRegisters(static_cast<unsigned>(request.tree().runtimesize()));
                newDesc.opTree = parseOperatorTree(request, newDesc);
                LOG4CPLUS_INFO(logger, "Request # " << rd.reqid << " has been Received " << endl);
//                LOG4CPLUS_INFO(logger, "Runtime size " << newDesc.runtime->getRegisterCount());                                    
//            }
//       }

       response.set_cid(rd.cid);
       response.set_reqid(rd.reqid);
       response.set_success(true);
       response.set_opid(newDesc.opid);
       prepareForExecution(newDesc, rd, logger);
//    }else{
//       assert(0);
//    }
}

void ExecutionServerHandler::process(const RequestDescriptor &rd, const ExecRequest &request, ExecResponse &response){
    //Access the descriptor in the map, get read access    
    Logger logger = getLogger();    
    boost::unique_lock<boost::mutex> lockObj(this->sharedData->operations_mutex);    
    map<uint64_t, ExecRequestDescriptor>::iterator it = this->sharedData->operations.find(request.prepareforexecid());

    LOG4CPLUS_INFO(logger, "Request # " << rd.reqid << " references the execution of request # " << request.prepareforexecid() << endl);
    if(it == this->sharedData->operations.end()){
        //Declare the request as forgotten
        response.set_status(ExecResponse::FORGOTTEN);
		ostringstream strstr;
		strstr << "The execution request with id " << request.prepareforexecid() << " could not be found by the server. It never arrived or was forgotten";
        response.set_errormsg(strstr.str());
        LOG4CPLUS_WARN(logger, response.errormsg());        
        lockObj.unlock();
		response.set_cid(rd.cid);
		response.set_opid(request.opid());
		response.set_reqid(rd.reqid);
    }else{
        //Time to prepare for execution        
        ExecRequestDescriptor &desc = it->second;
        lockObj.unlock();
                
        if(desc.status == UnableToRun){
            response.set_status(ExecResponse::ERROR);            
            response.set_errormsg("The request could not be executed because other hosts could not be contacted");
            LOG4CPLUS_WARN(logger, response.errormsg());                    
        }else{
            if(desc.status == ReadyToRun){
                desc.status = Running;                
                LOG4CPLUS_INFO(logger, "Request # " << request.prepareforexecid() << " is in Running state " << endl);                                                
            }
            //Execute the request
            executeOperator(desc.opTree, desc.runtime, response);  
        }
		response.set_cid(rd.cid);
		response.set_opid(request.opid());
		response.set_reqid(rd.reqid);

		if(response.status() == ExecResponse::ERROR || !response.hasmoreresults()){
		   boost::unique_lock<boost::mutex> lockObj2(this->sharedData->operations_mutex);
		   LOG4CPLUS_INFO(logger, "Request # " << request.prepareforexecid() << " will be removed " << endl);               
		   this->sharedData->operations.erase(request.prepareforexecid());
		}
    }       
}

void ExecutionServerHandler::executeOperator(DOperator *op, Runtime *runtime, ExecResponse &response){
    RemoteSend *rsop = static_cast<RemoteSend*>(op->getOperator());
    Logger logger = getLogger();
//    LOG4CPLUS_DEBUG(logger, "ExecutionServerHandler::executeOperator " << boost::this_thread::get_id());
    unsigned count = 0;
    proto::RepeatedField<proto::uint64> *regOffsets = response.mutable_registers();
    vector<unsigned long> regOffsetsVector;

    //The registers
    rsop->registersOffsets(regOffsetsVector);
//    LOG4CPLUS_DEBUG(logger, "Obtained registers");
    for(vector<unsigned long>::iterator offit = regOffsetsVector.begin(); offit != regOffsetsVector.end(); ++offit){
        regOffsets->Add(static_cast<proto::uint64>(*offit));
//        LOG4CPLUS_DEBUG(logger, " " << static_cast<proto::uint64>(*offit));
    }
    
    //Now the data
    proto::RepeatedField<proto::uint64> *data = response.mutable_data();
    unsigned card;
    try{
        if(!rsop->isStarted()){
            op->start = Timestamp();
            card = rsop->first();
        }else{
            card = rsop->next();
        }
        
        while(card > 0){
            vector<unsigned> values;
            rsop->values(values);
            for(vector<unsigned>::iterator dit = values.begin(); dit != values.end(); ++dit){
                data->Add(static_cast<proto::uint64>(*dit));
            }
            data->Add(static_cast<proto::uint64>(card));
            count += values.size() + 1;
            //Check first whether we reached the limit (the number of registers per row is the same)
			if(count + values.size() + 1 <= registersLimit)
            	card = rsop->next();
			else
				break;
        }
//        LOG4CPLUS_INFO(logger, " Time invested in local execution until now " << (Timestamp() - op->start)  );

        
        if(card > 0){
            response.set_hasmoreresults(true);
        }else{
            response.set_hasmoreresults(false);
            op->end = Timestamp();
            LOG4CPLUS_INFO(logger, " Time invested in local execution " << (op->end - op->start)  );
        }
        
        response.set_status(ExecResponse::OK);
    }catch(RemoteException &e){
        response.set_status(ExecResponse::ERROR);
        response.set_errormsg(e.what());
    }
    //LOG4CPLUS_DEBUG(logger, "end::ExecutionServerHandler::executeOperator " << boost::this_thread::get_id());
}

void ExecutionServerHandler::prepareForExecution(ExecRequestDescriptor &descriptor, const RequestDescriptor &rd, Logger &logger){
    //We have to instrument the operator 
    //An exec request might run concurrently
    //but it will proceed until the status is ReadyToRun
    descriptor.status = Scheduling;
//    LOG4CPLUS_INFO(logger, "Request # " << rd.reqid << " is in Scheduling state " << endl);

    //Prepare operator for distributed execution
    DOperatorTransformer transformer;

    bool isOk = transformer.prepareForRemoteOperations(this->sharedData->serverName , descriptor.opTree, descriptor.runtime, logger);
    //Contact other servers. If so, we are ready to run
    if(isOk){
        descriptor.opTree = transformer.addTopNetworkSender(descriptor.opTree, string(""), descriptor.runtime);        
        descriptor.status = ReadyToRun;
        LOG4CPLUS_INFO(logger, "Request # " << rd.reqid << " is in ReadyToRun state " << endl);        
    }else{
        descriptor.status = UnableToRun;
        LOG4CPLUS_WARN(logger, "Request # " << rd.reqid << " is in UnableToRun state " << endl);                
    }   
}





