/* 
 * File:   ExecutionHandler.hpp
 * Author: lgalarra
 *
 * Created on September 12, 2011, 7:58 PM
 */

#ifndef src_tools_drdf3xserver_executionhandler_hpp
#define	src_tools_drdf3xserver_executionhandler_hpp

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <iomanip>

#include "common.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DRDF3xClient.hpp"

using namespace log4cplus;
using namespace drdf;
using namespace drdf::message;

class ExecutionServerHandler {    
public:
    ExecutionServerHandler(ServerSharedData *sharedData);
    ~ExecutionServerHandler();
    void process(const RequestDescriptor &, const PrepareForExecRequest &, PrepareForExecResponse &);
    void process(const RequestDescriptor &, const ExecRequest &, ExecResponse &);
    static const unsigned registersLimit;
    
private:
    ServerSharedData *sharedData;
    DOperator * parseOperatorTree(const PrepareForExecRequest &, ExecRequestDescriptor &);

    Operator* parseAggregatedIndexScan(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseEmptyScan(const OperatorMessage &);
    Operator* parseFilter(const OperatorMessage &);
    Operator* parseFullyAggregatedIndexScan(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseHashGroupify(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseSimpleGroupify(const OperatorMessage &, ExecRequestDescriptor &);    
    Operator* parseHashJoin(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseIndexScan(const OperatorMessage &, ExecRequestDescriptor &);  
    Operator* parseMergeJoin(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseMergeUnion(const OperatorMessage &, ExecRequestDescriptor &); 
    Operator* parseMultiMergeUnion(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseNestedLoopFilter(const OperatorMessage &);    
    Operator* parseNestedLoopJoin(const OperatorMessage &);
    Operator* parseResultsPrinter(const OperatorMessage &);
    Operator* parseSelection(const OperatorMessage &, ExecRequestDescriptor&);
    Operator* parseSingletonScan(const OperatorMessage &);
    Operator* parseSort(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseTableFunction(const OperatorMessage &);
    Operator* parseUnion(const OperatorMessage &, ExecRequestDescriptor &);
    Operator* parseRemoteFetch(const OperatorMessage &);
    Operator* parseRemoteSend(const OperatorMessage &);
    Operator* parseDistributedResultsPrinter(const OperatorMessage &);
    Operator* parseAnyIndexScan(const OperatorMessage &, ExecRequestDescriptor&, OperatorMessage::Type);
        
    Selection::Predicate *parsePredicateTree(const OperatorMessage::PredicateTree &, ExecRequestDescriptor &);
    void prepareForExecution(ExecRequestDescriptor &, const RequestDescriptor &, Logger &);
    void executeOperator(DOperator *op, Runtime *runtime, ExecResponse &response);    
        
    DRDF3xClient<Runtime> *client;
};

#endif

