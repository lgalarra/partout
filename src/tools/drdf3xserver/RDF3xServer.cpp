/* 
 * File:   RDF3xServer.cpp
 * Author: luis
 * Created on September 8, 2011, 10:35 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "RDF3xServer.hpp"
#include <iostream>

RDF3xServer::RDF3xServer(const std::string& address, const std::string& port, std::size_t thread_pool_size, stdpt::DatabaseDescriptor &database): thread_pool_size_(thread_pool_size), 
        acceptor_(io_service_){
    this->sharedData = new ServerSharedData();
    this->sharedData->dbds = database;
    this->sharedData->serverName = string(address + ":" + port);
    // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
    boost::asio::ip::tcp::resolver resolver(io_service_);
    boost::asio::ip::tcp::resolver::query query(address, port);
    boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();
}

RDF3xServer::~RDF3xServer(){
    delete sharedData;
}

void RDF3xServer::run()
{
    boost::thread_group thr_grp;
    for (std::size_t i = 0; i < thread_pool_size_; ++i){
        thr_grp.create_thread(boost::bind(&boost::asio::io_service::run, &io_service_));
    }

    // Wait for all threads in the pool to exit.
    thr_grp.join_all();
}

void RDF3xServer::stop()
{
    io_service_.stop();
}

void RDF3xServer::start_accept() {
    
    RDF3xServerHandler::pointer new_connection = RDF3xServerHandler::create(io_service_, this->sharedData->dbds.db);

    acceptor_.async_accept(new_connection->socket(),
    boost::bind(&RDF3xServer::handle_accept, this, new_connection, boost::asio::placeholders::error));
}

void RDF3xServer::handle_accept(RDF3xServerHandler::pointer new_connection, const boost::system::error_code& e)
{
    if (!e){
        new_connection->start(this->sharedData);
        start_accept();
    }
}