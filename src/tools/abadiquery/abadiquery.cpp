#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/utsname.h>
#include "cts/semana/SemanticAnalysis.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DPlanGen.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DSemanticAnalysis.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DPlanSearch.hpp"
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DOperatorCostFunction.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"
#include <sstream>
#include <boost/thread.hpp>


#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
using namespace drdf;
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace drdf;

unsigned _limit = 0;
unsigned _queryCount = 0;
unsigned _records = 0;
vector<SAStatistics> statistics;
vector<vector<unsigned> > _globalResults;
boost::mutex _recordsMutex;

struct Job{
    DOperator *dOperator;
    DRuntime *druntime;
};

//---------------------------------------------------------------------------
bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}

static void showHelp(){      
    cerr << "usage: abadiquery <database> <originalDatabase> <queryfile>" << endl;
    cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
    cerr << "sa = simulated annealing; ls = local search" << endl;
}

static void printExecutionStats(){   
    unsigned i = 0;
    for(vector<SAStatistics>::iterator sit = statistics.begin(); sit != statistics.end(); ++sit, ++i){
       cout << "Q" << i << "," << (sit->endTime - sit->startTime) << "," << (sit->prepareForExecStartStamp - sit->startTime) << 
               "," << (sit->execEndStamp - sit->prepareForExecStartStamp) << "," << (sit->execEndStamp - sit->startTime) << "," << sit->cardinality  << endl;
    }
}

static void resetGlobals(){
    _queryCount = 0;
    _globalResults.clear();
    _limit = 0;
    _records = 0;
}

/// Requires mutual exclusion
static void extractResults(DOperator *dop, unsigned count){
    vector<Register*> &outputRegs = dop->getOutputRegisters();
    vector<unsigned> row;    
    for(vector<Register*>::iterator rit = outputRegs.begin(); rit != outputRegs.end(); ++rit){
        row.push_back((*rit)->value);
        cout << (*rit)->value << endl;
    }
    _globalResults.push_back(row);
    _records += count;
}

void runPlan(Job *aJob){
   DRDF3xClient<DRuntime> client;   
   PrepExecutionResponse prepResponse;
   client.prepareForExecution(aJob->dOperator->getHost(), aJob->dOperator, *aJob->druntime, prepResponse);
   if(!prepResponse.success){
       cerr << "The query could not be executed: " << prepResponse.errorMsg << endl;
       exit(1);
   }else{
       double observedCard = 0.0;
       bool firstIteration = true;                          
       unsigned count;
       while(true){
           drdf::message::ExecResponse execResp;               
           client.exec(aJob->dOperator->getHost(), prepResponse.reqid, prepResponse.opid, execResp);
           if(execResp.status() == drdf::message::ExecResponse::OK){
               DResultsPrinter *rp = new DResultsPrinter(*aJob->druntime, execResp, aJob->dOperator, observedCard, _limit, true);
               if((count = rp->first())){             
                   if(firstIteration){
                       firstIteration = false;
                   }
                  
                   //Count first record
                   {
                        boost::unique_lock<boost::mutex> lock(_recordsMutex);
                        extractResults(aJob->dOperator, count);                                
                        if(_records >= _limit){
                            delete rp;
                            break;
                        }

                            
                   }
                   
                   //Store records
                   while((count = rp->next())){
                       boost::unique_lock<boost::mutex> lock(_recordsMutex);
                       extractResults(aJob->dOperator, count);                                
                       if(_records >= _limit)
                           break;
                   }
                   
                   delete rp;
               }else{
                   if(firstIteration){
                       firstIteration = false;
                   }
                   delete rp;
                   break;                                      
               }               
           }else{
               if(execResp.status() == drdf::message::ExecResponse::ERROR){
                   cerr << "There was a problem when executing the query: " << execResp.errormsg() << endl;
               }
               break;               
           }
       }
   }    
}

//---------------------------------------------------------------------------
static void runQuery(DDatabase& db, Database &dummy, const string& query)
   // Evaluate a query
{
    
   SAStatistics stats;
   stats.startTime = Timestamp();
   QueryGraph queryGraph;
   {
      // Parse the query
      SPARQLLexer lexer(query);
      SPARQLParser parser(lexer);
      try {
         parser.parse();
      } catch (const SPARQLParser::ParserException& e) {
         cerr << "parse error: " << e.message << endl;
         return;
      }

      // And perform the semantic anaylsis
      try {
         SemanticAnalysis semana(dummy);
         semana.transform(parser,queryGraph);
      } catch (const SemanticAnalysis::SemanticException& e) {
         cerr << "semantic error: " << e.message << endl;
         return;
      }
      if (queryGraph.knownEmpty()) {
         cerr << "static analysis determined that the query result will be empty" << endl;
         cout << "<empty result>" << endl;
         return;
      }
   }
   
   // Run the optimizer
   _limit = queryGraph.getLimit();
   PlanGen plangen;
   Plan* plan=plangen.translate(dummy,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return;
   }

   // Build a physical plan
   Runtime baseRuntime(dummy, NULL, NULL);
   DRuntime runtime(db, baseRuntime);
   vector<drdf::Host> allHosts;
   db.getFragmentsDefinitionSegment().getAllHosts(allHosts);
   vector<DOperator*> operators;
   DOperatorTransformer transformer(&db);   
   Job *jobs = new Job[allHosts.size()];
   boost::thread::thread ** threads = new boost::thread::thread*[allHosts.size()];
   
   unsigned h = 0;
   for(vector<drdf::Host>::iterator hit = allHosts.begin(); hit != allHosts.end(); ++hit, ++h){
       vector<Register*> outputRegisters;       
       Operator *op = DCodeGen().translate(runtime, queryGraph, plan, outputRegisters, true);
       DOperator *dop = transformer.buildDOperator(op);
       operators.push_back(dop);
       //Implement a method which assigns the same home host to all operators
       transformer.forceHomeHost(dop, hit->uri);
       //Then retrieve everything in parallel
       jobs[h].dOperator = dop;
       jobs[h].druntime = &runtime;
   }

   //Here get all hosts and send the plan to all hosts
   for(h = 0; h < allHosts.size(); ++h){
       threads[h] = new boost::thread::thread(runPlan, &jobs[h]);
   }
   
   for(h = 0; h < allHosts.size(); ++h){
       threads[h]->join();
   }
   
  for(h = 0; h < allHosts.size(); ++h){
       delete threads[h];
       DOperator *dop = operators.back();
       operators.pop_back();       
       delete dop->getOperator();
       delete dop;
   }

   delete[] jobs;
   delete[] threads;
   
   stats.endTime = Timestamp();
   stats.cardinality = _records;
   statistics.push_back(stats);
           
}
//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    // Warn first
    if (smallAddressSpace())
       cerr << "Warning: Running DRDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "DRDF-3X query interface" << endl
         << "(c) 2011 Luis Galárraga, based on RDF-3X query interface by 2008 Thomas Neumann." << endl;

    // Check the arguments
    if (argc < 4) {
        showHelp();
        return 1;
    }

    //Open the dummy database
    Database db;
    if(!db.open(argv[2], true)){
       cerr << "An original RDF-3x database has not been provided " << endl;
       return 1;
    }

    // Open the database
    DDatabase ddb;
    if (!ddb.open(argv[1], &db.getDictionary(), false)) {
      cerr << "unable to open distributed database " << argv[1] << endl;
      return 1;
    }

    // Execute a single query?
    ifstream in(argv[3]);
    if (!in.is_open()) {
        cerr << "unable to open " << argv[3] << endl;
        return 1;
    }
    
    while(!in.eof()){
        char queryStr[8192];
        in.getline(queryStr, 8192);
        string query = string(queryStr);
        if(query.empty()) continue;
        resetGlobals();        
        runQuery(ddb,db,query);
    }        
    printExecutionStats();
}
//---------------------------------------------------------------------------
