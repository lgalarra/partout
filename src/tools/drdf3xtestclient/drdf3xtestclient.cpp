//
// blocking_tcp_echo_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>
#include "server/RDF3xServerMessages.h"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/CommonTypes.hpp"
#include "drdf/DOperator.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "partitioning/Utilities.hpp"
#include "drdf/operator.h"
#include "drdf/DOperatorCostFunction.hpp"

using boost::asio::ip::tcp;
using namespace std; // For strlen.
using namespace drdf::message;

enum { max_length = 1024 };

void f1(int argc, char* argv[]){
      try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: blocking_tcp_echo_client <host> <port>\n";
      return;
    }

    boost::asio::io_service io_service;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query(tcp::v4(), argv[1], argv[2]);
    tcp::resolver::iterator iterator = resolver.resolve(query);
    ostringstream out;
    string content;
    
    RDF3xServerRemoteRequest req;
    req.set_reqid(1);
    req.set_cid(23);
    req.SerializeToOstream(&out);
    content = out.str();
    

    tcp::socket s(io_service);
    s.connect(*iterator);

    size_t request_length = content.size();
    boost::asio::write(s, boost::asio::buffer(content.c_str(), request_length));

    char reply[max_length];
    size_t reply_length = boost::asio::read(s,
        boost::asio::buffer(reply, request_length));
    std::cout << "Reply is: ";
    std::cout.write(reply, reply_length);
    std::cout << "\n";
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

}

void f2(int argc, char* argv[]){
    if (argc != 2){
    
      std::cerr << "Usage: blocking_tcp_echo_client host:port\n";
      return;
    }    
    map<string, drdf::Host> hosts;
    drdf::DRDF3xClient<Runtime> client(23, hosts);
    DictionaryLookupByStringResponse::Status status;
    Type::ID type = Type::Literal; 
    unsigned subtype = 5;
    unsigned id;
    bool result = client.singleDictionaryLookupByString(string(argv[1]), string("Whatever"), type, subtype, id, status);
    if(result){
        cout << "Success " << endl;
    }
}

void f3(int argc, char* argv[]){    
    map<string, drdf::Host> hosts;
    hosts["localhost:1234"] = drdf::Host(1, "localhost:1234", "fragmentxxxx");
    hosts["localhost:1235"] = drdf::Host(2, "localhost:1235", "fragmentxxxy");    
    hosts["localhost:1236"] = drdf::Host(2, "localhost:1236", "fragmentxxxz");        
    drdf::DRDF3xClient<Runtime> client(23, hosts);
    DictionaryLookupByStringResponse::Status status;
    Type::ID type; 
    unsigned subtype;
    unsigned id;
    client.singleDictionaryLookupByString(string("localhost:1235"), string("http://dbpedia.org/resource/Aristotle"), Type::URI, 0, id, status);
    client.singleDictionaryLookupByString(string("localhost:1235"), string("http://dbpedia.org/resource/Abraham_Lincoln"), Type::URI, 0, id, status);
    client.singleDictionaryLookupByString(string("localhost:1235"), string("Abraham"), Type::CustomLanguage, 5, id, status);

    map<string, DictionaryLookupByStringResponse::Status> statusStringMap;
    map<string, DictionaryLookupByIdResponse::Status> statusIdMap;
    string text;
    client.broadcastSingleDictionaryLookupByString(string("http://dbpedia.org/resource/Empedocles"), Type::URI, 0, id, statusStringMap);
    client.broadcastSingleDictionaryLookupById(57989, type, subtype, text, statusIdMap);
    
    vector<drdf::LookupRequest> aRequest, anotherRequest;
    vector<drdf::LookupResponse> aResponse, anotherResponse;    
    drdf::LookupRequest r1, r2, r3, r4;
    srand(time(NULL));
    r1.id = 45;
    r2.id = 2345;
    r3.id = 9989;
    r4.id = rand() % 100000;
    aRequest.push_back(r1);
    aRequest.push_back(r2);
    aRequest.push_back(r3);
    aRequest.push_back(r4);
    client.broadcastDictionaryLookupById(aRequest, aResponse, statusIdMap);
    
    drdf::LookupRequest r5, r6, r7, r8, r9;
    r5.text = string("http://purl.org/dc/elements/1,1/description");
    r5.subtype = 0;
    r5.type = Type::URI;
   
    r6.text = string("http://dbpedia.org/resource/Serena_Williams");
    r6.subtype = 0;
    r6.type = Type::URI;

    r7.text = string("en");
    r7.subtype = 0;
    r7.type = Type::Literal;
    
    r8.text = string("es");
    r8.subtype = 0;
    r8.type = Type::String;
    
    r9.text = string("Alfonso 12 Of Spain");
    r9.subtype = 12; //"en"
    r9.type = Type::CustomLanguage;
    
    anotherRequest.push_back(r5);
    anotherRequest.push_back(r6);
    anotherRequest.push_back(r7);    
    anotherRequest.push_back(r8); 
    anotherRequest.push_back(r9);     
    client.broadcastDictionaryLookupByString(anotherRequest, anotherResponse, statusStringMap);   
}


void f4(int argc, char* argv[]){
    Database db;
    db.open("../dbpedia_frag", true);
    Runtime runtime(db);
    drdf::DRDF3xClient<drdf::DRuntime> client(12);
    runtime.allocateRegisters(9);
    Register* S1=runtime.getRegister(0),*P1=runtime.getRegister(1),*O1=runtime.getRegister(2);
    Register* S2=runtime.getRegister(3),*P2=runtime.getRegister(4),*O2=runtime.getRegister(5);
    Register* O3=runtime.getRegister(6), *P3 = runtime.getRegister(7), *S3 = runtime.getRegister(8);
    
    O1->value = 9989; //Irish novelist..
    O2->value = 34513; //Serena_Williams
    O3->value = 582; //Alfonso 12 de ...
    
    cout << "Building the operator tree " << endl;
    AggregatedIndexScan* scan1=AggregatedIndexScan::create(db,Database::Order_Object_Predicate_Subject,NULL,false,P1,false,O1,true,1);
    cout << "Building second operator tree " << endl;
    AggregatedIndexScan* scan2=AggregatedIndexScan::create(db,Database::Order_Object_Predicate_Subject,NULL,false,P2,false,O2,true,1);
    vector<Register*> tail1,tail2; tail1.push_back(O1); tail2.push_back(O2);
    MergeJoin *join = new MergeJoin(scan1,P1,tail1,scan2,P2,tail2,0);
    
    cout << "Building third scan" << endl;    
    IndexScan *scan3 = IndexScan::create(db, Database::Order_Object_Predicate_Subject, S3, false, P3, false, O3, true, 1);
    vector<Register*> tail3; tail3.push_back(O3);
    MergeJoin *join2 = new MergeJoin(scan3, P3, tail3, join, P1, tail2, 0);
    
    drdf::DOperatorTransformer transformer;
    cout << "Preparing for execution " << endl;
    
    drdf::DOperator *dop = transformer.buildDOperator(dynamic_cast<Operator*>(join2));
    cout << "Assigning hosts" << endl;
    
    dop->getChildren().at(0)->setHost(string("localhost:1235"));
    dop->getChildren().at(1)->getChildren().at(0)->setHost(string("localhost:1234"));    
    dop->getChildren().at(1)->getChildren().at(1)->setHost(string("localhost:1236"));        
    
    assert(transformer.assignHomeHost(dop, string("localhost:1236")));
    assert(!transformer.assignHomeHost(dop, string("localhost:1237"), true));    
    assert(!transformer.assignHomeHost(dop, string("localhost:1237")));
    assert(transformer.assignHomeHost(dop, string("localhost:1236"), true));        
    
    DebugPlanPrinter out(runtime, false);
    dop->getOperator()->print(out);

    delete join2;
    delete dop;    
}

void f5(int argc, char* argv[]){
    Database db;
    drdf::DDatabase ddb;
    db.open("../dbpedia_frag", true);
    Runtime runtime(db);
    drdf::DRuntime druntime(ddb, runtime);
    
    drdf::DRDF3xClient<drdf::DRuntime> client(12);
    druntime.allocateRegisters(10);
    Register* S1=runtime.getRegister(0),*P1=runtime.getRegister(1),*O1=runtime.getRegister(2);
    Register* S2=runtime.getRegister(3),*P2=runtime.getRegister(4),*O2=runtime.getRegister(5);
    Register* O3=runtime.getRegister(6), *P3 = runtime.getRegister(7), *S3 = runtime.getRegister(8);
    Register* S4=runtime.getRegister(9);
    
    O1->value = 9989; //Irish novelist..
    O2->value = 34513; //Serena_Williams
    O3->value = 582; //Alfonso 12 de ...
    S4->value = 34553;
    
    cout << "Building the operator tree " << endl;
    AggregatedIndexScan* scan1=AggregatedIndexScan::create(db,Database::Order_Object_Predicate_Subject,NULL,false,P1,false,O1,true,1);
    cout << "Building second operator tree " << endl;
    AggregatedIndexScan* scan2=AggregatedIndexScan::create(db,Database::Order_Object_Predicate_Subject,NULL,false,P2,false,O2,true,1);
    vector<Register*> tail1,tail2; tail1.push_back(O1); tail2.push_back(O2);
    MergeJoin *join = new MergeJoin(scan1,P1,tail1,scan2,P2,tail2,2048);
    
    cout << "Building third scan" << endl;    
    IndexScan *scan3 = IndexScan::create(db, Database::Order_Object_Subject_Predicate, S3, false, P3, false, O3, true, 1);
    vector<Register*> tail3; tail3.push_back(O3); tail3.push_back(S3);
    MergeJoin *join2 = new MergeJoin(scan3, P3, tail3, join, P1, tail2, 1025);
    
    FullyAggregatedIndexScan *scan4 = FullyAggregatedIndexScan::create(db, Database::Order_Subject_Predicate_Object, S4, true, NULL, false, NULL, false, 0);
    vector<Register*> tail4, tail5;
    MergeJoin *join3 = new MergeJoin(scan4, S4, tail4, join2, S3, tail5, 0);
    
    drdf::DOperatorTransformer transformer;
    cout << "Preparing for execution " << endl;
    
    drdf::DOperator *dop = transformer.buildDOperator(dynamic_cast<Operator*>(join3));
    cout << "Assigning hosts" << endl;
    
    DebugPlanPrinter earlyOut(runtime, false);         
    join3->print(earlyOut);
    
    //host home for scan4
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1235"), "x"));
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1236"), "x"));   
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1234"), "x"));        
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1239"), "x"));            

    //relevant hosts for scan3
    drdf::DOperator *mj2, *dis;
    mj2 = dop->getChildren().at(1);
    dis = mj2->getChild(0);
    dis->addRelevantHost(drdf::Host(string("localhost:1239"), "x"));
    dis->addRelevantHost(drdf::Host(string("localhost:1240"), "x"));    
    
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1237", "xx"));   
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1238", "xx"));        
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1239", "xx"));            
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(1)->setHost(string("localhost:1234"));    
    
    dop = transformer.resolveMultipleSourcesLeaves(dop, &druntime);
    DebugPlanPrinter out(runtime, false);         
    dop->getOperator()->print(out);

    assert(transformer.assignHomeHost(dop, string("localhost:1236")));
/*    assert(transformer.assignHomeHost(dop, string("localhost:1237"), true));    
    assert(transformer.assignHomeHost(dop, string("localhost:1237")));
    assert(transformer.assignHomeHost(dop, string("localhost:1236"), true));        
    assert(transformer.assignHomeHost(dop, string("localhost:1238"), true));        
    assert(!transformer.assignHomeHost(dop, string("localhost:1241")));        */

    
    drdf::PrepExecutionResponse response;
    drdf::message::ExecResponse exResponse;    
    client.prepareForExecution(string("localhost:1236"), dop, druntime, response);
        
    assert(client.exec(string("localhost:1236"), response.reqid, response.opid, exResponse));
    
    exResponse.PrintDebugString();

    //client.prepareForExecution(string("localhost:1236"), dop, runtime);    
    //client.prepareForExecution(string("localhost:1234"), dop, runtime);

    delete join3;
    delete dop;    

}

void f6(){
   Database db;
   db.open("testfrag", true);
   Register* sub, *pred, *obj;
   Runtime runtime(db);
   runtime.allocateRegisters(3);
   sub = runtime.getRegister(0);
   pred = runtime.getRegister(1);
   obj = runtime.getRegister(2);
   IndexScan *is = IndexScan::create(db, Database::Order_Subject_Predicate_Object, sub, false, pred, false, obj, false, 1);
   if(is->first()){
       do{
           string result;
           Type::ID type;
           unsigned subtype;
           cout << sub->value << " " << pred->value << " " << obj->value << endl;
           lookupById(db, sub->value, type, subtype, result);           
           cout << result << " ";
           lookupById(db, pred->value, type, subtype, result);
           unsigned test = 0;
          db.getDictionary().lookup(result, type, subtype, test);
           
           cout << result << " ";
           lookupById(db, obj->value, type, subtype, result);
           cout << result << endl;
           
          cout << test << endl;
       }while(is->next());       
   }
   
   
   
   delete is;
}

void f7(){
    Database db;
    db.open("../dbpedianew", true);
    Runtime runtime(db);
    drdf::DDatabase ddb;
    runtime.allocateRegisters(9);
    Register* S1=runtime.getRegister(0),*P1=runtime.getRegister(1),*O1=runtime.getRegister(2);
    Register* S2=runtime.getRegister(3),*P2=runtime.getRegister(4),*O2=runtime.getRegister(5);
    Register* O3=runtime.getRegister(6), *P3 = runtime.getRegister(7), *S3 = runtime.getRegister(8);
    drdf::DRuntime druntime(ddb, runtime);    

    O1->value = 9989; //Irish novelist..
    O2->value = 34513; //Serena_Williams
    O3->value = 582; //Alfonso 12 de ...
    
    cout << "Building the operator tree " << endl;
    IndexScan* scan1=IndexScan::create(db,Database::Order_Object_Predicate_Subject,S1,false,P1,false,O1,true,1);
    cout << "Building second operator tree " << endl;
    IndexScan* scan2=IndexScan::create(db,Database::Order_Object_Predicate_Subject,S2,false,P2,false,O2,true,1);
    vector<Register*> tail1,tail2; tail1.push_back(O1); tail2.push_back(O2);
    MergeJoin *join = new MergeJoin(scan1,P1,tail1,scan2,P2,tail2,2048);
    
    cout << "Building third scan" << endl;    
    IndexScan *scan3 = IndexScan::create(db, Database::Order_Object_Predicate_Subject, S3, false, P3, false, O3, true, 1);
    vector<Register*> tail3; tail3.push_back(O3);
    MergeJoin *join2 = new MergeJoin(scan3, P3, tail3, join, P1, tail2, 1025);
    vector<Register*> outRegs;
    outRegs.push_back(P3);
    outRegs.push_back(O3);    
    HashGroupify *hj = new HashGroupify(join2, outRegs, 1025);
    
    drdf::DOperatorTransformer transformer;
    cout << "Preparing for execution " << endl;
    
    drdf::DOperator *dop = transformer.buildDOperator(dynamic_cast<Operator*>(hj));
    cout << "Assigning hosts" << endl;
    
    dop->getChildren().at(0)->getChildren().at(0)->setHost(string("localhost:1235"));
    dop->getChildren().at(0)->getChildren().at(1)->getChildren().at(0)->setHost(string("localhost:1234"));    
    dop->getChildren().at(0)->getChildren().at(1)->getChildren().at(1)->setHost(string("localhost:1236"));        
    
    assert(transformer.assignHomeHost(dop, string("localhost:1236")));
    
    DebugPlanPrinter out(runtime, false);
    dop->getOperator()->print(out);

    drdf::DRDF3xClient<drdf::DRuntime> client;    
    drdf::PrepExecutionResponse exResponse;        
    client.prepareForExecution(string("localhost:1236"), dop, druntime, exResponse);

    
    drdf::DOperator *newDop = transformer.parallelize(dop->getChildren().at(0), druntime);
    
    client.prepareForExecution(string("localhost:1236"), dop, druntime, exResponse);
    
    delete dop->getOperator();
    delete dop;
    
}

void f8(){
    Database db;
    drdf::DDatabase ddb;
    db.open("../dbpedianew", true);
    ddb.open("../ddbpedianew", true);
    Runtime runtime(db);
    drdf::DRuntime druntime(ddb, runtime);
    
    drdf::DRDF3xClient<drdf::DRuntime> client;
    druntime.allocateRegisters(12);
    Register* S1=runtime.getRegister(0),*P1=runtime.getRegister(1),*O1=runtime.getRegister(2);
    Register* S2=runtime.getRegister(3),*P2=runtime.getRegister(4),*O2=runtime.getRegister(5);
    Register* O3=runtime.getRegister(6), *P3 = runtime.getRegister(7), *S3 = runtime.getRegister(8);
    Register* S4=runtime.getRegister(9), *P4 = runtime.getRegister(10), *O4 = runtime.getRegister(11);
    
    O1->value = 9989; //Irish novelist..
    O2->value = 34513; //Serena_Williams
    O3->value = 582; //Alfonso 12 de ...
    S4->value = 34553;
    
    cout << "Building the operator tree " << endl;
    IndexScan* scan1= IndexScan::create(db,Database::Order_Object_Predicate_Subject,S1,false,P1,false,O1,true,1);
    cout << "Building second operator tree " << endl;
    IndexScan* scan2= IndexScan::create(db,Database::Order_Object_Predicate_Subject,S2,false,P2,false,O2,true,1);
    vector<Register*> tail1,tail2; tail1.push_back(O1); tail2.push_back(O2);
    MergeJoin *join = new MergeJoin(scan1,P1,tail1,scan2,P2,tail2,1023);
    
    cout << "Building third scan" << endl;    
    IndexScan *scan3 = IndexScan::create(db, Database::Order_Object_Subject_Predicate, S3, false, P3, false, O3, true, 1);
    vector<Register*> tail3; tail3.push_back(O3); tail3.push_back(S3);
    MergeJoin *join2 = new MergeJoin(scan3, P3, tail3, join, P1, tail2, 10);
    
    IndexScan *scan4 = IndexScan::create(db, Database::Order_Subject_Predicate_Object, S4, true, P4, false, O4, false, 0);
    vector<Register*> tail4, tail5;
    MergeJoin *join3 = new MergeJoin(scan4, S4, tail4, join2, P3, tail5, 0);
    
    drdf::DOperatorTransformer transformer;
    cout << "Preparing for execution " << endl;
    
    drdf::DOperator *dop = transformer.buildDOperator(dynamic_cast<Operator*>(join3));
    cout << "Assigning hosts" << endl;
    
    DebugPlanPrinter earlyOut(runtime, false);         
    join3->print(earlyOut);
    
    //host home for scan4
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1235"), "x"));
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1236"), "x"));   
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1234"), "x"));        
    dop->getChildren().at(0)->addRelevantHost(drdf::Host(string("localhost:1239"), "x"));            

    //relevant hosts for scan3
    drdf::DOperator *mj2, *dis;
    mj2 = dop->getChildren().at(1);
    dis = mj2->getChild(0);
    dis->addRelevantHost(drdf::Host(string("localhost:1239"), "x"));
    dis->addRelevantHost(drdf::Host(string("localhost:1240"), "x"));    
    
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1237", "xx"));   
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1238", "xx"));        
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(0)->addRelevantHost(drdf::Host("localhost:1239", "xx"));            
    dop->getChildren().at(1)->getChildren().at(1)->getChildren().at(1)->setHost(string("localhost:1234"));    
    
    dop = transformer.resolveMultipleSourcesLeaves(dop, &druntime);
    DebugPlanPrinter out(runtime, false);         
    dop->getOperator()->print(out);

    assert(transformer.assignHomeHost(dop, string("localhost:1236")));
    
    drdf::PrepExecutionResponse response;
    client.prepareForExecution(string("localhost:1236"), dop, druntime, response);
    cout << "Old cost : " << drdf::DOperatorCostFunction::evaluate(dop, ddb) << endl;
    drdf::DOperator *guineaPig = dop->find(5);
    drdf::DOperator* newMMU = transformer.parallelize(guineaPig, druntime);
    dop->normalizeIds();
//    cout << "New cost : " << drdf::DOperatorCostFunction::evaluate(dop, ddb) << endl;    
    client.prepareForExecution(dop->getHost(), dop, druntime, response);
    
    delete guineaPig->getOperator();
    delete guineaPig;
            
    delete dop->getOperator();
    delete dop;       
    
}

int main(int argc, char* argv[])
{
    f8();
    return 0;
}