#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/utsname.h>
#include <boost/lexical_cast.hpp>
#include <ios>
#include <string.h>
#include <libgen.h>

#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DPlanGen.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DSemanticAnalysis.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DPlanSearch.hpp"
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DOperatorCostFunction.hpp"
#include <boost/thread/thread.hpp>

#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
using namespace drdf;
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace drdf;

//---------------------------------------------------------------------------
bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}


struct Job{
    string database;
    string ddatabase;
    string queryfile;
    string interval;
    string binPrefix;
    string method;
};


struct ThStatistics{
    //Start time for issuing the query
    Timestamp startTime;
    Timestamp endTime;
    unsigned nQueries;
    unsigned inbetween;
    
    double througput(){
        double elapsedTime = endTime - startTime - inbetween;        
        return (nQueries * 1000) / elapsedTime;
    }
};

ThStatistics stats;

void* scheduleQuery(void *data){
    Job *theJob = (Job*)data;    
    ostringstream command;
    
    command << theJob->binPrefix << "/partoutthroughput2 " << theJob->ddatabase << " " << theJob->database << " " << theJob->queryfile << " " << theJob->method << " 1 " << theJob->interval;
    int syrt = system(command.str().c_str());
    cerr << "Running command " << command.str() << endl;
    if(syrt != 0){
        cerr << "The command " << command.str() << " failed";
    }
    return data;
}

static void printThrougputStats(ThStatistics &stats){
    cout << "Elapsed time (ms): " << stats.endTime - stats.startTime << endl;
    cout << "# operations: " << stats.nQueries << endl;
    cout << "Throughput (operations/seconds): " << stats.througput() << endl;
}

//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    string searchMethod = "trivial";
    unsigned ntimes = 5;
    unsigned interval = 1;
    vector<string> queries;
    
    // Warn first
    if (smallAddressSpace())
       cerr << "Warning: Running DRDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "DRDF-3X query interface" << endl
         << "(c) 2011 Luis Galárraga, based on RDF-3X query interface by 2008 Thomas Neumann." << endl;

    // Check the arguments
    if (argc < 4) {
      cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method=trivial] [ntimes=5] [interval=1]" << endl;
      cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
      cerr << "sa = simulated annealing; ls = local search" << endl;
      return 1;
    }
    
    char *dirName = dirname(argv[0]);

    if(argc >= 4){
        searchMethod = string(argv[4]);
        if(searchMethod.compare("ls") != 0 && searchMethod.compare("sa") != 0 && searchMethod.compare("trivial") != 0){
            cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method=trivial] [ntimes=5] [interval=1]" << endl;
            cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
            cerr << "sa = simulated annealing; ls = local search" << endl;
            return 1;
        }            
    }
        
    if(argc >= 6){
         try{
           ntimes = boost::lexical_cast<unsigned int>(argv[5]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
    }
    
    if(argc >= 7){
         try{
           interval = boost::lexical_cast<unsigned int>(argv[6]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
    }

    //Open the dummy database
    Database db;
    if(!db.open(argv[2], true)){
       cerr << "An original RDF-3x database has not been provided " << endl;
       return 1;
    }

    // Open the database
    DDatabase ddb;
    if (!ddb.open(argv[1], &db.getDictionary() ,true)) {
      cerr << "unable to open distributed database " << argv[1] << endl;
      return 1;
    }

    // Execute a single query?
    ifstream in(argv[3]);
    if (!in.is_open()) {
         cerr << "unable to open " << argv[3] << endl;
         return 1;
    }
    while(!in.eof()){
        char queryStr[16384];
        in.getline(queryStr, 16384);
        string query = string(queryStr);
        if(query.empty()) continue;        
        queries.push_back(query);
    }        

    in.close();

    stats.startTime = Timestamp();
    stats.nQueries = queries.size() * ntimes;
    stats.inbetween = 2 * interval * ntimes;    
    pthread_t *threads = new pthread_t[ntimes];
    Job job;   
    job.database = string(argv[2]);
    job.ddatabase = string(argv[1]);
    job.queryfile = string(argv[3]);
    job.binPrefix = string(dirName);
    job.interval = string(argv[6]);
    job.method = searchMethod;
    
    for(unsigned k = 0; k < ntimes; ++k){       
       int rc = pthread_create(&threads[k], NULL, scheduleQuery, &job);
       if(rc){
           cerr << "Round could not be scheduled" << endl;
       }
       boost::this_thread::sleep(boost::posix_time::milliseconds(interval));
   }
  
   
   for(unsigned k = 0; k < ntimes; ++k){
       pthread_join(threads[k], NULL);
   }    
    
   stats.endTime = Timestamp();   
   printThrougputStats(stats);    
    
   delete[] threads;
}
//---------------------------------------------------------------------------
