#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/utsname.h>
#include <boost/lexical_cast.hpp>

#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DPlanGen.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DSemanticAnalysis.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DPlanSearch.hpp"
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DOperatorCostFunction.hpp"
#include <boost/thread/thread.hpp>


#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
using namespace drdf;
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace drdf;
using namespace boost;

//---------------------------------------------------------------------------
bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}


struct Job{
    DDatabase *ddb;
    Database *db;
    string query;
    string method;
};


struct ThStatistics{
    //Start time for issuing the query
    Timestamp startTime;
    Timestamp endTime;
    unsigned nQueries;
    unsigned inbetween;
    
    double througput(){
        double elapsedTime = endTime - startTime - inbetween;        
        return (nQueries * 1000) / elapsedTime;
    }
};

ThStatistics stats;
boost::mutex nQueriesMutex;



//---------------------------------------------------------------------------
static void runQuery(DDatabase& db, Database &dummy, const string& query, const string &searchMethod, bool explain, bool silent)
   // Evaluate a query
{
    
   QueryGraph queryGraph;
   {
      // Parse the query
      SPARQLLexer lexer(query);
      SPARQLParser parser(lexer);
      try {
         parser.parse();
      } catch (const SPARQLParser::ParserException& e) {
         cerr << "parse error: " << e.message << endl;
         return;
      }

      // And perform the semantic anaylsis
      try {
         DSemanticAnalysis semana(db);
         semana.transform(parser,queryGraph);
      } catch (const DSemanticAnalysis::SemanticException& e) {
         cerr << "semantic error: " << e.message << endl;
         return;
      }
      if (queryGraph.knownEmpty()) {
         if (explain)
            cerr << "static analysis determined that the query result will be empty" << endl; else
            cout << "<empty result>" << endl;
         return;
      }
   }
   
   // Run the optimizer
   DPlanGen plangen;
   Plan* plan=plangen.translate(db,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return;
   }
   
   // Build a physical plan
   Runtime baseRuntime(dummy, NULL, NULL);
   DRuntime runtime(db, baseRuntime);

      
   PrepExecutionResponse prepResponse;        
   DRDF3xClient<DRuntime> client; //Cid identifying the machine
   DOperator *dOperatorTree;
   DPlanSearch *search;
   
   if(searchMethod.compare("trivial") == 0){
       search = new DPlanTrivialSearch(runtime, queryGraph, plan);
   }else if(searchMethod.compare("ls") == 0){
       search = new DPlanLocalSearch(runtime, queryGraph, plan);
   }else{
       search = new DPlanSASearch(runtime, queryGraph, plan);
   }
   
   dOperatorTree = search->findGoodPlan();   
   double estimatedCost = DOperatorCostFunction::evaluate(dOperatorTree, db);
   cout << "Estimated cost (query response time in ms) for operator: " << estimatedCost << endl;   
   if(explain){       
       client.explainDistributedPlan(dOperatorTree, runtime);
   }else{
       client.prepareForExecution(dOperatorTree->getHost(), dOperatorTree, runtime, prepResponse);

       if(!prepResponse.success){
           cerr << "The query could not be executed: " << prepResponse.errorMsg << endl;
           {
               boost::unique_lock<boost::mutex> lock(nQueriesMutex);
               --stats.nQueries;
           }
       }else{
           double observedCard = 0.0;
           bool firstIteration = true;                          
           while(true){
               drdf::message::ExecResponse execResp;               
               client.exec(dOperatorTree->getHost(), prepResponse.reqid, prepResponse.opid, execResp);
               if(execResp.status() == drdf::message::ExecResponse::OK){
                   DResultsPrinter *rp = new DResultsPrinter(runtime, execResp, dOperatorTree, observedCard, queryGraph.getLimit(), silent);
                   if(rp->first()){                       
                       if(firstIteration){
                           firstIteration = false;
                       }
                       while(rp->next()){}                       
                   }else{
                       if(firstIteration){
                           firstIteration = false;
                       }                       
                   }

                   observedCard = rp->getObservedOutputCardinality();
                   delete rp;
                   if(!execResp.has_hasmoreresults() || !execResp.hasmoreresults() || observedCard >= queryGraph.getLimit())
                       break;                                      
               }else{
                   cerr << "There was a problem when executing the query: " << execResp.errormsg() << endl;
                   break;
               }               
           }

       }
   }
   
   delete dOperatorTree;
   delete search;
}


void* scheduleQuery(void *data){
    Job *theJob = (Job*)data;
    runQuery(*theJob->ddb, *theJob->db, theJob->query, theJob->method, false, true);
    return theJob;
}

static void printThrougputStats(ThStatistics &stats){
    cout << "Elapsed time (ms): " << stats.endTime - stats.startTime << endl;
    cout << "# operations: " << stats.nQueries << endl;
    cout << "Throughput (operations/seconds): " << stats.througput() << endl;
}

//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    string searchMethod = "trivial";
    unsigned ntimes = 5;
    unsigned interval = 1;
    vector<string> queries;
    
    // Warn first
    if (smallAddressSpace())
       cerr << "Warning: Running DRDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "DRDF-3X query interface" << endl
         << "(c) 2011 Luis Galárraga, based on RDF-3X query interface by 2008 Thomas Neumann." << endl;

    // Check the arguments
    if (argc < 4) {
      cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method=trivial] [ntimes=5] [interval=1]" << endl;
      cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
      cerr << "sa = simulated annealing; ls = local search" << endl;
      return 1;
    }

    if(argc >= 4){
        searchMethod = string(argv[4]);
        if(searchMethod.compare("ls") != 0 && searchMethod.compare("sa") != 0 && searchMethod.compare("trivial") != 0){
            cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method=trivial] [ntimes=5] [interval=1]" << endl;
            cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
            cerr << "sa = simulated annealing; ls = local search" << endl;
            return 1;
        }            
    }
        
    if(argc >= 6){
         try{
           ntimes = boost::lexical_cast<unsigned int>(argv[5]);
       }catch(bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
    }
    
    if(argc >= 7){
         try{
           interval = boost::lexical_cast<unsigned int>(argv[6]);
       }catch(bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
    }

    //Open the dummy database
    Database db;
    if(!db.open(argv[2], true)){
       cerr << "An original RDF-3x database has not been provided " << endl;
       return 1;
    }

    // Open the database
    DDatabase ddb;
    if (!ddb.open(argv[1], &db.getDictionary() ,true)) {
      cerr << "unable to open distributed database " << argv[1] << endl;
      return 1;
    }

    // Execute a single query?
    ifstream in(argv[3]);
    if (!in.is_open()) {
         cerr << "unable to open " << argv[3] << endl;
         return 1;
    }
    
    while(!in.eof()){
        char queryStr[16384];
        in.getline(queryStr, 16384);
        string query = string(queryStr);
        cout << query << endl;
        if(query.empty()) continue;        
        queries.push_back(query);
    }        


    unsigned nQueries = queries.size();
    stats.startTime = Timestamp();
    stats.nQueries = queries.size() * ntimes;
    stats.inbetween = interval * ntimes;
    pthread_t *threads = new pthread_t[stats.nQueries];
    Job *jobs = new Job[nQueries];   
    for(unsigned i = 0; i < nQueries; ++i){
       jobs[i].db = &db;
       jobs[i].ddb = &ddb;
       jobs[i].query = queries.at(i);
       jobs[i].method = searchMethod;
    }
    
    for(unsigned k = 0; k < ntimes; ++k){       
       unsigned i;
           i = 0;
           for(vector<string>::iterator qit = queries.begin(); qit != queries.end(); ++qit, ++i){
               int rc = pthread_create(&threads[k * nQueries + i], NULL, scheduleQuery, &jobs[i]);
               if(rc){
                   cerr << "ERROR; Query " << *qit << " could not be scheduled" << endl;
               }
           }       
       boost::this_thread::sleep(boost::posix_time::milliseconds(interval));
   }
  
   
   for(unsigned k = 0; k < ntimes; ++k){
       for(unsigned m = 0; m < nQueries; ++m){
           pthread_join(threads[k * nQueries + m], NULL);
       }
   }    
    
   stats.endTime = Timestamp();   
   printThrougputStats(stats);    
    
   delete[] threads;
   delete[] jobs;
    
    
}
//---------------------------------------------------------------------------
