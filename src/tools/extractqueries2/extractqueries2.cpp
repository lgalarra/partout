#include "rts/database/Database.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/segment/FactsSegment.hpp"
#include "rts/operator/AggregatedIndexScan.hpp"
#include "rts/operator/IndexScan.hpp"
#include "rts/operator/MergeJoin.hpp"
#include "rts/operator/HashJoin.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/segment/AggregatedFactsSegment.hpp"
#include "rts/segment/FullyAggregatedFactsSegment.hpp"
#include "drdf/CommonTypes.hpp"
#include <vector>
#include <set>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------
/// Desired star size
static const unsigned starSize = 6;
//---------------------------------------------------------------------------
static bool contains(const vector<unsigned>& allNodes,unsigned id)
   // Is the id in the list?
{
/*   vector<unsigned>::const_iterator pos=lower_bound(allNodes.begin(),allNodes.end(),id);
   return ((pos!=allNodes.end())&&((*pos)==id));*/
    for(vector<unsigned>::const_iterator pos = allNodes.begin(); pos != allNodes.end(); ++pos){
        if(*pos == id) return true;
    }
    
    return false;
}
//---------------------------------------------------------------------------
static string lookupId(Database& db,unsigned id)
   // Lookup a string id
{
   const char* start=0,*stop=0; Type::ID type; unsigned subType;
   db.getDictionary().lookupById(id,start,stop,type,subType);
   return string(start,stop);
}
//---------------------------------------------------------------------------
namespace {
	//---------------------------------------------------------------------------
	class LookupFilter : public Operator {
	   private:
	   /// The input
	   Operator* input;
	   /// The lookup register
	   Register* reg;
	   /// The filter
	   vector<unsigned>& filter;
	   //// Flag
	   bool flag;
	   

	   public:
	   /// Constructor
	   LookupFilter(Operator* input, Register* reg, vector<unsigned>& filter, bool flag = true) : Operator(0), input(input), reg(reg), filter(filter), flag(flag) {}
	   /// Destructor
	   ~LookupFilter() { delete input; }

	   /// Find the first tuple
	   unsigned first();
	   /// Find the next tuple
	   unsigned next();
	   /// Print the operator
	   void print(PlanPrinter& out);
	   /// Handle a merge hint
	   void addMergeHint(Register* l,Register* r);
	   /// Register parts of the tree that can be executed asynchronous
	   void getAsyncInputCandidates(Scheduler& scheduler);
	   ////
	   int n();
           
	};
	
	int LookupFilter::n(){
		cout << "Filter size: " << filter.size() << endl;
	}
	//---------------------------------------------------------------------------
	unsigned LookupFilter::first()
	   // Find the first tuple
	{
	   unsigned count=input->first();
	   if (!count)
		  return false;
	 
	   if(flag){
		   if (contains(filter,reg->value))
		      return count;
	   }else{
		   if (!contains(filter,reg->value))
		      return count;       
	   }

	   return next();
	}
	//---------------------------------------------------------------------------
	unsigned LookupFilter::next()
	   // Find the next tuple
	{
	   while (true) {
		  unsigned count=input->next();
		  if (!count)
		     return false;
		  if(flag){
		       if(contains(filter,reg->value))
		           return count;
		  }else{
		       if(!contains(filter,reg->value))
		           return count;       
		  }
	   }
	}
	//---------------------------------------------------------------------------
	void LookupFilter::print(PlanPrinter& out)
	   // Print the operator
	{
	   out.beginOperator("LookupFilter",expectedOutputCardinality,observedOutputCardinality);

	   std::string pred=out.formatRegister(reg);
	   pred+=" in {";
	   bool first=true;
	   for (std::vector<unsigned>::const_iterator iter=filter.begin(),limit=filter.end();iter!=limit;++iter) {
		  if (first) first=true; else pred+=" ";
		  pred+=out.formatValue(*iter);
	   }
	   pred+="}";
	   out.addGenericAnnotation(pred);

	   input->print(out);

	   out.endOperator();
	}
	//---------------------------------------------------------------------------
	void LookupFilter::addMergeHint(Register* l,Register* r)
	   // Handle a merge hint
	{
	   input->addMergeHint(l,r);
	}
	//---------------------------------------------------------------------------
	void LookupFilter::getAsyncInputCandidates(Scheduler& scheduler)
	   // Register parts of the tree that can be executed asynchronous
	{
	   input->getAsyncInputCandidates(scheduler);
	}
}

//---------------------------------------------------------------------------
static string lookupURL(Database& db,unsigned id)
   // Lookup a URL
{
   const char* start=0,*end=start; Type::ID type; unsigned subType;
   db.getDictionary().lookupById(id,start,end,type,subType);
   return "<"+string(start,end)+">";
}
//---------------------------------------------------------------------------
static string lookupLiteral(Database& db,unsigned id)
   // Lookup a literal value
{
   const char* start=0,*end=start; Type::ID type; unsigned subType;
   db.getDictionary().lookupById(id,start,end,type,subType);

   if (type==Type::URI)
      return "<"+string(start,end)+">"; else
      return "\""+string(start,end)+"\"";
}

void findProperties(Database &db, set<drdf::Pair> &allProperties, set<drdf::Pair> &frequentProperties, float threshold){
    FullyAggregatedFactsSegment::Scan scan;
    //Gather all possible properties
    if (scan.first(db.getFullyAggregatedFacts(Database::Order_Predicate_Object_Subject))) do {
         allProperties.insert(drdf::Pair(scan.getCount(), scan.getValue1()));
    }while (scan.next());
    
    //Now gather the most frequent
    unsigned card = ((float)allProperties.size()) * threshold;
    unsigned k = 0;
    cout << "The most frequent properties are: " << endl;
    for(set<drdf::Pair>::reverse_iterator pit = allProperties.rbegin(); pit != allProperties.rend(), k < card; ++pit, ++k){
        frequentProperties.insert(*pit);
        cout << pit->value1 << "|" << lookupId(db, pit->value2) << endl;
        
    }
}

static void buildStarStartingAt(Database &db, unsigned startProperty, set<unsigned> &frequentProperties, set<unsigned> &usedProperties, set<unsigned> &fusedProperties, bool isFrequentProperty, vector<drdf::Pair> &output, unsigned maxStarSize){
    //Pick a starting property
    unsigned joiningProperties = 0;
    
    if(isFrequentProperty){
        fusedProperties.insert(startProperty);
    }else{
        usedProperties.insert(startProperty);
    }
    
    //Now find joining properties 
    Register sub1, pred1, obj1, sub2, pred2, obj2;
    sub1.reset();
    pred1.value = startProperty;
    pred1.domain = NULL;
    obj1.reset();
    sub2.reset();
    pred2.reset();
    obj2.reset();  
  
    
    IndexScan* scan1 = IndexScan::create(db, Database::Order_Subject_Predicate_Object, &sub1, false, &pred1, true, &obj1, false, 0);    
    IndexScan* scan2 = IndexScan::create(db, Database::Order_Subject_Predicate_Object, &sub2, false, &pred2, false, &obj2, false, 0);     
    vector<unsigned> filteredValues;
    
    for(set<unsigned>::iterator it = usedProperties.begin(); it != usedProperties.end(); ++it)
        filteredValues.push_back(*it);
    
    LookupFilter *filter2 = new LookupFilter(scan2, &pred2, filteredValues, false);
    vector<Register*> leftTail, rightTail;
    rightTail.push_back(&pred2);
    rightTail.push_back(&obj2);
    MergeJoin *join = new MergeJoin(scan1, &sub1, leftTail, filter2, &sub2, rightTail, 0);    
    if(join->first()){	
        output.push_back(drdf::Pair(pred2.value, obj2.value));
//        filteredValues.push_back(pred2.value);
        ++joiningProperties;        
        if(pred1.value != pred2.value){
            output.push_back(drdf::Pair(pred1.value, obj1.value));
//            filteredValues.push_back(pred1.value);
            ++joiningProperties;
        }
        while(join->next() && joiningProperties < maxStarSize){    
            if(frequentProperties.find(pred2.value) == frequentProperties.end())
                usedProperties.insert(pred2.value);
            else
                fusedProperties.insert(pred2.value);
            
//            filteredValues.push_back(pred2.value);
            filteredValues2.push_back(pred2.value);
            output.push_back(drdf::Pair(pred2.value, obj2.value));
            ++joiningProperties;
        }
    }
      
    delete join;
	
}

static void buildStar(Database &db, set<drdf::Pair> &allProperties, set<unsigned> &frequentProperties, set<unsigned> &usedProperties, set<unsigned> &fusedProperties, bool useFrequentProperties, 
        vector<drdf::Pair> &output, unsigned maxStarSize){
    //Pick a starting property
    unsigned startProperty;
    unsigned startPosition;
    
    if(useFrequentProperties){
        startPosition = rand() % frequentProperties.size();
        set<unsigned>::iterator it = frequentProperties.begin();
        for(unsigned i = 0; i <= startPosition; ++i) ++it;
        startProperty = *it;
    }else{
        startPosition = frequentProperties.size() + rand() % (allProperties.size() - frequentProperties.size());
        set<drdf::Pair>::iterator it = allProperties.begin();
        for(unsigned i = 0; i <= startPosition; ++i) ++it;
        startProperty = it->value2;            
    }


    buildStarStartingAt(db, startProperty, frequentProperties, usedProperties, fusedProperties, useFrequentProperties, output, maxStarSize);
    
}

static void buildPathStartingAt(Database &db, unsigned startProperty, set<unsigned> &usedProperties, set<unsigned> &fusedProperties, bool isFrequentProperty, vector<drdf::Triple> &output, unsigned maxPathSize){
    unsigned joiningProperties = 0;
    vector<unsigned> filteredValues;

    if(isFrequentProperty)
        fusedProperties.insert(startProperty);
    else
        usedProperties.insert(startProperty);

    for(set<unsigned>::iterator it = usedProperties.begin(); it != usedProperties.end(); ++it)
        filteredValues.push_back(*it);
    
    //Now time to look for a path
    Register sub1, pred1, obj1, sub2, pred2, obj2;
    sub1.reset();
    pred1.value = startProperty;
    pred1.domain = NULL;
    obj1.reset();
    sub2.reset();
    pred2.reset();
    obj2.reset();    
    
    while(joiningProperties < maxPathSize){        
        Operator *left, *right;
        if(joiningProperties == 0){
            AggregatedIndexScan* scan1 = AggregatedIndexScan::create(db, Database::Order_Object_Predicate_Subject, NULL, false, &pred1, true, &obj1, false, 0);  
            left = dynamic_cast<Operator*>(scan1); 
            output.push_back(drdf::Triple(~0u, pred1.value, ~0u));
        }else{
            sub1.reset();
            pred1.value = pred2.value;
            obj1.value = obj2.value;
            IndexScan *scan1 = IndexScan::create(db, Database::Order_Object_Predicate_Subject, &sub1, false, &pred1, true, &obj1, true, 0);
            left = dynamic_cast<Operator*>(scan1);                                            
        }

        pred2.reset();
        sub2.reset();
        obj2.reset();
        IndexScan *scan2 = IndexScan::create(db, Database::Order_Subject_Object_Predicate, &sub2, false, &pred2, false, &obj2, false, 0);
        LookupFilter *filter2 = new LookupFilter(scan2, &pred2, filteredValues, false);
        right = dynamic_cast<Operator*>(filter2);                                
        
        //Now join them
        vector<Register*> lt, rt;
        MergeJoin *join = new MergeJoin(left, &obj1, lt, right, &sub2, rt, 0);
        if(join->first()){
            //Add the triple
            output.push_back(drdf::Triple(sub2.value, pred2.value, obj2.value));
        }else{
            cout << "Dead end" << endl;
            delete join;
            break;
        }
        ++joiningProperties;

        delete join;
    }

}

static void buildPath(Database &db, set<unsigned> &frequentProperties, set<unsigned> &usedProperties, set<unsigned> &fusedProperties, bool useFrequentProperties, vector<drdf::Pair> &star, 
        vector<drdf::Triple> &output, unsigned maxPathSize){
    //Pick one of the properties in the star
    unsigned startProperty;
    unsigned startPosition;
    vector<unsigned> filteredValues;
    for(set<unsigned>::iterator it = usedProperties.begin(); it != usedProperties.end(); ++it)
        filteredValues.push_back(*it);
    
    if(useFrequentProperties){
        startPosition = rand() % frequentProperties.size();
        set<unsigned>::iterator it = frequentProperties.begin();
        for(unsigned i = 0; i <= startPosition; ++i) ++it;
        startProperty = *it;
    }else{
        startPosition = rand() % star.size();
        vector<drdf::Pair>::iterator it = star.begin();
        for(unsigned i = 0; i <= startPosition; ++i) ++it;
        startProperty = it->value1;
    }
	
    buildPathStartingAt(db, startProperty, usedProperties, fusedProperties, useFrequentProperties, output, maxPathSize);
    //Start with a

}


static void buildStarPath(Database &db, set<unsigned> &frequentProperties, set<unsigned> &usedProperties, set<unsigned> &fusedProperties,
    bool useFrequentProperties, vector<drdf::Pair> &star1, vector<drdf::Triple> &path, vector<drdf::Pair> &star2){
    unsigned pathStart = rand() % star1.size();
    unsigned startProperty = star1.at(pathStart).value1;
    //Now build a path starting from any of star arms
    buildPathStartingAt(db, startProperty, usedProperties, fusedProperties, useFrequentProperties, path, 2);

    if(!path.empty()){
        //Build a star starting at the property of the last element of the path
        buildStarStartingAt(db, path.back().value2, frequentProperties, usedProperties, fusedProperties, useFrequentProperties, star2, 3);
    }
}

static void outputQuery(Database &db, vector<drdf::Pair> &patterns, ofstream &qloadStm, ofstream &experimentsStm){
    if(patterns.empty())
        return;
    
    set<unsigned> seenProps;
    
    qloadStm << "SELECT * WHERE { " << endl;
    experimentsStm << "SELECT * WHERE { " << endl;
    unsigned k = 0;
    unsigned rPos = rand() % patterns.size();
    for(vector<drdf::Pair>::iterator pit = patterns.begin(); pit != patterns.end(); ++pit, ++k){
        if(seenProps.find(pit->value1) == seenProps.end())
            seenProps.insert(pit->value1);
        else
            continue;
        
        string predicate = lookupURL(db, pit->value1);
        string object = lookupLiteral(db, pit->value2);        
        if(predicate.compare("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>") == 0){
            qloadStm << "?s " << predicate << " " << object << " ." << endl;            
        }else{
            qloadStm << "?s " << predicate << " ?o" << k << " ." << endl;
        }
        
        if(k == rPos){
            experimentsStm << "?s " << predicate << " " << object << " ." << endl;
        }else{
            experimentsStm << "?s " << predicate << " ?o" << k << " ." << endl;           
        }
        
    }
    
    experimentsStm << "}" << endl << endl;
    qloadStm << "}" << endl << endl;
}

static void outputQuery(Database &db, vector<drdf::Triple> &patterns, ofstream &qloadStm, ofstream &experimentsStm){
    if(patterns.empty())
        return;
    
    qloadStm << "SELECT * WHERE { " << endl;
    experimentsStm << "SELECT * WHERE { " << endl;
    unsigned k = 0;
    vector<drdf::Triple>::iterator pit = patterns.begin();
    qloadStm << "?s " << lookupURL(db, pit->value2) << " ?o0" << " ." << endl;    
    experimentsStm << "?s " << lookupURL(db, pit->value2) << " ?o0" << " ." << endl;    
    ++pit;
    for(pit;pit != patterns.end(); ++pit, ++k){
        string predicate = lookupURL(db, pit->value2);        
        qloadStm << "?o" << k << " " << predicate << " ?o" << (k+1) << " ." << endl;        

        if(k == patterns.size() - 1){
            if(rand() % 2 == 0){
                experimentsStm << "?o" << k << " " << predicate << " " << lookupLiteral(db, pit->value3) << " ." << endl;                        
                continue;
            }
        }
        experimentsStm << "?o" << k << " " << predicate << " ?o" << (k+1) << " ." << endl;                        
    }
    
    experimentsStm << "}" << endl << endl;
    qloadStm << "}" << endl << endl;
}

static void outputQuery(Database &db, vector<drdf::Pair> &star1, vector<drdf::Triple> &path, vector<drdf::Pair> &star2, ofstream &qloadStm, ofstream &experimentsStm){    
    qloadStm << "SELECT * WHERE { " << endl;
    experimentsStm << "SELECT * WHERE { " << endl;
    unsigned k = 0;
    for(vector<drdf::Pair>::iterator pit = star1.begin(); pit != star1.end(); ++pit, ++k){
        string predicate = lookupURL(db, pit->value1);
        string object = lookupLiteral(db, pit->value2);
        if(predicate.compare("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>") == 0){
            qloadStm << "?s " << predicate << " " << object << " ." << endl;
            experimentsStm << "?s " << predicate << " " << object << " ." << endl;                               
        }else{
            qloadStm << "?s " << predicate << " ?o" << k << " ." << endl;
            experimentsStm << "?s " << predicate << " ?o" << k << " ." << endl;
        }
    }
    
    unsigned m = k - 1;    
    for(vector<drdf::Triple>::iterator pit = path.begin(); pit != path.end(); ++pit, ++m){
        string predicate = lookupURL(db, pit->value2);        
        qloadStm << "?o" << m << " " << predicate << " ?o" << (m+1) << " ." << endl;        
        experimentsStm << "?o" << m << " " << predicate << " ?o" << (m+1) << " ." << endl;                        
    }
    unsigned l = 0;
    for(vector<drdf::Pair>::iterator pit = star2.begin(); pit != star2.end(); ++pit, ++l){
        string predicate = lookupURL(db, pit->value1);
        string object = lookupLiteral(db, pit->value2);
        if(predicate.compare("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>") == 0){
            qloadStm << "?o" << m << " " << predicate << " " << object << " ." << endl;
            experimentsStm << "?o" << m << " " << predicate << " " << object << " ." << endl;                               
        }else{
            qloadStm << "?o" << m << " " << predicate << " ?q" << l << " ." << endl;
            experimentsStm << "?o" << m << " " << predicate << " ?q" << l << " ." << endl;
        }
    }
    
    experimentsStm << "}" << endl << endl;
    qloadStm << "}" << endl << endl;    
}

static void outputByPropertyQueries(Database &db, set<unsigned> &usedProperties, set<unsigned> &fusedProperties, ofstream &outputStm){
    for(set<unsigned>::iterator upit = usedProperties.begin(); upit != usedProperties.end(); ++upit){
        outputStm << "SELECT * WHERE { ?s " << lookupURL(db, *upit) << " ?o }" << endl << endl;
    }
    
    for(set<unsigned>::iterator upit = fusedProperties.begin(); upit != fusedProperties.end(); ++upit){
        outputStm << "SELECT * WHERE { ?s " << lookupURL(db, *upit) << " ?o }" << endl << endl;
    }

}

static void buildQueries(Database &db, set<drdf::Pair> &allProperties, set<drdf::Pair> &frequentProperties, unsigned niter, 
        float fproperties, unsigned maxStarSize, unsigned maxPathSize){
    set<unsigned> usedProperties, frequentUsedProperties;
    srand (time(NULL));
    int threshold = RAND_MAX * fproperties;
    ofstream qloadStm("queryload");
    ofstream experimentsStm("experiments");
    ofstream byPropertyStm("byproperty");
    set<unsigned> fvals;    
    
    for(set<drdf::Pair>::iterator fit = frequentProperties.begin(); fit != frequentProperties.end(); ++fit){
        fvals.insert(fit->value2);
    }
    
    for(unsigned i = 0; i < niter; ++i){
        bool useFrequentProperty = rand() <= threshold;
        vector<drdf::Pair> star1, star2;
        vector<drdf::Triple> path1, path2;
        buildStar(db, allProperties, fvals, usedProperties, frequentUsedProperties, useFrequentProperty, star1, maxStarSize);
        if(star1.empty()) continue;
        buildPath(db, fvals, usedProperties, frequentUsedProperties, useFrequentProperty, star1, path1, maxPathSize);        
        buildStarPath(db, fvals, usedProperties, frequentUsedProperties, useFrequentProperty, star1, path2, star2);
        outputQuery(db, star1, qloadStm, experimentsStm);
        outputQuery(db, path1, qloadStm, experimentsStm);
        outputQuery(db, star1, path2, star2, qloadStm, experimentsStm);
    }
    
    
    outputByPropertyQueries(db, usedProperties, frequentUsedProperties, byPropertyStm);
    
    qloadStm.close();
    experimentsStm.close();
    byPropertyStm.close();
}

//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
   
   float threshold = 0.001;
   unsigned niter = 20;
   float frequentProps = 0.1;
   unsigned maxStarSize = 6;
   unsigned maxPathSize = 4;
   set<drdf::Pair> properties;
   set<drdf::Pair> frequentProperties;
   if (argc<2) {
      cout << "usage: " << argv[0] << " <rdfstore> [threshold=0.001] [niter=20] [frequentPropsUsage=0.1] [maxStarSize=6] [maxPathSize=4]" << endl;
      cout << "threshold = fraction of the most frequent properties. They treated in a different way during query construction" << endl;
      cout << "niter = # of iterations. Every iteration will try to build 3 queries: a full star, a long path and a mixture of them " << endl;
      cout << "frequentPropsUsage = % of iterations that should consider frequent properties" << endl;      
      return 1;
   }

   // Open the database
   Database db;
   if (!db.open(argv[1])) {
      cout << "unable to open " << argv[1] << endl;
      return 1;
   }
   
   if(argc >= 3){
       try{
           threshold = boost::lexical_cast<float>(argv[2]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for the number of threshold" << endl;
            exit(1);
       }
   }
   
   if(argc >= 4){
       try{
           niter = boost::lexical_cast<unsigned>(argv[3]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for niter" << endl;
            exit(1);
       }
   }

   if(argc >= 5){
       try{
           frequentProps = boost::lexical_cast<float>(argv[4]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for frequentProps" << endl;
            exit(1);
       }
   }   
   
   if(argc >= 6){
       try{
          maxStarSize = boost::lexical_cast<unsigned>(argv[5]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for frequentProps" << endl;
            exit(1);
       }       
   }
   
   if(argc >= 7){
       try{
          maxPathSize = boost::lexical_cast<unsigned>(argv[6]);
       }catch(boost::bad_lexical_cast &e){
            cerr << "Invalid value for maxPath" << endl;
            exit(1);
       }       
   }   
   
   findProperties(db, properties, frequentProperties, threshold);
   cout << "found " << properties.size() << " different properties" << endl;
   cout << frequentProperties.size() << " with very low selectivity" << endl;  
   
   buildQueries(db, properties, frequentProperties, niter, frequentProps, maxStarSize, maxPathSize);
   

}
//---------------------------------------------------------------------------
