#include "cts/codegen/CodeGen.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/database/Database.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/ResultsPrinter.hpp"
#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <pthread.h>
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>
#include <ios>
#include <string.h>
#include <libgen.h>
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace boost;
//---------------------------------------------------------------------------

struct Job{
    string database;
    string queryfile;
    string binPrefix;
    string interval;
};


struct ThStatistics{
    //Start time for issuing the query
    Timestamp startTime;
    Timestamp endTime;
    unsigned nQueries;
    unsigned inbetween;
    
    double througput(){
        double elapsedTime = endTime - startTime - inbetween;        
        return (nQueries * 1000) / elapsedTime;
    }
};


bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}



static void printThrougputStats(ThStatistics &stats){
    cout << "Elapsed time (ms): " << stats.endTime - stats.startTime << endl;
    cout << "# operations: " << stats.nQueries << endl;
    cout << "Throughput (operations/seconds): " << stats.througput() << endl;
}

static void* scheduleQuery(void *data){
    Job *theJob = (Job*)data;    
    ostringstream command;
    
    command << theJob->binPrefix << "/throughputcentralized2 " << theJob->database << " " << theJob->queryfile << " 1 1";
    int syrt = system(command.str().c_str());
    if(syrt != 0){
        cerr << "The command " << command.str() << " failed";
    }
    return data;
}

//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
   vector<string> queries;
   unsigned ntimes = 5;
   unsigned interval = 1;
   string binPrefix;
   queries.reserve(20);
   
   // Warn first
   if (smallAddressSpace())
      cerr << "Warning: Running RDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

   // Greeting
   cerr << "RDF-3X query interface" << endl
        << "(c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x" << endl;

   // Check the arguments
   if (argc < 3) {
      cerr << "usage: " << argv[0] << " <database> <queryfile> [ntimes=5] [interval=1]" << endl;
      return 1;
   }
   
   char *dirName = dirname(argv[0]);
   binPrefix = string(dirName);

   // Open the database
   Database db;
   if (!db.open(argv[1],true)) {
      cerr << "unable to open database " << argv[1] << endl;
      return 1;
   }
   db.close();

   ifstream in(argv[2]);
   if (!in.is_open()) {
       cerr << "unable to open " << argv[2] << endl;
       return 1;
   }
   
   if(argc >= 4){
       try{
           ntimes = boost::lexical_cast<unsigned int>(argv[3]);
       }catch(bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
   }
   
   if(argc >= 5){
       try{
           interval = boost::lexical_cast<unsigned int>(argv[4]);
       }catch(bad_lexical_cast &e){
            cerr << "Invalid value for the number of times" << endl;
            exit(1);
       }
   }
   
   while(!in.eof()){
       char queryStr[8192];
       in.getline(queryStr, 8192);
       string query = string(queryStr);
       if(query.empty()) continue;
       queries.push_back(query);
   }      
   
   ThStatistics stats;
   stats.startTime = Timestamp();
   stats.nQueries = queries.size() * ntimes;
   stats.inbetween = interval * ntimes;
   pthread_t *threads = new pthread_t[ntimes];
   Job job;   
   job.database = string(argv[1]);
   job.queryfile = string(argv[2]);
   job.binPrefix = binPrefix;
   
   for(unsigned k = 0; k < ntimes; ++k){       
       int rc = pthread_create(&threads[k], NULL, scheduleQuery, &job);
       if(rc){
           cerr << "ERROR: round could not be scheduled!" << endl;
       }
       boost::this_thread::sleep(boost::posix_time::milliseconds(interval));
   }
    
   for(unsigned k = 0; k < ntimes; ++k){
       pthread_join(threads[k], NULL);
   }

   stats.endTime = Timestamp();   
   printThrougputStats(stats);
   db.close();
   in.close();
   
   delete[] threads;
}
//---------------------------------------------------------------------------
