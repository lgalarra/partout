/* 
 * File:   StatisticsPrinter.hpp
 * Author: luis
 *
 * Created on June 23, 2011, 12:59 PM
 */

#ifndef stdpartitioner_fragmentstatitisticsprinter_hpp
#define	stdpartitioner_fragmentstatitisticsprinter_hpp

#include "partitioning/Fragment.hpp"
#include "partitioning/PartitioningDescription.hpp"
#include "qload/QueryLoadDecodedInputStream.hpp"
#include "cts/infra/QueryGraph.hpp"
#include <map>
#include <string>

using namespace stdpartitioning;
using namespace std;
using namespace qload;

struct QueryStatsDescriptor{    
public:
    struct Node{
        QueryGraph::Node node;
        unsigned int id;
        
        Node();
    };    
    
   string query;
   unsigned int subquery;
   vector<pair<string, vector<Node> > > assignment;
   unsigned int njoins;
   
   QueryStatsDescriptor(): remoteOperations(-1), localOperations(-1), njoins(0), relevantNodes(-1){};
   int getRemoteOperations();
   int getLocalOperations();
   int getRelevantNodes();   
   
   
private:
   int remoteOperations;
   int localOperations;
   int relevantNodes;
   static unsigned int qsid;
};




class FragmentStatisticsPrinter {
public:
    FragmentStatisticsPrinter(map<string, string> &dataFiles, stdpartitioning::DatabaseDescriptor &complete);
    virtual ~FragmentStatisticsPrinter();
    void outputStatistics(FragmentationScheme &scheme, const char* outputFile, FragmentsAllocationMap &map, PartitioningDescription &pdescription);    
    void outputFragmentsQualityStatistics(QueryLoadDecodedInputStream &in, FragmentsAllocationMap &map, const char* output);
    
private:
    map<string, Database*> databases;
    map<string, string> &fileMaps;
    static map<string, unsigned int> queryCache;
    stdpartitioning::DatabaseDescriptor &dbds;
    bool extractQueryGraph(Database &db, string query, QueryGraph &queryGraph);
    void evaluateInNodes(QueryGraph::SubQuery &subquery, FragmentsAllocationMap &map, QueryStatsDescriptor &descriptor);
    bool isRelevantToNode(QueryGraph::Node &triplePattern, stdpartitioning::Fragment *fragment);    
    void extractPredicates(QueryGraph::Node &triplePattern, vector<stdpartitioning::SPARQLPredicate> &predicates);    
    string buildQuery(QueryGraph::Node &triplePattern);
};

#endif	/* STATISTICSPRINTER_HPP */

