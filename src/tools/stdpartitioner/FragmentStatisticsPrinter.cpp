/* 
 * File:   FragmentStatisticsPrinter.cpp
 * Author: luis
 * Created on June 23, 2011, 12:59 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "FragmentStatisticsPrinter.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "partitioning/CommonTypes.hpp"
#include "partitioning/Utilities.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>


using namespace std;
using namespace stdpartitioning;

unsigned int QueryStatsDescriptor::qsid = 0;
map<string, unsigned int> FragmentStatisticsPrinter::queryCache;

QueryStatsDescriptor::Node::Node():id(++qsid){}

int QueryStatsDescriptor::getRemoteOperations(){
   cout << "getRemoteOperations" << endl;    
   if(remoteOperations == -1){
       remoteOperations = 0;           
       vector<pair<string, vector<Node> > >::const_iterator it, it2;           
       vector<Node>::const_iterator nit, nit2;           
       for(it = this->assignment.begin(); it != this->assignment.end(); ++it){
           cout << "Node " << it->first << endl;
           for(nit = it->second.begin(); nit != it->second.end(); ++nit ){
               for(it2 = it + 1; it2 != this->assignment.end(); ++it2){
                   for(nit2 = it2->second.begin(); nit2 != it2->second.end(); ++nit2){
                       cout << "Comparing nodes " << nit->id << " and " << nit2->id << endl;                       
                       if(nit2->id != nit->id && nit2->node.canJoin(nit->node)){
                           ++remoteOperations;
                       }
                   }
               }
           }
       }
   }

   return remoteOperations;
}


int QueryStatsDescriptor::getLocalOperations(){
   cout << "getLocalOperations" << endl;
   if(localOperations == -1){
       localOperations = 0;
       vector<pair<string, vector<Node> > >::const_iterator it;           
       vector<Node>::const_iterator nit, nit2;           
       for(it = this->assignment.begin(); it != this->assignment.end(); ++it){
           cout << "Node " << it->first << endl;
           for(nit = it->second.begin(); nit != it->second.end(); ++nit ){
               for(nit2 = nit + 1; nit2 != it->second.end(); ++nit2){
                   cout << "Comparing nodes " << nit->id << " and " << nit2->id << endl;
                   if(nit2->id != nit->id && nit2->node.canJoin(nit->node)){
                       ++localOperations;
                   }
               }                   
           }
       }            
   }

   return localOperations;
}

int QueryStatsDescriptor::getRelevantNodes(){
   vector<pair<string, vector<Node> > >::const_iterator it;
   if(this->relevantNodes == -1){
       this->relevantNodes = 0;
       for(it = this->assignment.begin(); it != this->assignment.end(); ++it){
           if(!it->second.empty())
               ++this->relevantNodes;
       }
   }
   
   return this->relevantNodes;
}


FragmentStatisticsPrinter::FragmentStatisticsPrinter(map<string, string> &dataFiles, DatabaseDescriptor &complete):dbds(complete), fileMaps(dataFiles){    
    for(map<string, string>::iterator it = dataFiles.begin(); it != dataFiles.end(); ++it){
        this->databases[it->first] = new Database();
        this->databases[it->first]->open(it->second.c_str(), true);		
    }
}

FragmentStatisticsPrinter::~FragmentStatisticsPrinter() {
    this->dbds.db->close();
    for(map<string, Database*>::iterator it = this->databases.begin(); it != this->databases.end(); ++it){
        Database *pdb;
        pdb = it->second;
        it->second = NULL;
        pdb->close();
        delete pdb;        
    }
}

void FragmentStatisticsPrinter::outputFragmentsQualityStatistics(QueryLoadDecodedInputStream &in, FragmentsAllocationMap &map, const char* output){
    ofstream outstream; 
    vector<QueryStatsDescriptor> descriptors;    
    int totalLocalJoins = 0, totalRemoteJoins = 0, totalJoins = 0;    
    outstream.open(output, fstream::out);
    double nodesContactedTotal = 0.0;    
    
    while(!in.eof()){
        string query;
        QueryGraph queryGraph;
        in >> query;
        vector<QueryGraph::SubQuery> subqueries;
        
        if(query.empty())
            continue;
        
        cout << "Building statistics for query " << query << endl;
        if(this->extractQueryGraph(*this->dbds.db, query, queryGraph)){
            cout << "Root query has " << queryGraph.getQuery().nodes.size() << " nodes " << endl;
            extractIndependentSubqueries(queryGraph.getQuery(), subqueries);
            unsigned int i = 1;
            for(vector<QueryGraph::SubQuery>::iterator it = subqueries.begin(); it != subqueries.end(); ++it){
                QueryStatsDescriptor queryStats;
                queryStats.query = query;
                queryStats.subquery = i;
                this->evaluateInNodes(*it, map, queryStats);
                i++;
                descriptors.push_back(queryStats);            
            }
        }else{
            cerr << "Problem parsing query " << query << endl;
        }        
    }
    
    //Now generate the overall statistics 
    outstream << "Original Database file=" << this->dbds.filename << endl;    
    for(vector<QueryStatsDescriptor>::iterator qsdit = descriptors.begin(); qsdit != descriptors.end(); ++qsdit){
        int localJoins, remoteJoins;
        localJoins = qsdit->getLocalOperations();
        remoteJoins = qsdit->getRemoteOperations();
        totalJoins += qsdit->njoins;
        totalLocalJoins += localJoins;
        totalRemoteJoins += remoteJoins;
        double ratioLocal = (double)localJoins/(double)qsdit->njoins;
        double ratioRemote = (double)remoteJoins/(double)qsdit->njoins;
        nodesContactedTotal +=  (double)qsdit->getRelevantNodes();
        outstream << "[SubQuery]" << endl;
        outstream << qsdit->query << endl;
        outstream << "originaledges=" << qsdit->njoins << endl;
        outstream << "subquery=" << qsdit->subquery << endl;
        outstream << "localedges=" << localJoins  << endl;
        outstream << "remoteedges=" << remoteJoins << endl;
        outstream << "ratioremote=" << ratioRemote << endl;
        outstream << "ratiolocal=" << ratioLocal << endl; 
        outstream << "nodesContacted=" << qsdit->getRelevantNodes() << endl;
        outstream << "[SubQuery]" << endl;
    }            
    
    for(vector<QueryStatsDescriptor>::iterator qsdit = descriptors.begin(); qsdit != descriptors.end(); ++qsdit){
        cout << endl;
        vector<pair<string, vector<QueryStatsDescriptor::Node> > >::iterator snit;
        cout << "*****************" << endl;
        cout << "Query " << qsdit->query << endl;
        cout << "Subquery " << qsdit->subquery << endl;
        for(snit = qsdit->assignment.begin(); snit != qsdit->assignment.end(); ++snit){
            cout << snit->first << ": ";
            for(vector<QueryStatsDescriptor::Node>::iterator nit = snit->second.begin(); nit != snit->second.end(); ++nit){
                cout << "[id: " << nit->id << " node: " << nit->node.constSubject << nit->node.constPredicate << nit->node.constObject;
                cout << " " << nit->node.subject << " " << nit->node.predicate << " " <<  nit->node.object << "]" << endl;
            }
            cout << endl;
        }
        cout << "*****************" << endl;
    }

    outstream << "[Total]" << endl;
    outstream << "edges=" << totalJoins << endl;
    outstream << "subqueries=" << descriptors.size() << endl;
    outstream << "averageedges=" << ((double)totalJoins / (double)descriptors.size()) << endl;
    outstream << "localedges=" << totalLocalJoins << endl;
    outstream << "remoteedges=" << totalRemoteJoins << endl;
    outstream << "ratioremote=" << ((double)totalRemoteJoins/(double)totalJoins) << endl;
    outstream << "ratiolocal=" << ((double)totalLocalJoins/(double)totalJoins) << endl;
    outstream << "averageNodesContacted=" << nodesContactedTotal / descriptors.size() << endl;
    
    
    outstream.close();
}

void FragmentStatisticsPrinter::evaluateInNodes(QueryGraph::SubQuery &subquery, FragmentsAllocationMap &map, 
       QueryStatsDescriptor &descriptor){
    vector<QueryGraph::Node>::iterator nit;
    vector<QueryStatsDescriptor::Node> nodes;
    vector<QueryStatsDescriptor::Node>::iterator qsnit, nit3, nit4;    
    cout << "FragmentStatisticsPrinter::evaluateInNodes" << endl;
    cout << "Nodes " <<  subquery.nodes.size() << endl;
    for(nit = subquery.nodes.begin(); nit != subquery.nodes.end(); ++nit){
        QueryStatsDescriptor::Node node;
        node.node = *nit;
        nodes.push_back(node);                
        cout << "Adding node " << nit->constSubject << nit->constPredicate << nit->constObject << "|";
        cout << nit->subject << " " << nit->predicate << " " << nit->object;
        cout << " Assigned id: " << node.id << endl;
        
    }
    
    
    for(nit3 = nodes.begin(); nit3 != nodes.end(); ++nit3){
        for(nit4 = nit3 + 1; nit4 != nodes.end(); ++nit4){
            cout << "Can " << nit3->node.subject << " " << nit3->node.predicate << " " << nit3->node.object << endl;
            cout << "join with " << nit4->node.subject << " " << nit4->node.predicate << " " << nit4->node.object << endl;            
            if(nit3->node.canJoin(nit4->node)){
                ++descriptor.njoins;
            }
        }
    }
    
    
    for(qsnit = nodes.begin(); qsnit != nodes.end(); ++qsnit){
        FragmentsAllocationMap::iterator amit;
        for(amit = map.begin(); amit != map.end(); ++amit){
            vector<FragmentAllocationAssign>::iterator pdit;
            
            //Search for the node
            vector<pair<string, vector<QueryStatsDescriptor::Node> > >::iterator it, theNode;
            bool nodeFound = false;
            for(it = descriptor.assignment.begin(); it != descriptor.assignment.end(); ++it){
                if(it->first.compare(amit->first) == 0){
                    theNode = it;
                    nodeFound = true;
                    break;
                }
            }
            
            if(!nodeFound){
                descriptor.assignment.push_back(pair<string, vector<QueryStatsDescriptor::Node> >(amit->first, vector<QueryStatsDescriptor::Node>()));                            
                theNode = descriptor.assignment.end();
                --theNode;
            }
                       
            for(pdit = amit->second.begin(); pdit != amit->second.end(); ++pdit){
                cout << "Asking if node " << qsnit->id << " is relevant to " << *pdit->fragment << endl;
                if(this->isRelevantToNode(qsnit->node, pdit->fragment)){
                    cout << "Node " << qsnit->id << " is relevant to " << *pdit->fragment << endl;
                    cout << " and therefore to node " << amit->first << endl;
                    theNode->second.push_back(*qsnit);                    
                    break;
                }                
            }            
        }
    }
    
}

void FragmentStatisticsPrinter::extractPredicates(QueryGraph::Node &triplePattern, vector<SPARQLPredicate> &predicates){
    //First build predicates
    QueryGraph::Filter filter;
    SPARQLPredicate::Position position = SPARQLPredicate::Object;
    filter.type = QueryGraph::Filter::Equal;
    filter.arg1 = new QueryGraph::Filter();
    filter.arg1->type = QueryGraph::Filter::Variable;            
    filter.arg2 = new QueryGraph::Filter();            

            
    if(triplePattern.constSubject){
        position = SPARQLPredicate::Subject;
        filter.arg2->type = QueryGraph::Filter::IRI;
        filter.arg2->id = triplePattern.subject;
        SPARQLPredicate newPred;
        newPred.position = position;
        newPred.predicate = filter;
        predicates.push_back(newPred);
    }
            
    if(triplePattern.constPredicate){
        position = SPARQLPredicate::Predicate;
        filter.arg2->type = QueryGraph::Filter::IRI;
        filter.arg2->id = triplePattern.predicate;
        SPARQLPredicate newPred;
        newPred.position = position;
        newPred.predicate = filter;
        predicates.push_back(newPred);
    }

    if(triplePattern.constObject){
        position = SPARQLPredicate::Object;
        QueryGraph::Filter::Type ftype;
        Type::ID type, type2; unsigned subType, subType2, typeId;
        ftype = determineEntityType(*this->dbds.db, triplePattern.object, type, subType);
        if(ftype == QueryGraph::Filter::Literal){
            filter.arg2->type = ftype;
            //Here things become little bit more complicated
            string result, typeDesc;                    
            lookupById(*this->dbds.db, triplePattern.object, type, subType, result);                    
            if(type == Type::CustomType){
                switch(subType){
                    case Type::String:
                        typeDesc = "http://www.w3.org/2001/XMLSchema#string";
                        break;
                    case Type::Integer:
                        typeDesc = "http://www.w3.org/2001/XMLSchema#integer";
                        break;
                    case Type::Boolean:
                        typeDesc = "http://www.w3.org/2001/XMLSchema#boolean";
                        break;
                    case Type::Decimal:
                        typeDesc = "http://www.w3.org/2001/XMLSchema#decimal";
                        break;
                    case Type::Double:
                        typeDesc = "http://www.w3.org/2001/XMLSchema#double";                                
                        break;
                }
                this->dbds.db->getDictionary().lookup(typeDesc, type2, subType2, typeId);

                filter.type = QueryGraph::Filter::And;                        
                filter.arg1->type = QueryGraph::Filter::Equal;
                filter.arg1->arg1 = new QueryGraph::Filter();
                filter.arg1->arg1->type = QueryGraph::Filter::Builtin_datatype;
                filter.arg1->arg1->arg1 = new QueryGraph::Filter();
                filter.arg1->arg1->arg1->type = QueryGraph::Filter::Variable;
                filter.arg1->arg2 = new QueryGraph::Filter();
                filter.arg1->arg2->type = QueryGraph::Filter::IRI;
                filter.arg1->arg2->id = typeId;
                filter.arg2->arg1 = new QueryGraph::Filter();
                filter.arg2->arg1->type = QueryGraph::Filter::Variable;
                filter.arg2->arg2 = new QueryGraph::Filter();
                filter.arg2->arg2->type = QueryGraph::Filter::Literal;
                filter.arg2->arg2->id = triplePattern.object;                        
            }else{
                filter.arg2->id = triplePattern.object;
                filter.arg2->type = QueryGraph::Filter::Literal;
            }                    
        }else{
            filter.arg2->id = triplePattern.object;                    
            filter.arg2->type = QueryGraph::Filter::IRI;                    
        }
    }    
}

bool FragmentStatisticsPrinter::isRelevantToNode(QueryGraph::Node &triplePattern, Fragment *fragment){
    //Check first whether it is possible to infer it from the predicates
    vector<SPARQLPredicate> predicates;
    this->extractPredicates(triplePattern, predicates);
    unsigned nPosEq = 0, nPosUn = 0;

    cout << "IsrelevantToNode" << endl;
    //Now for every predicate, check if it is explicitly part of the fragment's definition
    for(vector<SPARQLPredicate>::iterator pit = predicates.begin(); pit != predicates.end(); ++pit){
        cout << "Predicate " << *pit << " from pattern " << triplePattern.subject << " " << triplePattern.predicate << " " << triplePattern.object << endl;
        vector<SPARQLPredicate*> *fgPredicates = &fragment->getMinterm().predicates;
        for(unsigned int i = 0; i < fgPredicates->size(); ++i){
            cout << "Comparing equivalency with " << *(fgPredicates->at(i)) << endl;
            if(pit->isEquivalent(*fgPredicates->at(i))){
                if(fragment->getMinterm().flags[i]){
                    ++nPosEq;
                    cout << "Equivalence hit " << endl;
                }else{
                    cout << "Discarded, negation founded" << endl;                    
                    return false;
                }
            }else{
                SPARQLPredicate negated;
                pit->negated(negated);
                if(negated.isEquivalent(*fgPredicates->at(i))){
                    cout << "Discarded, negation founded" << endl;
                    return false;
                }
                ++nPosUn;
            }
        }
    }
    
    if(nPosUn < predicates.size())
        return true;

    cout << "Unable to give a reply without asking the database" << endl;
    //Otherwise run the query and verify    
    string queryPattern = buildQuery(triplePattern);
    
    QueryGraph queryGraph;
    this->extractQueryGraph(*this->databases[fragment->getMinterm().flagsString()], queryPattern, queryGraph);
    if(queryGraph.knownEmpty()){
        cout << "Empty query graph " << endl;
        return false;
    }
    
    return true;

}

string FragmentStatisticsPrinter::buildQuery(QueryGraph::Node &triplePattern){
    string s, p, o;
    Type::ID type;
    unsigned int subtype;
    
    if(triplePattern.constObject){
        if(!lookupById(*this->dbds.db, triplePattern.object, type, subtype, o))
            o = "?o";
    }else{
        o = "?o";
    }
    
    if(triplePattern.constPredicate){
        if(!lookupById(*this->dbds.db, triplePattern.predicate, type, subtype, p))
            p = "?p";
    }else{
        p = "?p";
    }
    
    if(triplePattern.constSubject){
        if(!lookupById(*this->dbds.db, triplePattern.subject, type, subtype, s))
            s = "?s";
    }else{
        s = "?s";
    }
    
    return "SELECT count * WHERE { " + s + " " + p + " " + o + " . }";    
}


bool FragmentStatisticsPrinter::extractQueryGraph(Database &db, string query, QueryGraph &queryGraph){
     SPARQLLexer lexer(query);
     SPARQLParser parser(lexer);
     
     try{
        parser.parse(true);
      } catch (const SPARQLParser::ParserException& e) {
         cerr << "parse error: " << e.message << endl;          
         return false;
      }

      try {
         SemanticAnalysis semana(db);
         semana.transform(parser,queryGraph);
      } catch (const SemanticAnalysis::SemanticException& e) {
         cerr << "semantic error: " << e.message << endl;
         return false;
      }

     return true;
}


void FragmentStatisticsPrinter::outputStatistics(FragmentationScheme &scheme, const char* outputFile, FragmentsAllocationMap &map, PartitioningDescription &pdescription){
    ofstream out;
    FragmentsAllocationMap::iterator amit;
    out.open(outputFile, fstream::out);
    unsigned int totalLoad; 
    
    if(!out.good()){
        cerr << "File " << outputFile << " could not be written " << endl;
    }

    totalLoad = scheme.getTotalLoad();
    out << "Original Database file=" << this->dbds.filename << endl;
    out << "totalLoad=" << totalLoad << endl;
	double totalRatioOverhead = 0.0;
    for(amit = map.begin(); amit != map.end(); ++amit){
        double weight = pdescription.getNodes().find(amit->first)->second.weight;
        unsigned int nodeTotalLoad = 0;
        double nodeTotalRatio = 0.0;
        double ratioOverhead;
        out << "[Node " << amit->first << "]" << endl;
        out << "weight=" << weight << endl;
        out << "expectedload=" << (weight * totalLoad) << endl;
        for(unsigned int i = 0; i < amit->second.size(); ++i){
            struct stat filestatus;
            string fragName = amit->second[i].fragment->getMinterm().flagsString();
            string fragFile = this->fileMaps[fragName];
            out << "[Fragment " << fragName << "]" << endl;
            stat(fragFile.c_str(), &filestatus );
            out << "predicates=" << endl;
            string predicatesText;
            vector<SPARQLPredicate*> *preds = &amit->second[i].fragment->getMinterm().predicates;
            dynamic_bitset<> *flags = &amit->second[i].fragment->getMinterm().flags;
            for(unsigned int k = 0 ; k < preds->size(); ++k){
                bool putSemicolon = k < preds->size() - 1;
                if(flags->test(k)){
                    predicatesText += preds->at(k)->toString();
                }else{
                    SPARQLPredicate negated;
                    preds->at(k)->negated(negated);                    
                    predicatesText += negated.toString();                    
                }                
                
                if(putSemicolon) predicatesText += ";";                
            }
            double relativeLoad = ((double)amit->second[i].fragment->getLoad() * 100 / (double)totalLoad);    
            out << predicatesText << endl;
            out << "filename=" << fragFile << endl;
            out << "filesize=" << filestatus.st_size << endl;
            out << "ntriples=" << amit->second[i].fragment->getSize() << endl;
            out << "frequency=" << amit->second[i].fragment->getFrequency() << endl;
            out << "load=" << amit->second[i].fragment->getLoad() << endl;
            out << "relativeload=" << relativeLoad  << "%" << endl;
            out << "[Fragment]" << endl << endl;
            nodeTotalLoad += amit->second[i].fragment->getLoad();
            nodeTotalRatio += relativeLoad;
        }        
        ratioOverhead = (weight * 100 - nodeTotalRatio);
        totalRatioOverhead += ratioOverhead;
        out << "nodeTotalLoad=" << nodeTotalLoad << endl;
        out << "nodeTotalRadio=" << nodeTotalRatio << endl;
        out << "ratioOverhead=" << ratioOverhead << "%" << endl; 
        out << "[Node]" << endl << endl;
    }
	
    out << "averageOverhead=" << (totalRatioOverhead / (double)map.size()) << endl;	    
    out.close();

}
