/* 
 * File:   stdpartitioner.cpp
 * Author: lgalarra
 *
 * Created on May 14, 2011, 2:37 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 *
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <ios>
#include <string.h>
#include <libgen.h>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <sys/types.h>
#include <sys/stat.h>

#include "rts/database/Database.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"

#include "qload/QueryLoadExtractor.hpp"
#include "qload/QueryLoadInputStream.hpp"
#include "qload/QueryLoadOutputStream.hpp"
#include "qload/QueryLoadRawInputStream.hpp"
#include "qload/QueryLoadDecodedInputStream.hpp"
#include "qload/QueryLoadDecodedOutputStream.hpp"
#include "qload/RandomSampler.hpp"
#include "qload/FrequentElementsAnalyzer.hpp"
#include "qload/QueryLoadNormalizer.hpp"
#include "qload/Utilities.hpp"

#include "partitioning/Utilities.hpp"
#include "partitioning/CommonTypes.hpp"
#include "partitioning/QueryGraphPredicateExtractor.hpp"
#include "partitioning/RDFStandardHorizontalPartitioner.hpp"
#include "partitioning/DependencyGraphBuilder.hpp"
#include "partitioning/PartitioningDescription.hpp"
#include "partitioning/RDFAllocator.hpp"
#include "partitioning/RDFFragmentBuilder.hpp"

#include "drdf/DDatabaseBuilder.hpp"
#include "drdf/CommonTypes.hpp"

#include "FragmentStatisticsPrinter.hpp"


#define MAX_QUERIES 500
#define FREQUENCY_THRESHOLD 0.00000001 


using namespace std;
using namespace qload;
using namespace stdpartitioning;
using namespace drdf;

unsigned int _globalSampleSize;
string _globalRandomTestFile;
unsigned int _globalNHosts;

typedef struct {
    stdpartitioning::DatabaseDescriptor dbds;
    string qloadFile;
    string _outputFile;
    unsigned int nResults;
}JobStruct;

typedef JobStruct Job;

string _globalBinPrefix;
string _globalTemporalPrefix;

// Is the address space too small?
bool smallAddressSpace()
{
   return sizeof(void*)<8;
}

void printFrequentElements(map<string, FrequentElementDescriptor> &fe){
    map<string, FrequentElementDescriptor>::const_iterator it;
    for(it = fe.begin(); it != fe.end(); ++it){
        cout << "[" << it->first << ", " << it->second << "]" << endl;
    }    
}

void printPredicates(vector<stdpartitioning::SPARQLPredicate> &predicatesMap){
    vector<stdpartitioning::SPARQLPredicate>::const_iterator it;
    for(it = predicatesMap.begin(); it != predicatesMap.end(); ++it){
        cout << *it << endl;
    }
}

bool sortByPredId(const stdpartitioning::SPARQLPredicate &a, const stdpartitioning::SPARQLPredicate &b){
	return a.id < b.id;
}


void printPredicatesDependencyGraph(PredicateDependencyGraph &pdg){
    PredicateDependencyGraph::iterator it;
    
    for(it = pdg.begin(); it != pdg.end(); ++it){
        cout << "Pattern id: " << it->first;
        cout << "| " << "Freq: " << it->second.first.freq << ",  " << it->second.first.id << ", ";
        cout << it->second.first.localNode.constObject << it->second.first.localNode.constPredicate << it->second.first.localNode.constSubject;
        cout << ", " << it->second.first.localNode.object << " " << it->second.first.localNode.predicate << " " << it->second.first.localNode.subject;        
        cout << ", Predicates: " << endl;
        printPredicates(it->second.second);
        cout << endl;  
    }
}

void printReversedPredicatesDependencyGraph(ReversePredicateDependencyGraph rpdg){
    ReversePredicateDependencyGraph::iterator rpdgit;
    
    for(rpdgit = rpdg.begin(); rpdgit != rpdg.end(); ++rpdgit){
        cout << "Predicate id: " << rpdgit->first << " " << rpdgit->second.first;
        cout << ", Patterns: [";
        vector<SPARQLPattern>::iterator pit;
        for(pit = rpdgit->second.second.begin(); pit != rpdgit->second.second.end(); ++pit)
            cout << pit->id << " ";
        cout << "]" << endl;
    }
    
}

void printPatternsDependencyGraph(PatternDependencyGraph &ptdg){
    PatternDependencyGraph::iterator pit;
    
    for(pit = ptdg.begin(); pit != ptdg.end(); ++pit){
        cout << "Pattern id: " << pit->first << " Edges: [";
        vector<SPARQLEdge>::iterator eit;
        for(eit = pit->second.begin(); eit != pit->second.end(); ++eit)
            cout << eit->from << "," << eit->to << ": " << eit->weight << ";";        
        cout << "]" << endl;
    }
}

Job mergeJobs(Job *works, int n){
    Job merged;
    //This should be enough (based on the structure of the temporal files)
    merged.dbds = works[0].dbds;
    merged.qloadFile = works[0]._outputFile;    
    FILE *mergedFile = fopen(merged.qloadFile.c_str(), "a");
    merged.nResults = works[0].nResults;    
    fprintf(mergedFile, "\n");
    
    //For validation against other queries
    _globalRandomTestFile = works[0]._outputFile;
    
    for(int i = 1; i < n; ++i){
        //Just merge the files into a single one
        FILE *file = fopen(works[i]._outputFile.c_str(), "r");
        if(file){
            char line[2048]; //I do not expect queries with more than 2K
            while(fgets(line, sizeof(line), file) != NULL){
              int len = strlen(line) - 1;
              if(line[len] == '\n') 
                 line[len] = 0;
              fprintf(mergedFile, "%s\n", line);
           }
       }
        
       merged.nResults += works[i].nResults;
       fclose(file);
    }  
    
    fclose(mergedFile);
    
    return merged;
}

void createDummyFile(string &filename){    
    string dummy = filename + ".n3";
    string dummyDb = filename + ".rdf3x";
    
    cout << "Creating dummy file" << endl;
    FILE *f = fopen(dummy.c_str(), "w");    
    string dummyTriple("<http://dummy.org/dummy_subject> <http://dummy.org/dummy_predicate> <http://dummy.org/dummy_object> .");    
    fprintf(f, "%s", dummyTriple.c_str());    
    fclose(f);
    
    string command = _globalBinPrefix + "/rdf3xload " + dummyDb + " " + dummy;
    cout << "Running command: " << command << endl;
    if(system(command.c_str()) != 0){
        cerr << "Error when creating dummy .rdf3x. This file is needed by the any query client to work" << endl;
    }
    
    remove(dummy.c_str());
}

void* runPartitioning(Job *workDesc){
   //Extract frequent terms
   string outRSamplerNameString(workDesc->qloadFile);
   QueryLoadDecodedInputStream inFrequentAnalysisStm(outRSamplerNameString);
   FrequentElementsAnalyzer<QueryLoadDecodedInputStream> freqAnalysis(*(workDesc->dbds.db), workDesc->nResults, FREQUENCY_THRESHOLD);
   map<string, FrequentElementDescriptor> frequencyMap;
   unsigned int nEntities;
   PredicateDependencyGraph pdg;
   PatternDependencyGraph ptdg;
   ReversePredicateDependencyGraph rpdg;
   DependencyGraphBuilder graphsBuilder;
   
   try{
       inFrequentAnalysisStm.open();
   }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inFrequentAnalysisStm.close();
       return NULL;
   }   
   
   try{
       nEntities = freqAnalysis.getFrequentEntities(inFrequentAnalysisStm, frequencyMap);
   }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inFrequentAnalysisStm.close();
       return NULL;
   }
   
   inFrequentAnalysisStm.close();
   cout << nEntities << " have been marked as frequent" << endl;
   printFrequentElements(frequencyMap);
   
   //Time to normalize and extract predicates
   QueryGraphPredicateExtractor predsExtractor(*(workDesc->dbds.db));
   QueryLoadNormalizer normalizer(*(workDesc->dbds.db), frequencyMap);
   QueryLoadDecodedInputStream inNormalizer(outRSamplerNameString);
   vector<stdpartitioning::SPARQLPredicate> predicatesMap;
   
   try{
       inNormalizer.open();
   }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inNormalizer.close();
       return NULL;
   }   
   
   while(!inNormalizer.eof()){
       string decodedQuery;
       unsigned bytesRead;

       decodedQuery = inNormalizer.get(bytesRead);
       if(decodedQuery.empty())
           continue;
       
       cout << decodedQuery << endl;
       QueryGraph queryGraph;

       try {
           normalizer.normalizeQuery(decodedQuery, queryGraph);
       } catch (const SPARQLParser::ParserException& e) {
           cerr << "Query normalization: The query ###" << decodedQuery << "### could not be normalized. " 
                   << "The database has said: " << e.message << endl;
           continue;
       }                
       
       graphsBuilder.extractDependencies(queryGraph, pdg, ptdg);
       predsExtractor.extractPredicates(queryGraph, predicatesMap, pdg);
   }
   //Get the reversed predicate dependency graph
   graphsBuilder.reversePredicateDependencyGraph(pdg, rpdg);
   sort(predicatesMap.begin(), predicatesMap.end(), sortByPredId);
   printPredicates(predicatesMap);
   inNormalizer.close();
   
   if(predicatesMap.empty()){
       cerr << "No predicates could be extracted from the query load " << endl;
       return NULL;
   }
   
   printPredicatesDependencyGraph(pdg);
   printPatternsDependencyGraph(ptdg);
   printReversedPredicatesDependencyGraph(rpdg);
        
   
   //Now time to generate a partitioning scheme based on the predicates
   RDFStandardHorizontalPartitioner *partitioner;
   
   //Use the sample
   if(workDesc->dbds.smdb != NULL){
       partitioner = new RDFStandardHorizontalPartitioner(*(workDesc->dbds.db), workDesc->dbds.smdb, workDesc->dbds.sfactor);       
   }else{
       partitioner = new RDFStandardHorizontalPartitioner(*(workDesc->dbds.db));       
   }
   
   FragmentationScheme scheme;
   cout << "Running the partitioning algorithm " << endl;
   partitioner->optimalHorizontalFragmentation(predicatesMap, scheme, rpdg);
   
   delete partitioner;
   
   if(scheme.empty()){
       cerr << "The program could generate a valid partitioning scheme " << endl;
       return NULL;
   }

   //Allocate the nodes
   cout << "Allocating the fragments to the available nodes" << endl;
   PartitioningDescription description;
   PartitioningDescription::sampleDescription(description, _globalNHosts);
   OneTopBenefitSelector top;
   RDFAllocator allocator(&top);
   FragmentsAllocationMap allocationMap;
   allocator.assignFragmentsToNodes(scheme, description, ptdg, allocationMap);
      
   //Building the fragments
   RDFFragmentBuilder fbuilder(workDesc->dbds, _globalBinPrefix, _globalTemporalPrefix);
   FragmentFilesAllocationMap fragmentFiles;
   
   //Create the files map
   for(FragmentsAllocationMap::iterator ffait = allocationMap.begin(); ffait != allocationMap.end(); ++ffait){
       string basePrefix = workDesc->dbds.dbBasename();    
       string theDbFile = basePrefix + "_fragment_" + ffait->first;                
       fragmentFiles[ffait->first] = theDbFile;
   }
   
   //Build the distributed database
   string distributedFile = "d" + workDesc->dbds.dbBasename();
   map<string, drdf::Host> hostMappings;
   DDatabaseBuilder ddbuilder(distributedFile.c_str(), _globalTemporalPrefix);
   //Create the dummy rdf data file
   createDummyFile(distributedFile);

   //Information about the assignments
   assert(ddbuilder.start());
   assert(ddbuilder.writePredicatesDefinition(predicatesMap));
   assert(ddbuilder.writeHostMappings(fragmentFiles, hostMappings));   
//   assert(ddbuilder.writeDictionaryEntries(fragmentFiles, hostMappings, frequencyMap, *workDesc->dbds.db));
   assert(ddbuilder.writePredicateHostMappings(allocationMap, hostMappings));
   assert(ddbuilder.writeExactStatisticsSegment(*(workDesc->dbds.db), ptdg, pdg));
   assert(ddbuilder.writeSegmentsMetadata(*(workDesc->dbds.db)));
   
   try{
       fbuilder.buildFragments5(allocationMap);
   }catch(std::exception &e){
       cerr << e.what() << endl;
       return NULL;
   }
   
   assert(ddbuilder.writeAggAndFullyAggFactsSegments(pdg, workDesc->dbds, fragmentFiles, hostMappings));
   ddbuilder.close();
   return workDesc;
}

void* sampleLoad(void *work){
   Job *workDesc = (Job*)work; 
   
   QueryLoadExtractor extractor;
   string tmpStrOutExtractorName = _globalTemporalPrefix + workDesc->dbds.dbBasename() +"_qloadnormalizer_extraction_XXXXXX";
   string tmpStrOutRSamplerName = _globalTemporalPrefix + workDesc->dbds.dbBasename() + "_qloadnormalizer_random_sampling_XXXXXX";
   char *outExtractorName = new char[tmpStrOutExtractorName.length() + 1];
   char *outRSamplerName = new char[tmpStrOutRSamplerName.length() + 1];
   strcpy(outExtractorName, tmpStrOutExtractorName.c_str());
   strcpy(outRSamplerName, tmpStrOutRSamplerName.c_str());

   QueryLoadRawInputStream inExtractor(workDesc->qloadFile);
   int fdOutExtractor = mkstemp(outExtractorName);
   string outExtractorNameString(outExtractorName);   
   QueryLoadOutputStream outExtractor(outExtractorNameString);

   RandomSampler<QueryLoadEncodedInputStream, QueryLoadDecodedOutputStream> rSampler(*(workDesc->dbds.db), MAX_QUERIES); 
   QueryLoadEncodedInputStream inRSampler(outExtractorNameString);
   int fdOutRSampler = mkstemp(outRSamplerName);   
   string outRSamplerNameString(outRSamplerName);
   QueryLoadDecodedOutputStream outRSampler(outRSamplerNameString);
   unsigned int nExtracted = 0; 
   unsigned int nQueries = 0;
           
   try{
      cout << "Opening file " << workDesc->qloadFile << endl;
      inExtractor.open();
      outExtractor.open();
      nExtracted = extractor.extract(inExtractor, outExtractor, bitset<5>(8));
   }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inExtractor.close();
       outExtractor.close();
       return work;
   }

   inExtractor.close();
   outExtractor.close();
   
   if(nExtracted == 0){
       cerr << "No queries could be extracted from the log file " << workDesc->qloadFile << endl;
       return work;
   }
      
   //Now sample them randomly
   try{
       inRSampler.open();
       outRSampler.open();
       nQueries = rSampler.sample(inRSampler, outRSampler, _globalSampleSize);
   }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inRSampler.close();
       outRSampler.close();
       return work;
   }   
   
   inRSampler.close();
   outRSampler.close();
   close(fdOutExtractor);
   close(fdOutRSampler);
   
   cout << nQueries << " have survived the random sampling" << endl;
   if(nQueries == 0){
       cerr << "Not valid queries were found after random sampling" << endl;
       return work;
   }
   
   workDesc->_outputFile = outRSamplerNameString;
   workDesc->nResults = nQueries;   
   return NULL;
}

void* sampleDecodedLoad(void* work){
    Job *workDesc = (Job*)work; 
    string tmpStrOutRSamplerName = _globalTemporalPrefix + workDesc->dbds.dbBasename() + "_qloadnormalizer_random_sampling_XXXXXX";    
    char *outRSamplerName = new char[tmpStrOutRSamplerName.length() + 1];
    strcpy(outRSamplerName, tmpStrOutRSamplerName.c_str());
    int fdOutRSampler = mkstemp(outRSamplerName);
    
    string inRSamplerNameString(workDesc->qloadFile);
    string outRSamplerNameString(outRSamplerName);

    RandomSampler<QueryLoadDecodedInputStream, QueryLoadDecodedOutputStream> rSampler(*(workDesc->dbds.db), MAX_QUERIES); 
    QueryLoadDecodedInputStream inRSampler(inRSamplerNameString);
    QueryLoadDecodedOutputStream outRSampler(outRSamplerNameString);
    unsigned int nQueries = 0;

    //Now sample them randomly
    try{
       inRSampler.open();
       outRSampler.open();
       nQueries = rSampler.sample(inRSampler, outRSampler, _globalSampleSize);
    }catch(ios_base::failure e){
       cerr << "Error: " << e.what() << endl;
       inRSampler.close();
       outRSampler.close();
       return work;
    }   

    inRSampler.close();
    outRSampler.close();
    close(fdOutRSampler);

    cout << nQueries << " have survived the random sampling" << endl;
    if(nQueries == 0){
       cerr << "Not valid queries were found after random sampling" << endl;      
       return work;
    }

    workDesc->_outputFile = outRSamplerNameString;
    workDesc->nResults = nQueries;   
    
    return NULL;    
}


/*
 * This program takes a database and a raw query load file (HTTP logs for SPARQL queries)
 * and outputs a new file with all the queries normalized. 
 */
int main(int argc, char** argv) {
    Database db, sampleDb;
    Database *samplePointer;
    pthread_t *threads;
    Job *works;
    int nThreads;
    string format;
    float sfactor;
    
   // Warn first
   if (smallAddressSpace()){
      cerr << "Warning: Running RDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;
   }
   // Greeting
   cerr << "RDF-3X load-aware partitioner" << endl << "(c) 2011 Luis Galarraga." << endl;

   // Check the arguments
   if (argc < 9) {
      cerr <<  "usage: " << argv[0] << " <database> <input-format> <sampleSize> <numberOfHosts> <temporalLocation> [sample] [sampleFactor] <input_1> [input_2] ... [input_n] " << endl;
      cerr <<  "input-format = dbpedia-logs | raw " << endl;
      cerr <<  "If sample is not required just provide - - for arguments sampleFactor and sample" << endl;
      return 1;      
   }
  
   //Open the database in read only mode
   if (!db.open(argv[1],true)) {
      cerr << "unable to open database " << argv[1] << endl;
      return 1;
   }
   //Create a context
   setDefaultDatabase(&db);
   nThreads = argc - 8;
   threads = new pthread_t[nThreads];
   works = new Job[nThreads];
   format += argv[2];
   
   if(format.compare("dbpedia-logs") != 0 && format.compare("raw") != 0)
       cerr << "The format " << format << " for query load is not supported " << endl;
   
   char *dirName = dirname(argv[0]);
   _globalBinPrefix = string(dirName);
   
   try{
       _globalSampleSize = boost::lexical_cast<unsigned int>(argv[3]);
   }catch(bad_lexical_cast &){
        cerr << "Invalid value for query sample the sample size" << endl;
        exit(1);
   }
   
   if(_globalSampleSize <= 0){
       cerr << "Invalid value for query sample size" << endl;
       exit(1);
   }
   
   try{
       _globalNHosts = boost::lexical_cast<unsigned int>(argv[4]);
   }catch(bad_lexical_cast &){
        cerr << "Invalid value for the number of hosts" << endl;
        exit(1);
   }
   
   //Location for temporal files
   _globalTemporalPrefix = string(argv[5]);
   if(_globalTemporalPrefix[_globalTemporalPrefix.length() - 1] != '/'){
       _globalTemporalPrefix += "/";
   }
   
   if(*argv[6] == '-' && strlen(argv[6]) == 1){
       samplePointer = NULL;
       sfactor = 1.0;
   }else{
      if (!sampleDb.open(argv[6],true)) {
        cerr << "unable to open sample database " << argv[6] << endl;
        return 1;
      }
      samplePointer = &sampleDb;
      try{
          sfactor = boost::lexical_cast<float>(argv[7]);
          if(sfactor <= 0.0 || sfactor > 1.0){
             cerr << "Invalid value for the data sample size" << endl;              
             return 1;
          }
      }catch(bad_lexical_cast &){
          cerr << "Invalid value for the data sample size" << endl;
          exit(1);
      }
   }
           
   for(int i = 0; i < nThreads; i++){
       works[i].dbds.db = &db;
       works[i].dbds.smdb = samplePointer;
       works[i].dbds.sfactor = sfactor;
       works[i].dbds.filename = string(argv[1]);
       works[i].qloadFile = string(argv[i + 8]);
       int rc;
       if(format.compare("dbpedia-logs") == 0)  
            rc = pthread_create(&threads[i], NULL, sampleLoad, (void*)(works + i * sizeof(Job)));
       else if(format.compare("raw") == 0)
            rc = pthread_create(&threads[i], NULL, sampleDecodedLoad, (void*)(works + i * sizeof(Job)));
           
       if(rc){
           cerr << "ERROR; Work for query load" << argv[i + 8] << " could not be scheduled" << endl;
       }else{
           void *result;
           pthread_join(threads[i], &result);
           if(result != NULL){
               cerr << "ERROR: Work for query load" << argv[i + 8] << " could not be scheduled" << endl;               
               exit(1);
           }
       }       
   }
   
   //Now merge the files into a single job. I have not figured out how to parallelize this :( yet
   Job mergedJob = mergeJobs(works, nThreads);
   runPartitioning(&mergedJob);

   delete[] works;
   delete[] threads;
   return 0;
}
