#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/utsname.h>

#include "cts/infra/QueryGraph.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "rts/operator/Operator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DPlanGen.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "server/RDF3xServerMessages.h"
#include "drdf/DSemanticAnalysis.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DPlanSearch.hpp"
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DOperatorCostFunction.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"
#include <sstream>
#include <iomanip>



#ifdef CONFIG_LINEEDITOR
#include "lineeditor/LineInput.hpp"
#endif
using namespace drdf;
//---------------------------------------------------------------------------
// RDF-3X
// (c) 2008 Thomas Neumann. Web site: http://www.mpi-inf.mpg.de/~neumann/rdf3x
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------
using namespace std;
using namespace drdf;

unsigned _queryCount = 0;
vector<SAStatistics> statistics;

//---------------------------------------------------------------------------
bool smallAddressSpace()
   // Is the address space too small?
{
   return sizeof(void*)<8;
}
//---------------------------------------------------------------------------
static string readInput(istream& in)
   // Read a stream into a string
{
   string result;
   while (true) {
      string s;
      getline(in,s);
      result+=s;
      if (!in.good())
         break;
      result+='\n';
   }
   return result;
}
//---------------------------------------------------------------------------
static bool readLine(string& query)
   // Read a single line
{
#ifdef CONFIG_LINEEDITOR
   // Use the lineeditor interface
   static lineeditor::LineInput editHistory(L">");
   return editHistory.readUtf8(query);
#else
   // Default fallback
   cerr << ">"; cerr.flush();
   return getline(cin,query);
#endif
}
//---------------------------------------------------------------------------
static void showHelp()
   // Show internal commands
{
   cout << "Recognized commands:" << endl
        << "help          shows this help" << endl
        << "select ...    runs a SPARQL query" << endl
        << "explain ...   shows the execution plan for a SPARQL query" << endl
        << "exit          exits the query interface" << endl;
}

static void printExecutionStats(){   
    unsigned i = 0;
	cout.precision(0);
    for(vector<SAStatistics>::iterator sit = statistics.begin(); sit != statistics.end(); ++sit, ++i){
       cout << fixed << "Q" << i << "," << (sit->endTime - sit->startTime) << "," << (sit->prepareForExecStartStamp - sit->startTime) << 
               "," << (sit->execEndStamp - sit->prepareForExecStartStamp) << "," << (sit->execEndStamp - sit->startTime) << "," 
               << sit->cardinality << "," << sit->estimatedCommunicationCost << "," << sit->estimatedTotalCommunicationCost  << ","
			   << sit->estimatedResponseTime << "," << sit->relevantHosts <<  endl;
    }
}

static void storeStatisticsISSPO(IndexScan *is, DDatabase &db, string host, Database &odb){
    ostringstream st;
    string table;
    drdf::Host hostObj;
    unsigned card;
    
    if(!db.getFragmentsDefinitionSegment().getHost(host, hostObj))
        return;
        
    if(is->bound1){
        if(is->bound2){
            table = "ASSubjectPredicateH";
            card = odb.getExactStatistics().getCardinality(is->value1->value, is->value2->value, ~0u);
            st << "INSERT INTO " << table << " VALUES(" << is->value1->value << "," << is->value2->value << "," << card << "," << hostObj.id << ");";
        }else if(is->bound3){
            table = "ASSubjectObjectH";
            card = odb.getExactStatistics().getCardinality(is->value1->value, ~0u, is->value3->value);            
            st << "INSERT INTO " << table << " VALUES(" << is->value1->value << "," << is->value3->value << "," << card << "," << hostObj.id << ");";            
        }else{
            table = "FASSubjectH";
            card = odb.getExactStatistics().getCardinality(is->value1->value, ~0u, ~0u);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value1->value << "," << card << "," << hostObj.id << ");";
        }
    }else if(is->bound2){
        if(is->bound1){
            table = "ASSubjectPredicateH";
            card = odb.getExactStatistics().getCardinality(is->value1->value, is->value2->value, ~0u);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value1->value << "," << is->value2->value << "," << card << "," << hostObj.id << ");";            
        }else if(is->bound3){
            table = "ASPredicateObjectH";
            card = odb.getExactStatistics().getCardinality(~0u, is->value2->value, is->value3->value);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value2->value << "," << is->value3->value << "," << card << "," << hostObj.id << ");";            
        }else{
            table = "FASPredicateH";
            card = odb.getExactStatistics().getCardinality(~0u, is->value2->value, ~0u);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value2->value << "," << card << "," << hostObj.id << ");";            
        }
    }else if(is->bound3){
        if(is->bound1){
            table = "ASSubjectObjectH";
            card = odb.getExactStatistics().getCardinality(is->value1->value, ~0u, is->value3->value);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value1->value << "," << is->value3->value << "," << card << "," << hostObj.id << ");";            
        }else if(is->bound2){
            table = "ASPredicateObjectH";
            card = odb.getExactStatistics().getCardinality(~0u, is->value2->value, is->value3->value);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value2->value << "," << is->value3->value << "," << card << "," << hostObj.id << ");";            
        }else{
            table = "FASObjectH";
            card = odb.getExactStatistics().getCardinality(~0u, ~0u, is->value3->value);                        
            st << "INSERT INTO " << table << " VALUES(" << is->value3->value << "," << card << "," << hostObj.id << ");";                        
        }
    }
        
   cout << "Query: " << st.str() << endl;
   db.getDataWrapper()->execute(st.str());
    
}

static void collectStatistics(DOperator *dOperator, DDatabase &db, Database &odb){
    //First, extract the triple patterns from the leaves
    vector<DOperator*> leaves;
    dOperator->getLeaves(leaves);
    
    //Now look for patterns and store them in the database
    for(vector<DOperator*>::iterator dit = leaves.begin(); dit != leaves.end(); ++dit){
        if((*dit)->getType() == DOperator::TIndexScan){
            IndexScan *is = static_cast<IndexScan*>((*dit)->getOperator());
            Database::DataOrder order = is->getOrder();
            
            switch(order){
                case Database::Order_Subject_Predicate_Object:
                    storeStatisticsISSPO(is, db, dOperator->getHost(), odb);
                    break;
                default: 
                    break;
            }
        }
    }

}

//---------------------------------------------------------------------------
static void runQuery(DDatabase& db, Database &dummy, const string& query, const string &searchMethod, bool explain, bool silent)
   // Evaluate a query
{
    
   SAStatistics stats;
   stats.startTime = Timestamp();
   QueryGraph queryGraph;
   {
      // Parse the query
      SPARQLLexer lexer(query);
      SPARQLParser parser(lexer);
      try {
         parser.parse();
      } catch (const SPARQLParser::ParserException& e) {
         cerr << "parse error: " << e.message << endl;
         return;
      }

      // And perform the semantic anaylsis
      try {
         DSemanticAnalysis semana(db);
         semana.transform(parser,queryGraph);
      } catch (const DSemanticAnalysis::SemanticException& e) {
         cerr << "semantic error: " << e.message << endl;
         return;
      }
      if (queryGraph.knownEmpty()) {
         if (explain)
            cerr << "static analysis determined that the query result will be empty" << endl; else
            cout << "<empty result>" << endl;
         return;
      }
   }
   
   // Run the optimizer
   DPlanGen plangen;
   Plan* plan=plangen.translate(db,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return;
   }
   
   // Build a physical plan
   Runtime baseRuntime(dummy, NULL, NULL);
   DRuntime runtime(db, baseRuntime);

      
   PrepExecutionResponse prepResponse;        
   DRDF3xClient<DRuntime> client; //Cid identifying the machine
   DOperator *dOperatorTree;
   DPlanSearch *search;
   
   if(searchMethod.compare("trivial") == 0){
       search = new DPlanTrivialSearch(runtime, queryGraph, plan);
   }else if(searchMethod.compare("ls") == 0){
       search = new DPlanLocalSearch(runtime, queryGraph, plan);
   }else{
       search = new DPlanSASearch(runtime, queryGraph, plan);
   }
   
   dOperatorTree = search->findGoodPlan();   
   stats.estimatedCommunicationCost = DOperatorCostFunction::evaluateCommunicationCost(dOperatorTree, db);
   stats.relevantHosts = dOperatorTree->getRelevantHosts().size();
   stats.estimatedTotalCommunicationCost = DOperatorCostFunction::evaluateTotalCommunicationCost(dOperatorTree, db);
   stats.estimatedResponseTime = DOperatorCostFunction::evaluate(dOperatorTree, db);
   if(explain){       
       client.explainDistributedPlan(dOperatorTree, runtime);
   }else{
       stats.prepareForExecStartStamp = Timestamp();
       client.prepareForExecution(dOperatorTree->getHost(), dOperatorTree, runtime, prepResponse);
       stats.prepareForExecEndStamp = Timestamp();

       cout << "Preparation time " << (stats.prepareForExecEndStamp - stats.prepareForExecStartStamp) << endl;
       if(!prepResponse.success){
           cerr << "The query could not be executed: " << prepResponse.errorMsg << endl;
           exit(1);
       }else{
           double observedCard = 0.0;
           bool firstIteration = true;                          
           while(true){
               drdf::message::ExecResponse execResp;               
               stats.execStartStamp = Timestamp();                                
               client.exec(dOperatorTree->getHost(), prepResponse.reqid, prepResponse.opid, execResp);
               if(execResp.status() == drdf::message::ExecResponse::OK){
                   DResultsPrinter *rp = new DResultsPrinter(runtime, execResp, dOperatorTree, observedCard, queryGraph.getLimit(), silent);
                   if(rp->first()){                       
                       if(firstIteration){
                           stats.execEndStamp = Timestamp();
                           firstIteration = false;
                       }
                       while(rp->next()){}                       
                   }else{
                       if(firstIteration){
                           stats.execEndStamp = Timestamp();
                           firstIteration = false;
                       }                       
                   }

                   observedCard = rp->getObservedOutputCardinality();
                   delete rp;
                   if(!execResp.has_hasmoreresults() || !execResp.hasmoreresults() || observedCard >= queryGraph.getLimit())
                       break;                                      
               }else{
                   cerr << "There was a problem when executing the query: " << execResp.errormsg() << endl;
                   break;
               }               
           }

           stats.cardinality = observedCard;
       }

       stats.endTime = Timestamp();
   }
   
   statistics.push_back(stats);   
   collectStatistics(dOperatorTree, db, dummy);
   
   delete dOperatorTree;
   delete search;
}
//---------------------------------------------------------------------------
int main(int argc,char* argv[])
{
    string searchMethod = "trivial";
    bool silent = false;
    bool justPrint = false;
    
    // Warn first
    if (smallAddressSpace())
       cerr << "Warning: Running DRDF-3X on a 32 bit system is not supported and will fail for large data sets. Please use a 64 bit system instead!" << endl;

    // Greeting
    cerr << "DRDF-3X query interface" << endl
         << "(c) 2011 Luis Galárraga, based on RDF-3X query interface by 2008 Thomas Neumann." << endl;

    // Check the arguments
    if (argc < 4) {
      cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method] [silent]" << endl;
      cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
      cerr << "sa = simulated annealing; ls = local search" << endl;
      return 1;
    }

    if(argc >= 4){
        searchMethod = string(argv[4]);
        if(searchMethod.compare("ls") != 0 && searchMethod.compare("sa") != 0 && searchMethod.compare("trivial") != 0){
            cerr << "usage: " << argv[0] << " <database> <originalDatabase> <queryfile> [search_method] [silent]" << endl;
            cerr << "[search_method] = sa|ls If not provided, no search is applied " << endl;
            cerr << "sa = simulated annealing; ls = local search" << endl;
            return 1;
        }            
    }
    
    if(argc >= 6){
        string silentString = string(argv[5]);
        if(silentString.compare("yes") == 0){
            silent = true;
        }
    }
    
    if(argc >= 7){
        string justPrintPlan = string(argv[6]);
        if(justPrintPlan.compare("yes") == 0){
            justPrint = true;
        }
    }

    //Open the dummy database
    Database db;
    if(!db.open(argv[2], true)){
       cerr << "An original RDF-3x database has not been provided " << endl;
       return 1;
    }

    // Open the database
    DDatabase ddb;
    if (!ddb.open(argv[1], &db.getDictionary(), false)) {
      cerr << "unable to open distributed database " << argv[1] << endl;
      return 1;
    }

    // Execute a single query?
    if (argc >= 3) {
      ifstream in(argv[3]);
      if (!in.is_open()) {
         cerr << "unable to open " << argv[3] << endl;
         return 1;
      }
      while(!in.eof()){
        char queryStr[8192];
        in.getline(queryStr, 8192);
        string query = string(queryStr);
        if(query.empty()) continue;
        if (query.substr(0,8)=="explain ") {
            runQuery(ddb,db,query.substr(8),searchMethod,true,silent);
        } else {
            runQuery(ddb,db,query,searchMethod,false,silent);
        }          
      }        
      printExecutionStats();      
    } else {
      // No, accept user input
      cerr << "Enter 'help' for instructions" << endl;
      while (true) {
         string query;
         if (!readLine(query))
            break;
         if (query=="") continue;

         if ((query=="quit")||(query=="exit")) {
            break;
         } else if (query=="help") {
            showHelp();
         } else if (query.substr(0,8)=="explain ") {
            runQuery(ddb,db,query.substr(8),searchMethod,true,silent);
         } else {
            runQuery(ddb,db,query,searchMethod,false,silent);
         }
         cout.flush();
      }
    }
}
//---------------------------------------------------------------------------
