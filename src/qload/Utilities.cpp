/* 
 * File:   Utilities.cpp
 * Author: luis
 * Created on May 26, 2011, 1:46 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */
#include "qload/Utilities.hpp"
#include <stdio.h>
#include <ctype.h>

static inline void next(string text, char &c, char &c1, char &c2, unsigned int &index){
    if( c == EOF )
       return;

    c = c1;
    c1 = c2;
    if(index >= text.size())
        c2 = EOF;
    else{
        index++;
        c2 = text[index];
    }
}


string urldecode(string text){
    unsigned int index = 0;
    char c, c1, c2;
    string output = "";

    if(text.size() < 3)	
        return text;

    c = text[0];
    c1 = text[1];
    c2 = text[2];
    index = 2;
    while(c != EOF){
        if(c == '%'){
            if( isxdigit(c1) && isxdigit(c2) ){
                /* Valid %HH sequence found. */
                c1 = tolower(c1);
                c2 = tolower(c2);
                if( c1 <= '9' )
                        c1 = c1 - '0';
                else
                        c1 = c1 - 'a' + 10;
                if( c2 <= '9' )
                        c2 = c2 - '0';
                else
                        c2 = c2 - 'a' + 10;

                char tmp = 16 * c1 + c2;
                output += tmp;

                next(text, c, c1, c2, index);
                next(text, c, c1, c2, index);
                next(text, c, c1, c2, index);

            } else {
                /* Invalid or incomplete sequence. */
                output += '%';
                next(text, c, c1, c2, index);
            }
        } else if( c == '+' ){
           output += ' ';
           next(text, c, c1, c2, index);
        } else {
           output += c;
           next(text, c, c1, c2, index);
        }
    }

    return output;
}

static string& ltrim(std::string &s, char c = ' '){
    size_t pos = 0;
    while(pos <= s.size() && s[pos] == c){
        ++pos;    
    }
    
    s.erase(0, pos);
    
    return s;
}

static string& rtrim(std::string &s, char c = ' '){
    int pos = s.size();
    while(pos >= 0 && s[pos - 1] == c){
        --pos;    
    }
    
    s.erase(pos, s.size() - pos);
    
    return s;
}

void trim(std::string &s, char c= ' '){
    ltrim(rtrim(s, c), c); 
}