/* 
 * File:   QueryLoadInputStream.cpp
 * Author: lgalarra
 * 
 * Created on May 13, 2011, 1:59 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "qload/QueryLoadInputStream.hpp"
#include "qload/Utilities.hpp"
#include <string>
#include <algorithm>


using namespace std;
using namespace qload;

QueryLoadInputStream::QueryLoadInputStream(string filename): filename(filename) {}

QueryLoadInputStream::~QueryLoadInputStream() {}

bool QueryLoadInputStream::open(){
    this->stream.open(this->filename.c_str(), ifstream::in);
    return this->stream.good();
}

bool QueryLoadInputStream::close(){
    this->stream.close();
    return !this->stream.fail();
}


string QueryLoadInputStream::get(unsigned int &charsRead){
    char next = '\0';
    string result = "";
    charsRead = 0;
    
    while(this->stream.good() && next != '\n'){
        this->stream.get(next);            
        if(next != '\n' && !this->stream.eof()){
            result += next;
            charsRead++;
        }
    }
    
    //Time to clean the string
    trim(result, ' ');
    trim(result, '\n');
    return result;
}

bool QueryLoadInputStream::good(){
    return this->stream.good();
}

bool QueryLoadInputStream::eof(){
    return this->stream.eof();
}

QueryLoadInputStream& QueryLoadInputStream::operator>> (std::string&s){
    unsigned int charsRead = 0;
    s = this->get(charsRead);    
    return (*this);
}