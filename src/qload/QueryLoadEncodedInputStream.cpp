/* 
 * File:   QueryLoadEncodedInputStream.cpp
 * Author: luis
 * Created on June 15, 2011, 6:40 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "qload/QueryLoadEncodedInputStream.hpp"
#include "qload/Utilities.hpp"

using namespace qload;
using namespace std;

QueryLoadEncodedInputStream::QueryLoadEncodedInputStream(string filename): QueryLoadInputStream(filename){}

QueryLoadEncodedInputStream::~QueryLoadEncodedInputStream() {}

string QueryLoadEncodedInputStream::get(unsigned int &charsRead){
    string result = QueryLoadInputStream::get(charsRead);
    return urldecode(result);
}