#include "qload/Sampler.hpp"

//---------------------------------------------------------------------------
// RDF-3X
// (c) 2011 Luis Galarraga
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------

using namespace std;
using namespace qload;

template <class InputStream, class OutputStream>
Sampler<InputStream, OutputStream>::Sampler(){}

template <class InputStream, class OutputStream>
Sampler<InputStream, OutputStream>::~Sampler(){}