/* 
 * File:   QueryLoadNormalizer.cpp
 * Author: lgalarra
 * 
 * Created on May 14, 2011, 2:44 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "qload/QueryLoadNormalizer.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include <bitset>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

using namespace qload;
using namespace std;

QueryLoadNormalizer::QueryLoadNormalizer(Database &db, map<string, FrequentElementDescriptor> &frequentElements): db(db), frequentElements(frequentElements) {}

QueryLoadNormalizer::~QueryLoadNormalizer() {}

void QueryLoadNormalizer::normalizeQuery(string query, QueryGraph &output){
    //Time to parse the query
    SPARQLLexer lexer(query);
    SPARQLParser parser(lexer);
    parser.parse(true);
    
    //Now normalize every pattern according to the rules
    vector<SPARQLParser::Pattern> patterns;
    this->extractAllPatterns(parser.getPatterns(), patterns);    
    vector<SPARQLParser::Pattern>::iterator pit;
    //This is a non-tight but safe upper bound for the number of variables
    int safeId = patterns.size() * 3;
    for(pit = patterns.begin(); pit != patterns.end(); ++pit){
        if(!allVariables(*pit)){
           safeId += this->normalizePattern(*pit, safeId);
        }
    }
    
    vector<SPARQLParser::PatternGroup*> groups;
    vector<SPARQLParser::PatternGroup*>::iterator pgit;    
    this->extractPatternGroups(parser.getPatternGroup(), groups);
    for(pgit = groups.begin(); pgit != groups.end(); ++pgit)
        this->normalizeFilters(*pgit);
    
        
    // And perform the semantic analysis
    SemanticAnalysis semana(this->db);
    semana.transform(parser, output);
}

unsigned int QueryLoadNormalizer::normalizePattern(SPARQLParser::Pattern &in, unsigned int safeId){
    bitset<3> flags(0);
    unsigned int nNormalized = 0;
    
    if(in.predicate.type == SPARQLParser::Element::Variable){
        flags.set(1, true);
    }
    
    if(in.object.type == SPARQLParser::Element::Variable){
        flags.set(0, true);
        if(flags.test(1)){
            return nNormalized;
        }
    }
    
    if(in.subject.type == SPARQLParser::Element::Variable){
        flags.set(2, true);
        if(flags.test(0) && flags.test(1)){
            return nNormalized;
        }        
    }
    
    if(!flags.test(2) && !flags.test(1) && !this->isFrequentEntity(in.subject)){  //Subject and predicate
        //Normalize subject
        in.subject.type = SPARQLParser::Element::Variable;
        in.subject.id = safeId;
        char str[21];
        sprintf(str, "%u", in.subject.id);
        in.subject.value = this->randomVariableName() + string(str);
        ++nNormalized;
        ++safeId;
    }
    
    if(!flags.test(0) && !flags.test(1) && !this->isFrequentEntity(in.object)){ //Object and predicate
        //Normalize object
        in.object.type = SPARQLParser::Element::Variable;
        in.object.id = safeId;
        char str[21];
        sprintf(str, "%u", in.object.id);
        in.object.value = this->randomVariableName() + string(str);
        ++nNormalized;        
        ++safeId;
    }
    
    if(!flags.test(2) && !flags.test(0) && !this->isFrequentEntity(in.object)){ //Subject and object
        //Normalize object
        in.object.type = SPARQLParser::Element::Variable;
        in.object.id = safeId;
        char str[21];
        sprintf(str, "%u", in.subject.id);
        in.object.value = this->randomVariableName() + string(str);
        ++nNormalized;        
        ++safeId;
    }
            
    return nNormalized;
} 

void QueryLoadNormalizer::extractAllPatterns(const SPARQLParser::PatternGroup &patternGroup, 
        vector<SPARQLParser::Pattern> &output){
    // So we have reached a leave
    if(patternGroup.unions.empty()){
        vector<SPARQLParser::Pattern>::const_iterator pit;
        for(pit = patternGroup.patterns.begin(); pit != patternGroup.patterns.end(); ++pit){
            output.push_back(*pit);
        }
    }else{
        //Recursively get the patterns
        vector<vector<SPARQLParser::PatternGroup> >::const_iterator unionIterator;
        for(unionIterator = patternGroup.unions.begin(); unionIterator != patternGroup.unions.end(); ++unionIterator){
            vector<SPARQLParser::PatternGroup>::const_iterator piterator;
            for(piterator = unionIterator->begin(); piterator != unionIterator->end(); ++piterator){
                extractAllPatterns(*piterator, output);
            }
        }
    }
}

void QueryLoadNormalizer::extractPatternGroups(SPARQLParser::PatternGroup *patternGroup, 
        vector<SPARQLParser::PatternGroup*> &output){    
    // So we have reached a leave
    if(patternGroup->unions.empty()){
        output.push_back(patternGroup);
    }else{
        //Recursively get the patterns
        vector<vector<SPARQLParser::PatternGroup> >::iterator uit;
        vector<SPARQLParser::PatternGroup>::iterator uit2;
        for(uit = patternGroup->unions.begin(); uit != patternGroup->unions.end(); ++uit){
            for(uit2 = uit->begin(); uit2 != uit->end(); ++uit2)
                extractPatternGroups(&(*uit2), output);
        }
    }
}

void QueryLoadNormalizer::normalizeFilters(SPARQLParser::PatternGroup *patternGroup){
    unsigned int i = 0;
    
    while(i < patternGroup->filters.size()){
        bool cuc;
        containsUnfrequentConstant(&patternGroup->filters[i], cuc);
        if(cuc){
            patternGroup->filters.erase(patternGroup->filters.begin() + i);
            --i;
        }        
        ++i;
    }
}

void QueryLoadNormalizer::containsUnfrequentConstant(SPARQLParser::Filter *filter, bool &result){
    if(filter == NULL){
        result = false;
    }else{
        if(filter->type == SPARQLParser::Filter::IRI || filter->type == SPARQLParser::Filter::Literal){
            SPARQLParser::Element element;
            element.value = filter->value;
            if(filter->type == SPARQLParser::Filter::IRI)
                element.type = SPARQLParser::Element::IRI;
            
            if(filter->type == SPARQLParser::Filter::Literal){
                element.type = SPARQLParser::Element::Literal;                           
                element.subTypeValue = filter->valueType;
                Type::ID type; unsigned int subType, id;
                this->db.getDictionary().lookup(element.value, type, subType, id);
                if(type >= Type::CustomType) 
                    element.subType = SPARQLParser::Element::CustomType;
                else if(type == Type::CustomLanguage)
                    element.subType = SPARQLParser::Element::CustomLanguage;
                else
                    element.subType = SPARQLParser::Element::None;
            }
            
            result = !this->isFrequentEntity(element);
        }else if(filter->type == SPARQLParser::Filter::Variable){
            result = false;
        }else{
            bool r1, r2, r3; 
            containsUnfrequentConstant(filter->arg1, r1);
            containsUnfrequentConstant(filter->arg2, r2);
            containsUnfrequentConstant(filter->arg3, r3);
            result = r1 || r2 || r3;
        }            
    }
}

bool QueryLoadNormalizer::isFrequentEntity(SPARQLParser::Element &element){
    if(this->frequentElements.find(element.value) != this->frequentElements.end()){
        //Now verify whether the types match
        if(element.type == this->frequentElements[element.value].second.type){
            if(element.type == SPARQLParser::Element::IRI){
                //URIs have to just be equal
                return true;
            }else if(element.type == SPARQLParser::Element::Literal){                                
                //If they are not language or typed literals, they have to be just equal
                if(element.subType == SPARQLParser::Element::None 
                        && element.subType == this->frequentElements[element.value].second.subType){
                    return true;
                }else{
                    //Otherwise their language tag OR datatype MUST match
                    return element.subType == this->frequentElements[element.value].second.subType 
                        && element.subTypeValue == this->frequentElements[element.value].second.subTypeValue;
                }
            }
        }
    }
    
    return false;
}

bool QueryLoadNormalizer::allVariables(SPARQLParser::Pattern &pattern){
    return pattern.object.type == SPARQLParser::Element::Variable && 
            pattern.predicate.type == SPARQLParser::Element::Variable &&
            pattern.subject.type == SPARQLParser::Element::Variable;
}

string QueryLoadNormalizer::randomVariableName(){
    srand(time(NULL));
    string result;
    //Be sure the first is a letter
    result += (char)(rand() % 26 + 97); 
    for(int i = 0; i < 4; ++i){
        if(rand() % 2 == 0)
            result += (char)(rand() % 26 + 65);
        else
            result += (char)(rand() % 26 + 97);
    }
    
    return result;
}
