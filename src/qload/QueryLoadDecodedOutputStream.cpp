/* 
 * File:   QueryLoadDecodedOutputStream.cpp
 * Author: luis
 * Created on June 15, 2011, 6:35 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "qload/QueryLoadDecodedOutputStream.hpp"

using namespace std;
using namespace qload;

QueryLoadDecodedOutputStream::QueryLoadDecodedOutputStream(string filename): QueryLoadOutputStream(filename){}

QueryLoadDecodedOutputStream::~QueryLoadDecodedOutputStream() {}

bool QueryLoadDecodedOutputStream::put(const std::string &s){
    if(this->stream.good()){
        this->stream << s.c_str() << endl << endl;
        return true;
    }else{
        return false;
    }        
}