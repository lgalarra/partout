/* 
 * File:   FrequentElementsAnalyzer.cpp
 * Author: lgalarra
 * 
 * Created on May 14, 2011, 3:10 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "qload/FrequentElementsAnalyzer.hpp"
#include "qload/Utilities.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include <iostream>

using namespace std;
using namespace qload;
        
FrequentElementsAnalyzer::FrequentElementsAnalyzer(Database &db, unsigned int nQueries, double threshold): db(db), nQueries(nQueries), threshold(threshold){}

FrequentElementsAnalyzer::~FrequentElementsAnalyzer(){}

int FrequentElementsAnalyzer::getFrequentEntities(QueryLoadInputStream &in, map<string, FrequentElementDescriptor> &output){
    unsigned int queriesLeft = this->nQueries;
    unsigned int nElements = 0;
    map<string, pair<unsigned int, SPARQLParser::Element> > temporal;
    
    if(!in.good()) throw ios_base::failure("The input stream could not be opened for elements frequency analysis");
    
    while(!in.eof() && queriesLeft > 0){
        string query, decodedQuery;
        in >> query;
        decodedQuery = urldecode(query);        
        SPARQLLexer lexer(decodedQuery);
        SPARQLParser parser(lexer);
        map<string, pair<unsigned int, SPARQLParser::Element> > extractedElements;
        map<string, pair<unsigned int, SPARQLParser::Element> >::iterator it;
        
        try {
           parser.parse(true);
        } catch (const SPARQLParser::ParserException& e) {
           cerr << "Semantic error " << e.message << " for query: " << decodedQuery << endl;
           cerr << "This might affect the accuracy of the frequency counts" << endl;
           continue;
        }                
        
        //Extract a vector of distinct elements
        this->extractElements(parser.getPatterns(), extractedElements);
        //Update the map with the elements found
        for(it = extractedElements.begin(); it != extractedElements.end(); ++it){
            if(temporal.find(it->first) == temporal.end()){
                //Here we could condition to the maximum frequency this element can obtain based 
                //on the left queries
                pair<unsigned int, SPARQLParser::Element> pair;
                pair.first = 1;
                pair.second = it->second.second;
                temporal[it->first] = pair;                
            }else{
                temporal[it->first].first = temporal[it->first].first + 1;
            }
        }
        
        queriesLeft--;
    }
    
    //Once ready, time to calculate the thresholds
    map<string, pair<unsigned int, SPARQLParser::Element> >::iterator tempIt;
    for(tempIt = temporal.begin(); tempIt != temporal.end(); ++tempIt){
        cout << "Element " << tempIt->first << " has appeared " << tempIt->second.first << "times " << " his description is " << tempIt->second.second << endl;
        double entryThreshold = (double)tempIt->second.first / (double)this->nQueries;
        // If the element is frequent enough, keep it
        if(entryThreshold >= this->threshold){
            output[tempIt->first].first = entryThreshold;
            output[tempIt->first].second = tempIt->second.second;
            nElements++;
        }
    }
    
    return nElements;
}

int FrequentElementsAnalyzer::extractElements(const SPARQLParser::PatternGroup &patterns, map<string, pair<unsigned int, SPARQLParser::Element> > &output){
    vector<SPARQLParser::Pattern>::const_iterator pit;
    int nEntities = 0;    
    if(patterns.unions.empty()){
        for(pit = patterns.patterns.begin(); pit != patterns.patterns.end(); ++pit){
            //Check whether every element is a literal or not
            if(pit->subject.type != SPARQLParser::Element::Variable){
                if(output.find(pit->subject.value) == output.end()){
                    cout << "Element " << pit->subject << " has been added (found in subject) " << endl; 
                    nEntities++;
                }
            }

            if(pit->predicate.type != SPARQLParser::Element::Variable){
                if(output.find(pit->predicate.value) == output.end()){
                    output[pit->predicate.value].second = pit->predicate;
                    cout << "Element " << pit->predicate << " has been added (found in predicate)" << endl;                 
                    nEntities++;
                }
            }

            if(pit->object.type != SPARQLParser::Element::Variable){
                if(output.find(pit->object.value) == output.end()){
                    output[pit->object.value].second = pit->object;
                    cout << "Element " << pit->object << " has been added (found in object)" << endl;                 
                    nEntities++;
                }
            }
        }
    }else{
        vector<vector<SPARQLParser::PatternGroup> >::const_iterator uit;
        for(uit = patterns.unions.begin(); uit != patterns.unions.end(); ++uit){
            vector<SPARQLParser::PatternGroup>::const_iterator ipit;
            for(ipit = uit->begin(); ipit != uit->end(); ++ipit){
                nEntities += extractElements(*ipit, output);
            }
        }        
    }
    
    return nEntities;
    
}