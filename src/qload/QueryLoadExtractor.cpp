/* 
 * File:   QueryLoadExtractor.cpp
 * Author: lgalarra
 * 
 * Created on May 13, 2011, 4:40 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "qload/QueryLoadExtractor.hpp"

using namespace std;
using namespace qload;

QueryLoadExtractor::QueryLoadExtractor() {}

QueryLoadExtractor::~QueryLoadExtractor() {}

int QueryLoadExtractor::extract(QueryLoadRawInputStream &in, QueryLoadOutputStream &out, bitset<5> features){
    if(!in.good()) throw std::ios_base::failure("The input stream could not be read.");
    if(!out.good()) throw std::ios_base::failure("The output stream cannot be written.");
    
    unsigned int count = 0;
    
    while(!in.eof()){
        unsigned int charsRead;
        bool success;
        struct QueryLoadRawInputStream::SPARQLLogQuery query = in.getQuery(charsRead, success);
        if(success){
            if(features[0])
                out << "[" << query.date << "]" << " ";
            if(features[1])
                out << "\"" << query.type << "\" ";
            if(features[2])
                out << query.graphUri << " ";
            if(features[3])
                out << query.query << " ";            
            if(features[4])
                out << query.format << " ";        

            out << "\n";
            count++;
        }
    }
    
    return count;
}
