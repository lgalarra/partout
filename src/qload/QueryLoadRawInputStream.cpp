//---------------------------------------------------------------------------
// DRDF-3X
// (c) 2011 Luis Galarraga
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------

#include "qload/QueryLoadRawInputStream.hpp"
#include <regex.h>

using namespace qload;
using namespace std;

QueryLoadRawInputStream::QueryLoadRawInputStream(string filename): QueryLoadInputStream(filename){}

QueryLoadRawInputStream::~QueryLoadRawInputStream(){}

QueryLoadRawInputStream::SPARQLLogQuery QueryLoadRawInputStream::getQuery(unsigned int &charsRead, bool &success){
    string entry;    
    entry = this->get(charsRead); 
    SPARQLLogQuery query;
    regex_t matcher;
    int rc;
    const char* pattern = "([0-9a-f]{32})\\s\\[([0-9]{2}/[a-zA-Z]{3}/[0-9]{4}\\s[0-9]{2}:[0-9]{2}:[0-9]{2}\\s-[0-9]{4})\\]\\s\"([a-zA-Z])\"\\s\"/sparql\\?default-graph-uri=(.*)&query=(.*)&format=(.*)\"";
    regmatch_t pmatch[7]; //Elements we are interested in
    success = false;
    
    //Now time to parse it
    if (regcomp(&matcher, pattern, REG_EXTENDED)) {
        cerr << "QueryLoadRawInputStream: Regular expression pattern for log entry does not compile" << endl;
        return query;
    }
        
    rc = regexec(&matcher, entry.c_str(), 7, pmatch, REG_EXTENDED);
    if(!rc){
        //Extract date string
        query.date = entry.substr(pmatch[2].rm_so, pmatch[2].rm_eo - pmatch[2].rm_so);
        query.type = entry[pmatch[3].rm_so];
        query.graphUri = entry.substr(pmatch[4].rm_so, pmatch[4].rm_eo - pmatch[4].rm_so);
        query.query = fixSelectClause(entry.substr(pmatch[5].rm_so, pmatch[5].rm_eo - pmatch[5].rm_so));
        query.format = entry.substr(pmatch[6].rm_so, pmatch[6].rm_eo - pmatch[6].rm_so);
        success = true;
    }
    
    regfree(&matcher);

    return query;
}

QueryLoadRawInputStream& QueryLoadRawInputStream::operator>>(QueryLoadRawInputStream::SPARQLLogQuery &query){
    unsigned int charsRead = 0;
    bool success;
    query = this->getQuery(charsRead, success);    
    return (*this);
}

string QueryLoadRawInputStream::fixSelectClause(string rawQuery){
    const char* pattern = "SELECT(.+)WHERE";
    const char* pattern2 = "%0APREFIX%20%3A%20%3Chttp%3A//dbpedia.org/resource/%3E%0A";
    regex_t matcher;    
    regmatch_t pmatch[2], pmatch2[1]; //Elements we are interested in
    string result = rawQuery;
    int rc;
    size_t commaPosition;
    
    //Now time to parse it
    if (regcomp(&matcher, pattern, REG_EXTENDED | REG_ICASE)) {
        cerr << "QueryLoadRawInputStream: Regular expression pattern for fixing select clause does not compile" << endl;
        return rawQuery;
    }
    
    rc = regexec(&matcher, result.c_str(), 2, pmatch, REG_EXTENDED | REG_ICASE);
    if(!rc){
        string selectClause = rawQuery.substr(pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so);
        commaPosition = 0;
        //Remove possible commas
        while(commaPosition != string::npos){
            commaPosition = selectClause.find("%2C");
            if(commaPosition != string::npos)
                selectClause.replace(commaPosition, 3, " ");
            
        }
                
        result.replace(pmatch[1].rm_so, pmatch[1].rm_eo - pmatch[1].rm_so, selectClause);
    }
    
    //Now time to match possible : prefixes
    if (regcomp(&matcher, pattern2, REG_EXTENDED | REG_ICASE)) {
        cerr << "QueryLoadRawInputStream: Regular expression pattern for fixing select clause does not compile" << endl;
        return result;
    }    
    
    rc = regexec(&matcher, result.c_str(), 1, pmatch2, REG_EXTENDED | REG_ICASE);    
    if(!rc){
        result.replace(pmatch2[0].rm_so, pmatch2[0].rm_eo - pmatch2[0].rm_so, "%0A");            
    }
    
    regfree(&matcher);        
    return result;
    
}

