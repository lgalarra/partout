/** 
 * File:   QueryLoadOutputStream.cpp
 * Author: Luis Galarraga
 * Created on May 11, 2011, 1:28 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 **/

#include "qload/QueryLoadOutputStream.hpp"

using namespace qload;

QueryLoadOutputStream::QueryLoadOutputStream(string filename): filename(filename) {
}

QueryLoadOutputStream::~QueryLoadOutputStream() {
}

QueryLoadOutputStream& QueryLoadOutputStream::operator<<(const string &s){    
    if(this->stream.good()){
        this->put(s);
    }        
    return (*this);
}

bool QueryLoadOutputStream::open(){
    this->stream.open(this->filename.c_str());
    return this->stream.good();    
}

bool QueryLoadOutputStream::close(){
    this->stream.close();
    return !this->stream.fail();
}

bool QueryLoadOutputStream::put(const std::string &s){
    if(this->stream.good()){
        this->stream << s.c_str();
        return true;
    }else{
        return false;
    }        
}

bool QueryLoadOutputStream::good(){
    return this->stream.good();
}