//---------------------------------------------------------------------------
// DRDF-3X
// (c) 2011 Luis Galarraga
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "qload/RandomSampler.hpp"
#include "qload/Utilities.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "cts/infra/QueryGraph.hpp"


using namespace std;
using namespace qload;


RandomSampler::RandomSampler(Database &db, unsigned int maxSize): db(db), maxSize(maxSize){}

RandomSampler::~RandomSampler(){}

/**
 * Performs a random sample of the specified size from the provided query load 
 * and locates it in out.
 * 
 * @param QueryLoadInputStream in
 * @param int SampleSize
 * @param QueryLoad
 */

int RandomSampler::sample(QueryLoadInputStream &in, QueryLoadOutputStream &out, unsigned int sampleSize){
    string *elements;
    unsigned int pivot, actualSampleSize, counter, probForNext, initialSample = 0;
    
    if(!in.good()) throw std::ios_base::failure("The input stream could not be read.");
    if(!out.good()) throw std::ios_base::failure("The output stream cannot be written.");
    
    if(sampleSize > this->maxSize)
        actualSampleSize = this->maxSize;
    else
        actualSampleSize = sampleSize;

    elements = new string[actualSampleSize];
    
    //Take the first actualSampleSize elements
    while(initialSample < actualSampleSize && !in.eof()){
        string query, decodedQuery;
        in >> query;
        decodedQuery = urldecode(query);
        trim(decodedQuery, ' ');
        trim(decodedQuery, '\n');
        if(!query.empty() && mightHaveResults(decodedQuery)){
            elements[initialSample] = query;
            ++initialSample;            
        }  
    }
    
    actualSampleSize = initialSample;
    srand(time(NULL));
    counter = actualSampleSize;

    while(counter < this->maxSize && !in.eof()){
        string nextQuery, decodedQuery;
        in >> nextQuery;
        decodedQuery = urldecode(nextQuery);
        trim(decodedQuery, ' ');
        trim(decodedQuery, '\n');
        
        if(!nextQuery.empty() && mightHaveResults(decodedQuery)){
            probForNext = rand() % (counter + 1);
            //Now decide if we take it or not
            if(probForNext < actualSampleSize){
                pivot = rand() % actualSampleSize;
                elements[pivot] = nextQuery;
            }

            counter++;
        }
    }
    
    for(unsigned int i = 0; i < actualSampleSize; i++){
        out << elements[i] << "\n";
    }
        
    delete[] elements;   
    return actualSampleSize;
    
}

bool RandomSampler::mightHaveResults(string query){
    SPARQLLexer lexer(query);
    SPARQLParser parser(lexer);
    QueryGraph queryGraph;
    try {
       parser.parse(true);
    } catch (const SPARQLParser::ParserException& e) {
       return false;
    }                

    // And perform the semantic analysis
    try{
         SemanticAnalysis semana(this->db);
         semana.transform(parser,queryGraph);
    }catch (const SemanticAnalysis::SemanticException& e) {
        return false;        
    }

    if (queryGraph.knownEmpty()) {
        return false;
    }
    
    return true;
}