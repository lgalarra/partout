/* 
 * File:   QueryLoadDecodedInputStream.cpp
 * Author: lgalarra
 * 
 * Created on May 13, 2011, 1:59 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "qload/QueryLoadDecodedInputStream.hpp"
#include "qload/Utilities.hpp"
#include <string>
#include <algorithm>


using namespace std;
using namespace qload;

QueryLoadDecodedInputStream::QueryLoadDecodedInputStream(string filename): QueryLoadInputStream(filename) {}

QueryLoadDecodedInputStream::~QueryLoadDecodedInputStream() {}

string QueryLoadDecodedInputStream::get(unsigned int &charsRead){
    char next = '\0';
    char previous = '\0';
    unsigned int nLineEnds = 0;
    string result = "";
    charsRead = 0;
    
    while(this->stream.good() && nLineEnds < 2){
        this->stream.get(next);
        if(next == '\n')
            nLineEnds++;

        result += next;
        charsRead++;

        if(previous == '\n' && next != '\n')
            nLineEnds = 0;

        previous = next;
    }
    //Time to clean the string
    trim(result, '\n');
    return result;
}
