/* 
 * File:   PredicateExtractor.cpp
 * Author: luis
 * Created on May 21, 2011, 9:43 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include <bitset>
#include "partitioning/QueryGraphPredicateExtractor.hpp"
#include "qload/Utilities.hpp"
#include "partitioning/CommonTypes.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/runtime/DifferentialIndex.hpp"
#include "partitioning/Utilities.hpp"
#include <iostream>

using namespace stdpartitioning;
using namespace std;


QueryGraphPredicateExtractor::QueryGraphPredicateExtractor(Database &db): db(db){
}

unsigned int QueryGraphPredicateExtractor::extractPredicates(QueryGraph &query, 
        vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg){
    unsigned int nPreds = 0;
    nPreds += this->extractPredicatesFromFilters(query, predicates, pdg);
    nPreds += this->extractPredicatesFromLiterals(query, predicates, pdg);
    
    return nPreds;
}

unsigned int QueryGraphPredicateExtractor::extractPredicatesFromFilters(QueryGraph &query, 
        vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg){
    vector<QueryGraph::SubQuery> subqueries;
    vector<QueryGraph::SubQuery>::iterator sit;
    unsigned int nPreds = 0;
    
    this->extractIndependentSubqueries(query.getQuery(), subqueries);
    for(sit = subqueries.begin(); sit != subqueries.end(); ++sit){
        vector<QueryGraph::Filter>::iterator fit;
        for(fit = sit->filters.begin(); fit != sit->filters.end(); ++fit){
            nPreds += this->extractAtomicPredicates(*fit, query.getQuery() ,predicates, pdg);            
        } 
    }
    
    return nPreds;
}

unsigned int QueryGraphPredicateExtractor::extractPredicatesFromLiterals(QueryGraph &query, vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg){
    vector<QueryGraph::SubQuery> subqueries;
    vector<QueryGraph::SubQuery>::iterator sit;
    unsigned int nPreds = 0;
    
    this->extractIndependentSubqueries(query.getQuery(), subqueries);    
    for(sit = subqueries.begin(); sit != subqueries.end(); ++sit){
        vector<QueryGraph::Node>::iterator nit;
        for(nit = sit->nodes.begin(); nit != sit->nodes.end(); ++nit){                        
            QueryGraph::Filter filter;
            SPARQLPredicate::Position position = SPARQLPredicate::Object;
            filter.type = QueryGraph::Filter::Equal;
            filter.arg1 = new QueryGraph::Filter();
            filter.arg1->type = QueryGraph::Filter::Variable;            
            filter.arg2 = new QueryGraph::Filter();            

            
            if(nit->constSubject){
                position = SPARQLPredicate::Subject;
                filter.arg2->type = QueryGraph::Filter::IRI;
                filter.arg2->id = nit->subject;
                this->addPredicate(*nit, filter, position, predicates, pdg);                        
                ++nPreds;                
            }
            
            if(nit->constPredicate){
                position = SPARQLPredicate::Predicate;
                filter.arg2->type = QueryGraph::Filter::IRI;
                filter.arg2->id = nit->predicate;
                this->addPredicate(*nit, filter, position, predicates, pdg);
                ++nPreds;            
            }
            
            if(nit->constObject){
                position = SPARQLPredicate::Object;
                QueryGraph::Filter::Type ftype;
                Type::ID type; unsigned subType;
                ftype = this->determineEntityType(nit->object, type, subType);
                filter.arg2->id = nit->object;           
                filter.arg2->type = ftype;                
//                if(ftype == QueryGraph::Filter::Literal){
//                    //Here things become little bit more complicated
//                    string result, typeDesc, key;                    
//                    lookupById(this->db, nit->object, type, subType, result);                    
//                    if(type > Type::CustomType){
//                        switch(type){
//                            case Type::String:
//                                typeDesc = "http://www.w3.org/2001/XMLSchema#string";
//                                key = "string-type-object";
//                                break;
//                            case Type::Integer:
//                                typeDesc = "http://www.w3.org/2001/XMLSchema#integer";
//                                key = "integer-type-object";                                
//                                break;
//                            case Type::Boolean:
//                                typeDesc = "http://www.w3.org/2001/XMLSchema#boolean";
//                                key = "boolean-type-object";                                
//                                break;
//                            case Type::Decimal:
//                                typeDesc = "http://www.w3.org/2001/XMLSchema#decimal";
//                                key = "decimal-type-object";                                
//                                break;
//                            case Type::Double:
//                                typeDesc = "http://www.w3.org/2001/XMLSchema#double";       
//                                key = "double-type-object";                                                                
//                                break;
//                            default:
//                                break;
//                        }
//
//                        if(!this->specialPredicateAlreadyAdded(key)){
//                            SPARQLPredicate dataTypePredicate;
//                            if(QueryGraphPredicateExtractor::lookupSpecialPredicate(this->db, key, dataTypePredicate)){
//                                predicates.push_back(dataTypePredicate);
//                                ++nPreds;
//                            }
//                        }                
//                        
//                        if(!this->specialPredicateAlreadyAdded("isliteral-object")){
//                            SPARQLPredicate isLiteralObject;
//                            QueryGraphPredicateExtractor::lookupSpecialPredicate(this->db, "isliteral-object", isLiteralObject);
//                            predicates.push_back(isLiteralObject);
//                            ++nPreds;
//                        }                        
//                    }else if(type == Type::Literal){
//                        filter.arg2->type = QueryGraph::Filter::Literal;
//                        if(!QueryGraphPredicateExtractor::specialPredicateAlreadyAdded("isliteral-object")){
//                            SPARQLPredicate isLiteralObject;
//                            QueryGraphPredicateExtractor::lookupSpecialPredicate(this->db, "isliteral-object", isLiteralObject);
//                            predicates.push_back(isLiteralObject);
//                            ++nPreds;
//                        }
//                    }else if(type == Type::CustomLanguage){
//                        //Obtain the language tag
//                        unsigned int i = result.size() - 1;
//                        string langTag;
//                        while(result[i] != '@') --i;
//                        langTag = result.substr(i + 1, result.size() - i - 1);
//                        
//                        SPARQLPredicate langMatches;                        
//                        if(!QueryGraphPredicateExtractor::lookupSpecialPredicate(this->db, "langMatches-" + langTag + "-object", langMatches)){
//                            this->addSpecialLanguagePredicate(subType, langTag, langMatches);
//			    predicates.push_back(langMatches);                            
//                        }else{
//                            if(!this->specialPredicateAlreadyAdded("langMatches-" + langTag + "-object")){
//                                predicates.push_back(langMatches);                           
//                                this->specialPredicatesAddedFlag["langMatches-" + langTag + "-object"] = true;
//                            }
//                        }                        
//                    }else if(type == Type::CustomType){                        
//                        string typeTag; char previous;
//                        unsigned int i = result.size() - 1;
//                        while(result[i] != '^' && previous != '^'){
//                            previous = result[i];
//                            --i;
//                        }
//                        typeTag = result.substr(i + 2, result.size() - i - 2);
//                        SPARQLPredicate typeMatches;
//                        if(!QueryGraphPredicateExtractor::lookupSpecialPredicate(this->db, "type-" + typeTag, typeMatches)){
//                            this->addSpecialTypePredicate(subType, typeTag, typeMatches);
//			    cout << "Recently added predicate: " << typeMatches << endl;
//                            predicates.push_back(typeMatches);                            
//                        }else{
//                            if(!this->specialPredicateAlreadyAdded("type-" + typeTag + "-object")){
//                                predicates.push_back(typeMatches);                           
//                                this->specialPredicatesAddedFlag["type-" + typeTag + "-object"] = true;
//                            }
//                        }                                                
//                    }                    
//                }else{
//                    //In this case add a new predicate
//                    if(!specialPredicateAlreadyAdded("isuri-object")){
//                        SPARQLPredicate isLiteralObject;
//                        lookupSpecialPredicate(this->db, "isuri-object", isLiteralObject);
//			cout << "Recently added predicate : " <<  isLiteralObject << endl;
//                        predicates.push_back(isLiteralObject);
//                        ++nPreds;
//                    }
//                }
                this->addPredicate(*nit, filter, position, predicates, pdg);        
                ++nPreds;                            
            }
        }
    }
    
    return nPreds;
    
}

bool QueryGraphPredicateExtractor::predicateAlreadyAdded(SPARQLPredicate &pred, vector<SPARQLPredicate> &predicates){
    vector<SPARQLPredicate>::iterator pit;
    
    for(pit = predicates.begin(); pit != predicates.end(); ++pit){
        if(pit->isEquivalent(pred))
            return true;
    }
    
    return false;
}

void QueryGraphPredicateExtractor::addPredicate(QueryGraph::Node &node, QueryGraph::Filter &filter, SPARQLPredicate::Position position,
        vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg){

    SPARQLPredicate newPredicate(filter, position);
    if(!this->predicateAlreadyAdded(newPredicate, predicates))
        predicates.push_back(newPredicate);
            
    //If there is a equivalent pattern then
    PatternId equivalent;
    bool thereIsEquivalent = this->findEquivalentPattern(node, equivalent, pdg); 
    if(thereIsEquivalent){
        pdg[equivalent].second.push_back(newPredicate);
    }    
}

bool QueryGraphPredicateExtractor::findEquivalentPattern(QueryGraph::Node &node, PatternId &result, PredicateDependencyGraph &pdg){
    //Iterate over the map
    PredicateDependencyGraph::iterator pdgit;
    
    for(pdgit = pdg.begin(); pdgit != pdg.end(); ++pdgit){
        if(nodeIsEquivalent(pdgit->second.first.localNode, node)){
            result = pdgit->first;
            return true;
        }
    }
    
    return false;
}

unsigned int QueryGraphPredicateExtractor::extractIndependentSubqueries(QueryGraph::SubQuery &subquery, vector<QueryGraph::SubQuery> &subqueries){
    unsigned int nSubqueries = 0;
    
    if(subquery.unions.empty()){
        subqueries.push_back(subquery);
        return 1;
    }else{
        vector<vector<QueryGraph::SubQuery> >::iterator sqit;
        for(sqit = subquery.unions.begin(); sqit != subquery.unions.end(); ++sqit){
            vector<QueryGraph::SubQuery>::iterator it;
            for(it = sqit->begin(); it != sqit->end(); ++it){
                nSubqueries += extractIndependentSubqueries(*it, subqueries);
            }
        }
    }    
    
    return nSubqueries;
}

unsigned int QueryGraphPredicateExtractor::extractAtomicPredicates(QueryGraph::Filter &filter, QueryGraph::SubQuery &query,
        vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg){
    //Check whether the predicate is already there
    if(isDiscardedOperator(filter) || isNotBooleanOperator(filter) || isLeafOperator(filter)){
        return 0;
    }else if(isBooleanConnector(filter)){        
        unsigned int left, right = 0;
        left = extractAtomicPredicates(*(filter.arg1), query, predicates, pdg);
        if(filter.type != QueryGraph::Filter::Not){        
            right = extractAtomicPredicates(*(filter.arg2), query, predicates, pdg);
        }
        return left + right;
    }else if(isBooleanAtomicOperator(filter)){
        //Here just copy the structure
        bitset<3> positions;
        this->determinePredicatePositions(filter, query, positions);
        unsigned int nPreds = 0;
        for(unsigned int i = 0; i < 3; i++){
            if(positions.test(i)){
                SPARQLPredicate newPredicate(filter, (SPARQLPredicate::Position)i);        
                if(!this->predicateAlreadyAdded(newPredicate, predicates)){
                    predicates.push_back(newPredicate);
                    //Now try to identify the patterns this filter affects (patterns who share variables with this filter)        
                    this->identifyAffectedPatterns(predicates.back(), query, pdg);                        
                    ++nPreds;
                }
            }
        }         
        
        return nPreds;
    }
    
    return 0;
}

void QueryGraphPredicateExtractor::identifyAffectedPatterns(SPARQLPredicate &pred, QueryGraph::SubQuery &subquery, PredicateDependencyGraph &pdg){
    vector<QueryGraph::Node>::iterator nit;
    
    for(nit = subquery.nodes.begin(); nit != subquery.nodes.end(); ++nit){
        if(pred.predicate.type == QueryGraph::Filter::Variable){
            
            //Just relate it to one pattern
            if(!nit->constObject && nit->object == pred.predicate.id){
                PatternId pid; 
                if(this->findEquivalentPattern(*nit, pid, pdg)){
                    pdg[pid].second.push_back(pred);
                }
            }
            
            if(!nit->constPredicate && nit->predicate == pred.predicate.id){
                PatternId pid; 
                if(this->findEquivalentPattern(*nit, pid, pdg)){
                    pdg[pid].second.push_back(pred);
                }                                
            }
            
            if(!nit->constSubject && nit->subject == pred.predicate.id){
                PatternId pid; 
                if(this->findEquivalentPattern(*nit, pid, pdg)){
                    pdg[pid].second.push_back(pred);
                }
            }            
        }
    }
}

void QueryGraphPredicateExtractor::determinePredicatePositions(QueryGraph::Filter &filter, 
        QueryGraph::SubQuery &query, bitset<3> &positions){

    vector<QueryGraph::Node>::iterator nid;
    vector<QueryGraph::Filter> variables;
    unsigned int nVars = this->extractVariables(filter, variables);
    
    for(nid = query.nodes.begin(); nid != query.nodes.end(); ++nid){
        for(unsigned int j = 0; j < nVars; ++j){            
            if(!nid->constSubject && variables[j].id == nid->subject) positions.set(2, true);
            if(!nid->constPredicate && variables[j].id == nid->predicate) positions.set(1, true);
            if(!nid->constObject && variables[j].id == nid->object) positions.set(0, true);
        }
    }
}

unsigned int QueryGraphPredicateExtractor::extractVariables(QueryGraph::Filter &filter, vector<QueryGraph::Filter> &output){
    unsigned int total = 0;
    
    if(filter.type == QueryGraph::Filter::Variable){
        output.push_back(filter);
        total = 1;
    }else{
        if(filter.arg1 != NULL)                        
            total += extractVariables(*(filter.arg1), output);
        
        if(filter.arg2 != NULL)
            total += extractVariables(*(filter.arg2), output);
        
        if(filter.arg3 != NULL)
            total += extractVariables(*(filter.arg3), output);
        
    }

    return total;
}

QueryGraph::Filter::Type QueryGraphPredicateExtractor::determineEntityType(unsigned int id, Type::ID &type, unsigned &subType){
    string result;
    if(lookupById(this->db, id, type, subType, result)){
        if(type == Type::URI){
            return QueryGraph::Filter::IRI;
        }else{
            return QueryGraph::Filter::Literal;
        }
    }
    
    return QueryGraph::Filter::Null;
}
