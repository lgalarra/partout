/* 
 * File:   Fragment.cpp
 * Author: luis
 * Created on June 10, 2011, 6:44 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/Fragment.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <algorithm>

#define MAX(a, b) a > b? a : b

using namespace stdpartitioning;

StatisticsMap MintermStatistics::statisticsCache;

MintermStatistics::MintermStatistics():db(NULL), samplingFactor(1.0){}

MintermStatistics::MintermStatistics(Database *db):db(db), samplingFactor(1.0){}

MintermStatistics::MintermStatistics(Database *db, float samplingFactor): db(db), samplingFactor(samplingFactor){}

bool MintermStatistics::lookupCardinalityInMap(Minterm &minterm, unsigned int &card){
    string flagsString = minterm.flagsString();    
    StatisticsMap::iterator stit = statisticsCache.find(flagsString);
    
    //If the flags string is not there, check if we can calculate the cardinality via substraction
    if(stit != statisticsCache.end()){
        vector<pair<Minterm, unsigned int> >::iterator it = this->findMinterm(minterm, stit);
        if(it != stit->second.end()){
            //Nicest case
            card = it->second;
            return true;
        }        
    }
    
    return false;
}

bool MintermStatistics::lookupCardinality(Minterm &minterm, unsigned int &card){
    string flagsString = minterm.flagsString();
    
    if(!minterm.isSatisfiable()){
        this->setCardinality(minterm, 0);
        card = 0;
//        cout << "(NS) lookupCard " << minterm << " card  " << card << endl;        
        return true;
    }
    
    if(this->lookupCardinalityInMap(minterm, card)){
//        cout << "(DH) lookupCard " << minterm << " card  " << card << endl;        
        return true;
    }
    
    //Special case, one predicate
    if(flagsString.compare("0") == 0){
        unsigned int S1, N;
        Minterm minS1 = minterm.complementMinterm();
        if(this->lookupCardinality(minS1, S1)){
            N = this->getDatabaseSize();
            card = N - S1;
            this->setCardinality(minterm, card);
//            cout << "(1P) lookupCard " << minterm << " card  " << card << endl;            
            return true;
        }       
    }else if(minterm.isPositive()){
        unsigned int sub, obj, pred;
        sub = pred = obj = ~0u;
        map<string, bool> predicatesFlagsMap;
        map<string, string> predicatesArgsMap;
        unsigned int predInd = 0;        
        string posArg;
        for(vector<SPARQLPredicate*>::iterator pit = minterm.predicates.begin(); pit != minterm.predicates.end(); ++pit){
            bool argInDatabase = false;
            bool binaryOperator = false;
            Type::ID type; unsigned subtype;
            string result, argument;
            if((*pit)->predicate.arg2 != NULL){
                binaryOperator = true;
                if(lookupById(*this->db, (*pit)->predicate.arg2->id, type, subtype, result))
                    argInDatabase = true;
            }
                
            if((*pit)->predicate.type == QueryGraph::Filter::Equal && (*pit)->predicate.arg1->type == QueryGraph::Filter::Variable && argInDatabase){
                switch((*pit)->position){
                    case SPARQLPredicate::Object:
                        obj = (*pit)->predicate.arg2->id;
                        break;
                    case SPARQLPredicate::Predicate:
                        pred = (*pit)->predicate.arg2->id;
                        break;
                    case SPARQLPredicate::Subject:
                        sub = (*pit)->predicate.arg2->id;
                        break;
                }
            }else{
                switch((*pit)->position){
                    case SPARQLPredicate::Object:
                        posArg = "object";
                        break;
                    case SPARQLPredicate::Predicate:
                        posArg = "predicate";
                        break;
                    case SPARQLPredicate::Subject:
                        posArg = "subject";
                        break;
                }
                
                if(binaryOperator){
                    if(argInDatabase){
                        argument = result;
                    }else{
                        argument = (*pit)->predicate.arg2->value;
                    }
                }
                
                if((*pit)->predicate.arg1->type == QueryGraph::Filter::Builtin_datatype){
                    predicatesFlagsMap["datatype-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["datatype-" + posArg] = argument;
                }else if((*pit)->predicate.type == QueryGraph::Filter::Builtin_isliteral){
                    predicatesFlagsMap["isliteral-" + posArg] = minterm.flags.test(predInd);
                }else if((*pit)->predicate.type == QueryGraph::Filter::Builtin_isiri){
                    predicatesFlagsMap["isiri-" + posArg] = minterm.flags.test(predInd);
                }else if((*pit)->predicate.type == QueryGraph::Filter::Builtin_langmatches){
                    predicatesFlagsMap["langMatches-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["langMatches-" + posArg] = argument;
                }else if((*pit)->predicate.type == QueryGraph::Filter::Greater){
                    predicatesFlagsMap["greater-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["greater-" + posArg] = argument;
                }else if((*pit)->predicate.type == QueryGraph::Filter::GreaterOrEqual){
                    predicatesFlagsMap["greater-or-equal-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["greater-or-equal-" + posArg] = argument;                                        
                }else if((*pit)->predicate.type == QueryGraph::Filter::Less){
                    predicatesFlagsMap["less-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["less-" + posArg] = argument;                                                            
                }else if((*pit)->predicate.type == QueryGraph::Filter::LessOrEqual){
                    predicatesFlagsMap["less-or-equal-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["less-or-equal-" + posArg] = argument;                    
                }else if((*pit)->predicate.type == QueryGraph::Filter::Equal){
                    predicatesFlagsMap["equal-" + posArg] = minterm.flags.test(predInd);
                    predicatesArgsMap["equal-" + posArg] = argument;                                        
                }
            }
            
            ++predInd;
        }

#if ENABLE_SAMPLING == 1
        card = MAX(1, this->db->getExactStatistics().getCardinalityUpdatesUnsafe(sub, pred, obj, predicatesFlagsMap, predicatesArgsMap, *this->db));
#else
        card = MAX(1, ((this->db->getExactStatistics().getCardinality(sub, pred, obj) / 4) * 3));
#endif

        card = (unsigned)((float)card / (this->samplingFactor));        
        this->setCardinality(minterm, card);
//        cout << "(SL) lookupCard " << minterm << " card  " << card << endl;        
        return true;        
    }else{
        //There is still hope!
        unsigned int Sx0, Sxx1;
        bool bSx0, bSxx1;
        //Build the prefix minterm
        Minterm prefixMinterm, complementMinterm, suffixMinterm;
        prefixMinterm = minterm.prefixMinterm();
        complementMinterm = minterm.complementMinterm();
        suffixMinterm = minterm.suffixMinterm();
        bSx0 = this->lookupCardinalityInMap(prefixMinterm, Sx0);
        bSxx1 = this->lookupCardinalityInMap(complementMinterm, Sxx1);        
        if(bSx0 && bSxx1){
            card = Sx0 > Sxx1? Sx0 - Sxx1 : 0;
            this->setCardinality(minterm, card);
//            cout << "(FS) lookupCard " << minterm << " card  " << card << endl;            
            return true;
        }else if(bSx0){
            //Assume independence
            unsigned int cardSuffix;
            float suffixSel;
            //Technically this should not happen
            if(this->lookupCardinality(suffixMinterm, cardSuffix))
                suffixSel = (float)cardSuffix / (float)this->getDatabaseSize();
            else
                suffixSel = 0.5;
            
            card = ((float)Sx0 * suffixSel);
            if(card == 0) card = 1;
            this->setCardinality(minterm, card);
//            cout << "(AP) " << suffixSel << " lookupCard " << minterm << " card  " << card << endl;            
            return false;
         }
    }
    
    //The query has to be issued and we cannot use the dictionary
    card = 1;
//    cout << "(CM) lookupCard " << minterm << " card  " << card << endl;
    this->setCardinality(minterm, card);
    return false;
}

vector<pair<Minterm, unsigned int> >::iterator MintermStatistics::findMinterm(Minterm &m, StatisticsMap::iterator &stit){
    vector<pair<Minterm, unsigned int> >::iterator it;
    for(it = stit->second.begin(); it != stit->second.end(); ++it){
        //We can calculate it :D
        if(it->first.samePredicates(m)){
            return it;
        }
    }            
    
    return stit->second.end();
}

void MintermStatistics::setCardinality(Minterm &minterm, unsigned int cardinality){
    string flagsString = minterm.flagsString();
    StatisticsMap::iterator stit = statisticsCache.find(flagsString);
    //If the flags string is not there, create a new entry
    if(stit == statisticsCache.end()){
        statisticsCache[flagsString].push_back(pair<Minterm, unsigned int>(minterm, cardinality));
    }else{
        //Check whether it refers to the same minterm (using the same predicates)
        vector<pair<Minterm, unsigned int> >::iterator it;
        for(it = stit->second.begin(); it != stit->second.end(); ++it){
            //Update it
            if(it->first.samePredicates(minterm)){
                it->second = cardinality;
                return;
            }
        }
        
        //If we reached this state, we must add the entry
        stit->second.push_back(pair<Minterm, unsigned int>(minterm, cardinality));        
    }
}

unsigned int MintermStatistics::getDatabaseSize(){
    ExactStatisticsSegment &statsSeg = this->db->getExactStatistics();
    return statsSeg.getCardinality(~0u, ~0u, ~0u);
}

Fragment::Fragment(): db(NULL), sdb(NULL), observedCardinality(0),
        sizeUpdated(false), dependencyGraph(NULL), frequencyUpdated(false){
}

Fragment::Fragment(Minterm m, Database *db, ReversePredicateDependencyGraph* g): minterm(m), db(db), observedCardinality(0),
        sizeUpdated(false), dependencyGraph(g), sdb(NULL), frequencyUpdated(false), statsCalculator(db){
    this->dictWrapper.setDatabase(this->db);
}

Fragment::Fragment(Minterm m, Database *db, Database *sampleDb, float sampleFactor, ReversePredicateDependencyGraph* g): minterm(m), db(db), observedCardinality(0),
        sizeUpdated(false), sdb(sampleDb), dependencyGraph(g), frequencyUpdated(false), statsCalculator(sampleDb, sampleFactor){
    this->dictWrapper.setDatabase(this->db);
}

Fragment::Fragment(const Fragment &anotherFragment){
    this->db = anotherFragment.db;
    this->dictWrapper.setDatabase(this->db);
    this->dependencyGraph = anotherFragment.dependencyGraph;
    this->frequencyUpdated = anotherFragment.frequencyUpdated;    
    this->observedCardinality = anotherFragment.observedCardinality;
    
    if(anotherFragment.frequencyUpdated){
        this->frequency = anotherFragment.frequency;
    }
    
    this->sizeUpdated = anotherFragment.sizeUpdated;    
    if(anotherFragment.sizeUpdated){
        this->size = anotherFragment.size;
    }
    
    this->minterm = anotherFragment.minterm;
    this->statsCalculator = anotherFragment.statsCalculator;
}

Fragment& Fragment::operator=(Fragment &anotherFragment){
    this->db = anotherFragment.db;
    this->dictWrapper.setDatabase(this->db);
    this->dependencyGraph = anotherFragment.dependencyGraph;
    this->frequencyUpdated = anotherFragment.frequencyUpdated;    
    this->observedCardinality = anotherFragment.observedCardinality;
    
    if(anotherFragment.frequencyUpdated){
        this->frequency = anotherFragment.frequency;
    }
    
    this->sizeUpdated = anotherFragment.sizeUpdated;    
    if(anotherFragment.sizeUpdated){
        this->size = anotherFragment.size;
    }
    
    this->minterm = anotherFragment.minterm;
    this->statsCalculator = anotherFragment.statsCalculator;
    return *this;
}

Fragment::~Fragment(){
    this->db = NULL;
    this->dependencyGraph = NULL;
}

unsigned int Fragment::getSize(){
    if(!this->sizeUpdated){
        this->calculateSize(); 
    }    
    return this->size;
}

Minterm& Fragment::getMinterm(){
    return this->minterm;
}

const Minterm& Fragment::getMinterm() const{
    return this->minterm;
}

void Fragment::calculateSize(){
    unsigned int card;
    this->minterm.sortPredicatesById();
    srand(time(NULL));
    this->statsCalculator.lookupCardinality(this->minterm, card);        
    this->size = card;
    this->sizeUpdated = true;    
}

unsigned int Fragment::getFrequency(){
    if(!this->frequencyUpdated)
        this->calculateFrequency();
    
    return this->frequency;
}
       
double Fragment::getRatio(){
    return (double)this->getLoad();
}

unsigned int Fragment::getLoad(){    
    if(!this->frequencyUpdated)
        this->calculateFrequency();
    
    if(!this->sizeUpdated)
        this->calculateSize();
    
    return this->size * this->frequency;
}

unsigned int Fragment::onesCount() const{
    int count = 0;
    for(unsigned int i = 0; i < this->minterm.flags.size(); ++i){
        if(this->minterm.flags[i])
            count++;
    }
    
    return count;
}

bool Fragment::meets(unsigned subject, unsigned predicate, unsigned object){
    unsigned index = 0;    
    for(vector<SPARQLPredicate*>::iterator pit = this->minterm.predicates.begin(); pit != this->minterm.predicates.end(); ++pit){
        if(!(*pit)->meets(subject, predicate, object, this->minterm.flags[index], this->dictWrapper)){
            return false;
        }
        ++index;
    }
    
    return true;
}

bool Fragment::isLeftOverFragment(){
    return this->minterm.isZeroMinterm();
}

vector<SPARQLPattern*> Fragment::getRelatedPatterns(){
    vector<SPARQLPattern*> result;
    if(this->dependencyGraph == NULL){
        throw Exception(Exception::INVALID_STATE, "getRelatedPatterns(): The predicates dependency graph is NULL");
    }
    
    for(unsigned int i = 0; i < this->minterm.flags.size(); ++i){
        if(this->minterm.flags[i]){
            SPARQLPredicate *thePredicate = this->minterm.predicates.at(i);
            ReversePredicateDependencyGraph::iterator rit;
            rit = this->dependencyGraph->find(thePredicate->id);
            if(rit != this->dependencyGraph->end()){
                vector<SPARQLPattern>::iterator pit;
                for(pit = rit->second.second.begin(); pit != rit->second.second.end(); ++pit){
                    result.push_back(&*pit);
                }
            }
        }
    }
    
    return result;
}

void Fragment::calculateFrequency(){
    unsigned int aggregatedFreq = 1;
    if(this->dependencyGraph == NULL){
        throw Exception(Exception::INVALID_STATE, "calculateFrequency(): The predicates dependency graph is NULL");
    }else{
        //For every predicate appearing in normal form
        for(unsigned int i = 0; i < this->minterm.flags.size(); ++i){
            if(this->minterm.flags[i]){
                ReversePredicateDependencyGraph::iterator rit;
                rit = this->dependencyGraph->find(this->minterm.predicates[i]->id); 
                if(rit != this->dependencyGraph->end()){
                    vector<SPARQLPattern> &patterns = rit->second.second;
                    vector<SPARQLPattern>::iterator pit;
                    for(pit = patterns.begin(); pit != patterns.end(); ++pit){
                        aggregatedFreq += pit->freq;
                    }
                }
            }
        }
        
        this->frequencyUpdated = true;
        this->frequency = aggregatedFreq;
    }
}

bool FragmentationScheme::sameSignature(const FragmentationScheme &otherScheme) const{
    if(otherScheme.fragments.size() != this->fragments.size())
        return false;
    
    //Now check if the signature ratio vector is the same
    for(unsigned int i = 0; i < this->freqSizeVector.size(); i++){
        if(this->freqSizeVector[i] != otherScheme.freqSizeVector[i])
            return false;
    }
    
    return true;
}

bool FragmentationScheme::empty(){
    return this->fragments.empty();
}

bool sortByOneCount(const Fragment &a, const Fragment &b){
    unsigned int aCount = a.onesCount();
    unsigned int bCount = b.onesCount();
    if(aCount > bCount) return true;
    else if(aCount < bCount) return false;
    else{
        if(aCount == 0) return false; //It is the same
        //Promote the one that has a 1 earlier
        unsigned afone = ~0u, bfone = ~0u;
        size_t theSize = a.getMinterm().flags.size();
        for(unsigned i = 0; i < theSize ; ++i){
            if(a.getMinterm().flags.test(i)){
                afone = i;
                break;
            }
        }
        
        for(unsigned i = 0; i < theSize; ++i){
            if(b.getMinterm().flags.test(i)){
                bfone = i;
                break;
            }
        }
        
        return afone < bfone;
    }
}

void FragmentationScheme::sortFragments(){
    sort(this->fragments.begin(), this->fragments.end(), sortByOneCount);
//    this->nOnesQuickSort(this->fragments, 0, this->fragments.size());
}

void FragmentationScheme::pushFragment(Fragment &f){
    this->fragments.push_back(f);
    this->freqSizeVector.push_back(-1.0);
}

void FragmentationScheme::nOnesQuickSort(vector<Fragment> &input, unsigned int start, unsigned int end){
    if(end - start <= 1)
        return;
    
    unsigned int pivot = (rand() % (end - start)) + start;
    unsigned int pivotCount = input[pivot].onesCount();    
    unsigned int k, pivot2, posS;
    vector<Fragment> tmpSmaller, tmpGreater, tmpEqual;

    
    for(unsigned int i = start; i < end; ++i){
        unsigned int theCount = input[i].onesCount();
        if(input[i].onesCount() > pivotCount){
            tmpGreater.push_back(input[i]);            
        }else if(theCount == pivotCount){
            tmpEqual.push_back(input[i]);
        }else{
            tmpSmaller.push_back(input[i]);            
        }
    }
        
    posS = start; 
    k = 0;
    while(k < tmpGreater.size()){
        input[posS] = tmpGreater[k];
        ++posS;
        ++k;
    }
    if(k > 0){
        nOnesQuickSort(input, start, posS);
    }

    k = 0;
    while(k < tmpEqual.size()){
        input[posS] = tmpEqual[k];
        ++k;
        ++posS;		
    }
   
    pivot2 = posS;
    
    k = 0;
    while(k < tmpSmaller.size()){
        input[posS] = tmpSmaller[k];
        ++k;
        ++posS;
    }
    if(k > 0){
        nOnesQuickSort(input, pivot2, end);
    }    
}


unsigned int FragmentationScheme::getTotalLoad(){
    if(!this->loadUpdated){
        //Calculate it
        unsigned int sum = 0;
        for(vector<Fragment>::iterator it = this->fragments.begin(); it != this->fragments.end(); ++it)
            sum += it->getLoad();
        
        this->totalLoad = sum;
        this->loadUpdated = true;        
    }
    
    return totalLoad;
}

void FragmentationScheme::calculateRatio(){
    if(!this->sizeUpdated){
        this->sortFragments();
        for(unsigned int i = 0; i < this->fragments.size(); ++i){
            this->freqSizeVector[i] = this->fragments[i].getRatio();
        }        
        this->sizeUpdated = true;
    }
}

void FragmentationScheme::cleanup(vector<unsigned int> &toRemove){
    unsigned int slant = 0;
    for(vector<unsigned int>::iterator it = toRemove.begin(); it != toRemove.end(); ++it){
        this->fragments.erase(this->fragments.begin() + *it - slant);
        ++slant;
    }
}
        
PartitionSummary PartitionSummary::summarize(vector<FragmentAllocationAssign> &fragments){
    if(fragments.empty()) return PartitionSummary();
       
    vector<SPARQLPredicate*> &predicates = fragments[0].fragment->getMinterm().predicates;
    vector<PredicateEntryStatus> status;
    Database *db = fragments[0].fragment->getDatabase();
    status.resize(predicates.size());
    
    //Initialize the status array with the first minterm
    Minterm &minstart = fragments[0].fragment->getMinterm();
    for(unsigned i = 0; i < minstart.flags.size(); ++i){
        if(minstart.flags[i])
            status[i] = Positive;
        else
            status[i] = Negative;
    }
    
    //Apply a bitwise XNOR
    for(vector<FragmentAllocationAssign>::iterator fit = fragments.begin() + 1; fit != fragments.end(); ++fit){
        Minterm &min = fit->fragment->getMinterm();
        for(unsigned i = 0; i < min.flags.size(); ++i){
            if(status[i] == Ignore) continue;
            
            if((min.flags[i] && status[i] == Negative) || (!min.flags[i] && status[i] == Positive)){
                status[i] = Ignore;                
            }
        }
    }
    
    return PartitionSummary(predicates, status, db);
}

PartitionSummary& PartitionSummary::operator=(const PartitionSummary &otherSummary){
    this->predicates = otherSummary.predicates;
    this->status = otherSummary.status;
    this->dictWrapper = otherSummary.dictWrapper;    
    return *this;
}
        
void PartitionSummary::summarize(FragmentsAllocationMap &allocMap, PartitionsSummaryMap &summary){
    //Just build a summary per host
    for(FragmentsAllocationMap::iterator famit = allocMap.begin(); famit != allocMap.end(); ++famit){
        summary[famit->first] = PartitionSummary::summarize(famit->second);       
        summary[famit->first].setFragments(famit->second);
    }
}

bool PartitionSummary::meets(unsigned subject, unsigned predicate, unsigned object){
    if(this->predicates.empty()) return false;
        
    //It means the host summary can be used to discard the triple
    for(unsigned i = 0; i < this->status.size(); ++i){
        if(this->status[i] != Ignore){
            bool flag = (this->status[i] == Positive);
            if(!this->predicates[i]->meets(subject, predicate, object, flag, dictWrapper))
                return false;
        }
    }
    
    //If it matches the predicate definition, there is no other solution than evaluating it per fragment    
    for(vector<FragmentAllocationAssign>::iterator fit = fragments.begin(); fit != fragments.end(); ++fit){
        if(fit->fragment->meets(subject, predicate, object)){            
            return true;
        }
    }    
    
    return false;
}
