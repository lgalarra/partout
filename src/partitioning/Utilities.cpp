/* 
 * File:   Utilities.cpp
 * Author: luis
 * Created on May 27, 2011, 1:26 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/Utilities.hpp"
#include <string>
#include "rts/database/Database.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/operator/Operator.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/codegen/CodeGen.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "rts/runtime/DifferentialIndex.hpp"
#include "infra/util/Type.hpp"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;


//---------------------------------------------------------------------------
string escapeURI(const char* iter,const char* limit)
{
   string s;
   for (;iter!=limit;++iter) {
      char c=(*iter);
      if ((c=='\\')||(c=='<')||(c=='>')||(c=='\"')||(c=='{')||(c=='}')||(c=='^')||(c=='|')||(c=='`')||((c&0xFF)<0x20))
         s+='\\';
      s+=c;
   }
   return s;
}
//---------------------------------------------------------------------------
string escape(const char* iter,const char* limit)
{
   string s;
   for (;iter!=limit; ++iter) {
      char c = (*iter);
      switch (c) {
         case '\t': s+= "\\t"; break;
         case '\n': s+= "\\n"; break;
         case '\r': s+= "\\r"; break;
         case '\"': s+= "\\\""; break;
         case '\\': s+= "\\\\"; break;
         default: s+= c; break;
      }
   }
   return s;
}
   
bool runQuery(Database& db, const string& query, vector<vector<string> > &values, bool showCardinalities){
   QueryGraph queryGraph;
   {
      // Parse the query
     SPARQLLexer lexer(query);
     SPARQLParser parser(lexer);
     parser.parse();

     SemanticAnalysis semana(db);
     semana.transform(parser,queryGraph);

      if (queryGraph.knownEmpty()) {
         return false;
      }
   }
   
   if(showCardinalities)
       queryGraph.setDuplicateHandling(QueryGraph::AllDuplicates);
   else
       queryGraph.setDuplicateHandling(QueryGraph::NoDuplicates);

   
   // Run the optimizer
   PlanGen plangen;
   Plan* plan=plangen.translate(db,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return false;
   }

   // Build a physical plan
   Runtime runtime(db);
   std::vector<Register*> output;   
   Operator* operatorTree = CodeGen().translateIntern(runtime,queryGraph,plan,output);
   // And execute it      
   vector<string> row;
   unsigned int card;
   if ((card = operatorTree->first()) != 0){
      do {
          row.clear();
          ostringstream sstr;
          sstr << card;      
          for (vector<Register*>::const_iterator iter=output.begin(),limit=output.end();iter!=limit;++iter) {
             unsigned v=(*iter)->value;
             if (!~v) {
                break;
             } else {
                Type::ID type; unsigned subType;
                string result;
                if(!lookupById(db, v, type, subType, result)){
                    break;
                }  
                row.push_back(result);
             }
          }
          if(showCardinalities)
              row.push_back(sstr.str());

          values.push_back(row);            
       } while ((card = operatorTree->next()) != 0);
   }
   
   delete operatorTree;
   return true;
}


bool lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result){
    const char* start, *stop;
    const char* start2, *stop2; 
    Type::ID type2; 
    unsigned subType2;  
    DifferentialIndex diff(db);    
    
    if (!db.getDictionary().lookupById(id, start, stop, type, subType)){
        if(!diff.lookupById(id, start, stop, type, subType))
            return false;
    }
    
    switch (type) {
       case Type::URI: result = "<"+escapeURI(start,stop)+">"; break;
       case Type::Literal: result = "\""+escape(start,stop)+"\""; break;
       case Type::String: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#string>"; break;
       case Type::Integer: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#integer>"; break;
       case Type::Decimal: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#decimal>"; break;
       case Type::Double: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#double>"; break;
       case Type::Boolean: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#boolean>"; break;
       case Type::CustomLanguage: 
           result = "\""+escape(start,stop)+"\""; 
           //Lookup language
           if(db.getDictionary().lookupById(subType, start2, stop2, type2, subType2))
               result += "@" + escape(start2, stop2);           
           break;
       case Type::CustomType: 
           result = "\""+escape(start,stop)+"\""; 
           //Lookup language
           const char* start2, *stop2; Type::ID type2; unsigned subType2;           
           if(db.getDictionary().lookupById(subType, start2, stop2, type2, subType2))
               result += "^^<" + escape(start2, stop2) + ">";           
           break;
                
    }
    
    return true;
}

bool queryHasResults(Database& db, const string& query, unsigned int &card){
   QueryGraph queryGraph;    
   {
      // Parse the query
     SPARQLLexer lexer(query);
     SPARQLParser parser(lexer);
     parser.parse();

     SemanticAnalysis semana(db);
     semana.transform(parser,queryGraph);

      if (queryGraph.knownEmpty()) {
         return false;
      }
   }
      
   // Run the optimizer
   PlanGen plangen;
   Plan* plan=plangen.translate(db,queryGraph);
   if (!plan) {
      cerr << "internal error plan generation failed" << endl;
      return false;
   }

   // Build a physical plan
   Runtime runtime(db);
   std::vector<Register*> output;   
   Operator* operatorTree = CodeGen().translateIntern(runtime, queryGraph, plan, output);
   card = operatorTree->first();
   bool result = (card != 0);

   delete operatorTree;
   return result;    
}

QueryGraph::Filter::Type determineEntityType(Database &db, unsigned int id, Type::ID &type, unsigned &subType){
    string result;
    if(lookupById(db, id, type, subType, result)){
        if(type == Type::URI){
            return QueryGraph::Filter::IRI;
        }else{
            return QueryGraph::Filter::Literal;
        }
    }
    
    return QueryGraph::Filter::Null;
}

unsigned int extractIndependentSubqueries(QueryGraph::SubQuery &subquery, 
        vector<QueryGraph::SubQuery> &subqueries){
    unsigned int nSubqueries = 0;
    
    if(subquery.unions.empty()){
        subqueries.push_back(subquery);
        return 1;
    }else{
        vector<vector<QueryGraph::SubQuery> >::iterator sqit;
        for(sqit = subquery.unions.begin(); sqit != subquery.unions.end(); ++sqit){
            vector<QueryGraph::SubQuery>::iterator it;
            for(it = sqit->begin(); it != sqit->end(); ++it){
                nSubqueries += extractIndependentSubqueries(*it, subqueries);
            }
        }
    }    
    
    return nSubqueries;
}

static inline string buildString(const char *&start, const char *&stop){
    string result;
    const char *ct = start;
    while(ct != stop){
        result.append(1, *ct);
        ++ct;
    }
    
    return result;
}

bool lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result, string &result2){
    const char* start, *stop;
    const char* start2 = NULL, *stop2 = NULL; 
    Type::ID type2; 
    unsigned subType2;  
    DifferentialIndex diff(db);    

    //First check in the cache
    if (!db.getDictionary().lookupById(id, start, stop, type, subType)){
        if(!diff.lookupById(id, start, stop, type, subType))
            return false;
    }

    result = buildString(start, stop);      
    
    switch (type) {
       case Type::URI: result2 = "<" + escapeURI(start, stop) + ">";  break;
       case Type::Literal: result2 = "\"" + escape(start,stop) + "\""; break;
       case Type::String:  result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#string>"; break;
       case Type::Integer: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#integer>"; break;
       case Type::Decimal: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#decimal>"; break;
       case Type::Double:  result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#double>"; break;
       case Type::Boolean: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#boolean>"; break;
       case Type::CustomLanguage: case Type::CustomType: 
           result2 = "\""+escape(start,stop)+"\""; 
           //Lookup language
           if(db.getDictionary().lookupById(subType, start2, stop2, type2, subType2)){
               if(start2 != NULL && stop2 != NULL){
                   if(type == Type::CustomLanguage)
                       result2 += "@" + escape(start2, stop2);           
                   else if(type == Type::CustomType)
                       result2 += "^^<" + escape(start2, stop2) + ">";                          
               }
           }
           
           break;                
    }
    
    return true;
}
