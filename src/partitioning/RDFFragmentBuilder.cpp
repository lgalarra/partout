/* 
 * File:   RDFFragmentBuilder.cpp
 * Author: luis
 * Created on June 18, 2011, 8:29 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include <vector>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <boost/thread.hpp>
#include <boost/version.hpp>
#include <math.h>

#include "rts/runtime/DifferentialIndex.hpp"
#include "rts/database/Database.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/operator/Operator.hpp"
#include "cts/parser/SPARQLLexer.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/codegen/CodeGen.hpp"
#include "cts/plangen/PlanGen.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include "rts/segment/FactsSegment.hpp"

#include "partitioning/RDFFragmentBuilder.hpp"
#include "partitioning/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"
#include "qload/QueryLoadOutputStream.hpp"
#include "partitioning/Utilities.hpp"
#include "partitioning/CommonTypes.hpp"
#include "rts/segment/FullyAggregatedFactsSegment.hpp"

using namespace stdpartitioning;
using namespace qload;
using namespace boost;

#ifndef FULL_DICTIONARY
#define FULL_DICTIONARY
#endif


//A rough estimate for 32 MB of data per cache (to speed up things)
const unsigned RDFFragmentBuilder::DictionaryLookupAndBackup::cacheMax = 32 * 1024 * 1024 / (sizeof(DictionaryLookupAndBackup::DictionaryEntry) + sizeof(unsigned));
const unsigned RDFFragmentBuilder::DictionaryLookupAndBackup::defaultBackupLimit = ~0u;

RDFFragmentBuilder::RDFFragmentBuilder(DatabaseDescriptor &dbds, string &binPrefix, string &tmpPrefix): dbds(dbds), binPrefix(binPrefix), tmpPrefix(tmpPrefix) {}

RDFFragmentBuilder::~RDFFragmentBuilder() {}

void RDFFragmentBuilder::buildSingleFragment(vector<FragmentAllocationAssign> &assignment, FragmentFilesAllocationMap *fileNames){
    string node = assignment.begin()->host;
    string basePrefix = this->dbds.dbBasename();    
    string buildCommand = binPrefix + "rdf3xload2 ";
    string filename = tmpPrefix + basePrefix +"_fragment_" + node + ".frag";
    string dbfile = basePrefix + "_fragment_" + node;                
    unsigned nTriples = 0;
    //We will go for a trivial heuristic. Just count the ones in the flags and give preferences to those fragments
    //with positive predicates. It does not consider correlations
    unsigned size = 0;
    for(vector<FragmentAllocationAssign>::iterator it = assignment.begin(); it != assignment.end(); ++it){
        size += it->fragment->getSize();
    }
    //We could assume every new triple will contribute with at least one new dictionary term
    //But we want to give preference to those fragments with less triples
    #ifndef FULL_DICTIONARY 
    double factor;
    double nFrags = (double)assignment.size() + 1.0;    
    if(size > 0){
        factor = log((double)size);
        if(factor >= 0) factor = nFrags/factor;
        else factor = nFrags / 0.125;
    }else{
        factor = nFrags / 0.125;
    }
    #else
    double factor = 1.0;
    #endif
    
    cout << "Default Backup limit " << factor << " for fragment " << node  << endl; 
    DictionaryLookupAndBackup lookupByIdAndStore(dbfile + ".dict", factor * DictionaryLookupAndBackup::defaultBackupLimit);
    QueryLoadOutputStream output(filename);    
    output.open();    
    
    for(vector<FragmentAllocationAssign>::iterator asit = assignment.begin(); asit != assignment.end(); ++asit){
        string query = asit->fragment->getMinterm().buildQuery();
        ResultsTable rows;

        cout << "Running query: " << query << endl;
        this->runQuery(*this->dbds.db, query, rows, lookupByIdAndStore, false);

        for(ResultsTable::iterator rtit = rows.begin(); rtit != rows.end(); ++rtit){
            //Just write the triple in the output file
            output << rtit->at(0) << " " << rtit->at(1) << " " << rtit->at(2) << " . " << "\n";
            ++nTriples;
        }
    }

    output.close();
    
    if(nTriples > 0){
        buildCommand += dbfile + " " + this->dbds.filename + " " + filename;
        cout << "Running command: " << buildCommand << endl;
        unsigned int syrt = system(buildCommand.c_str());
        cout << "Return valfilenameue for command execution " << syrt << endl;
        if(syrt != 0){
            cerr << "The command " + buildCommand + " failed";
        }else{        
            cout << "Moving to next fragment " << endl;
            boost::unique_lock<boost::mutex> lock(this->filesMapMutex);
            fileNames->insert(pair<string, string>(node, dbfile));                
            lock.unlock();
        }
    }else{
        cout << "Fragment " << node << " is empty " << endl;
    }
    
    cout << "Removing temporal file " << filename << endl;
    remove(filename.c_str());

}

void RDFFragmentBuilder::buildFragments(FragmentsAllocationMap &allocMap, FragmentFilesAllocationMap &fileNames){
    //My powerful development machine at MMCI does not have an updated version of boost :( 
    #if (BOOST_VERSION / 100 % 1000) >= 47
        unsigned nThreads = thread::hardware_concurrency();    
    #else
        unsigned nThreads = 1;
    #endif
    cout << "Using " << nThreads << " threads " << endl;
    unsigned nNodes = allocMap.size();
    unsigned rounds = nNodes / nThreads;
    unsigned remain = nNodes % nThreads;
    if(remain > 0) rounds++;
    FragmentsAllocationMap::iterator amit  = allocMap.begin();  
    for(unsigned j = 0; j < rounds; ++j){
        unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;
            
        thread **works = new thread*[nJobs];
        for(unsigned i = 0; i < nJobs; ++i){
            cout << "Scheduling fragments for node " << amit->first << endl;
            #if (BOOST_VERSION / 100 % 1000) >= 47
            works[i] = new thread(&RDFFragmentBuilder::buildSingleFragment, this, amit->second, &fileNames);
            #else
            this->buildSingleFragment(amit->second, &fileNames);
            #endif
            amit++;
        }
        
        #if (BOOST_VERSION / 100 % 1000) >= 47        
        for(unsigned i = 0; i < nJobs; ++i){
            works[i]->join();
        }
        
        for(unsigned i = 0; i < nJobs; ++i){
            delete works[i];
        }
        #endif
                
        delete[] works;
    }
}

void RDFFragmentBuilder::buildSinglePartition(string &node, string &temporalFile, FragmentsAllocationMap *allocMap){
    string basePrefix = this->dbds.dbBasename();    
    string buildCommand = binPrefix + "/rdf3xload2 ";
    string dbfile = basePrefix + "_fragment_" + node;                
    ifstream temporalStream(temporalFile.c_str(), ios_base::in);

    //If the file is not empty
    if(temporalStream.peek() != std::ifstream::traits_type::eof()){
        buildCommand += dbfile + " " + this->dbds.filename + " " + temporalFile;
        cout << "Running command: " << buildCommand << endl;
        unsigned int syrt = system(buildCommand.c_str());
        cout << "Return valfilenameue for command execution " << syrt << endl;
        if(syrt != 0){
            cerr << "The command " + buildCommand + " failed";
        }
    }else{
        cout << "Fragment " << node << " is empty " << endl;
        boost::unique_lock<boost::mutex> lock(this->allocMapMutex);
        allocMap->erase(node);
        lock.unlock();
        
    }

    temporalStream.close();
    
}

void RDFFragmentBuilder::buildSingleFragment5(map<string, fstream*> *outputStreams, map<string, DictionaryLookupAndBackup*> *tmpDictionaries,
    map<string, boost::mutex*> *mutexes, PartitionsSummaryMap *summary, Fragment *leftOverFragment){
    boost::unique_lock<boost::mutex> propertiesLock(this->propertiesMutex);
    bool empty = this->properties.empty();
    unsigned key;
    if(empty){
        propertiesLock.unlock();
        return;
    }else{
        key = this->properties.front();
        this->properties.pop();
    }    
    propertiesLock.unlock();
    
    FactsSegment &facts = dbds.db->getFacts(Database::Order_Predicate_Subject_Object);
    FactsSegment::Scan scan;
    map<string, vector<unsigned> > temporalIds;
    unsigned batchSize = 64 * 3;
    
    for(map<string, fstream*>::iterator fit = outputStreams->begin(); fit != outputStreams->end(); ++fit){
        temporalIds[fit->first] = vector<unsigned>();
        temporalIds[fit->first].reserve(batchSize);
    }        

       
    while(!empty){
        cout << "Scheduling job for region with property: " << key  << endl;                    
        //We will scan only the triples having this key as predicate
        if (scan.first(facts, key, 0, 0)){
            unsigned subject, predicate, object;  
            predicate = scan.getValue1();
            subject = scan.getValue2();
            object = scan.getValue3();
            //Once we have covered the region, break
            while(predicate == key){
                bool match = false;
                string hostSlot;
                for(PartitionsSummaryMap::iterator fit = summary->begin(); fit != summary->end(); ++fit){                
                    if(fit->second.meets(subject, predicate, object)){
                        hostSlot =  fit->first;
                        match = true;
                        break;
                    }
                }

                if(!match){
                    match = leftOverFragment->meets(subject, predicate, object);
                    //Hash the triple and assign it to a random host (Very simple hash function)
                    unsigned hash = (subject + predicate + object) % summary->size();
                    PartitionsSummaryMap::iterator hashIt = summary->begin();
                    for(unsigned k = 0; k < hash; ++k) ++hashIt;
                    hostSlot = hashIt->first;
                }


                temporalIds[hostSlot].push_back(subject);
                temporalIds[hostSlot].push_back(predicate);
                temporalIds[hostSlot].push_back(object);

                vector<unsigned> &theIds = temporalIds[hostSlot];
                //Flush the values
                if(theIds.size() >= batchSize){                            
                    //Here write to the temporary data structures!!!
                    //This part must be synchronized!!
                    boost::mutex *involvedMutex = (*mutexes)[hostSlot];
                    boost::unique_lock<boost::mutex> lock(*involvedMutex);

                    for(vector<unsigned>::iterator sit = theIds.begin(); sit != theIds.end(); sit = sit + 3){
                        string subjectText, predicateText, objectText;
                        Type::ID typeSubject, typePredicate, typeObject;
                        unsigned subtypeSubject, subtypePredicate, subtypeObject;   

                        bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                        bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                        bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  

                        if(subFound && predFound && objFound){
                            (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                            (*outputStreams)[hostSlot]->write(" ", 1);
                            (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                            (*outputStreams)[hostSlot]->write(" ", 1);
                            (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                            (*outputStreams)[hostSlot]->write(" .\n", 3); 
                        }

                    }

                    temporalIds[hostSlot].clear();
                    lock.unlock();                        
                }

                if(!match){
                    unsigned fsubtype;
                    Type::ID ftype;
                    string ftext;
                    cout << "Warning, triple " << subject << ", " << predicate << ", " << object << " could not be allocated " << endl;
                    lookupById(*this->dbds.db, subject, ftype, fsubtype, ftext);
                    cout << " " << ftext;
                    lookupById(*this->dbds.db, predicate, ftype, fsubtype, ftext);
                    cout << " " << ftext;
                    lookupById(*this->dbds.db, object, ftype, fsubtype, ftext);
                    cout << " " << ftext << endl;
                }            


                if(!scan.next()) break;
                subject = scan.getValue2();
                predicate = scan.getValue1();
                object = scan.getValue3();  
            }

            //At the end there might be some unflushed entries
            for(map<string, vector<unsigned> >::iterator tit = temporalIds.begin(); tit != temporalIds.end(); ++tit){
                string hostSlot = tit->first;            
                boost::mutex *involvedMutex = (*mutexes)[hostSlot];
                boost::unique_lock<boost::mutex> lock(*involvedMutex);

                for(vector<unsigned>::iterator sit = tit->second.begin(); sit != tit->second.end(); sit = sit + 3){
                    string subjectText, predicateText, objectText;
                    Type::ID typeSubject, typePredicate, typeObject;
                    unsigned subtypeSubject, subtypePredicate, subtypeObject;

                    bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                    bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                    bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  

                    if(subFound && predFound && objFound){
                        (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                        (*outputStreams)[hostSlot]->write(" ", 1);
                        (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                        (*outputStreams)[hostSlot]->write(" ", 1);
                        (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                        (*outputStreams)[hostSlot]->write(" .\n", 3); 
                    }
                }

                temporalIds[hostSlot].clear();
                lock.unlock();        
            }        
        }else{
            assert(0);
        }
        
        propertiesLock.lock();
        empty = this->properties.empty();
        if(!empty){
            key = this->properties.front();
            this->properties.pop();
        }
        
        propertiesLock.unlock();   
    }
}        

void RDFFragmentBuilder::buildSingleFragment4(unsigned key, map<string, fstream*> *outputStreams, map<string, DictionaryLookupAndBackup*> *tmpDictionaries,
    map<string, boost::mutex*> *mutexes, PartitionsSummaryMap *summary, Fragment *leftOverFragment){
    FactsSegment &facts = dbds.db->getFacts(Database::Order_Predicate_Subject_Object);
    FactsSegment::Scan scan;
    map<string, vector<unsigned> > temporalIds;
    unsigned batchSize = 64 * 3;
    
    for(map<string, fstream*>::iterator fit = outputStreams->begin(); fit != outputStreams->end(); ++fit){
        temporalIds[fit->first] = vector<unsigned>();
        temporalIds[fit->first].reserve(batchSize);
    }        
    
    //We will scan only the triples having this key as predicate
    if (scan.first(facts, key, 0, 0)){
        unsigned subject, predicate, object;  
        predicate = scan.getValue1();
        subject = scan.getValue2();
        object = scan.getValue3();
        //Once we have covered the region, break
        while(predicate == key){
            bool match = false;
            string hostSlot;
            for(PartitionsSummaryMap::iterator fit = summary->begin(); fit != summary->end(); ++fit){                
                if(fit->second.meets(subject, predicate, object)){
                    hostSlot =  fit->first;
                    match = true;
                    break;
                }
            }
            
            if(!match){
                match = leftOverFragment->meets(subject, predicate, object);
                //Hash the triple and assign it to a random host (Very simple hash function)
                unsigned hash = (subject + predicate + object) % summary->size();
                PartitionsSummaryMap::iterator hashIt = summary->begin();
                for(unsigned k = 0; k < hash; ++k) ++hashIt;
                hostSlot = hashIt->first;
            }
           
                        
            temporalIds[hostSlot].push_back(subject);
            temporalIds[hostSlot].push_back(predicate);
            temporalIds[hostSlot].push_back(object);
                        
            vector<unsigned> &theIds = temporalIds[hostSlot];
            //Flush the values
            if(theIds.size() >= batchSize){                            
                //Here write to the temporary data structures!!!
                //This part must be synchronized!!
                boost::mutex *involvedMutex = (*mutexes)[hostSlot];
                boost::unique_lock<boost::mutex> lock(*involvedMutex);

                for(vector<unsigned>::iterator sit = theIds.begin(); sit != theIds.end(); sit = sit + 3){
                    string subjectText, predicateText, objectText;
                    Type::ID typeSubject, typePredicate, typeObject;
                    unsigned subtypeSubject, subtypePredicate, subtypeObject;   

                    bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                    bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                    bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  

                    if(subFound && predFound && objFound){
                        (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                        (*outputStreams)[hostSlot]->write(" ", 1);
                        (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                        (*outputStreams)[hostSlot]->write(" ", 1);
                        (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                        (*outputStreams)[hostSlot]->write(" .\n", 3); 
                    }

                }

                temporalIds[hostSlot].clear();
                lock.unlock();                        
            }

            if(!match){
                unsigned fsubtype;
                Type::ID ftype;
                string ftext;
                cout << "Warning, triple " << subject << ", " << predicate << ", " << object << " could not be allocated " << endl;
                lookupById(*this->dbds.db, subject, ftype, fsubtype, ftext);
                cout << " " << ftext;
                lookupById(*this->dbds.db, predicate, ftype, fsubtype, ftext);
                cout << " " << ftext;
                lookupById(*this->dbds.db, object, ftype, fsubtype, ftext);
                cout << " " << ftext << endl;
            }            

            
            if(!scan.next()) break;
            subject = scan.getValue2();
            predicate = scan.getValue1();
            object = scan.getValue3();  
        }
        
        //At the end there might be some unflushed entries
        for(map<string, vector<unsigned> >::iterator tit = temporalIds.begin(); tit != temporalIds.end(); ++tit){
            string hostSlot = tit->first;            
            boost::mutex *involvedMutex = (*mutexes)[hostSlot];
            boost::unique_lock<boost::mutex> lock(*involvedMutex);

            for(vector<unsigned>::iterator sit = tit->second.begin(); sit != tit->second.end(); sit = sit + 3){
                string subjectText, predicateText, objectText;
                Type::ID typeSubject, typePredicate, typeObject;
                unsigned subtypeSubject, subtypePredicate, subtypeObject;

                bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  

                if(subFound && predFound && objFound){
                    (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                    (*outputStreams)[hostSlot]->write(" ", 1);
                    (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                    (*outputStreams)[hostSlot]->write(" ", 1);
                    (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                    (*outputStreams)[hostSlot]->write(" .\n", 3); 
                }
            }

            temporalIds[hostSlot].clear();
            lock.unlock();        
        }        
    }else{
        assert(0);
    }
    
}

void RDFFragmentBuilder::buildSingleFragment3(unsigned key, map<string, fstream*> *outputStreams, map<string, DictionaryLookupAndBackup*> *tmpDictionaries,
    map<string, boost::mutex*> *mutexes, FragmentsAllocationMap *allocMap){
    FactsSegment &facts = dbds.db->getFacts(Database::Order_Predicate_Subject_Object);
    FactsSegment::Scan scan;
    map<string, vector<unsigned> > temporalIds;
    unsigned batchSize = 1024 * 3;
    
    for(map<string, fstream*>::iterator fit = outputStreams->begin(); fit != outputStreams->end(); ++fit){
        temporalIds[fit->first] = vector<unsigned>();
        temporalIds[fit->first].reserve(batchSize);
    }        
    
    //We will scan only the triples having this key as predicate
    if (scan.first(facts, key, 0, 0)){
        unsigned subject, predicate, object;  
        predicate = scan.getValue1();
        subject = scan.getValue2();
        object = scan.getValue3();
        //Once we have covered the region, break
        while(predicate == key){
            bool match = false;
            for(FragmentsAllocationMap::iterator fit = allocMap->begin(); fit != allocMap->end(); ++fit){
                for(vector<FragmentAllocationAssign>::iterator faait = fit->second.begin(); faait != fit->second.end(); ++faait){                                        
                    if(faait->fragment->meets(subject, predicate, object)){      
                        faait->fragment->hit();
                        string hostSlot;

                        if(!faait->fragment->isLeftOverFragment()){
                            hostSlot = fit->first;
                        }else{
                            //Hash the triple and assign it to a random host (Very simple hash function)
                            unsigned hash = (subject + predicate + object) % allocMap->size();
                            FragmentsAllocationMap::iterator hashIt = allocMap->begin();
                            for(unsigned k = 0; k < hash; ++k) ++hashIt;
                            hostSlot = hashIt->first;                            
                        }
                        
                        temporalIds[hostSlot].push_back(subject);
                        temporalIds[hostSlot].push_back(predicate);
                        temporalIds[hostSlot].push_back(object);
                        
                        vector<unsigned> &theIds = temporalIds[hostSlot];
                        //Flush the values
                        if(theIds.size() >= batchSize){                            
                            //Here write to the temporary data structures!!!
                            //This part must be synchronized!!
                            boost::mutex *involvedMutex = (*mutexes)[hostSlot];
                            boost::unique_lock<boost::mutex> lock(*involvedMutex);
                            
                            for(vector<unsigned>::iterator sit = theIds.begin(); sit != theIds.end(); sit = sit + 3){
                                string subjectText, predicateText, objectText;
                                Type::ID typeSubject, typePredicate, typeObject;
                                unsigned subtypeSubject, subtypePredicate, subtypeObject;   

                                bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                                bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                                bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  
                                                  
                                if(subFound && predFound && objFound){
                                    (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                                    (*outputStreams)[hostSlot]->write(" ", 1);
                                    (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                                    (*outputStreams)[hostSlot]->write(" ", 1);
                                    (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                                    (*outputStreams)[hostSlot]->write(" .\n", 3); 
                                }
                                
                            }

                            temporalIds[hostSlot].clear();
                            lock.unlock();                        
                        }
                        

                        match = true;
                        break;
                    }
                }                
            }
            //Every triple has to match a node, otherwise the fragmentation would be not complete
            if(!match){
                unsigned fsubtype;
                Type::ID ftype;
                string ftext;
                cout << "Warning, triple " << subject << ", " << predicate << ", " << object << " could not be allocated " << endl;
                lookupById(*this->dbds.db, subject, ftype, fsubtype, ftext);
                cout << " " << ftext;
                lookupById(*this->dbds.db, predicate, ftype, fsubtype, ftext);
                cout << " " << ftext;
                lookupById(*this->dbds.db, object, ftype, fsubtype, ftext);
                cout << " " << ftext << endl;
            }            
            
            if(!scan.next()) break;
            subject = scan.getValue2();
            predicate = scan.getValue1();
            object = scan.getValue3();  
        }
        
        //At the end there might be some unflushed entries
        for(map<string, vector<unsigned> >::iterator tit = temporalIds.begin(); tit != temporalIds.end(); ++tit){
            string hostSlot = tit->first;            
            boost::mutex *involvedMutex = (*mutexes)[hostSlot];
            boost::unique_lock<boost::mutex> lock(*involvedMutex);

            for(vector<unsigned>::iterator sit = tit->second.begin(); sit != tit->second.end(); sit = sit + 3){
                string subjectText, predicateText, objectText;
                Type::ID typeSubject, typePredicate, typeObject;
                unsigned subtypeSubject, subtypePredicate, subtypeObject;

                bool subFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *sit , typeSubject, subtypeSubject, subjectText);
                bool predFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 1), typePredicate, subtypePredicate, predicateText);                                        
                bool objFound = (*(*tmpDictionaries)[hostSlot])(*this->dbds.db, *(sit + 2), typeObject, subtypeObject, objectText);  

                if(subFound && predFound && objFound){
                    (*outputStreams)[hostSlot]->write(subjectText.c_str(), subjectText.size());
                    (*outputStreams)[hostSlot]->write(" ", 1);
                    (*outputStreams)[hostSlot]->write(predicateText.c_str(), predicateText.size());
                    (*outputStreams)[hostSlot]->write(" ", 1);
                    (*outputStreams)[hostSlot]->write(objectText.c_str(), objectText.size());
                    (*outputStreams)[hostSlot]->write(" .\n", 3); 
                }
            }

            temporalIds[hostSlot].clear();
            lock.unlock();        
        }
        
    }else{
        assert(0);
    }
}

void RDFFragmentBuilder::buildFragments5(FragmentsAllocationMap &allocMap){
    FullyAggregatedFactsSegment &fas = this->dbds.db->getFullyAggregatedFacts(Database::Order_Predicate_Subject_Object);
    FullyAggregatedFactsSegment::Scan fasScan;
    string basePrefix = this->dbds.dbBasename();
    map<string, fstream*> outputStreams;    
    map<string, DictionaryLookupAndBackup*> tmpDictionaries;
    map<string, string> temporalFiles;
    map<string, boost::mutex*> mutexes;
    Fragment *leftOverFragment;
    
    //Let's remove the leftover fragment
    for(FragmentsAllocationMap::iterator amit = allocMap.begin(); amit != allocMap.end(); ++amit){
        for(vector<FragmentAllocationAssign>::iterator ait = amit->second.begin(); ait != amit->second.end(); ++ait){
            if(ait->fragment->isLeftOverFragment()){
                leftOverFragment = ait->fragment;
                amit->second.erase(ait);
                break;
            }
        }
    }
    
    //Extract the partition summary
    PartitionsSummaryMap summary;
    PartitionSummary::summarize(allocMap, summary);
               
    cout << "Nodes summary" << endl;
    cout << summary;
    //My powerful development machine at MMCI does not have an updated version of boost :( 
    #if (BOOST_VERSION / 100 % 1000) >= 47
        unsigned nThreads = thread::hardware_concurrency();    
    #else
        unsigned nThreads = 1;
    #endif

    //First initialize the data structures for fragments generation
    for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
        string dbfile = basePrefix + "_fragment_" + fit->first;              
        string tmpFile = tmpPrefix + basePrefix +"_fragment_" + fit->first + ".frag";        
        fstream *filestr = new fstream(tmpFile.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
        tmpDictionaries[fit->first] = new DictionaryLookupAndBackup(tmpPrefix + dbfile + ".dict", DictionaryLookupAndBackup::defaultBackupLimit);
        outputStreams[fit->first] = filestr;
        temporalFiles[fit->first] = tmpFile;
        mutexes[fit->first] = new boost::mutex();
        assert(filestr->good());
    }
        
    //Get the values
    if(fasScan.first(fas)){
        do{
            properties.push(fasScan.getValue1());
        }while(fasScan.next());
    }else{
        //This cannot happen, then it means there is something wrong with the database
        assert(0);
    }    

    {
        thread **works = new thread*[nThreads];        
        for(unsigned i = 0; i < nThreads; ++i){
            works[i] = new thread(&RDFFragmentBuilder::buildSingleFragment5, this, &outputStreams, &tmpDictionaries, &mutexes, &summary, leftOverFragment);
        }        

        for(unsigned i = 0; i < nThreads; ++i){
            works[i]->join();
        }

        for(unsigned i = 0; i < nThreads; ++i){
            delete works[i];
        }

        delete[] works;
        
        //Close the temporal files
        for(map<string, fstream*>::iterator tfit = outputStreams.begin(); tfit != outputStreams.end(); ++tfit){
            tfit->second->close();
        }                
    }
    //Rounds to generate the partitions in RDF-3x format    
    {        
        unsigned nParts = allocMap.size();
        unsigned rounds = nParts / nThreads;
        unsigned remain = nParts % nThreads;
        if(remain > 0) rounds++;
        map<string, string>::iterator amit  = temporalFiles.begin();  
        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;

            thread **works = new thread*[nJobs];
            for(unsigned i = 0; i < nJobs; ++i){
                cout << "Scheduling fragments for node " << amit->first << endl;
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap);
                #else
                boost::bind(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap)();
                #endif
                amit++;
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;     
        }
    }
        
    //Clean memory
    while(!tmpDictionaries.empty()){
        DictionaryLookupAndBackup *toRemove = tmpDictionaries.begin()->second;
        tmpDictionaries.erase(tmpDictionaries.begin());
        delete toRemove;
    }
    
    while(!outputStreams.empty()){
        fstream *toRemove = outputStreams.begin()->second;
        outputStreams.erase(outputStreams.begin());
        delete toRemove;
    }
    
    for(map<string, string>::iterator it = temporalFiles.begin(); it != temporalFiles.end(); ++it){
        cout << "Removing temporal partition file " << it->second << endl;
        remove(it->second.c_str());
    }
    
    while(!mutexes.empty()){
        boost::mutex *toRemove = mutexes.begin()->second;
        mutexes.erase(mutexes.begin());
        delete toRemove;        
    }       
    
}

void RDFFragmentBuilder::buildFragments4(FragmentsAllocationMap &allocMap){
    vector<unsigned> properties;
    FullyAggregatedFactsSegment &fas = this->dbds.db->getFullyAggregatedFacts(Database::Order_Predicate_Subject_Object);
    FullyAggregatedFactsSegment::Scan fasScan;
    string basePrefix = this->dbds.dbBasename();
    map<string, fstream*> outputStreams;    
    map<string, DictionaryLookupAndBackup*> tmpDictionaries;
    map<string, string> temporalFiles;
    map<string, boost::mutex*> mutexes;
    Fragment *leftOverFragment;
    
    //Let's remove the leftover fragment
    for(FragmentsAllocationMap::iterator amit = allocMap.begin(); amit != allocMap.end(); ++amit){
        for(vector<FragmentAllocationAssign>::iterator ait = amit->second.begin(); ait != amit->second.end(); ++ait){
            if(ait->fragment->isLeftOverFragment()){
                leftOverFragment = ait->fragment;
                amit->second.erase(ait);
                break;
            }
        }
    }
    
    //Extract the partition summary
    PartitionsSummaryMap summary;
    PartitionSummary::summarize(allocMap, summary);
               
    cout << "Nodes summary" << endl;
    cout << summary;
    //My powerful development machine at MMCI does not have an updated version of boost :( 
    #if (BOOST_VERSION / 100 % 1000) >= 47
        unsigned nThreads = thread::hardware_concurrency();    
    #else
        unsigned nThreads = 1;
    #endif

    //First initialize the data structures for fragments generation
    for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
        string dbfile = basePrefix + "_fragment_" + fit->first;              
        string tmpFile = tmpPrefix + basePrefix +"_fragment_" + fit->first + ".frag";        
        fstream *filestr = new fstream(tmpFile.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
        tmpDictionaries[fit->first] = new DictionaryLookupAndBackup(tmpPrefix + dbfile + ".dict", DictionaryLookupAndBackup::defaultBackupLimit);
        outputStreams[fit->first] = filestr;
        temporalFiles[fit->first] = tmpFile;
        mutexes[fit->first] = new boost::mutex();
        assert(filestr->good());
    }
        
    //Get the values
    if(fasScan.first(fas)){
        do{
            properties.push_back(fasScan.getValue1());
        }while(fasScan.next());
    }else{
        //This cannot happen, then it means there is something wrong with the database
        assert(0);
    }
    

    //Rounds to generate .n3 intermediate partition files        
    {
        unsigned nParts = properties.size();
        unsigned rounds = nParts / nThreads;
        unsigned remain = nParts % nThreads;
        if(remain > 0) rounds++;

        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;
            thread **works = new thread*[nJobs];

            //Every thread will be in charge of a subsection of the scan        
            for(unsigned i = 0; i < nJobs; ++i){
                unsigned regionKey = properties[j * nThreads + i];
                cout << "Scheduling region with property " << regionKey << endl;
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSingleFragment4, this, regionKey, &outputStreams, &tmpDictionaries, &mutexes, &summary, leftOverFragment);
                #else
                boost::bind(&RDFFragmentBuilder::buildSingleFragment4, this, regionKey, &outputStreams, &tmpDictionaries, &mutexes, &summary, leftOverFragment)();
                #endif
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;
        }

        //Close the temporal files
        for(map<string, fstream*>::iterator tfit = outputStreams.begin(); tfit != outputStreams.end(); ++tfit){
            tfit->second->close();
        }
    }
    
    //Rounds to generate the partitions in RDF-3x format    
    {        
        unsigned nParts = allocMap.size();
        unsigned rounds = nParts / nThreads;
        unsigned remain = nParts % nThreads;
        if(remain > 0) rounds++;
        map<string, string>::iterator amit  = temporalFiles.begin();  
        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;

            thread **works = new thread*[nJobs];
            for(unsigned i = 0; i < nJobs; ++i){
                cout << "Scheduling fragments for node " << amit->first << endl;
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap);
                #else
                boost::bind(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap)();
                #endif
                amit++;
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;     
        }
    }
        
    //Clean memory
    while(!tmpDictionaries.empty()){
        DictionaryLookupAndBackup *toRemove = tmpDictionaries.begin()->second;
        tmpDictionaries.erase(tmpDictionaries.begin());
        delete toRemove;
    }
    
    while(!outputStreams.empty()){
        fstream *toRemove = outputStreams.begin()->second;
        outputStreams.erase(outputStreams.begin());
        delete toRemove;
    }
    
    for(map<string, string>::iterator it = temporalFiles.begin(); it != temporalFiles.end(); ++it){
        cout << "Removing temporal partition file " << it->second << endl;
        remove(it->second.c_str());
    }
    
    while(!mutexes.empty()){
        boost::mutex *toRemove = mutexes.begin()->second;
        mutexes.erase(mutexes.begin());
        delete toRemove;        
    }   
}

void RDFFragmentBuilder::buildFragments3(FragmentsAllocationMap &allocMap, FragmentFilesAllocationMap &filenames){
    vector<unsigned> properties;
    FullyAggregatedFactsSegment &fas = this->dbds.db->getFullyAggregatedFacts(Database::Order_Predicate_Subject_Object);
    FullyAggregatedFactsSegment::Scan fasScan;
    string basePrefix = this->dbds.dbBasename();
    map<string, fstream*> outputStreams;    
    map<string, DictionaryLookupAndBackup*> tmpDictionaries;
    map<string, string> temporalFiles;
    map<string, boost::mutex*> mutexes;
    //My powerful development machine at MMCI does not have an updated version of boost :( 
    #if (BOOST_VERSION / 100 % 1000) >= 47
        unsigned nThreads = thread::hardware_concurrency();    
    #else
        unsigned nThreads = 1;
    #endif

    //First initialize the data structures for fragments generation
    for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
        string dbfile = basePrefix + "_fragment_" + fit->first;              
        string tmpFile = tmpPrefix + basePrefix +"_fragment_" + fit->first + ".frag";        
        fstream *filestr = new fstream(tmpFile.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
        tmpDictionaries[fit->first] = new DictionaryLookupAndBackup(tmpPrefix + dbfile + ".dict", DictionaryLookupAndBackup::defaultBackupLimit);
        outputStreams[fit->first] = filestr;
        temporalFiles[fit->first] = tmpFile;
        mutexes[fit->first] = new boost::mutex();
        assert(filestr->good());
    }
        
    //Get the values
    if(fasScan.first(fas)){
        do{
            properties.push_back(fasScan.getValue1());
        }while(fasScan.next());
    }else{
        //This cannot happen, then it means there is something wrong with the database
        assert(0);
    }
    

    //Rounds to generate .n3 intermediate partition files        
    {
        unsigned nParts = properties.size();
        unsigned rounds = nParts / nThreads;
        unsigned remain = nParts % nThreads;
        if(remain > 0) rounds++;

        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;
            thread **works = new thread*[nJobs];

            //Every thread will be in charge of a subsection of the scan        
            for(unsigned i = 0; i < nJobs; ++i){
                unsigned regionKey = properties[j * nThreads + i];
                cout << "Scheduling job for region with property: " << regionKey  << endl;            
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSingleFragment3, this, regionKey, &outputStreams, &tmpDictionaries, &mutexes, &allocMap);
                #else
                boost::bind(&RDFFragmentBuilder::buildSingleFragment3, this, regionKey, &outputStreams, &tmpDictionaries, &mutexes, &allocMap)();
                #endif
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;
        }

        //Close the temporal files
        for(map<string, fstream*>::iterator tfit = outputStreams.begin(); tfit != outputStreams.end(); ++tfit){
            tfit->second->close();
        }
    }
    
    //Rounds to generate the partitions in RDF-3x format    
    {        
        unsigned nParts = allocMap.size();
        unsigned rounds = nParts / nThreads;
        unsigned remain = nParts % nThreads;
        if(remain > 0) rounds++;
        map<string, string>::iterator amit  = temporalFiles.begin();  
        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;

            thread **works = new thread*[nJobs];
            for(unsigned i = 0; i < nJobs; ++i){
                cout << "Scheduling fragments for node " << amit->first << endl;
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap);
                #else
                boost::bind(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap)();
                #endif
                amit++;
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;     
        }
    }
    
    //Generate output file with fragments summary
    string fragSumFile = "fragments_" + this->dbds.dbBasename();
    fstream fragSumStream(fragSumFile.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
    for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
        ostringstream line;
        line << fit->first + ",";
        unsigned count = 0;
        for(vector<FragmentAllocationAssign>::iterator faait = fit->second.begin(); faait != fit->second.end(); ++faait){       
            count += faait->fragment->getObservedCardinality();
        }
        line << count << endl;
        fragSumStream.write(line.str().c_str(), line.str().size());
    }
    fragSumStream.close();
    
    //Clean memory
    while(!tmpDictionaries.empty()){
        DictionaryLookupAndBackup *toRemove = tmpDictionaries.begin()->second;
        tmpDictionaries.erase(tmpDictionaries.begin());
        delete toRemove;
    }
    
    while(!outputStreams.empty()){
        fstream *toRemove = outputStreams.begin()->second;
        outputStreams.erase(outputStreams.begin());
        delete toRemove;
    }
    
    for(map<string, string>::iterator it = temporalFiles.begin(); it != temporalFiles.end(); ++it){
        cout << "Removing temporal partition file " << it->second << endl;
        remove(it->second.c_str());
    }
    
    while(!mutexes.empty()){
        boost::mutex *toRemove = mutexes.begin()->second;
        mutexes.erase(mutexes.begin());
        delete toRemove;        
    }
}

void RDFFragmentBuilder::buildFragments2(FragmentsAllocationMap &allocMap, FragmentFilesAllocationMap &filenames){    
    FactsSegment::Scan scan;
    string basePrefix = this->dbds.dbBasename();
    map<string, fstream*> outputStreams;    
    map<string, DictionaryLookupAndBackup*> tmpDictionaries;
    map<string, string> temporalFiles;

    for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
        string dbfile = basePrefix + "_fragment_" + fit->first;              
        string tmpFile = tmpPrefix + basePrefix +"_fragment_" + fit->first + ".frag";        
        fstream *filestr = new fstream(tmpFile.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
        tmpDictionaries[fit->first] = new DictionaryLookupAndBackup(dbfile + ".dict", DictionaryLookupAndBackup::defaultBackupLimit);
        outputStreams[fit->first] = filestr;
        temporalFiles[fit->first] = tmpFile;
        assert(filestr->good());
    }
    
    if (scan.first(dbds.db->getFacts(Database::Order_Subject_Predicate_Object))){
        do {
            unsigned subject, predicate, object;  
            subject = scan.getValue1();
            predicate = scan.getValue2();
            object = scan.getValue3();
            bool match = false;
            for(FragmentsAllocationMap::iterator fit = allocMap.begin(); fit != allocMap.end(); ++fit){
                for(vector<FragmentAllocationAssign>::iterator faait = fit->second.begin(); faait != fit->second.end(); ++faait){                                        
                    if(faait->fragment->meets(subject, predicate, object)){ 
                        string subjectText, predicateText, objectText;
                        Type::ID typeSubject, typePredicate, typeObject;
                        unsigned subtypeSubject, subtypePredicate, subtypeObject;                
                        (*tmpDictionaries[fit->first])(*this->dbds.db, subject, typeSubject, subtypeSubject, subjectText);
                        (*tmpDictionaries[fit->first])(*this->dbds.db, predicate, typePredicate, subtypePredicate, predicateText);                                        
                        (*tmpDictionaries[fit->first])(*this->dbds.db, object, typeObject, subtypeObject, objectText);                        
                        outputStreams[fit->first]->write(subjectText.c_str(), subjectText.size());
                        outputStreams[fit->first]->write(" ", 1);
                        outputStreams[fit->first]->write(predicateText.c_str(), predicateText.size());
                        outputStreams[fit->first]->write(" ", 1);
                        outputStreams[fit->first]->write(objectText.c_str(), objectText.size());
                        outputStreams[fit->first]->write(" .\n", 3);        
                        faait->fragment->hit();
                        match = true;
                        break;
                    }
                }                
            }
            //Every triple has to match a node, otherwise the fragmentation would be not complete
            assert(match);
        //Now check which node should store this
        }while (scan.next());

        //Close the temporal files
        for(map<string, fstream*>::iterator tfit = outputStreams.begin(); tfit != outputStreams.end(); ++tfit)
            tfit->second->close();
        
        //My powerful development machine at MMCI does not have an updated version of boost :( 
        #if (BOOST_VERSION / 100 % 1000) >= 47
            unsigned nThreads = thread::hardware_concurrency();    
        #else
            unsigned nThreads = 1;
        #endif
        cout << "Using " << nThreads << " threads " << endl;
        unsigned nNodes = allocMap.size();
        unsigned rounds = nNodes / nThreads;
        unsigned remain = nNodes % nThreads;
        if(remain > 0) rounds++;
        
        map<string, string>::iterator amit  = temporalFiles.begin();  
        for(unsigned j = 0; j < rounds; ++j){
            unsigned nJobs = ( j == (rounds - 1) && (remain > 0) )? remain : nThreads;

            thread **works = new thread*[nJobs];
            for(unsigned i = 0; i < nJobs; ++i){
                cout << "Scheduling fragments for node " << amit->first << endl;
                #if (BOOST_VERSION / 100 % 1000) >= 47
                works[i] = new thread(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap);
                #else
                boost::bind(&RDFFragmentBuilder::buildSinglePartition, this, amit->first, amit->second, &allocMap)();
                #endif
                amit++;
            }

            #if (BOOST_VERSION / 100 % 1000) >= 47        
            for(unsigned i = 0; i < nJobs; ++i){
                works[i]->join();
            }

            for(unsigned i = 0; i < nJobs; ++i){
                delete works[i];
            }
            #endif

            delete[] works;     
        }
    }
    
    //Clean memory
    while(!tmpDictionaries.empty()){
        DictionaryLookupAndBackup *toRemove = tmpDictionaries.begin()->second;
        tmpDictionaries.erase(tmpDictionaries.begin());
        delete toRemove;
    }
    
    while(!outputStreams.empty()){
        fstream *toRemove = outputStreams.begin()->second;
        cout << "Removing temporal file " << outputStreams.begin()->first << endl;
        remove(outputStreams.begin()->first.c_str());            
        outputStreams.erase(outputStreams.begin());
        delete toRemove;
    }
}

bool RDFFragmentBuilder::runQuery(Database& db, const string& query, vector<vector<string> > &values, DictionaryLookupAndBackup &lookupByIdAndStore, bool showCardinalities = false){
    QueryGraph queryGraph;
    {
        // Parse the query
        SPARQLLexer lexer(query);
        SPARQLParser parser(lexer);
        parser.parse();

        SemanticAnalysis semana(db);
        semana.transform(parser,queryGraph);

        if (queryGraph.knownEmpty()) {
            return false;
        }
    }
   
    if(showCardinalities)
        queryGraph.setDuplicateHandling(QueryGraph::AllDuplicates);
    else
        queryGraph.setDuplicateHandling(QueryGraph::NoDuplicates);

   
    // Run the optimizer
    PlanGen plangen;
    Plan* plan=plangen.translate(db,queryGraph);
    if (!plan) {
        cerr << "internal error plan generation failed" << endl;
        return false;
    }

    // Build a physical plan
    Runtime runtime(db);
    std::vector<Register*> output;   
    Operator* operatorTree = CodeGen().translateIntern(runtime,queryGraph,plan,output);
    // And execute it      
    vector<string> row;
    unsigned int card;
    if ((card = operatorTree->first()) != 0){
        do {
            row.clear();
            ostringstream sstr;
            sstr << card;      
            for (vector<Register*>::const_iterator iter=output.begin(),limit=output.end();iter!=limit;++iter) {
                unsigned v=(*iter)->value;
                if (!~v) {
                    break;
                } else {
                    Type::ID type; unsigned subType;
                    string fullResult;
                    if(!lookupByIdAndStore(db, v, type, subType, fullResult)){
                        break;
                    }  
                    row.push_back(fullResult);
                }
            }
            if(showCardinalities)
                row.push_back(sstr.str());

            values.push_back(row);            
        } while ((card = operatorTree->next()) != 0);
    }
   
    delete operatorTree;
    return true;
}

RDFFragmentBuilder::DictionaryLookupAndBackup::DictionaryLookupAndBackup(string backupFile, unsigned backupLimit): backupLimit(backupLimit), storedEntries(0){
    this->tempFile = new TempFile(backupFile);
}

RDFFragmentBuilder::DictionaryLookupAndBackup::~DictionaryLookupAndBackup(){
    this->flush();
    this->tempFile->close();
    delete tempFile;
}

void RDFFragmentBuilder::DictionaryLookupAndBackup::flush(){
    for(map<unsigned, DictionaryEntry>::iterator cit = this->cache.begin(); cit != this->cache.end(); ++cit){
        this->tempFile->writeId(static_cast<uint64_t>(cit->first));
        this->tempFile->writeId(static_cast<uint64_t>(cit->second.type));
        this->tempFile->writeId(static_cast<uint64_t>(cit->second.subtype));
        this->tempFile->writeString(cit->second.value.size(), cit->second.value.c_str());
    }
    this->tempFile->flush();
    this->cache.clear();
}

bool RDFFragmentBuilder::DictionaryLookupAndBackup::operator()(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result){    
    string rawResult;
    if(lookupById(db, id, type, subType, rawResult, result)){
        if(this->cache.size() >= cacheMax){
            this->flush();
        }        
        return true;
    }    
    
    return false;
}

static inline string buildString(const char *&start, const char *&stop){
    string result;
    const char *ct = start;
    while(ct != stop){
        result.append(1, *ct);
        ++ct;
    }
    
    return result;
}

bool RDFFragmentBuilder::DictionaryLookupAndBackup::lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result, string &result2){
    const char* start, *stop;
    const char* start2 = NULL, *stop2 = NULL; 
    Type::ID type2; 
    unsigned subType2;  
    DifferentialIndex diff(db);    
    bool foundInCache = false;
    map<unsigned, DictionaryEntry>::iterator it1, it2;
    //First check in the cache
    it1 = cache.find(id);
    if(it1 != cache.end()){
        type = it1->second.type;
        subType = it1->second.subtype;
        start = it1->second.value.c_str();
        stop = start + it1->second.value.size();        
        foundInCache = true;
    }else{
        if (!db.getDictionary().lookupById(id, start, stop, type, subType)){
            if(!diff.lookupById(id, start, stop, type, subType))
                return false;
        }                
    }

    result = buildString(start,stop);      
    if(!foundInCache && this->storedEntries < this->backupLimit)
        this->store(id, type, subType, result);
    
    switch (type) {
       case Type::URI: result2 = "<" + escapeURI(start, stop) + ">";  break;
       case Type::Literal: result2 = "\"" + escape(start,stop) + "\""; break;
       case Type::String:  result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#string>"; break;
       case Type::Integer: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#integer>"; break;
       case Type::Decimal: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#decimal>"; break;
       case Type::Double:  result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#double>"; break;
       case Type::Boolean: result2 = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#boolean>"; break;
       case Type::CustomLanguage: case Type::CustomType: 
           result2 = "\""+escape(start,stop)+"\""; 
           it2 = cache.find(subType);
           if(it2 != cache.end()){
               start2 = it2->second.value.c_str();
               stop2 = start2 + it2->second.value.size();
           }else{
               //Lookup language
               if(db.getDictionary().lookupById(subType, start2, stop2, type2, subType2)){
                   string subtypeText = buildString(start2, stop2);
                   if(this->storedEntries < this->backupLimit)
                       this->store(subType, type2, subType2, subtypeText);
               }               
           }
           
           if(start2 != NULL && stop2 != NULL){
               if(type == Type::CustomLanguage)
                   result2 += "@" + escape(start2, stop2);           
               else if(type == Type::CustomType)
                   result2 += "^^<" + escape(start2, stop2) + ">";                          
           }
           
           break;                
    }
    
    return true;
}

void RDFFragmentBuilder::DictionaryLookupAndBackup::store(unsigned int id, Type::ID &type, unsigned &subType, string &result){
    map<unsigned, DictionaryEntry>::iterator diit = this->cache.find(id);
    if(diit != this->cache.end()){
        diit->second.type = type;
        diit->second.subtype = subType;
        diit->second.value = result;
        ++storedEntries;
    }else{
        this->cache[id].type = type;
        this->cache[id].subtype = subType;
        this->cache[id].value = result;
    }
}

