/* 
 * File:   JoinOccurrenceExtractor.cpp
 * Author: luis
 * Created on May 22, 2011, 1:05 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "JoinOccurrenceExtractor.hpp"

JoinOccurrenceExtractor::JoinOccurrenceExtractor() {
}

JoinOccurrenceExtractor::JoinOccurrenceExtractor(const JoinOccurrenceExtractor& orig) {
}

JoinOccurrenceExtractor::~JoinOccurrenceExtractor() {
}

