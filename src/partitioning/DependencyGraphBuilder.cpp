/* 
 * File:   DependencyGraphBuilder.cpp
 * Author: luis
 * Created on June 7, 2011, 1:38 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/DependencyGraphBuilder.hpp"
#include <iostream>

using namespace stdpartitioning;
using namespace std;


DependencyGraphBuilder::DependencyGraphBuilder(){}

void DependencyGraphBuilder::extractDependencies(QueryGraph &query, PredicateDependencyGraph &prdg, 
        PatternDependencyGraph &pndg){
    vector<QueryGraph::SubQuery> subqueries;
    vector<QueryGraph::SubQuery>::iterator sit;
    vector<QueryGraph::Node>::iterator nit;
            
    this->extractIndependentSubqueries(query.getQuery(), subqueries);
    for(sit = subqueries.begin(); sit != subqueries.end(); ++sit){
        //Assign a unique id to every independent subquery
        for(nit = sit->nodes.begin(); nit != sit->nodes.end(); ++nit){
            //First check whether this pattern is already in the pnd
            PatternId equivalent;
            vector<PatternId> joins;
            SPARQLPattern *thePattern;
            if(this->existsEquivalentPattern(*nit, prdg, equivalent)){
                prdg[equivalent].first.freq++;                
                thePattern = &prdg[equivalent].first;
            }else{
                //Update the pndg and prdg
                SPARQLPattern newPattern;
                newPattern.freq = 1;
                newPattern.localNode = *nit;
                prdg[newPattern.id] = pair<SPARQLPattern, vector<SPARQLPredicate> >();
                prdg[newPattern.id].first = newPattern;
                thePattern = &prdg[newPattern.id].first;

                //Update the prdg
                pndg[newPattern.id] = vector<SPARQLEdge>();                
            }
            
            if(this->joinsEquivalentPattern(thePattern->localNode, *sit, prdg, joins)){
                for(vector<PatternId>::iterator pidit = joins.begin(); pidit != joins.end(); ++pidit){
                    //Look if there is an edge 
                    SPARQLEdge theEdge;
                    if(!stdpartitioning::ptdgExistsEdge(pndg, thePattern->id, *pidit, theEdge)){                        
                        if(thePattern->id != *pidit){
                            SPARQLEdge newEdge;
                            newEdge.from = thePattern->id;
                            newEdge.to = *pidit;
                            newEdge.weight = 1;
                            pndg[thePattern->id].push_back(newEdge);
                        }
                    }else{
                        //Now look for the edge
                        for(vector<SPARQLEdge>::iterator eit = pndg[theEdge.from].begin(); eit != pndg[theEdge.from].end(); ++eit){
                            if(eit->to == theEdge.to)
                                eit->weight++;
                        }
                    }
                }                        
            }
        }
    }
    
}

void DependencyGraphBuilder::reversePredicateDependencyGraph(PredicateDependencyGraph &orig, ReversePredicateDependencyGraph &reversed){
    PredicateDependencyGraph::iterator pdgit;
    
    for(pdgit = orig.begin(); pdgit != orig.end(); ++pdgit){
        vector<SPARQLPredicate>::iterator pit;
        for(pit = pdgit->second.second.begin(); pit != pdgit->second.second.end(); ++pit){
            //If not found, build new entry
            if(reversed.find(pit->id) == reversed.end()){ 
                PredicateId prid = pit->id;
                reversed[prid] = pair<SPARQLPredicate, vector<SPARQLPattern> >(*pit, vector<SPARQLPattern>());  
            }
            //In any case add the pattern to the vector
            reversed[pit->id].second.push_back(pdgit->second.first);
        }
    }
}


unsigned int DependencyGraphBuilder::extractIndependentSubqueries(QueryGraph::SubQuery &subquery, vector<QueryGraph::SubQuery> &subqueries){
    unsigned int nSubqueries = 0;
    
    if(subquery.unions.empty()){
        subqueries.push_back(subquery);
        return 1;
    }else{
        vector<vector<QueryGraph::SubQuery> >::iterator sqit;
        for(sqit = subquery.unions.begin(); sqit != subquery.unions.end(); ++sqit){
            vector<QueryGraph::SubQuery>::iterator it;
            for(it = sqit->begin(); it != sqit->end(); ++it){
                nSubqueries += extractIndependentSubqueries(*it, subqueries);
            }
        }
    }    
    
    return nSubqueries;
}

bool DependencyGraphBuilder::existsEquivalentPattern(QueryGraph::Node &node, PredicateDependencyGraph &prdg, PatternId &equivalent){
    PredicateDependencyGraph::iterator it;

    for(it = prdg.begin(); it != prdg.end(); ++it){
        //If they have different addresses and are different
        if(&node != &it->second.first.localNode && nodeIsEquivalent(it->second.first.localNode, node)){
            equivalent = it->second.first.id;
            return true;
        }
    }
    
    return false;
}

bool DependencyGraphBuilder::joinsEquivalentPattern(QueryGraph::Node &node, QueryGraph::SubQuery &subquery, 
        PredicateDependencyGraph &ptdg, vector<PatternId> &join){
    vector<QueryGraph::Node>::iterator nit;
    bool joinsAny = false;
    for(nit = subquery.nodes.begin(); nit != subquery.nodes.end(); ++nit){
        if(node.canJoin(*nit)){
            PatternId pid;
            //Check whether this node has a equivalent in the graph
            if(this->existsEquivalentPattern(*nit, ptdg, pid)){
                join.push_back(pid);
                joinsAny = true;
            }
        }
    }
    
    return joinsAny;
}

