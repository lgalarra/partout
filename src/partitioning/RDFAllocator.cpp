/* 
 * File:   RDFAllocator.cpp
 * Author: luis
 * Created on June 6, 2011, 6:34 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/RDFAllocator.hpp"
#include <iostream>

using namespace stdpartitioning;
using namespace std;

RDFAllocator::RDFAllocator(BenefitSelector *benefitSelector): benefitSelector(benefitSelector){}

void RDFAllocator::assignFragmentsToNodes(FragmentationScheme &scheme, PartitioningDescription &desc, PatternDependencyGraph &ptdg, FragmentsAllocationMap &assign){
    vector<Fragment*> fragsSorted;
    vector<unsigned int>::iterator bit;    
    
    cout << "Sort the fragments descendently by load " << endl;
    this->sortDescendentlyByLoad(scheme, fragsSorted);
    cout << "The fragments are " << endl;
    for(vector<Fragment*>::iterator fpit = fragsSorted.begin(); fpit != fragsSorted.end(); ++fpit){
        cout << **fpit << " Load: " << (*fpit)->getLoad() <<  endl;
    }
    
    cout << "Now time for benefits calculation " << endl;
    //Assign the heaviest one to the most powerful node
    Host& mostCapable = desc.getMostCapable();
    FragmentAllocationAssign firstAssign;
    
    firstAssign.host = mostCapable.name;
    firstAssign.fragment = *fragsSorted.begin();
    assign[mostCapable.name] = vector<FragmentAllocationAssign>();
    assign[mostCapable.name].push_back(firstAssign);
    
    //Remove the first one
    fragsSorted.erase(fragsSorted.begin());
    
    cout << "First node assigned to most powerful node" << endl;
    
    //Now for every fragment, calculate the benefit of assigning it to every node
    while(!fragsSorted.empty()){
        vector<Fragment*>::iterator fit;    
        vector<BenefitDescriptor> benefitVector;        
        vector<unsigned int> benefitialPositions;        
        fit = fragsSorted.begin();        
        this->calculateBenefitVectorForFragment(*fit, scheme, desc, ptdg ,benefitVector, assign);
        for(vector<BenefitDescriptor>::iterator bdit = benefitVector.begin(); bdit != benefitVector.end(); ++bdit)
            cout << "Fragment " << *(bdit->fragment) << " Node " << bdit->node << " benefit " << bdit->benefit << endl;
        //Now assign it to the most benefitial nodes, according to the benefit criteria
        benefitSelector->selectBenefitial(benefitVector, benefitialPositions);
        
        //Now just create the assignments
        for(bit = benefitialPositions.begin(); bit != benefitialPositions.end(); ++bit){
            FragmentAllocationAssign newAssign;
            newAssign.host = benefitVector[*bit].node;
            newAssign.fragment = benefitVector[*bit].fragment;
            assign[newAssign.host].push_back(newAssign);            
        }   
        
        fragsSorted.erase(fit);
    }
    
    for(FragmentsAllocationMap::iterator amit = assign.begin(); amit != assign.end(); ++amit){
        cout << "Node: " << amit->first << " assigned: " << endl;
        for(vector<FragmentAllocationAssign>::iterator ait = amit->second.begin(); ait != amit->second.end(); ++ait){
            cout << *(ait->fragment) << " with load " << ait->fragment->getLoad() << " frequency " << ait->fragment->getFrequency();
            cout << " ratio " << ait->fragment->getRatio() << " size " << ait->fragment->getSize() << endl;
        }
        cout << " End for node " << amit->first << endl;
    }
    
}

void RDFAllocator::sortDescendentlyByLoad(FragmentationScheme &scheme, vector<Fragment*> &output){
    vector<Fragment>::iterator fit;
    
    for(fit = scheme.fragments.begin(); fit != scheme.fragments.end(); ++fit){
        output.push_back(&(*fit));
    }
    quickSort(output, 0, output.size());
}

void RDFAllocator::quickSort(vector<Fragment*>& input, unsigned int start, unsigned int end){  
    if(end - start <= 1)
        return;
    
    unsigned int pivot = (rand() % (end - start)) + start;
    unsigned int pivotLoad = input[pivot]->getLoad();    
    unsigned int k, pivot2, posS;
    vector<Fragment*> tmpSmaller, tmpGreater, tmpEqual;

    
    for(unsigned int i = start; i < end; ++i){
        unsigned int theLoad = input[i]->getLoad();
        if(theLoad > pivotLoad){
            tmpGreater.push_back(input[i]);            
        }else if(theLoad == pivotLoad){
            tmpEqual.push_back(input[i]);
        }else{
            tmpSmaller.push_back(input[i]);
        }
    }
        
    posS = start; 
    k = 0;
    while(k < tmpGreater.size()){
        input[posS] = tmpGreater[k];
        ++posS;
        ++k;
    }
    if(k > 0){
        quickSort(input, start, posS);
    }
    
    k = 0;
    while(k < tmpEqual.size()){
        input[posS] = tmpEqual[k];
        ++k;
        ++posS;		
    }
    
    pivot2 = posS;
    k = 0;
    while(k < tmpSmaller.size()){
        input[posS] = tmpSmaller[k];
        ++k;
        ++posS;
    }

    if(k > 0){
        quickSort(input, pivot2, end);
    }
}

void RDFAllocator::calculateBenefitVectorForFragment(Fragment *frag, FragmentationScheme &scheme, 
        PartitioningDescription &desc, PatternDependencyGraph &ptdg,
        vector<BenefitDescriptor> &output, FragmentsAllocationMap &previousAssigns){
    
    const map<string, Host> &nodes = desc.getNodes();
    map<string, Host>::const_iterator nit;
    
    for(nit = nodes.begin(); nit != nodes.end(); ++nit){
        double expectedLoad =  nit->second.weight * (double)scheme.getTotalLoad();
        double currentLoad = 0.0;
        double neighboursWeight = 0.0;
        double benefit;
        //Calculate current load for this node
        FragmentsAllocationMap::iterator ait = previousAssigns.find(nit->first);
        if(ait != previousAssigns.end()){
            //First extract related patterns
            vector<SPARQLPattern*> patterns = frag->getRelatedPatterns();                        
            
            vector<FragmentAllocationAssign>::iterator aasit;
            for(aasit = ait->second.begin(); aasit != ait->second.end(); ++aasit){
                vector<SPARQLPattern *> fragPatterns = aasit->fragment->getRelatedPatterns();
                currentLoad += aasit->fragment->getLoad();                       
                //Search in the patterns dependency graph
                neighboursWeight += this->aggregateEdges(patterns, fragPatterns, ptdg);
            }            
        }
        
        //The +1 is just to guarantee we never get zero
        benefit = ( expectedLoad / (expectedLoad + currentLoad + 1) )* (neighboursWeight + 1);
        //Prepare the benefit descriptor
        BenefitDescriptor newbd;
        newbd.fragment = frag;
        newbd.benefit = benefit;
        newbd.node = nit->first;
        output.push_back(newbd);                
    }
}

double RDFAllocator::aggregateEdges(vector<SPARQLPattern*> &set1, vector<SPARQLPattern*> &set2, 
        PatternDependencyGraph &ptdg){
    vector<SPARQLPattern*>::iterator pit1, pit2;
    double aggr = 0.0;
    
    for(pit1 = set1.begin(); pit1 != set1.end(); ++pit1){
        for(pit2 = set2.begin(); pit2 != set2.end(); ++pit2){
            SPARQLEdge theEdge; 
            if((*pit1)->id != (*pit2)->id){
               if(ptdgExistsEdge(ptdg, (*pit1)->id, (*pit2)->id, theEdge)){        
                   aggr += (double)theEdge.weight;
               }else if(ptdgExistsEdge(ptdg, (*pit2)->id, (*pit1)->id, theEdge)){
                   aggr += (double)theEdge.weight;                   
               }                                  
            } 
        }
    }
    
    return aggr;
}