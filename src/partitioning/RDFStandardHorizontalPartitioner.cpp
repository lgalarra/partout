/* m
 * File:   RDFStandardHorizontalPartitioner.cpp
 * Author: luis
 * Created on May 22, 2011, 1:17 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/RDFStandardHorizontalPartitioner.hpp"
#include "partitioning/CommonTypes.hpp"
#include <iostream>

using namespace stdpartitioning;
using namespace std;

RDFStandardHorizontalPartitioner::RDFStandardHorizontalPartitioner(Database &db): db(db), sdb(NULL), samplingFactor(1.0){}

RDFStandardHorizontalPartitioner::RDFStandardHorizontalPartitioner(Database &db, Database *sdb, float samplingFactor): db(db), sdb(sdb), samplingFactor(samplingFactor){}

RDFStandardHorizontalPartitioner::~RDFStandardHorizontalPartitioner() {}

void RDFStandardHorizontalPartitioner::optimalHorizontalFragmentation(vector<SPARQLPredicate> &input, FragmentationScheme &scheme, ReversePredicateDependencyGraph &pdg){
    vector<SPARQLPredicate*> minPredsPointers; 
    
    this->minimalSetOfPredicates(input, minPredsPointers, pdg);
    cout << "Minimal set of predicates" << endl;
    for(vector<SPARQLPredicate*>::iterator it = minPredsPointers.begin(); it != minPredsPointers.end(); ++it)
        cout << *(*it) << endl;
    
    this->buildFragmentationScheme(minPredsPointers, scheme, pdg);    
}

unsigned int RDFStandardHorizontalPartitioner::minimalSetOfPredicates(vector<SPARQLPredicate>& input, 
        vector<SPARQLPredicate*>& output, ReversePredicateDependencyGraph &pdg){
    unsigned int index = 0;
    unsigned int nPredicates = 0; 
    unsigned int inputSize = input.size();
    
    while(index < inputSize){        
        SPARQLPredicate &thePredicate = input[index];
        index++;
        if(this->isPresentOrNegated(output, thePredicate)){
            continue;
        }
        
        //Otherwise check if it is NOT relevant
        if(this->isRedundant(output, thePredicate, pdg)){
            continue;
        }        
                
        //Now verify if this has not made other predicates redundant
        unsigned int nRemoved = removeRedundantPredicates(output, thePredicate, pdg);
        output.push_back(&thePredicate);
    
        nPredicates = nPredicates + 1 - nRemoved;
    }
    
    return nPredicates;
}

bool RDFStandardHorizontalPartitioner::isPresentOrNegated(vector<SPARQLPredicate*> &set, SPARQLPredicate &predicate){
    vector<SPARQLPredicate*>::iterator it;
    
    for(it = set.begin(); it != set.end(); ++it){
        //Check whether there is some equivalent predicate
        if(!(*it)->isEquivalent(predicate)){
            //First check whether can be negated trivially
            QueryGraph::Filter::Type output;
            QueryGraph::Filter::Type previousType = predicate.predicate.type;
            if(negatePredicate(previousType, output)){                 
                predicate.predicate.type = output;
                if(predicate.isEquivalent(**it)){
                    //Back to normal
                    predicate.predicate.type = previousType;
                    return true;
                }                                
                predicate.predicate.type = previousType;                
            }
            
            //Now check if it has a NOT clause at the beginning            
            SPARQLPredicate negated;
            negated.position = (*it)->position;
            negated.predicate.type = QueryGraph::Filter::Not;
            negated.predicate.arg1 = (*it)->predicate.arg1;
            //Now check if its negated version is not there
            if(negated.isEquivalent(predicate)){
                //Its negated form is present
                negated.predicate.arg1 = NULL;
                return true;
            }else{
                negated.predicate.arg1 = NULL;                
                return false;
            }
        }else{
            return true;
        } 
    }
    
    return false;
}

unsigned int RDFStandardHorizontalPartitioner::removeRedundantPredicates(vector<SPARQLPredicate*> &input, 
        SPARQLPredicate &pred, ReversePredicateDependencyGraph &pdg){
 
    vector<SPARQLPredicate*>::iterator oit;        
    unsigned int removedCount = 0;
    
    oit = input.begin();
    while(oit != input.end()){
        vector<SPARQLPredicate*>::iterator dit;        
        vector<SPARQLPredicate*> temporal;
        for(dit = input.begin(); dit != input.end(); ++dit){
            if(dit != oit){
                temporal.push_back(*dit);
            }
        }
                
        temporal.push_back(&pred);
        
        if(this->isRedundant(temporal, **oit, pdg)){
            oit = input.erase(oit);
            removedCount++;
        }else{
            ++oit;
        }
    }
    
    return removedCount;
}

bool RDFStandardHorizontalPartitioner::isRedundant(vector<SPARQLPredicate*> &allPreds, 
        SPARQLPredicate &pred, ReversePredicateDependencyGraph &pdg){
    
    FragmentationScheme originalScheme, newScheme;
    
    if(allPreds.empty())
        return false;
    
    this->buildFragmentationScheme(allPreds, originalScheme, pdg);
    allPreds.push_back(&pred);
    this->buildFragmentationScheme(allPreds, newScheme, pdg);
    allPreds.pop_back();
    
    //The predicate makes the partitioning infeasible
    if(newScheme.empty())
        return true;
            
    return originalScheme.sameSignature(newScheme);
}   

void RDFStandardHorizontalPartitioner::buildFragmentationScheme(vector<SPARQLPredicate*>& pred, 
        FragmentationScheme& scheme, ReversePredicateDependencyGraph &pdg){
    vector<SPARQLPredicate*>::iterator predIt;
    vector<Minterm> relevantMinterms;
    vector<SPARQLPredicate*> tmp;
    
    for(predIt = pred.begin(); predIt != pred.end(); ++predIt){
        tmp.push_back(*predIt);
        this->buildRelevantPredicateCombinations(tmp, relevantMinterms);    
    }    
 

    vector<Minterm>::iterator rcit;    
    //Now use the relevant combinations to calculate size
    for(rcit = relevantMinterms.begin(); rcit != relevantMinterms.end(); ++rcit){
        if(this->sdb == NULL){
            Fragment newFragment(*rcit, &this->db, &pdg);
            scheme.pushFragment(newFragment);
        }else{
            Fragment newFragment(*rcit, &this->db, this->sdb, samplingFactor, &pdg);
            scheme.pushFragment(newFragment);            
        }
    }
    
    scheme.calculateRatio();
}

void RDFStandardHorizontalPartitioner::buildRelevantPredicateCombinations(vector<SPARQLPredicate*>& pred, 
        vector<Minterm> &output){
    
    //Just consider the new predicate with the already relevant combinations
    vector<Minterm>::iterator it;
    
    //Base case
    if(pred.size() == 1){
        Minterm mpos, mneg;
        mpos.flags = dynamic_bitset<>(1);
        mneg.flags = dynamic_bitset<>(1);
        mpos.flags.set(0, true);
        mneg.flags.set(0, false);
        mpos.predicates = pred;
        mneg.predicates = pred;
        output.push_back(mpos);
        output.push_back(mneg);        
        return;
    }
    
    //Elements to remove
    vector<Minterm> toAdd;
    vector<Minterm>::iterator toit;
    for(it = output.begin(); it != output.end(); ++it){
        Minterm mpos = it->extendedMinterm(pred.back(), true);
        Minterm mneg = mpos.complementMinterm();
        
        if(mpos.isSatisfiable()){
           toAdd.push_back(mpos); 
        }else{
            MintermStatistics::addCardinality(mpos, 0);
        }
        
        if(mneg.isSatisfiable()){
            toAdd.push_back(mneg);
        }else{
            MintermStatistics::addCardinality(mneg, 0);
        }
    }    
    //Remove old minterms    
    output.clear();
    
    //Add new ones
    for(toit = toAdd.begin(); toit != toAdd.end(); ++toit)
        output.push_back(*toit);
}