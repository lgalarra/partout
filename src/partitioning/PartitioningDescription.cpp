/* 
 * File:   PartitioningDescription.cpp
 * Author: luis
 * Created on June 8, 2011, 2:56 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include <map>

#include "partitioning/PartitioningDescription.hpp"

using namespace stdpartitioning;

PartitioningDescription::PartitioningDescription() {}

PartitioningDescription::PartitioningDescription(map<string, Host> &nodesMap): nodes(nodesMap){}

PartitioningDescription::~PartitioningDescription() {
}

void PartitioningDescription::addNode(string name, double weight){
    this->nodes[name] = Host(name, weight);
}

void PartitioningDescription::removeNode(string name){
    map<string, Host>::iterator it;
    it = this->nodes.find(name);
    
    if(it != this->nodes.end())
        this->nodes.erase(it);
}

const map<string, Host>& PartitioningDescription::getNodes() const{
    return this->nodes;
}

Host& PartitioningDescription::getMostCapable(){
    map<string, Host>::iterator it;
    string heaviest;
    double maxWeight = -1.0;
    
    if(this->nodes.empty())
        throw Exception(Exception::INVALID_STATE, "The partitioning manifest does not contain any nodes");
    
    for(it = this->nodes.begin(); it != this->nodes.end(); ++it){
        if(it->second.weight > maxWeight){
            heaviest = it->first;
            maxWeight = it->second.weight;
        }
    }
    
    return this->nodes.find(heaviest)->second;

}

