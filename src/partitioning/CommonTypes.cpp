/* 
 * File:   CommonFiles.cpp
 * Author: luis
 * Created on May 24, 2011, 11:25 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "partitioning/CommonTypes.hpp"
#include "cts/semana/SemanticAnalysis.hpp"
#include <stdlib.h>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <math.h>
#include <algorithm>

using namespace stdpartitioning;
using namespace std;

static Database *defaultDb;

unsigned int SPARQLPredicate::pid = 0;   
unsigned int SPARQLPattern::pid = 0;


void setDefaultDatabase(Database *aDb){
    defaultDb = aDb;
}

Database* getDefaultDatabase(){
    return defaultDb;
}

static string filterToString(const QueryGraph::Filter &f, string var){
    string result = "";
    string text;
    Type::ID type; 
    unsigned int subtype = 0;
    Database *ddb = getDefaultDatabase();
        
    switch(f.type){
        case QueryGraph::Filter::Variable:
            result = var;
            break;
        case QueryGraph::Filter::And:
            result = "(" + filterToString(*(f.arg1), var) + " && " + filterToString(*(f.arg2), var) + ")";
            break;
        case QueryGraph::Filter::Or:
            result = "(" + filterToString(*(f.arg1), var) + " || " + filterToString(*(f.arg2), var) + ")";
            break;
        case QueryGraph::Filter::Not:
            result = "! (" + filterToString(*(f.arg1), var) + ")";
            break;
        case QueryGraph::Filter::Builtin_bound:
            result = "bound(" + var + ")";
            break;
        case QueryGraph::Filter::Builtin_datatype:
            result = "datatype(" + var + ")";
            break;
        case QueryGraph::Filter::Builtin_in:
            break;
        case QueryGraph::Filter::ArgumentList:
            break;
        case QueryGraph::Filter::Function:
            break;
        case QueryGraph::Filter::Builtin_isblank:
            result = "isBlank(" + var + ")";
            break;
        case QueryGraph::Filter::Builtin_isiri:
            result = "isIRI(" + var + ")";            
            break;
        case QueryGraph::Filter::Builtin_isliteral:
            result = "isLiteral(" + var + ")";
            break;
        case QueryGraph::Filter::Builtin_lang:
            result = "lang(" + var + ")";
            break;
        case QueryGraph::Filter::Builtin_langmatches:
            result = "langMatches(" + filterToString(*(f.arg1), var) + ", " + filterToString(*(f.arg2), var) + ")";
            break;
        case QueryGraph::Filter::Builtin_regex:
            if(f.arg3)
                result = "regex(" + var + ", " + filterToString(*(f.arg2), var) + ", " + filterToString(*(f.arg3), var) + ")";
            else
                result = "regex(" + var + ", " + filterToString(*(f.arg2), var) + ")";                
            break;
        case QueryGraph::Filter::Builtin_sameterm:            
            break;
        case QueryGraph::Filter::Builtin_str:
            result = "str(" + var + ")";            
            break;
        case QueryGraph::Filter::Div:
            result = filterToString(*(f.arg1), var) + "/" + filterToString(*(f.arg2), var); 
            break;
        case QueryGraph::Filter::Minus:
            result = filterToString(*(f.arg1), var) + "-" + filterToString(*(f.arg2), var);
            break;
        case QueryGraph::Filter::Plus:
            result = filterToString(*(f.arg1), var) + "+" + filterToString(*(f.arg2), var);
            break;
        case QueryGraph::Filter::Mul:
            result = filterToString(*(f.arg1), var) + "*" + filterToString(*(f.arg2), var);
            break;
        case QueryGraph::Filter::Greater:
            result = filterToString(*(f.arg1), var) + ">" + filterToString(*(f.arg2), var);
            break;            
        case QueryGraph::Filter::GreaterOrEqual:
            result = filterToString(*(f.arg1), var) + ">=" + filterToString(*(f.arg2), var);
            break;           
        case QueryGraph::Filter::Less:
            result = filterToString(*(f.arg1), var) + "<" + filterToString(*(f.arg2), var);
            break;            
        case QueryGraph::Filter::LessOrEqual:
            result = filterToString(*(f.arg1), var) + "<=" + filterToString(*(f.arg2), var);
            break;
        case QueryGraph::Filter::Equal:
            result = filterToString(*(f.arg1), var) + "=" + filterToString(*(f.arg2), var);            
            break;
        case QueryGraph::Filter::Null:
            result = "NULL";
            break;
        case QueryGraph::Filter::Literal:
        {
            ostringstream sstr;
            sstr << "(" << f.id << ") ";
            result += sstr.str();
            if(lookupById(*defaultDb, f.id, type, subtype, text)){
                result += text;
            }else{
                result += "\"" + f.value + "\"";
            }
        }
            break;
        case QueryGraph::Filter::UnaryMinus:
            result = "-" + filterToString(*(f.arg1), var);
            break;
        case QueryGraph::Filter::UnaryPlus:    
            result = "+" + filterToString(*(f.arg1), var);
            break;
        case QueryGraph::Filter::NotEqual:
            result = filterToString(*(f.arg1), var)  + "!=" + filterToString(*(f.arg2), var) ;
            break;
        case QueryGraph::Filter::IRI:
            if(lookupById(*ddb, f.id, type, subtype, text))
                result = text ;
            else
                result = "<" + f.value + ">";
            break;
    }
    
    return result;
}

SPARQLPredicate::SPARQLPredicate(const QueryGraph::Filter &aFilter, 
        SPARQLPredicate::Position aPosition): predicate(aFilter), position(aPosition){
    this->id = ++SPARQLPredicate::pid;
}

SPARQLPredicate::SPARQLPredicate(){
    this->id = ++SPARQLPredicate::pid;
}

SPARQLPattern::SPARQLPattern(): freq(0){
    this->id = ++SPARQLPattern::pid;
}

string SPARQLPredicate::toString() const{
    string var;
    switch(this->position){
        case SPARQLPredicate::Subject:
            var = "?s";
            break;
        case SPARQLPredicate::Object:
            var = "?o";
            break;
        case SPARQLPredicate::Predicate:
            var = "?p";
            break;        
    }
    
    return filterToString(this->predicate, var);
}

bool Minterm::isSatisfiable(){
   //Here there are many cases which can be used to prune options in advance
   vector<SPARQLPredicate*>::iterator it;
   unsigned int i = 0;
   unsigned int j;
   for(it = this->predicates.begin(); it != this->predicates.end(); ++it, ++i){
       vector<SPARQLPredicate*>::iterator jt;
       j = i + 1;
       for(jt = it + 1; jt != this->predicates.end(); ++jt, ++j){
           //Equalities over the same position with different values are simply discarded
           if(this->flags[i] && this->flags[j] && (*it)->position == (*jt)->position && (*it)->predicate.type == QueryGraph::Filter::Equal
                   && (*it)->predicate.type == (*jt)->predicate.type && (*it)->predicate.arg1->type == (*jt)->predicate.arg1->type && (*it)->predicate.arg2->id != (*jt)->predicate.arg2->id
               ){
               return false;
           }else if(this->flags[i] && this->flags[j] && (*it)->position == (*jt)->position && 
                   (
                        ((*it)->predicate.type == QueryGraph::Filter::Builtin_isiri && (*jt)->predicate.type == QueryGraph::Filter::Builtin_isliteral)
                   ||
                        ((*it)->predicate.type == QueryGraph::Filter::Builtin_isliteral && (*jt)->predicate.type == QueryGraph::Filter::Builtin_isiri)                   
                   )
                   ){
               //Combinations isIRI and isLiteral
               return false;
           }
               
       }
   }

   return true;
}

Exception::Exception(Exception::Code code, string message) throw(): code(code), message(message){}

const char* Exception::what() const throw(){
    string fullMessage;
    switch(this->code){
        case Exception::DATABASE_ERROR:
            fullMessage = "[ERROR] The database has said : " + this->message;
            return fullMessage.c_str();
        case Exception::INVALID_STATE:
            fullMessage = "[ERROR] Invalid fragment state: " + this->message;
            return fullMessage.c_str();
        default: 
            return message.c_str();
    }
}

Exception::Exception(const Exception &e) throw(){
    this->message = e.message;
    this->code = e.code;
}

Exception::~Exception() throw(){}       

bool negatePredicate(QueryGraph::Filter::Type f, QueryGraph::Filter::Type &output){
    switch(f){
        case QueryGraph::Filter::Equal:
            output = QueryGraph::Filter::NotEqual;
            return true;
        case QueryGraph::Filter::Less:
            output = QueryGraph::Filter::GreaterOrEqual;
            return true;
        case QueryGraph::Filter::Greater:
            output = QueryGraph::Filter::LessOrEqual;
            return true;
        case QueryGraph::Filter::LessOrEqual:
            output = QueryGraph::Filter::Greater;
            return true;
        case QueryGraph::Filter::GreaterOrEqual:
            output = QueryGraph::Filter::Less;
            return true;
        case QueryGraph::Filter::NotEqual:
            output = QueryGraph::Filter::Equal;
            return true;
        default:
            return false;
    }    
}

string Minterm::buildCountQuery(){
    //Express predicates as filters
    string query, s_, p_, o_;
    string filterContent;
    SPARQLPredicate *boundSub = NULL, *boundPred = NULL, *boundObj = NULL;
    Database *db = getDefaultDatabase();
    s_ = "?s"; p_ = "?p"; o_ = "?o"; 
    SPARQLPredicate *isiriob, *isliteralob, *isnotiriob, *isnotliteralob;
    isiriob = isliteralob =  isnotiriob = isnotliteralob = NULL;
    Type::ID typeObj;
    bool redundantNotLiteral = false;
    bool redundantNotIri = false;
    bool redundantLiteral = false;
    bool redundantIri = false;
    bool emptyQuery = false;
        
    //First check whether we can bind one of the variables directly to a literal
    cout << "Bounding predicates " << this->predicates.size() << endl;
    for(unsigned int i = 0; i < this->predicates.size(); ++i){    
        cout << "Bounding Predicate " << (*this->predicates[i]) << endl;        
        if((this->predicates[i]->predicate.type == QueryGraph::Filter::Equal && this->flags[i])
                || (this->predicates[i]->predicate.type == QueryGraph::Filter::NotEqual && !this->flags[i]) 
                ){
            Type::ID type; unsigned int subtype;
            string value;
            if(!lookupById(*db, this->predicates[i]->predicate.arg2->id, type, subtype, value)){
                value = this->predicates[i]->predicate.arg2->value;
            }
            switch(this->predicates[i]->position){
                case SPARQLPredicate::Object:
                    o_ = value;
                    boundObj = this->predicates[i];
                    cout << "Bounding object for pid " << this->predicates[i]->id  << endl;
                    typeObj = type;
                    break;
                case SPARQLPredicate::Predicate:
                    p_ = value;
                    boundPred = this->predicates[i];
                    cout << "Bounding predicate for pid " << this->predicates[i]->id  << endl;                    
                    break;
                case SPARQLPredicate::Subject:
                    s_ = value;
                    boundSub = this->predicates[i];
                    cout << "Bounding subject for pid " << this->predicates[i]->id  << endl;
                    break;
            }            
        }else if(this->predicates[i]->position == SPARQLPredicate::Object && this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isiri && this->flags[i]){
            isiriob = this->predicates[i];
        }else if(this->predicates[i]->position == SPARQLPredicate::Object && this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isiri && !this->flags[i]){
            isnotiriob = this->predicates[i];
        }else if(this->predicates[i]->position == SPARQLPredicate::Object && this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isliteral && this->flags[i]){
            isliteralob = this->predicates[i];
        }else if(this->predicates[i]->position == SPARQLPredicate::Object && this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isliteral && !this->flags[i]){
            isnotliteralob = this->predicates[i];
        }
    }
        
    query = "SELECT count * WHERE { " + s_ + " " + p_ + " " + o_ + " . ";
    
    if(boundObj){
        if(typeObj == Type::URI){
            if(isliteralob || isnotiriob){
                emptyQuery = true;
            }                
            redundantIri = true;
            redundantNotLiteral = true;
        }else{
            if(isiriob || isnotliteralob){
                emptyQuery = true;
            }
                
            redundantLiteral = true;
            redundantNotIri = true;
        }
    }else{
        if(isiriob && isnotliteralob)
            redundantNotLiteral = true;

        if(isliteralob && isnotiriob)
            redundantNotIri = true;        
    }
    
        
    if(!emptyQuery){
        //Now build conditions with the predicates
        for(unsigned int i = 0; i < this->predicates.size(); ++i){
            //Already bounded
            if(this->predicates[i] == boundObj || 
                    this->predicates[i] == boundPred || this->predicates[i] == boundSub){
                continue;
            }

            bool redundant = (this->predicates[i]->predicate.type == QueryGraph::Filter::NotEqual && this->flags[i]) ||
            (this->predicates[i]->predicate.type == QueryGraph::Filter::Equal && !this->flags[i]);        

            switch(this->predicates[i]->position){
                case SPARQLPredicate::Object:
                    if(boundObj && redundant) continue;
                    break;
                case SPARQLPredicate::Predicate:
                    if(boundPred && redundant) continue;
                    break;
                case SPARQLPredicate::Subject:
                    if(boundSub && redundant) continue;
                    break;
            }

            if(this->predicates[i]->position == SPARQLPredicate::Object && 
               this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isliteral && 
                !this->flags[i] && redundantNotLiteral)
                continue;


            if(this->predicates[i]->position == SPARQLPredicate::Object && 
            this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isiri && 
                    !this->flags[i] && redundantNotIri)
                continue;

            if(this->predicates[i]->position == SPARQLPredicate::Object && 
               this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isliteral && 
                this->flags[i] && redundantLiteral)
                continue;

            if(this->predicates[i]->position == SPARQLPredicate::Object && 
            this->predicates[i]->predicate.type == QueryGraph::Filter::Builtin_isiri && 
                    this->flags[i] && redundantIri)
                continue;


            SPARQLPredicate *thePredicate = NULL;
            SPARQLPredicate negation;
            if(this->flags[i]){
                //It means the predicate is in normal form
                thePredicate = this->predicates.at(i);
            }else{
                //Here it is worth to consider some cases
                negation.position = this->predicates.at(i)->position;
                QueryGraph::Filter::Type opposite;
                if(negatePredicate(this->predicates.at(i)->predicate.type, opposite)){
                    //Easy to negate
                    negation.predicate.type = opposite;
                    negation.predicate.arg1 = this->predicates.at(i)->predicate.arg1;
                    negation.predicate.arg2 = this->predicates.at(i)->predicate.arg2;
                    negation.predicate.arg3 = this->predicates.at(i)->predicate.arg3;                
                }else{
                    //Otherwise
                    negation.predicate.type = QueryGraph::Filter::Not;
                    negation.predicate.arg1 = &this->predicates.at(i)->predicate;
                }
                thePredicate = &negation;            
            }

            if(!filterContent.empty())
                filterContent += " && " + thePredicate->toString();
            else
                filterContent += thePredicate->toString();        

            negation.predicate.arg1 = negation.predicate.arg2 = negation.predicate.arg3 = NULL;        
        }
    }else{
        filterContent = "false";
    }
    
    if(!filterContent.empty())
        query += " FILTER (" + filterContent + ")";
    
    query += "}";
    
    return query;
}

void Minterm::mintermsQuickSort(unsigned int start, unsigned int end){
    if(end - start <= 1)
        return;
    
    unsigned int pivot = (rand() % (end - start)) + start;
    unsigned int pivotId = this->predicates[pivot]->id;    
    unsigned int k, pivot2, posS;
    vector<SPARQLPredicate*> tmpSmaller, tmpGreater, tmpEqual;
    vector<bool> tmpFlagsSmaller, tmpFlagsGreater, tmpFlagsEqual;
    
    for(unsigned int i = start; i < end; ++i){
        if(this->predicates[i]->id > pivotId){
            tmpGreater.push_back(this->predicates[i]);
            tmpFlagsGreater.push_back(this->flags[i]);
        }else if(this->predicates[i]->id == pivotId){
            tmpEqual.push_back(this->predicates[i]);
            tmpFlagsEqual.push_back(this->flags[i]);            
        }else{
            tmpSmaller.push_back(this->predicates[i]);
            tmpFlagsSmaller.push_back(this->flags[i]);            
        }
    }
        
    posS = start; 
    k = 0;
    while(k < tmpSmaller.size()){
        this->predicates[posS] = tmpSmaller[k];
        this->flags[posS] = tmpFlagsSmaller[k];
        ++posS;
        ++k;
    }
    if(k > 0){
        mintermsQuickSort(start, posS);
    }
    
    k = 0;
    while(k < tmpEqual.size()){
        this->predicates[posS] = tmpEqual[k];
        this->flags[posS] = tmpFlagsEqual[k];
        ++k;
        ++posS;		
    }
    
    pivot2 = posS;
    k = 0;
    while(k < tmpGreater.size()){
        this->predicates[posS] = tmpGreater[k];
        this->flags[posS] = tmpFlagsGreater[k];
        ++k;
        ++posS;
    }
    if(k > 0){
        mintermsQuickSort(pivot2, end);
    }
    
}

string Minterm::buildQuery(){
    //Express predicates as filters
    string query = "SELECT ?s ?p ?o WHERE { ?s ?p ?o . FILTER(";
    string filterContent;
    //Now build conditions with the predicates    
    unsigned int i = 0;
    for(i = 0; i < this->predicates.size(); ++i){
        SPARQLPredicate *thePredicate = NULL, negation;
        if(this->flags[i]){
            //It means the predicate is in normal form
            thePredicate = this->predicates.at(i);
        }else{
            //Here it is worth to consider some cases
            negation.position = this->predicates.at(i)->position;
            QueryGraph::Filter::Type opposite;
            if(negatePredicate(this->predicates.at(i)->predicate.type, opposite)){
                //Easy to negate
                negation.predicate.type = opposite;
                negation.predicate.arg1 = this->predicates.at(i)->predicate.arg1;
                negation.predicate.arg2 = this->predicates.at(i)->predicate.arg2;
                negation.predicate.arg3 = this->predicates.at(i)->predicate.arg3;                
            }else{
                //Otherwise
                negation.predicate.type = QueryGraph::Filter::Not;
                negation.predicate.arg1 = &this->predicates.at(i)->predicate;
            }
            
            thePredicate = &negation;            
        }

        if(!filterContent.empty())
            filterContent += " && " + thePredicate->toString();
        else
            filterContent += thePredicate->toString();        

        negation.predicate.arg1 = NULL;        
        negation.predicate.arg2 = NULL;
        negation.predicate.arg3 = NULL;
        thePredicate = NULL;
    }
    
    query += filterContent + ")}";
    return query;

}

string Minterm::flagsString() const{
    ostringstream sstr;
    sstr << this->flags;
    return sstr.str();
}

bool Minterm::samePredicates(Minterm &anotherMin){
    //Check whether they refer to the same predicates in the same order
    unsigned int nPreds = this->predicates.size();
    
    if(anotherMin.predicates.size() != this->predicates.size())
        return false;
    
    for(unsigned int i = 0; i < nPreds; ++i){
        if(anotherMin.predicates[i] != this->predicates[i])
            return false;
    }

    return true;
}

string Minterm::idsString() const{
    vector<SPARQLPredicate*>::iterator mit;
    ostringstream sstr;
    for(vector<SPARQLPredicate*>::const_iterator mit = this->predicates.begin(); mit != this->predicates.end(); ++mit){
        sstr << (*mit)->id << ", ";
    }
    
    return sstr.str();
}

bool Minterm::isPositive(){
    for(unsigned int i = 0; i < this->flags.size(); ++i){
        if(
            !this->flags[i]
            ||
            (this->flags[i] && this->predicates[i]->predicate.type == QueryGraph::Filter::NotEqual)
          )
            return false;
    }
    
    return true;
}

bool Minterm::isNegative(){
    for(unsigned int i = 0; i < this->flags.size(); ++i){
        if(
            (!this->flags[i] && this->predicates[i]->predicate.type != QueryGraph::Filter::Equal)
            || 
            (this->predicates[i]->predicate.type == QueryGraph::Filter::NotEqual && !this->flags[i])
        )
            
            return false;
    }
    
    return true;
}

Minterm Minterm::prefixMinterm(){
    Minterm prefixMinterm;
    string flagsString = this->flagsString();
    
    prefixMinterm.flags = dynamic_bitset<>(flagsString.size() - 1);
    for(unsigned int i = 0; i < this->flags.size() - 1; ++i){
        prefixMinterm.flags[i] = this->flags[i];
    }
    prefixMinterm.flags.resize(flagsString.size() - 1);
    prefixMinterm.predicates = this->predicates;
    prefixMinterm.predicates.pop_back();
    
    return prefixMinterm;
}

Minterm Minterm::suffixMinterm(){
    Minterm suffixMinterm;
    
    suffixMinterm.flags = dynamic_bitset<>(1);
    suffixMinterm.flags[0] = this->flags.test(this->flags.size() - 1);
    suffixMinterm.predicates.push_back(this->predicates.back());
    
    return suffixMinterm;
}
       
Minterm Minterm::complementMinterm(){
    Minterm complementMinterm;
    
    complementMinterm.flags = this->flags;
    complementMinterm.flags[this->flags.size() - 1].flip();
    complementMinterm.predicates = this->predicates;    

    return complementMinterm;
}

Minterm Minterm::extendedMinterm(SPARQLPredicate* pred, bool value){
    Minterm extended;
    extended.predicates = this->predicates;
    extended.predicates.push_back(pred);
    extended.flags = this->flags;
    extended.flags.resize(this->flags.size() + 1, value);
    
    return extended;
}

static bool sortById(SPARQLPredicate *a, SPARQLPredicate *b){
    return a->id < b->id;
}

void Minterm::sortPredicatesById(){
    sort(this->predicates.begin(), this->predicates.end(), sortById);
//    this->mintermsQuickSort(0, this->predicates.size());
}

bool Minterm::isZeroMinterm(){
    for(unsigned i = 0; i < this->flags.size(); ++i){
        if(this->flags[i]) return false;
    }
    
    return true;
}

static bool filterIsEquivalent(const QueryGraph::Filter &a, const QueryGraph::Filter &b){    
    //Start with the easy stuff
    if(a.type != b.type)
        return false;

    switch(a.type){
        case QueryGraph::Filter::Variable:
            return true;
        
        case QueryGraph::Filter::And:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
        
        case QueryGraph::Filter::Or:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
        
        case QueryGraph::Filter::Not:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_bound:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_datatype:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_in:
            return true;
        
        case QueryGraph::Filter::ArgumentList:
            return true;
        
        case QueryGraph::Filter::Function:
            return true;
        
        case QueryGraph::Filter::Builtin_isblank:
             return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_isiri:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));            
        
        case QueryGraph::Filter::Builtin_isliteral:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_lang:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));
        
        case QueryGraph::Filter::Builtin_langmatches:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
        
        case QueryGraph::Filter::Builtin_regex:
            if((a.arg3 && !b.arg3) || (!a.arg3 && b.arg3)) return false;
            
            if(a.arg3 && b.arg3)
                return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2)) && filterIsEquivalent(*(a.arg3), *(b.arg3));                
            else
                return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));

        case QueryGraph::Filter::Builtin_sameterm:            
            return true;

        case QueryGraph::Filter::Builtin_str:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));            
        
        case QueryGraph::Filter::Div:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
        
        case QueryGraph::Filter::Minus:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));        
        
        case QueryGraph::Filter::Plus:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));            
 
        case QueryGraph::Filter::Mul:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
            
        case QueryGraph::Filter::Greater:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
            
        case QueryGraph::Filter::GreaterOrEqual:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));
            
        case QueryGraph::Filter::Less:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));            
 
        case QueryGraph::Filter::LessOrEqual:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));            
 
        case QueryGraph::Filter::Equal:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));            
 
        case QueryGraph::Filter::Null:
            return true;

        case QueryGraph::Filter::Literal:
            return a.id == b.id;
            
        case QueryGraph::Filter::UnaryMinus:
            return filterIsEquivalent(*(a.arg1), *(b.arg1));

        case QueryGraph::Filter::UnaryPlus:    
            return filterIsEquivalent(*(a.arg1), *(b.arg1));            

        case QueryGraph::Filter::NotEqual:
            return filterIsEquivalent(*(a.arg1), *(b.arg1)) && filterIsEquivalent(*(a.arg2), *(b.arg2));            
            
        case QueryGraph::Filter::IRI:
            return a.id == b.id;
    }
    
    return false;    
    
}

bool SPARQLPredicate::isEquivalent(const SPARQLPredicate &anotherPredicate){
    return this->position == anotherPredicate.position 
            && filterIsEquivalent(this->predicate, anotherPredicate.predicate);
}

void SPARQLPredicate::negated(SPARQLPredicate &negated){
    negated.position = this->position;
    QueryGraph::Filter::Type output;
    if(negatePredicate(this->predicate.type, output)){
        negated.predicate.type = output;
        if(this->predicate.arg1){
            negated.predicate.arg1 = new QueryGraph::Filter(*this->predicate.arg1);
        }
        
        if(this->predicate.arg2){
            negated.predicate.arg2 = new QueryGraph::Filter(*this->predicate.arg2);
        }
        
        if(this->predicate.arg3){
            negated.predicate.arg3 = new QueryGraph::Filter(*this->predicate.arg3);
        }
        
    }else{
        negated.predicate.type = QueryGraph::Filter::Not;
        negated.predicate.arg1 = new QueryGraph::Filter(this->predicate);
    }
    
    negated.predicate.id = this->predicate.id;
    negated.predicate.value = this->predicate.value;
    negated.predicate.type = this->predicate.type;
}

bool nodeIsEquivalent(QueryGraph::Node &a, QueryGraph::Node &b){
    if(a.constObject != b.constObject || 
       a.constPredicate != b.constPredicate || a.constSubject != b.constSubject){
        return false;
    }
    
    if(a.constObject && a.object != b.object)
        return false;
    
    if(a.constPredicate && a.predicate != b.predicate)
        return false;
    
    if(a.constSubject && a.subject != b.subject)
        return false;
    
    return true;
}

unsigned SPARQLPredicate::argumentsCount(){            
    if(isBooleanAtomicOperator(this->predicate)){
        //Special cases
        if(this->predicate.type == QueryGraph::Filter::Builtin_regex) return 2;
        if(this->predicate.type == QueryGraph::Filter::Builtin_isiri || this->predicate.type == QueryGraph::Filter::Builtin_isliteral) return 0;              
        return 1;
    }

    return 0;
}

void SPARQLPredicate::argumentsList(vector<pair<unsigned, string> > &args){
    unsigned nargs = this->argumentsCount(); 
    if(nargs > 0){
        if(this->predicate.type == QueryGraph::Filter::Builtin_langmatches){
            Type::ID type; unsigned subtype; string text;
            if(lookupById(*defaultDb, this->predicate.arg2->id, type, subtype, text)){
                args.push_back(pair<unsigned, string>(this->predicate.arg2->id, ""));
            }else{
                args.push_back(pair<unsigned, string>(~0u, this->predicate.arg2->value));
            } 
        }else if(this->predicate.type == QueryGraph::Filter::Builtin_regex){
            args.push_back(pair<unsigned, string>(~0u, this->predicate.arg2->value));
            if(!this->predicate.arg3->value.empty())
                args.push_back(pair<unsigned, string>(~0u, this->predicate.arg2->value));
        }else if(this->predicate.type == QueryGraph::Filter::Equal && this->predicate.arg1->type == QueryGraph::Filter::Builtin_datatype){
            if(!this->predicate.arg2->value.empty()){
                //For basic types like string, integer
                args.push_back(std::pair<unsigned, string>(~0u, this->predicate.arg2->value));
            }else{
                //For custom types
                args.push_back(std::pair<unsigned, string>(this->predicate.arg2->id, ""));                
            }            
        }else{
            args.push_back(pair<unsigned, string>(this->predicate.arg2->id, ""));            
        }
    }
}

bool SPARQLPredicate::meets(unsigned subject, unsigned predicate, unsigned object, bool flag, DictionaryWrapper &dictWrapper){
    unsigned valueOfInterest;
    switch(this->position){
        case Subject:
            valueOfInterest = subject;
            break;
        case Predicate:
            valueOfInterest = predicate;
            break;
        case Object:
            valueOfInterest = object;
            break;            
    }
        
    switch(this->predicate.type){
        case QueryGraph::Filter::Equal:
            if(this->predicate.arg1->type == QueryGraph::Filter::Variable){
                if(this->predicate.arg2->value.empty()){
                    if(flag) return this->predicate.arg2->id == valueOfInterest;
                    else return this->predicate.arg2->id != valueOfInterest;
                }else{
                    Type::ID type;
                    unsigned subtype;
                    string result;
                    bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);    
                    if(flag) return result.compare(this->predicate.arg2->value) == 0;
                    else return result.compare(this->predicate.arg2->value) != 0;
                }
            }else if(this->predicate.arg1->type == QueryGraph::Filter::Builtin_datatype){
                Type::ID type;
                unsigned subtype;
                string result;
                bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);            
                //cout << "Testing whether " << result << " with type " << type << " ";
                //cout << "subtype " << subtype << " meets " << flag << "(" << *this << ")" << endl;
                //cout <<  this->predicate.arg2->value << endl;
                switch(type){
                    case Type::String:
                        if(flag) return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#string") == 0;
                        else return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#string") != 0;
                    case Type::Integer:
                        if(flag) return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#integer") == 0;
                        else return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#integer") != 0;                        
                    case Type::Decimal:
                        if(flag) return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#decimal") == 0;
                        else return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#decimal") != 0;                         
                    case Type::Boolean:
                        if(flag) return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#boolean") == 0;
                        else return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#boolean") != 0;
                    case Type::Double:
                        if(flag) return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#double") == 0;
                        else return this->predicate.arg2->value.compare("http://www.w3.org/2001/XMLSchema#double") != 0;                        
                    case Type::CustomType:
                    {
                        if(flag) return subtype == this->predicate.arg2->id;
                        else return subtype != this->predicate.arg2->id;                
                    }
                    default:
                        return !flag; 
                }
            }
            break;
        case QueryGraph::Filter::NotEqual:
            if(this->predicate.arg2->value.empty()){
                if(flag) return this->predicate.arg2->id != valueOfInterest;
                else return this->predicate.arg2->id == valueOfInterest;
            }else{
                Type::ID type;
                unsigned subtype;
                string result;
                bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);    
                if(flag) return result.compare(this->predicate.arg2->value) != 0;
                else return result.compare(this->predicate.arg2->value) == 0;                
            }
        case QueryGraph::Filter::Builtin_isiri:
        {
            Type::ID type;
            unsigned subtype;
            string result;
            bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);
            if(flag) return type == Type::URI;
            else return type != Type::URI;
        }
        case QueryGraph::Filter::Builtin_isliteral:
        {
            Type::ID type;
            unsigned subtype;
            string result;
            bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);
            if(flag) return type != Type::URI;
            else return type == Type::URI;
        }
        case QueryGraph::Filter::Builtin_langmatches:
        {
            Type::ID type;
            unsigned subtype;
            string result;
            bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);            
                        
            if(type == Type::CustomLanguage){
                if(flag) return subtype == this->predicate.arg2->id;
                else return subtype != this->predicate.arg2->id;
            }else{
                return !flag;
            }
        }
        case QueryGraph::Filter::Greater: case QueryGraph::Filter::GreaterOrEqual: case QueryGraph::Filter::Less: case QueryGraph::Filter::LessOrEqual:
        {
            Type::ID type0, type;
            unsigned subtype, subtype0;
            string result, result0, argument;
            bool resultLookup = dictWrapper.lookupById(valueOfInterest, type, subtype, result);            
            bool argLookup = dictWrapper.lookupById(this->predicate.arg2->id , type0, subtype0, result0); 
            if(argLookup) argument = result0;
            else argument = this->predicate.arg2->value;
            
            switch(type){
                case Type::String:
                    if(this->predicate.type == QueryGraph::Filter::Greater){
                        if(flag) return result.compare(argument) > 0;
                        else return result.compare(argument) <= 0;
                    }else if(this->predicate.type == QueryGraph::Filter::Less){
                        if(flag) return result.compare(argument) < 0;
                        else return result.compare(argument) >= 0;                        
                    }else if(this->predicate.type == QueryGraph::Filter::GreaterOrEqual){
                        if(flag) return result.compare(argument) >= 0;
                        else return result.compare(argument) < 0;                                                
                    }else{
                        if(flag) return result.compare(argument) <= 0;
                        else return result.compare(argument) > 0;                        
                    }
                    break;
                case Type::Integer:{
                    try{
                        size_t typeDelim = result.find("^^");
                        string numericValue, numericArgs;
                        if(typeDelim != string::npos){
                            numericValue = result.substr(1, typeDelim - 2);
                        }else{
                            assert(0);
                        }  

                        size_t typeDelim2 = argument.find("^^");
                        if(typeDelim2 != string::npos){
                            numericArgs = argument.substr(1, typeDelim2 - 2);
                        }else{
                            numericArgs = argument.substr(1, argument.size() - 2);                    
                        }                       
                                                
                        int value1 = boost::lexical_cast<int>(numericValue);
                        int value2 = boost::lexical_cast<int>(numericArgs);
                        
                        if(this->predicate.type == QueryGraph::Filter::Greater){
                            if(flag) return value1 > value2;
                            else return value1 <= value2;
                        }else if(this->predicate.type == QueryGraph::Filter::Less){
                            if(flag) return value1 < value2;
                            else return value1 >= value2;                        
                        }else if(this->predicate.type == QueryGraph::Filter::GreaterOrEqual){
                            if(flag) return value1 >= value2;
                            else return value1 < value2;                                                
                        }else{
                            if(flag) return value1 <= value2;
                            else return value1 > value2;                        
                        }
                        break;
                        
                    }catch(boost::bad_lexical_cast &e){
                        return !flag;
                    }
                }
                    break;
                default:
                    if(flag) return subtype == this->predicate.arg2->id;
                    else return subtype != this->predicate.arg2->id;               
            }
        }
        default:
            break;            
    }
    
    return false;
}

bool stdpartitioning::ptdgExistsEdge(PatternDependencyGraph &prdg, PatternId from, PatternId to, SPARQLEdge &theEdge){
    PatternDependencyGraph::iterator pit;
    
    for(pit = prdg.begin(); pit != prdg.end(); ++pit){
        vector<SPARQLEdge>::iterator eit;
        if(pit->first == from){
            for(eit = pit->second.begin(); eit != pit->second.end(); ++eit){
                if(eit->to == to){
                    theEdge = *eit;
                    return true;
                }
            }
        }else if(pit->first == to){
            for(eit = pit->second.begin(); eit != pit->second.end(); ++eit){
                if(eit->to == from){
                    theEdge = *eit;                    
                    return true;
                }
            }            
        }
    }
    
    return false;
}


bool DictionaryWrapper::lookupById(unsigned id, Type::ID &type, unsigned &subtype, string &result){
   boost::unique_lock<boost::mutex> lock(this->dictMutex);
   unsigned slot = id % lookupSize;
   
   bool outcome;
   if(dictHashCache[slot].id == id){
       type = dictHashCache[slot].type;
       subtype = dictHashCache[slot].subtype;
       result = dictHashCache[slot].text;
       outcome = true;
   }else{
       outcome = ::lookupById(*db, id, type, subtype, result);
       if(outcome){
           CacheEntry newEntry;
           newEntry.id = id;
           newEntry.subtype = subtype;
           newEntry.text = result;
           newEntry.type = type;
           dictHashCache[slot] = newEntry;
       }
   }

   lock.unlock();
   return outcome;
}

