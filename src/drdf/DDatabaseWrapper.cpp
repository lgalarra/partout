/* 
 * File:   DDatabaseWrapper.cpp
 * Author: lgalarra
 * 
 * Created on August 31, 2011, 1:18 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "drdf/DDatabaseWrapper.hpp"
#include <stdlib.h>
#include <iostream>
#include <sstream>

using namespace drdf;
using namespace std;

const int DDatabaseWrapper::NULL_INT = -1;
const string DDatabaseWrapper::NULL_STRING = "NULL";

bool DDatabaseWrapper::execute(const string &sql, ResultSet &result){    
    char *zErrMsg = 0;
    result.dp = this;    
    int rc = sqlite3_exec(this->db, sql.c_str(), ResultSet::processRow, &result, &zErrMsg);
    if( rc!=SQLITE_OK ){
      cerr << "Query: " << sql << endl;
      cerr << "SQL error: " << zErrMsg << endl;
      sqlite3_free(zErrMsg);
      return false;
    }
    
    return true;
}

bool DDatabaseWrapper::execute(const char *sql, ResultSet &result){
    char *zErrMsg = 0;
    result.dp = this;    
    int rc = sqlite3_exec(this->db, sql, ResultSet::processRow, &result, &zErrMsg);
    if( rc!=SQLITE_OK ){
      cerr << "SQL error: " << zErrMsg << endl;
      sqlite3_free(zErrMsg);
      return false;
    }
    
    return true;
}

bool DDatabaseWrapper::execute(const char *sql){
    char *zErrMsg = 0;
    int rc = sqlite3_exec(this->db, sql, NULL, NULL, &zErrMsg);
    if( rc!=SQLITE_OK ){
      cerr << "SQL error: " << zErrMsg << endl;
      sqlite3_free(zErrMsg);
      return false;
    }
    
    return true;
}

bool DDatabaseWrapper::execute(const string &sql){
    char *zErrMsg = 0;
    int rc = sqlite3_exec(this->db, sql.c_str(), NULL, NULL, &zErrMsg);
    if( rc!=SQLITE_OK ){
      cerr << "SQL error: " << zErrMsg << endl;
      sqlite3_free(zErrMsg);
      return false;
    }
    
    return true;
}

int DDatabaseWrapper::getIntMetadata(MetadataTag key){
    std::map<MetadataTag, int>::iterator it = this->intMetadata.find(key);
    if(it != this->intMetadata.end()){
        return it->second;
    }
    
    ostringstream sql;
    ResultSet rs;
    
    sql << "SELECT COUNT(*) as `n` FROM `Metadata` WHERE `id` = " << key << ";";
    
    int value;
    this->execute(sql.str(), rs);
    rs.next();
    rs.getInt("n", value);
    intMetadata[key] = value;
    return value;
}

int ResultSet::processRow(char** data, char** columns){
    Row row;
    for(unsigned i = 0; i < this->nResults; ++i){
        char ** currentCol;
        char ** currentData;
        currentCol = columns + i;
        currentData = data + i;
        if(*currentData == NULL)
            row[string(*currentCol)] = string(DDatabaseWrapper::NULL_STRING);
        else
            row[string(*currentCol)] = string(*currentData);            
    }
    
    this->table.push_back(row);    
    
    return 0;
} 

bool ResultSet::next(){
   if(this->first){
       this->cursor = this->table.begin();
       this->first = false;
   }else{
       ++this->cursor;       
   } 
   
   if(this->cursor == this->table.end())
       return false;   
   
   return true;
}

bool ResultSet::getInt(const string &columnName, int &value){
    Row::iterator cell = this->cursor->find(columnName);
    
    if(cell == this->cursor->end())
        return false;
    
    if(cell->second.compare(DDatabaseWrapper::NULL_STRING) == 0)
        value = DDatabaseWrapper::NULL_INT;
    else        
        value = atoi(cell->second.c_str());
    
    return true;
}

bool ResultSet::getInt(const char *columnName, int &value){
    Row::iterator cell = this->cursor->find(string(columnName));
    
    if(cell == this->cursor->end())
        return false;
    
    if(cell->second.compare(DDatabaseWrapper::NULL_STRING) == 0)
        value = DDatabaseWrapper::NULL_INT;
    else        
        value = atoi(cell->second.c_str());
    
    return true;
}

bool ResultSet::getString(const string &columnName, string &value){
    Row::iterator cell = this->cursor->find(columnName);
    
    if(cell == this->cursor->end())
        return false;
    
    value = cell->second;    
    return true;    
}

bool ResultSet::getString(const char *columnName, string &value){
    Row::iterator cell = this->cursor->find(string(columnName));
    
    if(cell == this->cursor->end())
        return false;
    
    value = cell->second;    
    return true;    
}