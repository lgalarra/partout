/* 
 * File:   DDebugPlanPrinter.cpp
 * Author: luis
 * Created on September 7, 2011, 12:09 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "drdf/DDebugPlanPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DDictionarySegment.hpp"
#include <sstream>

using namespace drdf;
using namespace std;

//---------------------------------------------------------------------------
string DDebugPlanPrinter::formatRegister(const Register* reg)
   // Format a register (for generic annotations)
{
   stringstream result;
   // Regular register?
   if (druntime.getRegisterCount()&&(reg>=druntime.getRegister(0))&&(reg<=druntime.getRegister(druntime.getRegisterCount()-1))) {
      result << "?" << (reg-druntime.getRegister(0));
   } else {
      // Arbitrary register outside the runtime system. Should not occur except for debugging!
      result << "@0x" << hex << reinterpret_cast<uintptr_t>(reg);
   }
   return result.str();
}

//---------------------------------------------------------------------------
string DDebugPlanPrinter::formatValue(unsigned value)
   // Format a constant value (for generic annotations)
{
   stringstream result;
   if (~value) {
      string text; Type::ID type; unsigned subType;
      
      if (druntime.getDDatabase().getDictionary().lookupById(value,text,type,subType)) {
         result << '\"' << text << '\"';
      } else{ 
          result << "@?" << value;
      }
   } else {
      result << "NULL";
   }
   return result.str();
}
