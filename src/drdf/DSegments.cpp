/* 
 * File:   FragmentsDefinitionSegment.cpp
 * Author: lgalarra
 * 
 * Created on August 22, 2011, 2:54 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <set>
#include <vector>
#include "drdf/DSegments.hpp"
#include "rts/database/Database.hpp"
#include "drdf/DDatabase.hpp"
#include <iostream>
#include <sstream>
#include <stdlib.h>

using namespace std;
using namespace drdf;

static inline bool gt(unsigned a1,unsigned a2,unsigned b1,unsigned b2) {
   return (a1>b1)||((a1==b1)&&(a2>b2));
}

DAggregatedFactsSegment::DAggregatedFactsSegment(DDatabaseWrapper &wrapper, ATag tag, unsigned pageSize): DSegment(wrapper), 
   tag(tag), limit(pageSize), limitStart(0){
    switch(this->tag){
        case Tag_SP :
            this->table = "ASSubjectPredicate";
            this->g1MetaTag = DDatabaseWrapper::AS_SP_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_SP_G2;
            this->pagesMetaTag = DDatabaseWrapper::AS_SP_PAGES;
            break;
        case Tag_SO:
            this->table = "ASSubjectObject";
            this->g1MetaTag = DDatabaseWrapper::AS_SO_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_SO_G2;
            this->pagesMetaTag = DDatabaseWrapper::AS_SO_PAGES;
            break;
        case Tag_PS:
            this->table = "ASPredicateSubject";
            this->g1MetaTag = DDatabaseWrapper::AS_PS_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_PS_G2;            
            this->pagesMetaTag = DDatabaseWrapper::AS_PS_PAGES;
            break;
        case Tag_PO:
            this->table = "ASPredicateObject";
            this->g1MetaTag = DDatabaseWrapper::AS_PO_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_PO_G2;            
            this->pagesMetaTag = DDatabaseWrapper::AS_PO_PAGES;
            break;
        case Tag_OS:
            this->table = "ASObjectSubject";
            this->g1MetaTag = DDatabaseWrapper::AS_OS_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_OS_G2;            
            this->pagesMetaTag = DDatabaseWrapper::AS_OS_PAGES;
            break;
        case Tag_OP:
            this->table = "ASObjectPredicate";
            this->g1MetaTag = DDatabaseWrapper::AS_OP_G1;
            this->g2MetaTag = DDatabaseWrapper::AS_OP_G2;            
            this->pagesMetaTag = DDatabaseWrapper::AS_OP_PAGES;
            break;
    }
 
    this->groups1 = this->dwrapper.getIntMetadata(this->g1MetaTag);    
    this->groups2 = this->dwrapper.getIntMetadata(this->g2MetaTag);
    this->pages = this->dwrapper.getIntMetadata(this->pagesMetaTag);
}

bool DAggregatedFactsSegment::Scan::first(){
    ostringstream sql;
    ResultSet result;
    sql << "SELECT * FROM `" << this->seg->table << "` ORDER BY `value1` ASC, `value2` DESC, `card` DESC " 
        << " LIMIT " << this->seg->limitStart << ", " << this->seg->limit << ";";
    
    this->seg->dwrapper.execute(sql.str(), result);
    while(result.next()){
        int value1, value2, card;
        result.getInt("value1", value1);
        result.getInt("value2", value2);
        result.getInt("card", card);        
        this->triples.push_back(Triple((unsigned)value1, (unsigned)value2, (unsigned)card));
    }
    
    this->cursor = this->triples.begin();    
    return this->cursor == this->triples.end();    
}

//---------------------------------------------------------------------------
bool DAggregatedFactsSegment::Scan::first(DAggregatedFactsSegment& segment)
   // Start a new scan over the whole segment
{
    this->seg = &segment;
    return this->first();
}

bool DAggregatedFactsSegment::Scan::next(){
    ++this->cursor;        
    if(this->cursor == this->triples.end())
        return this->readNextPage();
    else
        return true;
}
//---------------------------------------------------------------------------
bool DAggregatedFactsSegment::Scan::first(DAggregatedFactsSegment& segment,unsigned start1,unsigned start2)
   // Start a new scan starting from the first entry >= the start condition
{
    if(!this->first(segment))
        return false;

    while(true){
        if(this->find(start1, start2))
            return true;
        else{
            bool morePages = this->readNextPage();
            if(!morePages)
                break;
        }
    }
    
    return false;
}
//---------------------------------------------------------------------------
bool DAggregatedFactsSegment::Scan::find(unsigned value1,unsigned value2)
    // Perform a binary search
{
    unsigned l = 0;
    unsigned r = this->triples.size() - 1;
    unsigned pos;
    while (l<r) {
      unsigned m= l+((r-l)/2);
      if (gt(this->triples[m].value1,this->triples[m].value2,value1,value2)) {
         r=m;
      } else if (gt(value1,value2,this->triples[m].value1,this->triples[m].value2)) {
         l=m+1;
      } else {
         pos=m;
         this->cursor = this->triples.begin() + pos;
         return true;
      }
    }

    return false;
}

//---------------------------------------------------------------------------
void DAggregatedFactsSegment::Scan::close()
{
    this->cursor = this->triples.end();
}
//---------------------------------------------------------------------------

bool DAggregatedFactsSegment::Scan::readNextPage(){
    this->seg->limitStart += this->seg->limit;
    this->triples.clear();
    return this->first();
}

DFullyAggregatedFactsSegment::DFullyAggregatedFactsSegment(DDatabaseWrapper &wrapper, FATag tag, unsigned pageSize): DSegment(wrapper), tag(tag), pageSize(pageSize) {
    switch(tag){
        case Tag_S:
            this->table = "FASubject";
            this->gMetatag = DDatabaseWrapper::FAS_S_G1;
            this->pagesMetatag = DDatabaseWrapper::FAS_S_PAGES;
            break;
        case Tag_P:
            this->table = "FAPredicate";
            this->gMetatag = DDatabaseWrapper::FAS_P_G1;
            this->pagesMetatag = DDatabaseWrapper::FAS_P_PAGES;
            break;
        case Tag_O:
            this->table = "FAObject";
            this->gMetatag = DDatabaseWrapper::FAS_O_G1;
            this->pagesMetatag = DDatabaseWrapper::FAS_O_PAGES;
            break;
    }
    
    this->groups1 = this->dwrapper.getIntMetadata(this->gMetatag);
    this->pages = this->dwrapper.getIntMetadata(this->pagesMetatag);
}

bool DFullyAggregatedFactsSegment::Scan::first(){
    ostringstream sql;
    ResultSet result;
    sql << "SELECT * FROM `" << this->seg->table << "` ORDER BY `value` ASC, `card` DESC " 
        << " LIMIT " << this->seg->limitStart << ", " << this->seg->limit << ";";
    
    this->seg->dwrapper.execute(sql.str(), result);
    while(result.next()){
        int value1, card;
        result.getInt("value", value1);
        result.getInt("card", card);        
        this->pairs.push_back(Pair(value1, (unsigned)card));
    }
    
    this->cursor = this->pairs.begin();    
    return this->cursor == this->pairs.end();    
}

//---------------------------------------------------------------------------
bool DFullyAggregatedFactsSegment::Scan::first(DFullyAggregatedFactsSegment& segment)
   // Start a new scan over the whole segment
{
    this->seg = &segment;
    return this->first();
}

bool DFullyAggregatedFactsSegment::Scan::next(){
    ++this->cursor;        
    if(this->cursor == this->pairs.end())
        return this->readNextPage();
    else
        return true;
}
//---------------------------------------------------------------------------
bool DFullyAggregatedFactsSegment::Scan::first(DFullyAggregatedFactsSegment& segment,unsigned start1)
   // Start a new scan starting from the first entry >= the start condition
{
    if(!this->first(segment))
        return false;

    while(true){
        if(this->find(start1))
            return true;
        else{
            bool morePages = this->readNextPage();
            if(!morePages)
                break;
        }
    }
    
    return false;
}

/// Perform a binary search
bool DFullyAggregatedFactsSegment::Scan::find(unsigned value1){
    unsigned l = 0;
    unsigned r = this->pairs.size() - 1;
    unsigned pos;
    while (l<r) {
      unsigned m= l+((r-l)/2);
      if (this->pairs[m].value1 > value1) {
         r=m;
      } else if (this->pairs[m].value1 < value1) {
         l=m+1;
      } else {
         pos=m;
         this->cursor = this->pairs.begin() + pos;
         return true;
      }
    }

    return false;
}

bool DFullyAggregatedFactsSegment::Scan::readNextPage(){
    this->seg->limitStart += this->seg->limit;
    this->pairs.clear();
    return this->first();
}

const unsigned DFactsSegment::pageSize = 16384;


DFactsSegment::DFactsSegment(DDatabaseWrapper& wrapper, Database::DataOrder order): DSegment(wrapper), order(order){
    this->loadMetadata();
}

void DFactsSegment::loadMetadata(){    
    this->totalCardinality = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_CARD);

    switch(order){
        case Database::Order_Object_Predicate_Subject:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OPS_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OPS_G2);     
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OPS_PAGES);            
            break;
        case Database::Order_Object_Subject_Predicate:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OSP_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OSP_G2);
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_OSP_PAGES);            
            break;
        case Database::Order_Predicate_Object_Subject:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_POS_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_POS_G2); 
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_POS_PAGES);             
            break;
        case Database::Order_Predicate_Subject_Object:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_PSO_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_PSO_G2);
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_PSO_PAGES);
            break;
        case Database::Order_Subject_Object_Predicate:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SOP_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SOP_G2);
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SOP_PAGES);
            break;
        case Database::Order_Subject_Predicate_Object:
            this->groups1 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SPO_G1);
            this->groups2 = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SPO_G2); 
            this->pages = this->dwrapper.getIntMetadata(DDatabaseWrapper::IS_SPO_PAGES);
            break;
    }
}
