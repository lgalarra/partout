/* 
 * File:   CommonTypes.hpp
 * Author: luis
 * Created on Aug 24, 2011, 5:41 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 **/


#include "drdf/CommonTypes.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "drdf/DDatabaseWrapper.hpp"
#include <iostream>
#include <sstream>

using namespace drdf;    
using namespace std;

unsigned Host::hostId = 0;

bool SPARQLArgument::equivalent(const SPARQLArgument &arg){
    if(this->id_pred != arg.id_pred)
        return false;
    
    if(this->id_literal >= 0){
        if(this->id_literal != arg.id_literal)
            return false;
    }else{
        if(this->value.compare(arg.value) != 0){
            return false;
        }
    }
    
    return true;
}

bool SPARQLPredicate::getType(unsigned id, Type::ID &type){
    ostringstream sql;
    ResultSet rs;
    
    sql << "SELECT `type` FROM `Dictionary` WHERE `id` = " << id << ";";
    cout << sql.str() << endl;
    this->wrapper->execute(sql.str(), rs);
            
    if(!rs.next()) return false;
    
    int typeResult;
    rs.getInt("type", typeResult);
    type = static_cast<Type::ID>(typeResult);
    return true;
}

bool SPARQLPredicate::lookupSubtype(unsigned id, unsigned &result){
    ostringstream sql;
    ResultSet rs;
    
    sql << "SELECT `value` FROM `Dictionary` WHERE `id` = " << id << ";";
    this->wrapper->execute(sql.str(), rs);
            
    if(!rs.next()) return false;
    
    int subtype;
    rs.getInt("value", subtype);
    result = (unsigned)subtype;
    return true;

}


bool SPARQLPredicate::contradicts(const SPARQLPredicate &pred){
    //There are infinite of cases. The more cases, the best since we can save
    //unnecesary node lookups
    
    int equalOp = static_cast<int>(QueryGraph::Filter::Equal);
    int isLiteralOp = static_cast<int>(QueryGraph::Filter::Builtin_isliteral);
    int isIriOp = static_cast<int>(QueryGraph::Filter::Builtin_isiri);
    int langMatchOp = static_cast<int>(QueryGraph::Filter::Builtin_langmatches); 
    int typeMatchOp = static_cast<int>(QueryGraph::Filter::Builtin_datatype);
    
    if(this->id == pred.id && this->flag != pred.flag){
        cout << "drdf::SPARQLPredicate::contradicts, same ids, differente flags " << " id " << this->id << endl;
        return true;
    }else if(this->flag && pred.flag && this->operator_ == pred.operator_ && this->operator_ == equalOp && this->position == pred.position){
        //Second case, they are equal statements in the same position over different values

        //This should not happen
        if(this->args.size() != pred.args.size() && this->args.size() != 1)
            return false;
        
        if(!this->args[0].equivalent(pred.args[0])){
            cout << "drdf::SPARQLPredicate::contradicts, equal operators, different arguments" << " id " << this->id << endl;        
            return true;
        }
    }else if(this->flag && pred.flag && this->position == pred.position && ((this->operator_ == isIriOp && pred.operator_ == isLiteralOp) || (this->operator_ == isLiteralOp && pred.operator_ == isIriOp))){
        //Is literal and isUri presences
        cout << "drdf::SPARQLPredicate::contradicts, isIRI/isLiteral over the same the column" << " id " << this->id << endl;        
        return true;
    }else if(this->wrapper && this->position == pred.position){
        Type::ID aType;
        unsigned subtype1, subtype2;
        //Asking for a literal but the predicate definition says only URIs are stored for that position
        //or
        //Asking for a URI but the predicate definition says only Literals are stored for that position        
        if(this->operator_ == equalOp){
            if(pred.operator_ == isIriOp && this->getType(this->args[0].id_literal, aType) && aType != Type::URI){
                cout << "drdf::SPARQLPredicate::contradicts, the fragment contains only uris and an equality to a literal is given" << " id " << this->id << endl;         
                return true;
            }else if(pred.operator_ == isLiteralOp && this->getType(this->args[0].id_literal, aType) && aType == Type::URI){
                cout << "drdf::SPARQLPredicate::contradicts, the fragment contains only literals and an equality to a uri is given" << " id " << this->id << endl;                         
                return true;
            }else if( (pred.operator_ == langMatchOp || pred.operator_ == typeMatchOp)
                    && this->lookupSubtype(this->args[0].id_literal, subtype1) && this->lookupSubtype(pred.args[0].id_literal, subtype2) && subtype1 != subtype2){
                cout << "drdf::SPARQLPredicate::contradicts, subtypes do not match" << " id " << this->id << endl;                
                return true;                
            }
        }
        
        if(pred.operator_ == equalOp){
            if(this->operator_ == isIriOp && this->getType(pred.args[0].id_literal, aType) && aType != Type::URI){
                cout << "drdf::SPARQLPredicate::contradicts, the fragment contains only uris and an equality to a literal is given" << " id " << this->id << endl;                                         
                return true;
            }else if(this->operator_ == isLiteralOp && this->getType(pred.args[0].id_literal, aType) && aType == Type::URI){
                cout << "drdf::SPARQLPredicate::contradicts, the fragment contains only literals and an equality to a uri is given" << " id " << this->id << endl;                                         
                return true;
            }else if( (this->operator_ == langMatchOp || this->operator_ == typeMatchOp)
                    && this->lookupSubtype(this->args[0].id_literal, subtype1) && this->lookupSubtype(pred.args[0].id_literal, subtype2) && subtype1 != subtype2){
                cout << "drdf::SPARQLPredicate::contradicts, subtypes do not match" << " id " << this->id << endl;                
                return true;            
            }
        }        
    }
    
    return false;
}