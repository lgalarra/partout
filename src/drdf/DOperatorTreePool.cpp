/* 
 * File:   DOperatorTreePool.cpp
 * Author: luis
 * Created on September 25, 2011, 3:44 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "drdf/DOperatorTreePool.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "rts/operator/ResultsPrinter.hpp"
#include <iostream>

using namespace drdf;

DOperatorTreePool::DOperatorTreePool(DRuntime& runtime, const QueryGraph& query, Plan* plan, unsigned initialSize): runtime(runtime), queryGraph(query),
plan(plan), size(initialSize){
    populatePool();
}

DOperatorTreePool::~DOperatorTreePool() {
    while(!this->pool.empty()){
        DOperator *toRemove = this->pool.front();
        this->pool.pop();
        delete toRemove->getOperator();
        delete toRemove;
    }
}

void DOperatorTreePool::populatePool(){
    DOperatorTransformer transformer(&this->runtime.getDDatabase());
    for(unsigned i = 0; i < this->size; ++i){
        set<Host, cmpHosts> hosts;
        vector<Register*> outputRegisters;
        Operator *op;
        op = DCodeGen().translate(this->runtime, this->queryGraph, plan, outputRegisters, true);
        
        DebugPlanPrinter out(runtime.getRuntime(), false);        
        op->print(out);

        // And add the output generation
        ResultsPrinter::DuplicateHandling duplicateHandling = ResultsPrinter::ExpandDuplicates;
        switch (queryGraph.getDuplicateHandling()) {
          case QueryGraph::AllDuplicates: duplicateHandling=ResultsPrinter::ExpandDuplicates; break;
          case QueryGraph::CountDuplicates: duplicateHandling=ResultsPrinter::CountDuplicates; break;
          case QueryGraph::ReducedDuplicates: duplicateHandling=ResultsPrinter::ReduceDuplicates; break;
          case QueryGraph::NoDuplicates: duplicateHandling=ResultsPrinter::ReduceDuplicates; break;
          case QueryGraph::ShowDuplicates: duplicateHandling=ResultsPrinter::ShowDuplicates; break;
        }

        //Building the new operator tree
        DOperator *dop = transformer.buildDOperator(op);
        transformer.setLeavesHomeHosts(dop, hosts);
        string startHost = hosts.begin()->uri;
        dop = transformer.resolveMultipleSourcesLeaves(dop, &this->runtime);     
        assert(dop);
        assert(transformer.assignHomeHost(dop, startHost));
        dop->normalizeIds();
        dop->setDuplicateHandling(duplicateHandling);
        dop->setOutputRegisters(outputRegisters);        
        
        //Solve the projection mappings caused by the transformations
        vector<pair<Register*, Register*> > projectionMappings;
        transformer.findProjectionMappingsForOperator(&runtime, dop, projectionMappings);
        for(unsigned i = 0; i < dop->outputRegisters.size(); ++i){
            for(vector<pair<Register*, Register*> >::iterator mit = projectionMappings.begin(); mit != projectionMappings.end(); ++mit){
                if(mit->first == dop->outputRegisters[i])
                    dop->outputRegisters[i] = mit->second;
            }                
        } 
                            
        this->pool.push(dop);
    }
}

DOperator* DOperatorTreePool::getCleanDOperator(){
    if(this->pool.empty())
        this->populatePool();
    
    DOperator *next = this->pool.front();
    this->pool.pop();
    return next;
}
