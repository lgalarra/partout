/* 
 * File:   DOperatorCostFunction.cpp
 * Author: luis
 * Created on September 22, 2011, 10:23 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */


#include "drdf/DOperatorCostFunction.hpp"
#include "drdf/operator.h"
#include "cts/plangen/Costs.hpp"
#include <iostream>
#include <math.h>

using namespace drdf;
using namespace std;

/// Costs for a seek in 1/10ms
const unsigned DOperatorCostFunction::seekCosts = 95;
/// Costs for a sequential page read in 1/10ms
const unsigned DOperatorCostFunction::scanCosts = 17;
/// Number of CPU operations per 1/10ms
const unsigned DOperatorCostFunction::cpuSpeed = 100000;
/// Cost for sending and receiving a execRequest
const unsigned DOperatorCostFunction::execRequestCost = 95 * 1.6 + 17 * 1.6; //Cost for sending + cost for receiving
/// Cost for sending and receiving a prepareForExecRequest
const unsigned DOperatorCostFunction::prepareForExecRequestCost = 17 * 1.6 + 95 * 1.6; //Cost for sending + cost for receiving
// In number of 64 bits integers transmitted (8K)
const unsigned DOperatorCostFunction::remoteDataPageSize = 1024; 

static inline double evaluateFullyAggregatedIndexScan(DOperator *op, DDatabase &db){
   FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(op->getOperator());
   Database::DataOrder order = fais->getOrder();   
   unsigned scanned = fais->getExpectedOutputCardinality();
   unsigned fullSize = db.getFacts(order).getCardinality(); 
   if (scanned > fullSize)
      scanned = fullSize;
   if (!scanned) scanned=1;

   unsigned pages= 1 + static_cast<unsigned>(db.getAggregatedFacts(order).getPages()*(static_cast<double>(scanned)/static_cast<double>(fullSize)));
   return Costs::seekBtree() + Costs::scan(pages);
}


static inline double evaluateIndexScan(DOperator *op, DDatabase &db){
   IndexScan *is = static_cast<IndexScan*>(op->getOperator());
   Database::DataOrder order = is->getOrder();   
   unsigned scanned = is->getExpectedOutputCardinality();
   unsigned fullSize = db.getFacts(order).getCardinality();   
   unsigned pages= 1 + static_cast<unsigned>(db.getFacts(order).getPages()*(static_cast<double>(scanned)/static_cast<double>(fullSize)));
   return Costs::seekBtree() + Costs::scan(pages);    
}

static inline double evaluateAggregatedIndexScan(DOperator *op, DDatabase &db){
   AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(op->getOperator());
   Database::DataOrder order = ais->getOrder();   
   unsigned scanned = ais->getExpectedOutputCardinality();
   unsigned fullSize = db.getFacts(order).getCardinality();   

   if (scanned > fullSize)
      scanned = fullSize;
   if (!scanned) scanned = 1;
   
   unsigned pages= 1 + static_cast<unsigned>(db.getFullyAggregatedFacts(order).getPages()*(static_cast<double>(scanned)/static_cast<double>(fullSize)));
   return Costs::seekBtree() + Costs::scan(pages);    
}

static inline double evaluateMergeUnion(DOperator *op){
    return (op->getOperator()->getExpectedOutputCardinality()) / (double)DOperatorCostFunction::cpuSpeed;
}

static inline double evaluateMergeJoin(DOperator *op){
    double leftCard, rightCard;
    leftCard = op->getChild(0)->getOperator()->getExpectedOutputCardinality();
    rightCard = op->getChild(1)->getOperator()->getExpectedOutputCardinality();
    return Costs::mergeJoin(leftCard, rightCard);
}

static inline double evaluateHashGroupify(DOperator *op){
    //This operator is not pipelined since we have to retrieve everything from the input
    return op->getOperator()->getExpectedOutputCardinality() / (double)DOperatorCostFunction::cpuSpeed;
}

static inline double evaluateSort(DOperator *op){
    //Let's assume it takes approx O(n*lgn)
    //He does also dictionary lookups which will assume 
    //require 1 page to be retrieved per 20 lookups (arbitrary value)
    double card = op->getOperator()->getExpectedOutputCardinality();
    if(card > 1)
        return (card * (log(card) / log(2)) + (card / 20) * Costs::seekBtree() + Costs::scan(1)) / (double)DOperatorCostFunction::cpuSpeed;
    else
        return 1 / (double)DOperatorCostFunction::cpuSpeed;
}

static inline double evaluateUnion(DOperator *op){
	return (op->getOperator()->getExpectedOutputCardinality() / (double)DOperatorCostFunction::cpuSpeed);
}

static inline double evaluateRemoteOperation(DOperator *child){
    //Check how many exec requests will be needed
    unsigned execRequests = ceil(child->getOperator()->getExpectedOutputCardinality() / (double)DOperatorCostFunction::remoteDataPageSize);
    return DOperatorCostFunction::execRequestCost * execRequests + DOperatorCostFunction::prepareForExecRequestCost;
}

static inline double evaluateSimpleGroupify(DOperator *op){
    //This is basically the time of inserting the tuples in the sorted data structure (n*log(n))   
    //This is not a pipelined operator :(
    double card = op->getOperator()->getExpectedOutputCardinality();
    if(card > 1)
        return card * (log(card) / log(2));
    else
        return 1 / (double)DOperatorCostFunction::cpuSpeed;
}

static inline double evaluateMultiMergeUnion(DOperator *op){
    return (op->getOperator()->getExpectedOutputCardinality()) / (double)DOperatorCostFunction::cpuSpeed;
}

static inline double evaluateSelection(DOperator *op){
    return (op->getOperator()->getExpectedOutputCardinality()) / (double)DOperatorCostFunction::cpuSpeed;
}

double DOperatorCostFunction::evaluate(DOperator *op, DDatabase &db){
    double nodeCost;
    switch(op->getType()){
        case DOperator::TFullyAggregatedIndexScan:
            nodeCost = evaluateFullyAggregatedIndexScan(op, db);
            break;
        case DOperator::TAggregatedIndexScan:
            nodeCost = evaluateAggregatedIndexScan(op, db);
            break;
        case DOperator::TIndexScan:
            nodeCost = evaluateIndexScan(op, db);
            break;
        case DOperator::TMergeJoin:
            nodeCost = evaluateMergeJoin(op);
            break;
        case DOperator::TMergeUnion:
            nodeCost = evaluateMergeUnion(op);
            break;
        case DOperator::TSort:
            nodeCost = evaluateSort(op);
            break;
        case DOperator::TUnion:
            nodeCost = evaluateUnion(op);
            break;
        case DOperator::THashGroupify:
            nodeCost = evaluateHashGroupify(op);
            break;
        case DOperator::TSimpleGroupify:
            nodeCost = evaluateSimpleGroupify(op);
            break;
        case DOperator::TMultiMergeUnion:
            nodeCost = evaluateMultiMergeUnion(op);
            break;
        case DOperator::TSelection:
            nodeCost = evaluateSelection(op);
            break;
        default:
            nodeCost = 1 / (double)DOperatorCostFunction::cpuSpeed;
            break;
    }
    
    //Now consider the children
    double maxCostChild = 0;
    vector<DOperator*> &children = op->getChildren();
    for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit){
        double childCost = evaluate(*chit, db);
        if(op->getHost().compare((*chit)->getHost()) != 0){
            childCost += evaluateRemoteOperation(*chit);
        }
        
        if(childCost > maxCostChild)
            maxCostChild = childCost;
    }
    
    op->setEstimatedCost(nodeCost + maxCostChild);
    
    return op->getEstimatedCost();
    
}

double DOperatorCostFunction::evaluateCommunicationCost(DOperator *op, DDatabase &db){
    double maxCostChild = 0.0;
    if(op->isLeaf())
        return maxCostChild;
   
    vector<DOperator*> &children = op->getChildren();
    for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit){
        double childCost = evaluateCommunicationCost(*chit, db);
        if(op->getHost().compare((*chit)->getHost()) != 0){
            childCost += evaluateRemoteOperation(*chit);
        }
        
        if(childCost > maxCostChild)
            maxCostChild = childCost;
    }
    
    return maxCostChild;
}

double DOperatorCostFunction::evaluateTotalCommunicationCost(DOperator *op, DDatabase &db){
    double totalCost = 0.0;
    if(op->isLeaf())
        return totalCost;
   
    vector<DOperator*> &children = op->getChildren();
    for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit){
        double childCost = evaluateTotalCommunicationCost(*chit, db);
        if(op->getHost().compare((*chit)->getHost()) != 0){
            childCost += evaluateRemoteOperation(*chit);
        }
        
        totalCost += childCost;
    }
    
    return totalCost;
    
}
