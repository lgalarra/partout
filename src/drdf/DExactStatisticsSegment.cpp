/* 
 * File:   DExactStatisticsSegment.cpp
 * Author: lgalarra
 * 
 * Created on September 3, 2011, 5:18 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <iosfwd>
#include <iostream>
#include <sstream>
#include <map>
#include <stdlib.h>
#include "drdf/DExactStatisticsSegment.hpp"

using namespace drdf;
using namespace std;

//Roughly 1 MB
const unsigned DExactStatisticsSegment::cacheJoinSize = (1024 * 1024) / sizeof(std::pair<Triple, StatisticsEntry>);
const unsigned DExactStatisticsSegment::cacheCardsSize = (1024 * 1024) / sizeof(std::pair<Triple, unsigned>);
const unsigned DExactStatisticsSegment::total_cardinality_key = 1;

DExactStatisticsSegment::DExactStatisticsSegment(DDatabaseWrapper &dataWrapper): DSegment(dataWrapper) {
    //Check the total cardinality
    ostringstream sql;
    ResultSet rs;
    string cardString;
    sql << "SELECT * FROM `Metadata` WHERE `id` = " << total_cardinality_key << ";";
    this->dwrapper.execute(sql.str(), rs);
    if(!rs.next()){
        this->totalCardinality = 0;
        return;
    }
    rs.getString("value", cardString);
    this->totalCardinality = atoi(cardString.c_str());
}

DExactStatisticsSegment::~DExactStatisticsSegment() {}

/// Compute the cardinality of a single pattern
unsigned DExactStatisticsSegment::getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant){
    if(~subjectConstant && ~predicateConstant && ~objectConstant) return 1;
    if(!~subjectConstant && !~predicateConstant && !~objectConstant) return this->totalCardinality;

    Triple search(subjectConstant, predicateConstant, objectConstant);
    map<Triple, unsigned, cmpTriple>::iterator it = this->cacheCards.find(search);
    
    if(it == this->cacheCards.end()){
        ResultSet rs;
        int cardinality;
        string sql = getCardinalityQuery(subjectConstant, predicateConstant, objectConstant);
        this->dwrapper.execute(sql, rs);
        if(!rs.next()) return 1;
        rs.getInt("card", cardinality);
        if(this->cacheCards.size() >= cacheCardsSize)
            this->freeCacheCardsSpace();
        this->cacheCards[search] = cardinality;
        return cardinality;
    }else{
        return it->second;
    }
    
    return 1;
}     

unsigned DExactStatisticsSegment::getHostId(string host){
    stringstream sql;
    sql << "SELECT id FROM `Host` WHERE uri LIKE \"" << host << "\" ;";
    ResultSet rs;
    int hid;
    this->dwrapper.execute(sql.str(), rs);
    if(!rs.next()) return 0;
    rs.getInt("id", hid);
    return hid;
}

unsigned DExactStatisticsSegment::getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, string host){
    if(~subjectConstant && ~predicateConstant && ~objectConstant) return 1;
    if(!~subjectConstant && !~predicateConstant && !~objectConstant) return this->totalCardinality;

    ResultSet rs;
    int cardinality;
    string sql = getCardinalityQuery(subjectConstant, predicateConstant, objectConstant, host);
    this->dwrapper.execute(sql, rs);
    if(!rs.next()) return 1;
    rs.getInt("card", cardinality);
    return cardinality;
}

unsigned DExactStatisticsSegment::getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, unsigned host){
    if(~subjectConstant && ~predicateConstant && ~objectConstant) return 1;
    if(!~subjectConstant && !~predicateConstant && !~objectConstant) return this->totalCardinality;

    ResultSet rs;
    int cardinality;
    string sql = getCardinalityQuery(subjectConstant, predicateConstant, objectConstant, host);
    this->dwrapper.execute(sql, rs);
    if(!rs.next()) return 1;
    rs.getInt("card", cardinality);
    return cardinality;
}


void DExactStatisticsSegment::freeCacheCardsSpace(){
    while(this->cacheCards.size() > cacheCardsSize / 3){
        this->cacheCards.erase(this->cacheCards.begin());
    }
}

void DExactStatisticsSegment::freeCacheJoinsSpace(){
    while(this->cacheJoins.size() > cacheJoinSize / 3){
        this->cacheJoins.erase(this->cacheJoins.begin());
    }    
}

string DExactStatisticsSegment::getCardinalityQuery(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant){
    ostringstream sql;

    if (~subjectConstant) {
      if (~predicateConstant) {
         if (~objectConstant) {
            return "";
         } else {
             sql << "SELECT * FROM ASSubjectPredicate WHERE value1 = " << subjectConstant << " AND value2 = " << predicateConstant << ";";
         }
      } else {
         if (~objectConstant) {
             sql << "SELECT * FROM ASSubjectObject WHERE value1 = " << subjectConstant << " AND value2 = " << objectConstant << ";";             
         } else {
             sql << "SELECT * FROM FASSubject WHERE value = " << subjectConstant << ";";             
         }
      }
   } else {
      if (~predicateConstant) {
         if (~objectConstant) {
             sql << "SELECT * FROM ASPredicateObject WHERE value1 = " << predicateConstant << " AND value2 = " << objectConstant << ";";             
         } else {
             sql << "SELECT * FROM FASPredicate WHERE value = " << predicateConstant << ";";                          
         }
      } else {
         if (~objectConstant) {
             sql << "SELECT * FROM FASObject WHERE value = " << objectConstant << ";";                          
         } else {
            return "";
         }
      }
   }
    
    return sql.str();
}


string DExactStatisticsSegment::getCardinalityQuery(unsigned subjectConstant, unsigned predicateConstant,unsigned objectConstant, unsigned id_host){
    ostringstream sql;

    if (~subjectConstant) {
      if (~predicateConstant) {
         if (~objectConstant) {
            return "";
         } else {
             sql << "SELECT * FROM ASSubjectPredicateH WHERE value1 = " << subjectConstant << " AND value2 = " << predicateConstant << " AND id_host = " << id_host << ";";
         }
      } else {
         if (~objectConstant) {
             sql << "SELECT * FROM ASSubjectObjectH WHERE value1 = " << subjectConstant << " AND value2 = " << objectConstant << " AND id_host = " << id_host << ";";             
         } else {
             sql << "SELECT * FROM FASSubjectH WHERE value = " << subjectConstant << " AND id_host = " << id_host << ";";             
         }
      }
   } else {
      if (~predicateConstant) {
         if (~objectConstant) {
             sql << "SELECT * FROM ASPredicateObjectH WHERE value1 = " << predicateConstant << " AND value2 = " << objectConstant << " AND id_host = " << id_host << ";";             
         } else {
             sql << "SELECT * FROM FASPredicateH WHERE value = " << predicateConstant << " AND id_host = " << id_host << ";";                          
         }
      } else {
         if (~objectConstant) {
             sql << "SELECT * FROM FASObjectH WHERE value = " << objectConstant << ";";                          
         } else {
            return "";
         }
      }
   }
    
    return sql.str();

}

string DExactStatisticsSegment::getCardinalityQuery(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, string host){
    unsigned id_host = this->getHostId(host);
    return getCardinalityQuery(subjectConstant, predicateConstant, objectConstant, id_host);
    
}


/// Compute the join selectivity
double DExactStatisticsSegment::getJoinSelectivity(bool s1c,unsigned s1,bool p1c,unsigned p1,bool o1c,unsigned o1,bool s2c,unsigned s2,bool p2c,unsigned p2,bool o2c,unsigned o2){
    // Compute the individual sizes
   double card1=getCardinality(s1c?s1:~0u,p1c?p1:~0u,o1c?o1:~0u);
   double card2=getCardinality(s2c?s2:~0u,p2c?p2:~0u,o2c?o2:~0u);

   // Check that 1 is smaller than 2
   if (card2<card1) {
      swap(card1,card2);
      swap(s1c,s2c);
      swap(s1,s2);
      swap(p1c,p2c);
      swap(p1,p2);
      swap(o1c,o2c);
      swap(o1,o2);
   }

   // Lookup the join info
   unsigned long long joinInfo[9]; double crossCard;
   if (!getJoinInfo(joinInfo,s1c?s1:~0u,p1c?p1:~0u,o1c?o1:~0u)) {
      // Could no locate 1, check 2
      if (!getJoinInfo(joinInfo,s2c?s2:~0u,p2c?p2:~0u,o2c?o2:~0u)) {
         // Could not locate either, guess!
         return 1; // we could guess 0 here, as the entry was not found, but this might be due to stale statistics
      } else {
         crossCard=card2*static_cast<double>(totalCardinality);
      }
   } else {
      crossCard=card1*static_cast<double>(totalCardinality);
   }

   // And construct the most likely result size
   double resultSize=crossCard;
   if ((s1==s2)&&(!s1c)&&(!s2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[0]));
      if (p1c&&o1c) resultSize=min(resultSize,card2);
      if (p2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((s1==p2)&&(!s1c)&&(!p2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[1]));
      if (p1c&&o1c) resultSize=min(resultSize,card2);
      if (s2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((s1==o2)&&(!s1c)&&(!o2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[2]));
      if (p1c&&o1c) resultSize=min(resultSize,card2);
      if (s2c&&p2c) resultSize=min(resultSize,card1);
   }
   if ((p1==s2)&&(!p1c)&&(!s2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[3]));
      if (s1c&&o1c) resultSize=min(resultSize,card2);
      if (p2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((p1==p2)&&(!p1c)&&(!p2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[4]));
      if (s1c&&o1c) resultSize=min(resultSize,card2);
      if (s2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((p1==o2)&&(!p1c)&&(!o2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[5]));
      if (s1c&&o1c) resultSize=min(resultSize,card2);
      if (s2c&&p2c) resultSize=min(resultSize,card1);
   }
   if ((o1==s2)&&(!o1c)&&(!s2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[6]));
      if (s1c&&p1c) resultSize=min(resultSize,card2);
      if (p2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((o1==p2)&&(!o1c)&&(!p2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[7]));
      if (s1c&&p1c) resultSize=min(resultSize,card2);
      if (s2c&&o2c) resultSize=min(resultSize,card1);
   }
   if ((o1==o2)&&(!o1c)&&(!o2c)) {
      resultSize=min(resultSize,static_cast<double>(joinInfo[8]));
      if (s1c&&p1c) resultSize=min(resultSize,card2);
      if (s2c&&p2c) resultSize=min(resultSize,card1);
   }

   // Derive selectivity
   return resultSize/crossCard;

}

bool DExactStatisticsSegment::getJoinInfo(unsigned long long* joinInfo,unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant){
   // Reset all entries to "impossible" first
   ostringstream sql;
   ResultSet rs;
   int joinInfoTmp[9];
   ostringstream subCond, predCond, objCond;
   
   map<Triple, StatisticsEntry>::iterator tit;
   Triple search(subjectConstant, predicateConstant, objectConstant);
   tit = this->cacheJoins.find(search);
   if(tit != this->cacheJoins.end()){
       joinInfo[0] = tit->second.s_s;
       joinInfo[1] = tit->second.s_p;
       joinInfo[2] = tit->second.s_o;
       joinInfo[3] = tit->second.p_s;
       joinInfo[4] = tit->second.p_p;
       joinInfo[5] = tit->second.p_o;
       joinInfo[6] = tit->second.o_s;
       joinInfo[7] = tit->second.o_p;
       joinInfo[8] = tit->second.o_o;
       
       return true;
   }
   
   for (unsigned index=0;index<9;index++){
      joinInfo[index]=~static_cast<unsigned long long>(0);
   }
   

   if(~subjectConstant)
       subCond << " = " << subjectConstant;
   else
       subCond << " isnull";
   
   if(~predicateConstant)
       predCond << " = " << predicateConstant;
   else
       predCond << " isnull";
   
   if(~objectConstant)
       objCond << " = " << objectConstant;
   else
       objCond << " isnull";

   sql << "SELECT * FROM `Statistics` WHERE `subject` " << subCond.str() << " AND `predicate` " << predCond.str() << " AND `object` " << objCond.str() << ";";
   
   this->dwrapper.execute(sql.str(), rs);
   
   if(!rs.next()) return false;
  
   rs.getInt("s_s", joinInfoTmp[0]);
   rs.getInt("s_p", joinInfoTmp[1]);
   rs.getInt("s_o", joinInfoTmp[2]);
   rs.getInt("p_s", joinInfoTmp[3]);
   rs.getInt("p_p", joinInfoTmp[4]);
   rs.getInt("p_o", joinInfoTmp[5]);
   rs.getInt("o_s", joinInfoTmp[6]);
   rs.getInt("o_p", joinInfoTmp[7]);
   rs.getInt("o_o", joinInfoTmp[8]);
   
   for(unsigned i = 0; i < 9; ++i){
       if(joinInfoTmp[i] == DDatabaseWrapper::NULL_INT)
            joinInfo[i] = ~static_cast<unsigned long long>(0);
       else
            joinInfo[i] = (unsigned)joinInfoTmp[i];           
   }
   
   if(this->cacheJoins.size() >= cacheJoinSize)
       this->freeCacheJoinsSpace();

   StatisticsEntry se;
   se.subject = subjectConstant;
   se.predicate = predicateConstant;
   se.object = objectConstant;
   se.s_s = joinInfo[0];
   se.s_p = joinInfo[1];
   se.s_o = joinInfo[2];
   se.p_s = joinInfo[3];
   se.p_p = joinInfo[4];
   se.p_o = joinInfo[5];
   se.o_s = joinInfo[6];
   se.o_p = joinInfo[7];
   se.o_o = joinInfo[8];
           
   this->cacheJoins[search] = se;
   
   return true;
}