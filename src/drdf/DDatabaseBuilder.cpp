/* 
 * File:   DDatabaseBuilder.cpp
 * Author: lgalarra
 * 
 * Created on August 22, 2011, 5:33 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */


#include <sqlite3.h>
#include <iostream>
#include <sstream>
#include <set>
#include <vector>
#include <algorithm>
#include <cassert>
#include <cstring>

#include "cts/infra/QueryGraph.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "rts/segment/ExactStatisticsSegment.hpp"

#include "drdf/DDatabaseBuilder.hpp"
#include "drdf/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"
#include "partitioning/TempFile.hpp"
#include "infra/osdep/MemoryMappedFile.hpp"

using namespace drdf;
using namespace stdpartitioning;
using namespace std;

#ifndef FULL_DICTIONARY
#define FULL_DICTIONARY
#endif

/// Maximum amount of usable memory. XXX detect at runtime!
static const unsigned memoryLimit = sizeof(void*)*(1<<27);

/// A memory range
struct Range {
   const char* from,*to;

   /// Constructor
   Range(const char* from,const char* to) : from(from),to(to) {}

   /// Some content?
   bool equals(const Range& o) { return ((to-from)==(o.to-o.from))&&(memcmp(from,o.from,to-from)==0); }
};
//---------------------------------------------------------------------------
/// Sort wrapper that colls the comparison function
struct CompareSorter
{
   /// Comparison function
   typedef int (*func)(const char*,const char*);

   /// Comparison function
   const func compare;

   /// Constructor
   CompareSorter(func compare) : compare(compare) {}

   /// Compare two entries
   bool operator()(const Range& a,const Range& b) const { return compare(a.from,b.from)<0; }
};

//---------------------------------------------------------------------------
static char* spool(char* ofs,TempFile& out,const vector<Range>& items,bool eliminateDuplicates)
   // Spool items to disk
{
   Range last(0,0);
   for (vector<Range>::const_iterator iter=items.begin(),limit=items.end();iter!=limit;++iter) {
      if ((!eliminateDuplicates)||(!last.equals(*iter))) {
         last=*iter;
         out.write(last.to-last.from,last.from);
         ofs+=last.to-last.from;
      }
   }
   return ofs;
}

static const char* skipIdIdIdString(const char* reader)
   // Skip a materialized id/string/id triple
{
   return TempFile::skipString(TempFile::skipId(TempFile::skipId(TempFile::skipId(reader))));
}

static const char* skipIdId(const char* reader)
   // Skip a materialized id/string/id triple
{
   return TempFile::skipId(TempFile::skipId(reader));
}

static int compareId(const char* left,const char* right)
   // Sort by integer value
{
   uint64_t leftId,rightId;
   TempFile::readId(left,leftId);
   TempFile::readId(right,rightId);
   if (leftId<rightId)
      return -1;
   if (leftId>rightId)
      return 1;
   return 0;
}

static int compareId12(const char* left, const char *right){
    Pair p1, p2;
    uint64_t leftId1, rightId1, leftId2, rightId2;
    TempFile::readId(left, leftId1);
    TempFile::readId(right, rightId1);
    TempFile::readId(left, leftId2);
    TempFile::readId(right,rightId2);
    
    p1.value1 = static_cast<unsigned>(leftId1);
    p1.value2 = static_cast<unsigned>(leftId2);
    p2.value1 = static_cast<unsigned>(rightId1);
    p2.value2 = static_cast<unsigned>(rightId2);
    
    if(p1 > p2)
        return 1;
    else if(p1 < p2)
        return -1;
    else
        return 0;
}

//---------------------------------------------------------------------------
static void sort(TempFile& in,TempFile& out,const char* (*skip)(const char*),int (*compare)(const char*,const char*),bool eliminateDuplicates)
   // Sort a temporary file
{
   // Open the input
   in.close();
   MemoryMappedFile mappedIn;
   assert(mappedIn.open(in.getFile().c_str()));
   const char* reader=mappedIn.getBegin(),*limit=mappedIn.getEnd();

   // Produce runs
   vector<Range> runs;
   TempFile intermediate(out.getBaseFile());
   char* ofs=0;
   while (reader<limit) {
      // Collect items
      vector<Range> items;
      const char* maxReader=reader+memoryLimit;
      while (reader<limit) {
         const char* start=reader;
         reader=skip(reader);
         items.push_back(Range(start,reader));

         // Memory Overflow?
         if ((reader+(sizeof(Range)*items.size()))>maxReader)
            break;
      }

      // Sort the run
      std::sort(items.begin(),items.end(),CompareSorter(compare));

      // Did everything fit?
      if ((reader==limit)&&(runs.empty())) {
         spool(0,out,items,eliminateDuplicates);
         break;
      }

      // No, spool to intermediate file
      char* newOfs=spool(ofs,intermediate,items,eliminateDuplicates);
      runs.push_back(Range(ofs,newOfs));
      ofs=newOfs;
   }
   intermediate.close();
   mappedIn.close();

   // Do we have to merge runs?
   if (!runs.empty()) {
      // Map the ranges
      MemoryMappedFile tempIn;
      assert(tempIn.open(intermediate.getFile().c_str()));
      for (vector<Range>::iterator iter=runs.begin(),limit=runs.end();iter!=limit;++iter) {
         (*iter).from=tempIn.getBegin()+((*iter).from-static_cast<char*>(0));
         (*iter).to=tempIn.getBegin()+((*iter).to-static_cast<char*>(0));
      }

      // Sort the run heads
      std::sort(runs.begin(),runs.end(),CompareSorter(compare));

      // And merge them
      Range last(0,0);
      while (!runs.empty()) {
         // Write the first entry if no duplicate
         Range head(runs.front().from,skip(runs.front().from));
         if ((!eliminateDuplicates)||(!last.equals(head)))
            out.write(head.to-head.from,head.from);
         last=head;

         // Update the first entry. First entry done?
         if ((runs.front().from=head.to)==runs.front().to) {
            runs[0]=runs[runs.size()-1];
            runs.pop_back();
         }

         // Check the heap condition
         unsigned pos=0,size=runs.size();
         while (pos<size) {
            unsigned left=2*pos+1,right=left+1;
            if (left>=size) break;
            if (right<size) {
               if (compare(runs[pos].from,runs[left].from)>0) {
                  if (compare(runs[pos].from,runs[right].from)>0) {
                     if (compare(runs[left].from,runs[right].from)<0) {
                        std::swap(runs[pos],runs[left]);
                        pos=left;
                     } else {
                        std::swap(runs[pos],runs[right]);
                        pos=right;
                     }
                  } else {
                     std::swap(runs[pos],runs[left]);
                     pos=left;
                  }
               } else if (compare(runs[pos].from,runs[right].from)>0) {
                  std::swap(runs[pos],runs[right]);
                  pos=right;
               } else break;
            } else {
               if (compare(runs[pos].from,runs[left].from)>0) {
                  std::swap(runs[pos],runs[left]);
                  pos=left;
               } else break;
            }
         }
      }
   }

   out.close();
}

DDatabaseBuilder::DDatabaseBuilder(const char *filename, string &tmpPrefix): file(filename), tmpPrefix(tmpPrefix) {}


DDatabaseBuilder::~DDatabaseBuilder() {}

bool DDatabaseBuilder::start(){
    return this->out.create(this->file);
}

void DDatabaseBuilder::close(){
    this->out.close();
}

bool DDatabaseBuilder::execute(string sql){
    int rc;
    return this->execute(sql, rc);
}

bool DDatabaseBuilder::execute(string sql, int &rc){
    char *zErrMsg = NULL;
    
    rc = sqlite3_exec(this->out.db, sql.c_str(), NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK){
        cerr << zErrMsg << endl;
        sqlite3_free(zErrMsg);
        return false;
    }
    
    return true;    
}

bool DDatabaseBuilder::writeDictionaryEntries(FragmentFilesAllocationMap &filesMap, map<string, drdf::Host> &hostMappings, map<string, qload::FrequentElementDescriptor> &freqElements, Database &inDb){
    TempFile mergedDict(tmpPrefix + string(this->file) + ".dict");
    TempFile sortedDict(tmpPrefix + string(this->file) + "_sorted.dict");
    MemoryMappedFile sin;
    //Write the entries gathered during fragment construction
    for(FragmentFilesAllocationMap::iterator fit = filesMap.begin(); fit != filesMap.end(); ++fit){
        MemoryMappedFile in;
        string partialFile = tmpPrefix + fit->second + ".dict";
        assert(in.open(partialFile.c_str()));
#ifndef FULL_DICTIONARY
        TempFile hostMappingsFile(string(this->file) + "_" + fit->first + ".mappings");
        TempFile sortedHostMappingsFile(string(this->file) + "_" + fit->first + ".sortedmappings");
        ostringstream insert;
#endif
        for (const char* iter=in.getBegin(),*limit=in.getEnd();iter!=limit;) {
            uint64_t id, type, subtype;          
            const char *value;
            unsigned len;
            iter=TempFile::readId(iter,id);
            iter=TempFile::readId(iter,type);
            iter=TempFile::readId(iter,subtype);
            iter=TempFile::readString(iter, len, value);
            mergedDict.writeId(id);
            mergedDict.writeId(type);
            mergedDict.writeId(subtype);
            mergedDict.writeString(len, value);
#ifndef FULL_DICTIONARY
            hostMappingsFile.writeId(id);
            hostMappingsFile.writeId(hostId);
#endif
        }
        //Remove the partial file => not needed
        remove(partialFile.c_str());
#ifndef FULL_DICTIONARY        
        //Time to insert the mappings
        string smf = string(this->file) + "_" + fit->first + ".sortedmappings";
        sort(hostMappingsFile, sortedHostMappingsFile, skipIdId, compareId12, true);
        MemoryMappedFile min;
        assert(min.open(smf.c_str()));
               
        for (const char* iter=min.getBegin(),*_limit=min.getEnd();iter!=_limit;) {
            uint64_t id, host;          
            iter=TempFile::readId(iter,id);
            iter=TempFile::readId(iter,host);
            if(!this->out.dataWrapper->execute(insert.str())){
                cerr << "Query " << insert.str() << " failed! " << endl;
            }
        }
        
        sortedHostMappingsFile.discard(true);
        hostMappingsFile.discard(true);
#endif
    }
    
    //Write the frequent entries
    for(map<string, qload::FrequentElementDescriptor>::iterator feit = freqElements.begin(); feit != freqElements.end(); ++feit){
        Type::ID type;
        unsigned subtype, id;
        string text;
        SPARQLParser::Element &element = feit->second.second;
        text = element.value;
        switch(element.type){
            case SPARQLParser::Element::IRI:
                type = Type::URI;
                subtype = 0;
                break;
            case SPARQLParser::Element::Literal:
                //Check subtype                
                switch(element.subType){
                    case SPARQLParser::Element::None:
                        subtype = 0;
                        type = Type::Literal;
                        break;
                    case SPARQLParser::Element::CustomType:
                        if (element.subTypeValue=="http://www.w3.org/2001/XMLSchema#string") {
                           type=Type::String;
                        } else if (element.subTypeValue=="http://www.w3.org/2001/XMLSchema#integer") {
                           type=Type::Integer;
                        } else if (element.subTypeValue=="http://www.w3.org/2001/XMLSchema#decimal") {
                           type=Type::Decimal;
                        } else if (element.subTypeValue=="http://www.w3.org/2001/XMLSchema#double") {
                           type=Type::Double;
                        } else if (element.subTypeValue=="http://www.w3.org/2001/XMLSchema#boolean") {
                           type=Type::Boolean;
                        } else {
                           type=Type::CustomType;
                        }                                                
                        //Lookup the id for the subtype URI
                        if(!inDb.getDictionary().lookup(element.subTypeValue, Type::URI, 0, subtype))
                            continue;
                        break;
                    case SPARQLParser::Element::CustomLanguage:
                        type = Type::CustomLanguage;                                                                        
                        if(!inDb.getDictionary().lookup(element.subTypeValue, Type::String, 0, subtype))
                            continue;
                        break;
                        
                }
                break;
            case SPARQLParser::Element::Variable:
                continue;
        }
        
        if(!inDb.getDictionary().lookup(feit->second.second.value, type, subtype, id))
            continue;
        
        mergedDict.writeId(static_cast<uint64_t>(id));
        mergedDict.writeId(static_cast<uint64_t>(type));
        mergedDict.writeId(static_cast<uint64_t>(subtype));
        mergedDict.writeString(text.size(), text.c_str());                
    }
        
    sort(mergedDict, sortedDict, skipIdIdIdString, compareId, true);
    mergedDict.discard(true);
    assert(sin.open(sortedDict.getFile().c_str()));
    
    const char* iter=sin.getBegin();
    const char *limit=sin.getEnd();
    while(iter != limit){
        ostringstream sql;            
        uint64_t fid, ftype, fsubtype;          
        const char *fvalue;
        unsigned flen;
        string fliteralText;
        iter=TempFile::readId(iter,fid);
        iter=TempFile::readId(iter,ftype);
        iter=TempFile::readId(iter,fsubtype);
        iter=TempFile::readString(iter, flen, fvalue);
        for(const char* c = fvalue; c < (fvalue + flen); ++c) fliteralText.append(1, *c);                  
        char *charSql = sqlite3_mprintf("INSERT INTO `Dictionary` SELECT %llu as id, %llu as type, %llu as subtype, '%q' as value ", fid, ftype, fsubtype, fliteralText.c_str());
        sql << charSql;
        sqlite3_free(charSql);
        
        for(unsigned i = 0; iter!=limit && i < 499; ++i){
             uint64_t id, type, subtype;          
             const char *value;
             unsigned len;
             iter=TempFile::readId(iter,id);
             iter=TempFile::readId(iter,type);
             iter=TempFile::readId(iter,subtype);
             iter=TempFile::readString(iter, len, value);
             string literalText;
             for(const char* c = value; c < (value + len); ++c) literalText.append(1, *c);
             charSql = sqlite3_mprintf("UNION SELECT %llu, %llu, %llu, '%q' ", id, type, subtype, literalText.c_str());
             sql << charSql;   
             sqlite3_free(charSql);
        }        
        
        sql << ";";


        bool returnVal = this->execute(sql.str());        
        sortedDict.discard(true);
        
        if(!returnVal) return false;
    }

    return true;
}

bool DDatabaseBuilder::writePredicatesDefinition(vector<stdpartitioning::SPARQLPredicate> &predicates){
    vector<stdpartitioning::SPARQLPredicate>::iterator pedit;
    unsigned nStatements = 0;
    ostringstream sql;
    bool success;
    cout << "writePredicatesDefinition::query: ";
    for(pedit = predicates.begin(); pedit != predicates.end(); ++pedit){        
        
        //Here we have to analyze some nice cases
        if(pedit->predicate.type == QueryGraph::Filter::Equal){
            if(pedit->predicate.arg1->type == QueryGraph::Filter::Variable){
                sql << "INSERT INTO `Predicate` VALUES(" << pedit->id << ", " << pedit->position << ", " << pedit->predicate.type << ");"; 
                ++nStatements;
            }else if(pedit->predicate.arg1->type == QueryGraph::Filter::Builtin_datatype){
                sql << "INSERT INTO `Predicate` VALUES(" << pedit->id << ", " << pedit->position << ", " << QueryGraph::Filter::Builtin_datatype << ");";                            
                ++nStatements;                
            }
        }else{
            sql << "INSERT INTO `Predicate` VALUES(" << pedit->id << ", " << pedit->position << ", " << pedit->predicate.type << ");";             
            ++nStatements;
        }

        vector<pair<unsigned, string> > arguments;
        vector<pair<unsigned, string> >::iterator arit;        
        pedit->argumentsList(arguments);
        unsigned ind = 0;
        cout << "Storing predicate: " << *pedit << endl;
        for(arit = arguments.begin(); arit != arguments.end(); ++arit){
            
            sql << "INSERT INTO `Argument` VALUES(" << pedit->id  << ", " << ind << ", ";
            
            if(~(arit->first))
                sql << arit->first;
            else
                sql << "NULL";
            
            if(arit->second.empty())    
                sql << ", NULL);";
            else{
                char *charSql = sqlite3_mprintf(", '%q');", arit->second.c_str());
                sql << charSql;
                sqlite3_free(charSql);
            }
            
            ++ind;
            ++nStatements;
        }        
        
        if(nStatements > 450){
            cout << sql.str();
            success = execute(sql.str());
            if(success){
                sql.str("");
                nStatements = 0;
            }else{
                return false;
            }
        }            
    }    
    
    cout << sql.str() << endl;
    if(!sql.str().empty())
        return execute(sql.str());
    else 
        return true;
}

bool DDatabaseBuilder::writeSegmentsMetadata(Database &db){
    ostringstream sql;
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SP_G1 << ", '" << db.getAggregatedFacts(Database::Order_Subject_Predicate_Object).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SP_G2 << ", '" << db.getAggregatedFacts(Database::Order_Subject_Predicate_Object).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SP_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Object_Predicate_Subject).getPages() << "'" << ");";
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SO_G1 << ", '" << db.getAggregatedFacts(Database::Order_Subject_Object_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SO_G2 << ", '" << db.getAggregatedFacts(Database::Order_Subject_Object_Predicate).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_SO_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Subject_Object_Predicate).getPages() << "'" << ");";
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PS_G1 << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Subject_Object).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PS_G2 << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Subject_Object).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PS_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Subject_Object).getPages() << "'" << ");";
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PO_G1 << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Object_Subject).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PO_G2 << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Object_Subject).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_PO_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Predicate_Object_Subject).getPages() << "'" << ");";
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OS_G1 << ", '" << db.getAggregatedFacts(Database::Order_Object_Subject_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OS_G2 << ", '" << db.getAggregatedFacts(Database::Order_Object_Subject_Predicate).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OS_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Object_Subject_Predicate).getPages() << "'" << ");";
        
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OP_G1 << ", '" << db.getAggregatedFacts(Database::Order_Object_Predicate_Subject).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OP_G2 << ", '" << db.getAggregatedFacts(Database::Order_Object_Predicate_Subject).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::AS_OP_PAGES << ", '" << db.getAggregatedFacts(Database::Order_Object_Predicate_Subject).getPages() << "'" << ");";

    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_O_G1 << ", '" << db.getFullyAggregatedFacts(Database::Order_Object_Subject_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_O_PAGES << ", '" << db.getFullyAggregatedFacts(Database::Order_Object_Subject_Predicate).getPages() << "'" << ");";

    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_P_G1 << ", '" << db.getFullyAggregatedFacts(Database::Order_Predicate_Object_Subject).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_P_PAGES << ", '" << db.getFullyAggregatedFacts(Database::Order_Predicate_Object_Subject).getPages() << "'" << ");";
    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_S_G1 << ", '" << db.getFullyAggregatedFacts(Database::Order_Subject_Object_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::FAS_S_PAGES << ", '" << db.getFullyAggregatedFacts(Database::Order_Subject_Object_Predicate).getPages() << "'" << ");";

    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SPO_PAGES << ", '" << db.getFacts(Database::Order_Subject_Predicate_Object).getPages() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SOP_PAGES << ", '" << db.getFacts(Database::Order_Subject_Object_Predicate).getPages() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_PSO_PAGES << ", '" << db.getFacts(Database::Order_Predicate_Subject_Object).getPages() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_POS_PAGES << ", '" << db.getFacts(Database::Order_Predicate_Object_Subject).getPages() << "'" << ");";        
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OSP_PAGES << ", '" << db.getFacts(Database::Order_Object_Subject_Predicate).getPages() << "'" << ");";   
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OPS_PAGES << ", '" << db.getFacts(Database::Order_Object_Predicate_Subject).getPages() << "'" << ");";        

    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SPO_G1 << ", '" << db.getFacts(Database::Order_Subject_Predicate_Object).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SPO_G2 << ", '" << db.getFacts(Database::Order_Subject_Predicate_Object).getLevel2Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SOP_G1 << ", '" << db.getFacts(Database::Order_Subject_Object_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_SOP_G2 << ", '" << db.getFacts(Database::Order_Subject_Object_Predicate).getLevel2Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_PSO_G1 << ", '" << db.getFacts(Database::Order_Predicate_Subject_Object).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_PSO_G2 << ", '" << db.getFacts(Database::Order_Predicate_Subject_Object).getLevel2Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_POS_G1 << ", '" << db.getFacts(Database::Order_Predicate_Object_Subject).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_POS_G2 << ", '" << db.getFacts(Database::Order_Predicate_Object_Subject).getLevel2Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OSP_G1 << ", '" << db.getFacts(Database::Order_Object_Subject_Predicate).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OSP_G2 << ", '" << db.getFacts(Database::Order_Object_Subject_Predicate).getLevel2Groups() << "'" << ");";    
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OPS_G1 << ", '" << db.getFacts(Database::Order_Object_Predicate_Subject).getLevel1Groups() << "'" << ");";
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_OPS_G2 << ", '" << db.getFacts(Database::Order_Object_Predicate_Subject).getLevel2Groups() << "'" << ");";
    
    return execute(sql.str());
}

bool DDatabaseBuilder::writeAggAndFullyAggFactsSegments(PredicateDependencyGraph &pdg, stdpartitioning::DatabaseDescriptor &dbds, FragmentFilesAllocationMap &fragFiles, map<string, drdf::Host> &hostMappings){
    PredicateDependencyGraph::iterator pdgit;
    ostringstream sqlst;            
    map<string, set<Quad, cmpQuad> > quads;
    map<string, set<Triple, cmpTriple> > triples;
    map<string, Database*> databasesMap;
    
    //First of all, open all the databases
    for(FragmentFilesAllocationMap::iterator dit = fragFiles.begin(); dit != fragFiles.end(); ++dit){
        Database *newDb = new Database();
        if(newDb->open(dit->second.c_str(), true)){
            cout << dit->first << endl;
            databasesMap[dit->first] = newDb;
        }
    }
    
    for(pdgit = pdg.begin(); pdgit != pdg.end(); ++pdgit){
        //Check how many constants do the pattern have
        unsigned cardinality, value1, value2;
        SPARQLPattern &pattern = pdgit->second.first;

        if(pattern.localNode.constSubject){
            if(pattern.localNode.constPredicate){
                if(pattern.localNode.constObject){
                    //This case does not make sense
                    continue;
                }else{
                    //Subject and predicate are constants
                    value1 = pattern.localNode.subject;
                    value2 = pattern.localNode.predicate;
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;
                        cardinality = fragment->getExactStatistics().getCardinality(value1, value2, ~0u);
                        quads["ASSubjectPredicateH"].insert(Quad(value1, value2, cardinality, id_host));
                        triples["FASSubjectH"].insert(Triple(value1, fragment->getExactStatistics().getCardinality(value1, ~0u, ~0u), id_host));
                        triples["FASPredicateH"].insert(Triple(value2, fragment->getExactStatistics().getCardinality(~0u, value2, ~0u), id_host));                        
                    }
                }                           
            }else{
                if(pattern.localNode.constObject){
                    //Subject and Object are constants
                    value1 = pattern.localNode.subject;
                    value2 = pattern.localNode.object;                    
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;
                        cardinality = fragment->getExactStatistics().getCardinality(value1, ~0u, value2);
                        quads["ASSubjectObjectH"].insert(Quad(value1, value2, cardinality, id_host));         
                        triples["FASSubjectH"].insert(Triple(value1, fragment->getExactStatistics().getCardinality(value1, ~0u, ~0u), id_host));
                        triples["FASObjectH"].insert(Triple(value2, fragment->getExactStatistics().getCardinality(~0u, ~0u, value2), id_host));                    
                    }
                }else{
                    //Only subject is constant
                    value1 = pattern.localNode.subject;                    
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){                    
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;                        
                        cardinality = fragment->getExactStatistics().getCardinality(value1, ~0u, ~0u);
                        triples["FASSubjectH"].insert(Triple(value1, cardinality, id_host));
                    }
                }
            }
        }else{
            if(pattern.localNode.constPredicate){
                if(pattern.localNode.constObject){
                    //Here predicate and object are constants
                    value1 = pattern.localNode.predicate;
                    value2 = pattern.localNode.object;
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){        
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;                                                
                        cardinality = fragment->getExactStatistics().getCardinality(~0u, value1, value2);
                        quads["ASPredicateObjectH"].insert(Quad(value1, value2, cardinality, id_host));
                        triples["FASPredicateH"].insert(Triple(value1, fragment->getExactStatistics().getCardinality(~0u, value1, ~0u), id_host));                    
                        triples["FASObjectH"].insert(Triple(value2, fragment->getExactStatistics().getCardinality(~0u, ~0u, value2), id_host));                                        
                    }
                }else{
                    //Only predicate is constant
                    value1 = pattern.localNode.predicate;
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){        
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;                                                
                        cardinality = fragment->getExactStatistics().getCardinality(~0u, value1, ~0u);
                        triples["FASPredicateH"].insert(Triple(value1, cardinality, id_host));
                    }
                }
            }else{
                if(pattern.localNode.constObject){
                    //Only object is constant
                    value1 = pattern.localNode.object;
                    for(map<string, Database*>::iterator dbit = databasesMap.begin(); dbit != databasesMap.end(); ++dbit){        
                        Database *fragment = dbit->second;
                        unsigned id_host = hostMappings[dbit->first].id;                                                                    
                        cardinality = fragment->getExactStatistics().getCardinality(~0u, ~0u , value1);
                        triples["FASObjectH"].insert(Triple(value1, cardinality, id_host));
                    }
                }else{
                    //All variables: this case does not make sense either
                    continue;
                }
            }            
        }
    }
    
    //Time to build the queries
    unsigned nStatements = 0;
    for(map<string, set<Quad, cmpQuad> >::iterator tit = quads.begin(); tit != quads.end(); ++tit){
        for(set<Quad, cmpQuad>::iterator stit = tit->second.begin(); stit != tit->second.end(); ++stit){
            sqlst << "INSERT INTO " << tit->first << " VALUES(" << stit->value1 << ", " << stit->value2 << ", " << stit->value3 << ", " << stit->value4 << ");";
            ++nStatements;
            if(nStatements > 450){
                bool success = execute(sqlst.str());
                if(success){
                    sqlst.str("");
                    nStatements = 0;
                }else{
                    return false;
                }
            }
        }
    }
    
    for(map<string, set<Triple, cmpTriple> >::iterator pit = triples.begin(); pit != triples.end(); ++pit){
        for(set<Triple, cmpTriple>::iterator spit = pit->second.begin(); spit != pit->second.end(); ++spit){
            sqlst << "INSERT INTO " << pit->first << " VALUES (" << spit->value1 << ", " << spit->value2 << ", " << spit->value3 << ");";
            ++nStatements;
            if(nStatements > 450){
                bool success = execute(sqlst.str());
                if(success){
                    sqlst.str("");
                    nStatements = 0;
                }else{
                    return false;
                }
            }
        }
    }
    
    while(!databasesMap.empty()){
        Database *dbToRemove = databasesMap.begin()->second;
        databasesMap.erase(databasesMap.begin());
        delete dbToRemove;        
    }
    
    if(!sqlst.str().empty())
        return execute(sqlst.str());
    else
        return true;
}

bool DDatabaseBuilder::writeExactStatisticsSegment(Database &db, PatternDependencyGraph &pdg, PredicateDependencyGraph &prdg){
    //Write the cardinality of the database
    ostringstream sql;
    unsigned nStatements = 0;
    sql << "INSERT INTO `Metadata` VALUES(" << DDatabaseWrapper::IS_CARD << ", '" << db.getExactStatistics().getCardinality(~0u, ~0u, ~0u) << "'" << ");";

    for(PatternDependencyGraph::iterator pdit = pdg.begin(); pdit != pdg.end(); ++pdit){
        for(vector<SPARQLEdge>::iterator eit = pdit->second.begin(); eit != pdit->second.end(); ++eit){
            PredicateDependencyGraph::iterator fromIt = prdg.find(eit->from);
            PredicateDependencyGraph::iterator toIt = prdg.find(eit->to);
            QueryGraph::Node *from, *to;
            from = &fromIt->second.first.localNode;
            to = &toIt->second.first.localNode;
            long long unsigned joinInfoFrom[9], joinInfoTo[9];      
            ostringstream fromVerifSql, toVerifSql;
                
            fromVerifSql << "SELECT * FROM `Statistics` WHERE ";
                
            if(!from->constSubject){
                fromVerifSql << "`subject` IS null";
            }else{
                fromVerifSql << "`subject` = " << from->subject;
            }
                
            if(!from->constPredicate){
                fromVerifSql << " AND `predicate` IS null";
            }else{
                fromVerifSql << " AND `predicate` = " << from->predicate;
            }

            if(!from->constObject){
                fromVerifSql << " AND `object` IS null";
            }else{
                fromVerifSql << " AND `object` = " << from->object;
            }     
            
            fromVerifSql << ";";
            
            ResultSet fromVerifRS;
            this->out.dataWrapper->execute(fromVerifSql.str(), fromVerifRS);            
            if(!fromVerifRS.next() && db.getExactStatistics().getJoinInfo(joinInfoFrom, from->constSubject? from->subject : ~0u, from->constPredicate? from->predicate : ~0u, from->constObject? from->object: ~0u)){                 
                sql << "INSERT INTO `Statistics` VALUES (";                
                if(!from->constSubject){
                    sql << "NULL";
                }else{
                    sql << from->subject;
                }
                
                if(!from->constPredicate){
                    sql << ", NULL";
                }else{
                    sql << ", " << from->predicate;
                }

                if(!from->constObject){
                    sql << ", NULL";
                }else{
                    sql << ", " << from->object;
                }
                
                
                for(unsigned i = 0; i < 9; ++i){
                    if(joinInfoFrom[i] == ~static_cast<unsigned long long>(0))
                        sql << ", NULL";
                    else
                        sql << ", " << joinInfoFrom[i];                    
                }
                sql << ");";
                
                ++nStatements;
            }
            
            toVerifSql << "SELECT * FROM `Statistics` WHERE ";
                
            if(!to->constSubject){
                toVerifSql << "`subject` IS null";
            }else{
                 toVerifSql << "`subject` = " << from->subject;
            }
                
            if(!to->constPredicate){
                toVerifSql << " AND `predicate` IS null";
            }else{
                toVerifSql << " AND `predicate` = " << to->predicate;
            }

            if(!to->constObject){
                toVerifSql << " AND `object` IS null";
            }else{
                toVerifSql << " AND `object` = " << to->object;
            } 
            
            toVerifSql << ";";            

            ResultSet toVerifRS;
            this->out.dataWrapper->execute(toVerifSql.str(), toVerifRS);            

            if(!toVerifRS.next() && db.getExactStatistics().getJoinInfo(joinInfoTo, to->constSubject? to->subject : ~0u, to->constPredicate? to->predicate : ~0u, to->constObject? to->object: ~0u)){
                sql << "INSERT INTO `Statistics` VALUES (";
                if(!to->constSubject)
                    sql << "NULL";
                else
                    sql << to->subject;
                
                if(!to->constPredicate)
                    sql << ", NULL";
                else
                    sql << ", " << to->predicate;

                if(!to->constObject)
                    sql << ", NULL";
                else
                    sql << ", " << to->object;
                
                for(unsigned i = 0; i < 9; ++i){
                    if(joinInfoTo[i] == ~static_cast<unsigned long long>(0))
                        sql << ", NULL";
                    else
                        sql << ", " << joinInfoTo[i];                    
                }
                sql << ");";
                
                ++nStatements;
                if(nStatements > 450){
                    bool success = this->out.dataWrapper->execute(sql.str());
                    if(success){
                        sql.str("");
                        nStatements = 0;
                    }else{
                        return false;
                    }
                }                                
            }
        }
    }
    
    if(!sql.str().empty())
        return this->out.dataWrapper->execute(sql.str());
    else
        return true;
}

bool DDatabaseBuilder::writeHostMappings(FragmentFilesAllocationMap &filesMap, map<string, drdf::Host> &mappings){
    FragmentFilesAllocationMap::iterator flit;
    ostringstream sql; 
    unsigned rounds = filesMap.size() / 450 + 1;
    flit = filesMap.begin();
    
    for(unsigned i = 0; i < rounds; ++i){
        unsigned nStats = (i == rounds - 1)? filesMap.size() % 450 : 450;
        for(unsigned j = 0; j < nStats, flit != filesMap.end(); ++j, ++flit){
            Host newHost(flit->first, flit->second);
            sql << "INSERT INTO `Host` VALUES(" << newHost.id << ", '" << newHost.uri << "', '" << newHost.filename << "');";
            mappings[newHost.uri] = newHost;
        }
        
        if(!sql.str().empty()){
            bool success = execute(sql.str());
            if(success){
                sql.str("");
            }else{
                return false;
            }
        }
    }
    
    return true;
}

bool DDatabaseBuilder::writePredicateHostMappings(FragmentsAllocationMap& fragMap, map<string, drdf::Host> &mappings){
    ostringstream sql; 
    set<Triple> seenTriples;
    for(FragmentsAllocationMap::iterator famit = fragMap.begin(); famit != fragMap.end(); ++famit){
        map<string, Host>::iterator theHost = mappings.find(famit->first);        
        for(vector<FragmentAllocationAssign>::iterator ait = famit->second.begin(); ait != famit->second.end(); ++ait){
//            if(ait->fragment->getObservedCardinality() == 0)
//                continue;
            
            string flagsString = ait->fragment->getMinterm().flagsString();
            for(unsigned i = 0; i < flagsString.size(); ++i){
                unsigned idPred = ait->fragment->getMinterm().predicates[i]->id;
                unsigned idHost = theHost->second.id;
                unsigned flag = (unsigned)ait->fragment->getMinterm().flags[i];
                Triple triple(idPred, idHost, flag);                    
                seenTriples.insert(triple);
            }
        }
    }
    
    unsigned nStatements = 0;
    for(set<Triple>::iterator tit = seenTriples.begin(); tit != seenTriples.end(); ++tit, ++nStatements){
        sql << "INSERT INTO `PredicateHost` VALUES(" << tit->value1 << ", " << tit->value2 << ", " << tit->value3 << ");";
        if(nStatements == 450){
            bool success = execute(sql.str());
            if(success){
                sql.str("");
                nStatements = 0;
            }else{
                return false;
            }
        }
    }
    
    if(!sql.str().empty())
        return execute(sql.str());
    else
        return true;
}
