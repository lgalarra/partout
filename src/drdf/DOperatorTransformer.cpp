/* 
 * File:   DTransformer.cpp
 * Author: lgalarra
 * 
 * Created on September 5, 2011, 5:56 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <vector>
#include <typeinfo>
#include <stack>
#include <map>
#include <stdexcept>
#include <stdlib.h>
#include <queue>
#include <iostream>
#include <sstream>
#include <set>

#include "drdf/operator.h"
#include "drdf/RemoteOperators.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/constants.hpp"

using namespace drdf;
using namespace std;
using namespace log4cplus;


DOperatorTransformer::DOperatorTransformer(DDatabase *db): db(db) {}

DOperatorTransformer::DOperatorTransformer(){}

DOperatorTransformer::~DOperatorTransformer() {}

static void printRegisterSet(DRuntime *runtime, set<Register*> &regs){
    for(set<Register*>::iterator it = regs.begin(); it != regs.end(); ++it)
        cout << runtime->registerOffset(*it) << " ";
    
    cout << endl;

}

static void printRegisterVector(DRuntime *runtime, vector<Register*> &regs){
    for(vector<Register*>::iterator it = regs.begin(); it != regs.end(); ++it)
        cout << runtime->registerOffset(*it) << " ";
    
    cout << endl;
}

static void printRegisterMappings(DRuntime *runtime, vector<pair<Register*, Register*> > &regs){
    for(vector<pair<Register*, Register*> >::iterator it = regs.begin(); it != regs.end(); ++it)
        cout << runtime->registerOffset(it->first) << ": " << runtime->registerOffset(it->second) << " | ";
    
    cout << endl;
}


DOperator* DOperatorTransformer::buildDOperator(AggregatedIndexScan *op){
    return wrap(dynamic_cast<Operator*>(op));
}

DOperator* DOperatorTransformer::buildDOperator(EmptyScan *op){
    return wrap(dynamic_cast<Operator*>(op));
}

DOperator* DOperatorTransformer::buildDOperator(Filter *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;
    }
    return newOp;
}

DOperator* DOperatorTransformer::buildDOperator(FullyAggregatedIndexScan *op){
    return wrap(dynamic_cast<Operator*>(op));
}

DOperator* DOperatorTransformer::buildDOperator(HashGroupify *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;        
    }
    return newOp;    
}

DOperator* DOperatorTransformer::buildDOperator(SimpleGroupify *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;        
    }
    return newOp;    
}

DOperator* DOperatorTransformer::buildDOperator(HashJoin *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->left && op->right){
        DOperator *left = buildDOperator(op->left);
        DOperator *right = buildDOperator(op->right);
        newOp->children.push_back(left);
        newOp->children.push_back(right);
        left->parent = newOp;
        right->parent = newOp;
    }    
    return newOp;    
}        

DOperator* DOperatorTransformer::buildDOperator(IndexScan *op){
    return wrap(dynamic_cast<Operator*>(op));
}
DOperator* DOperatorTransformer::buildDOperator(MergeJoin *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->left && op->right){
        DOperator *left = buildDOperator(op->left);
        DOperator *right = buildDOperator(op->right);
        newOp->children.push_back(left);
        newOp->children.push_back(right);
        left->parent = newOp;
        right->parent = newOp;       
    }    
    return newOp;
}   

DOperator* DOperatorTransformer::buildDOperator(MergeUnion *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->left && op->right){
        DOperator *left = buildDOperator(op->left);
        DOperator *right = buildDOperator(op->right);
        newOp->children.push_back(left);
        newOp->children.push_back(right);
        left->parent = newOp;
        right->parent = newOp;        
    }    
    return newOp;
    
}

DOperator* DOperatorTransformer::buildDOperator(MultiMergeUnion *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->left && op->right){
        DOperator *left = buildDOperator(op->left);
        DOperator *right = buildDOperator(op->right);
        newOp->children.push_back(left);
        newOp->children.push_back(right);
        left->parent = newOp;
        right->parent = newOp;        
    }    
    return newOp;
    
}


DOperator* DOperatorTransformer::buildDOperator(NestedLoopFilter *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;
    }
    return newOp;    
}

DOperator* DOperatorTransformer::buildDOperator(NestedLoopJoin *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->left && op->right){
        DOperator *left = buildDOperator(op->left);
        DOperator *right = buildDOperator(op->right);
        newOp->children.push_back(left);
        newOp->children.push_back(right);
        left->parent = newOp;
        right->parent = newOp;        
    }  
    
    return newOp;

}

DOperator* DOperatorTransformer::buildDOperator(ResultsPrinter *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;
    }
    return newOp;
}       

DOperator* DOperatorTransformer::buildDOperator(Selection *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;
    }
    return newOp;        
}   

DOperator* DOperatorTransformer::buildDOperator(SingletonScan *op){
    return wrap(dynamic_cast<Operator*>(op));
}

DOperator* DOperatorTransformer::buildDOperator(Sort *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;        
    }
    return newOp;    
}

DOperator* DOperatorTransformer::buildDOperator(TableFunction *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    if(op->input){
        DOperator *child = buildDOperator(op->input);
        newOp->children.push_back(child);
        child->parent = newOp;        
    }
    return newOp;    
    
}   

DOperator* DOperatorTransformer::buildDOperator(Union *op){
    DOperator *newOp = wrap(dynamic_cast<Operator*>(op));
    for(vector<Operator*>::iterator pit = op->parts.begin(); pit != op->parts.end(); ++pit){
        DOperator *child = buildDOperator(dynamic_cast<Operator*>(*pit));
        newOp->children.push_back(child);
        child->parent = newOp;
    }
    return newOp;
}

DOperator * DOperatorTransformer::buildDOperator(Operator *op){
    string className(typeid(*op).name());
    DOperator *result = NULL;
    
    if(className.find("FullyAggregatedIndexScan") != string::npos)
        result =  buildDOperator(static_cast<FullyAggregatedIndexScan*>(op));
    else if(typeid(*op) == typeid(EmptyScan))
        result =  buildDOperator(static_cast<EmptyScan*>(op));
    else if(typeid(*op) == typeid(Filter))
        result =  buildDOperator(static_cast<Filter*>(op));
    else if(className.find("AggregatedIndexScan") != string::npos)
        result =  buildDOperator(static_cast<AggregatedIndexScan*>(op));
    else if(typeid(*op) == typeid(HashGroupify))
        result =  buildDOperator(static_cast<HashGroupify*>(op));
    else if(typeid(*op) == typeid(HashJoin))
        result =  buildDOperator(static_cast<HashJoin*>(op));
    else if(className.find("IndexScan") != string::npos)
        result =  buildDOperator(static_cast<IndexScan*>(op));
    else if(typeid(*op) == typeid(MergeJoin))
        result =  buildDOperator(static_cast<MergeJoin*>(op));    
    else if(typeid(*op) == typeid(MergeUnion))
        result =  buildDOperator(static_cast<MergeUnion*>(op));
    else if(typeid(*op) == typeid(NestedLoopFilter))
        result =  buildDOperator(static_cast<NestedLoopFilter*>(op));
    else if(typeid(*op) == typeid(NestedLoopJoin))
        result =  buildDOperator(static_cast<NestedLoopJoin*>(op));
    else if(typeid(*op) == typeid(ResultsPrinter))
        result =  buildDOperator(static_cast<ResultsPrinter*>(op));    
    else if(typeid(*op) == typeid(Selection))
        result =  buildDOperator(static_cast<Selection*>(op));    
    else if(typeid(*op) == typeid(SingletonScan))
        result =  buildDOperator(static_cast<SingletonScan*>(op));    
    else if(typeid(*op) == typeid(Sort))
        result =  buildDOperator(static_cast<Sort*>(op));    
    else if(typeid(*op) == typeid(TableFunction))
        result =  buildDOperator(static_cast<TableFunction*>(op));        
    else if(typeid(*op) == typeid(Union))
        result =  buildDOperator(static_cast<Union*>(op));        
    else if(className.find("RemoteSend") != string::npos)
        result =  buildDOperator(static_cast<RemoteSend*>(op));
    else if(className.find("RemoteFetch") != string::npos)
        result =  buildDOperator(static_cast<RemoteFetch*>(op));
    else if(typeid(*op) == typeid(MultiMergeUnion))
        result = buildDOperator(static_cast<MultiMergeUnion*>(op));
    
    return result;
}

DOperator* DOperatorTransformer::addMergeUnionChain(DOperator *op, DRuntime *runtime){
    if(op->relevantHosts.size() > 1 && op->type == DOperator::TFullyAggregatedIndexScan){
        //Create a Union + sort
        vector<Operator*> parts;
        vector<vector<Register*> > init, mappings;
        vector<MergeUnion*> mergeUnions;
        unsigned nMergeUnions;
        
        this->resolveMultipleSources(op, runtime, parts, init, mappings);
        nMergeUnions = parts.size() - 1;
        
        mergeUnions.resize(nMergeUnions);
        MergeUnion *lastMergeUnion = NULL;
        DOperator *lastDOperator = NULL;

        for(unsigned i = 0; i < nMergeUnions; ++i){            
            if(lastMergeUnion != NULL){
                FullyAggregatedIndexScan *scan = static_cast<FullyAggregatedIndexScan*>(parts[i + 1]);
                runtime->requestExtraAllocation(1);
                double totalExCard = scan->getExpectedOutputCardinality() + lastMergeUnion->getExpectedOutputCardinality();
                mergeUnions[i] = new MergeUnion(runtime->getRegister(runtime->getRegisterCount() - 1), lastMergeUnion, lastMergeUnion->result, scan, scan->value1, totalExCard);
                lastMergeUnion = mergeUnions[i];                        
                
                //Linking
                DOperator *dmu, *dscan;                
                dscan = wrap(scan);
                dmu = wrap(mergeUnions[i]);
                dscan->parent = dmu;
                dscan->setHost(op->relevantHosts[i + 1].uri);
                lastDOperator->parent = dmu;
                dmu->children.push_back(lastDOperator);                
                dmu->children.push_back(dscan);
                lastDOperator = dmu;
            }else{
                FullyAggregatedIndexScan *left, *right;
                left = static_cast<FullyAggregatedIndexScan*>(parts[i]);
                right = static_cast<FullyAggregatedIndexScan*>(parts[i + 1]);
                double totalExCard = left->getExpectedOutputCardinality() + right->getExpectedOutputCardinality();
                runtime->requestExtraAllocation(1);
                mergeUnions[0] = new MergeUnion(runtime->getRegister(runtime->getRegisterCount() - 1), left, left->value1, right, right->value1, totalExCard);
                lastMergeUnion = mergeUnions[0];
                
                //Linking
                DOperator *dleft, *dright;
                lastDOperator = wrap(mergeUnions[0]);
                dleft = wrap(left);
                dright = wrap(right);
                dleft->setHost(op->relevantHosts[i].uri);                
                dleft->parent = lastDOperator;
                dright->setHost(op->relevantHosts[i + 1].uri);
                dright->parent = lastDOperator;
                lastDOperator->children.push_back(dleft);
                lastDOperator->children.push_back(dright);                                
            }
        }
        
        if(lastMergeUnion != NULL && lastDOperator != NULL)
            lastDOperator->outputRegisters.push_back(lastMergeUnion->result);
        
        return lastDOperator;        
    }else{
        return op;
    }
}


DOperator* DOperatorTransformer::addMultiMergeUnionChain(DOperator *op, DRuntime *runtime){
    if(op->relevantHosts.size() > 1){
        //Create a Union + sort
        vector<Operator*> parts;
        vector<vector<Register*> > init, mappings;
        vector<MultiMergeUnion*> mmergeUnions;
        unsigned nMergeUnions;

        this->resolveMultipleSources(op, runtime, parts, init, mappings);
        nMergeUnions = parts.size() - 1;

        mmergeUnions.resize(nMergeUnions);
        MultiMergeUnion *lastMergeUnion = NULL;
        DOperator *lastDOperator = NULL;
        for(unsigned i = 0; i < nMergeUnions; ++i){           
            if(lastMergeUnion != NULL){
                vector<Register*> result, rightReg;
                Operator *nextRightChild;

                if(op->type == DOperator::TAggregatedIndexScan){
                    AggregatedIndexScan *scan = static_cast<AggregatedIndexScan*>(parts[i + 1]);
                    rightReg.push_back(scan->value1);
                    rightReg.push_back(scan->value2);
                    runtime->requestExtraAllocation(2);            
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 2));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 1));
                    nextRightChild = dynamic_cast<Operator*>(scan);                
                }else{
                    IndexScan *scan = static_cast<IndexScan*>(parts[i + 1]);
                    rightReg.push_back(scan->value1);
                    rightReg.push_back(scan->value2);                
                    rightReg.push_back(scan->value3); 
                    runtime->requestExtraAllocation(3);            
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 3));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 2));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 1));                
                    nextRightChild = dynamic_cast<Operator*>(scan);
                }            
                //Now the result            
                double totalExCard = nextRightChild->getExpectedOutputCardinality() + lastMergeUnion->getExpectedOutputCardinality();
                mmergeUnions[i] = new MultiMergeUnion(result, lastMergeUnion, lastMergeUnion->result, nextRightChild, rightReg, totalExCard);

                //Linking
                DOperator *dmmu, *dscan;                
                dscan = wrap(nextRightChild);
                dmmu = wrap(mmergeUnions[i]);
                dscan->parent = dmmu;
                dscan->setHost(op->relevantHosts[i + 1].uri);
                lastDOperator->parent = dmmu;
                dmmu->children.push_back(lastDOperator);                
                dmmu->children.push_back(dscan);
                lastDOperator = dmmu;
                lastMergeUnion = mmergeUnions[i];            
            }else{
                Operator *left, *right;
                vector<Register*> leftReg, rightReg, result;
                if(op->type == DOperator::TAggregatedIndexScan){
                    AggregatedIndexScan *aisleft, *aisright;
                    aisleft = static_cast<AggregatedIndexScan*>(parts[i]);
                    aisright = static_cast<AggregatedIndexScan*>(parts[i + 1]);
                    leftReg.push_back(aisleft->value1);
                    leftReg.push_back(aisleft->value2);
                    rightReg.push_back(aisright->value1);
                    rightReg.push_back(aisright->value2);
                    runtime->requestExtraAllocation(2);  
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 2));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 1));
                    left = dynamic_cast<Operator*>(aisleft);
                    right = dynamic_cast<Operator*>(aisright);
                }else{
                    IndexScan *isleft, *isright;
                    isleft = static_cast<IndexScan*>(parts[i]);
                    isright = static_cast<IndexScan*>(parts[i + 1]);
                    leftReg.push_back(isleft->value1);
                    leftReg.push_back(isleft->value2);
                    leftReg.push_back(isleft->value3);                
                    rightReg.push_back(isright->value1);
                    rightReg.push_back(isright->value2);
                    rightReg.push_back(isright->value3);                
                    runtime->requestExtraAllocation(3);  
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 3));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 2));
                    result.push_back(runtime->getRegister(runtime->getRegisterCount() - 1));
                    left = dynamic_cast<Operator*>(isleft);
                    right = dynamic_cast<Operator*>(isright);                
                }

                double totalExCard = left->getExpectedOutputCardinality() + right->getExpectedOutputCardinality();
                mmergeUnions[0] = new MultiMergeUnion(result, left, leftReg, right, rightReg, totalExCard);

                //Linking
                DOperator *dleft, *dright;
                lastDOperator = wrap(mmergeUnions[0]);
                dleft = wrap(left);
                dright = wrap(right);
                dleft->setHost(op->relevantHosts[i].uri);                
                dleft->parent = lastDOperator;
                dright->setHost(op->relevantHosts[i + 1].uri);
                dright->parent = lastDOperator;
                lastDOperator->children.push_back(dleft);
                lastDOperator->children.push_back(dright);

                lastMergeUnion = mmergeUnions[0];            
            }
        }

        if(lastDOperator != NULL){
            lastDOperator->outputRegisters = static_cast<MultiMergeUnion*>(lastDOperator->getOperator())->result;
        }
    
        return lastDOperator;        
    }else{
        return op;
    }
}

static int findRegisterMapping(Register *reg, vector<pair<Register*, Register*> > &inputMappings){
    for(int i = 0; i < inputMappings.size(); ++i){
        if(inputMappings[i].first == reg)
            return i;
    }
    
    return -1;
}

static int findRegisterMappingInv(Register *reg, vector<pair<Register*, Register*> > &inputMappings){
    for(int i = 0; i < inputMappings.size(); ++i){
        if(inputMappings[i].second == reg)
            return i;
    }
    
    return -1;    
}

static int findRegisterMapping(Register *reg, vector<vector<pair<Register*, Register*> > > &allMappings, int &index){
    int result = -1;
    for(int i = 0; i < allMappings.size(); ++i){
        result = findRegisterMapping(reg, allMappings[i]);
        if(result != -1){
            index = i;
            break;
        }
            
    }
    
    return result;
}

static int findRegisterMappingInv(Register *reg, vector<vector<pair<Register*, Register*> > > &allMappings, int &index){
    int result = -1;
    for(int i = 0; i < allMappings.size(); ++i){
        result = findRegisterMappingInv(reg, allMappings[i]);
        if(result != -1){
            index = i;
            break;
        }
            
    }
    
    return result;
}

static Register* findRegisterMapping2(Register *reg, vector<pair<Register*, Register*> > &mappings){
    for(vector<pair<Register*, Register*> >::iterator rit = mappings.begin(); rit != mappings.end(); ++rit){
        if(rit->first == reg)
            return rit->second;
    }
    
    return NULL;
}

static Register* findRegisterMappingInv2(Register *reg, vector<pair<Register*, Register*> > &mappings){
    for(vector<pair<Register*, Register*> >::iterator rit = mappings.begin(); rit != mappings.end(); ++rit){
        if(rit->second == reg)
            return rit->first;
    }
    
    return NULL;
}

void DOperatorTransformer::fixProjectionsMergeJoin(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    MergeJoin *mj = static_cast<MergeJoin*>(dop->op);
    
    for(unsigned i = 0; i < mj->leftTail.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(mj->leftTail[i], inputMappings)) != -1){
            mj->leftTail[i] = inputMappings[mappingIndex].second;
        }
    }
    
    for(unsigned i = 0; i < mj->rightTail.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(mj->rightTail[i], inputMappings)) != -1){
            mj->rightTail[i] = inputMappings[mappingIndex].second;
        }
    }
    
    
    int mappingIndex = findRegisterMapping(mj->leftValue, inputMappings);
    if(mappingIndex != -1) mj->leftValue = inputMappings[mappingIndex].second;
    mappingIndex = findRegisterMapping(mj->rightValue, inputMappings);
    if(mappingIndex != -1) mj->rightValue = inputMappings[mappingIndex].second;            
}

void DOperatorTransformer::fixProjectionsHashJoin(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    HashJoin *hj = static_cast<HashJoin*>(dop->op);
    
    for(unsigned i = 0; i < hj->leftTail.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(hj->leftTail[i], inputMappings)) != -1){
            hj->leftTail[i] = inputMappings[mappingIndex].second;
        }
    }
    
    for(unsigned i = 0; i < hj->rightTail.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(hj->rightTail[i], inputMappings)) != -1){
            hj->rightTail[i] = inputMappings[mappingIndex].second;
        }
    }
    
    
    int mappingIndex = findRegisterMapping(hj->leftValue, inputMappings);
    if(mappingIndex != -1) hj->leftValue = inputMappings[mappingIndex].second;
    mappingIndex = findRegisterMapping(hj->rightValue, inputMappings);
    if(mappingIndex != -1) hj->rightValue = inputMappings[mappingIndex].second;            
}


void DOperatorTransformer::fixProjectionsHashGroupify(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    HashGroupify *hg = static_cast<HashGroupify*>(dop->op);
    for(unsigned i = 0; i < hg->values.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(hg->values[i], inputMappings)) != -1){
            hg->values[i] = inputMappings[mappingIndex].second;
        }
    }
}

void DOperatorTransformer::fixProjectionsSimpleGroupify(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    SimpleGroupify *sg = static_cast<SimpleGroupify*>(dop->op);
    for(unsigned i = 0; i < sg->values.size(); ++i){
        int mappingIndex;
        if((mappingIndex = findRegisterMapping(sg->values[i], inputMappings)) != -1){
            sg->values[i] = inputMappings[mappingIndex].second;
        }
    }
}

void DOperatorTransformer::fixProjectionsSelection(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    Selection *sel = static_cast<Selection*>(dop->op);
    Selection::Predicate *predicate = sel->predicate;
    queue<Selection::Predicate *> queue;
    queue.push(predicate);
    
    while(!queue.empty()){
        Selection::Predicate *current = queue.front();
        string className(typeid(*current).name());
        
        if(className.find("NotEqual") != string::npos){
            Selection::NotEqual *noteq = static_cast<Selection::NotEqual*>(current);
            queue.push(noteq->left);
            queue.push(noteq->right);
        }else if(className.find("Equal") != string::npos){
            Selection::Equal *eq = static_cast<Selection::Equal*>(current);
            queue.push(eq->left);
            queue.push(eq->right);            
        }else if(className.find("Variable") != string::npos){
            Selection::Variable *variable = static_cast<Selection::Variable*>(current);
            Register *mapped = findRegisterMapping2(variable->reg, inputMappings);
            if(mapped != NULL) variable->reg = mapped;
        }
        
        queue.pop();
    }
}


void DOperatorTransformer::collectCandidateRegisters(DOperator *dop){
    switch(dop->type){
        case DOperator::TMergeJoin:
        {
            MergeJoin *mj = static_cast<MergeJoin*>(dop->op);
            dop->outputRegisters.insert(dop->outputRegisters.begin(), mj->leftTail.begin(), mj->leftTail.end());
            dop->outputRegisters.insert(dop->outputRegisters.begin(), mj->rightTail.begin(), mj->rightTail.end());
            dop->outputRegisters.push_back(mj->leftValue);
            dop->outputRegisters.push_back(mj->rightValue);
        }
            break;
        case DOperator::THashGroupify:
        {
            HashGroupify *hg = static_cast<HashGroupify*>(dop->op);
            dop->outputRegisters.insert(dop->outputRegisters.begin(), hg->values.begin(), hg->values.end());
        }
            break;
        case DOperator::TSimpleGroupify:
        {
            SimpleGroupify *sg = static_cast<SimpleGroupify*>(dop->op);
            dop->outputRegisters.insert(dop->outputRegisters.begin(), sg->values.begin(), sg->values.end());            
        }
            break;
        case DOperator::TMultiMergeUnion:
        {
            MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(dop->op);            
            dop->outputRegisters.insert(dop->outputRegisters.begin(), mmu->leftReg.begin(), mmu->leftReg.end());
            dop->outputRegisters.insert(dop->outputRegisters.begin(), mmu->rightReg.begin(), mmu->rightReg.end());
        }
            break;
        case DOperator::TMergeUnion:
        {
            MergeUnion *mu = static_cast<MergeUnion*>(dop->op);            
            dop->outputRegisters.push_back(mu->leftReg);
            dop->outputRegisters.push_back(mu->rightReg);
        }
            break;            
        case DOperator::THashJoin:
        {
            HashJoin *hj = static_cast<HashJoin*>(dop->op);
            dop->outputRegisters.insert(dop->outputRegisters.begin(), hj->leftTail.begin(), hj->leftTail.end());
            dop->outputRegisters.insert(dop->outputRegisters.begin(), hj->rightTail.begin(), hj->rightTail.end());
            dop->outputRegisters.push_back(hj->leftValue);
            dop->outputRegisters.push_back(hj->rightValue);            
        }            
        default:            
            break;
    }
}

void DOperatorTransformer::fixProjections(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings){
    DOperator *current = dop;
    do{
        switch(current->getType()){
            case DOperator::TMergeJoin:
                fixProjectionsMergeJoin(current, inputMappings);
                break;
            case DOperator::THashJoin:
                fixProjectionsHashJoin(current, inputMappings);
                break;
            case DOperator::THashGroupify:
                fixProjectionsHashGroupify(current, inputMappings);
                break;
            case DOperator::TSimpleGroupify:
                fixProjectionsSimpleGroupify(current, inputMappings);
                break;
            case DOperator::TSelection:
                fixProjectionsSelection(current, inputMappings);
            default:
                break;
        }
        
        current = current->parent;
    }while(current != NULL);
}

void DOperatorTransformer::findProjectionMappingsForOperator(DRuntime *runtime, DOperator *dop, vector<pair<Register*, Register*> > &outputMappings){
    set<Register*> unmappedRegisters(dop->outputRegisters.begin(), dop->outputRegisters.end());
    
    //Find the leaves
    queue<DOperator*> theQueue;
    theQueue.push(dop);
    map<unsigned, vector<pair<DOperator*, Register*> > > leafMappings;
    while(!theQueue.empty()){
        DOperator *current = theQueue.front();
        if(current->isAnyIndexScan()){
            for(set<Register*>::iterator dit = unmappedRegisters.begin(); dit != unmappedRegisters.end(); ++dit){
                if(current->type == DOperator::TFullyAggregatedIndexScan){
                    FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(current->op);
                    if(*dit == fais->value1) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, fais->value1));                    
                }else if(current->type == DOperator::TAggregatedIndexScan){
                    AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(current->op);
                    if(*dit == ais->value1) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, ais->value1));
                    if(*dit == ais->value2) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, ais->value2));                        
                }else{
                    IndexScan *is = static_cast<IndexScan*>(current->op);
                    if(*dit == is->value1) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, is->value1));                    
                    if(*dit == is->value2) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, is->value2));                                        
                    if(*dit == is->value3) leafMappings[current->id].push_back(pair<DOperator*, Register*>(current, is->value3));                                                            
                }
            }
        }
        
        for(vector<DOperator*>::iterator chit = current->children.begin(); chit != current->children.end(); ++chit){
            theQueue.push(*chit);
        }         
        
        theQueue.pop();
    }
    
    //Now search for the mappings
    map<unsigned, Register*> partialMappings;
    for(map<unsigned, vector<pair<DOperator*, Register*> > >::iterator lmpit = leafMappings.begin(); lmpit != leafMappings.end(); ++lmpit){        
        for(vector<pair<DOperator*, Register*> >::iterator pit = lmpit->second.begin(); pit != lmpit->second.end(); ++pit){
            //Collect mappings the way up
            DOperator *theOp = pit->first;
            do{
                if(theOp->type == DOperator::TUnion){
                    Union *un = static_cast<Union*>(theOp->op);
                    //Find mappings
                    for(vector<vector<Register*> >::iterator mit = un->mappings.begin(); mit != un->mappings.end(); ++mit){
                        for(vector<Register*>::iterator smit = mit->begin(); smit != mit->end(); smit = smit + 2){
                            partialMappings[runtime->registerOffset(*smit)] = *(smit + 1);
                        }
                    }
                }else if(theOp->type == DOperator::TMultiMergeUnion){
                    MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(theOp->op);
                    for(unsigned i = 0; i < mmu->result.size(); ++i){
                        partialMappings[runtime->registerOffset(mmu->leftReg[i])] = mmu->result[i];
                        partialMappings[runtime->registerOffset(mmu->rightReg[i])] = mmu->result[i];
                    }
                }else if(theOp->type == DOperator::TMergeUnion){
                    MergeUnion *mu = static_cast<MergeUnion*>(theOp->op);
                    partialMappings[runtime->registerOffset(mu->leftReg)] = mu->result;
                    partialMappings[runtime->registerOffset(mu->rightReg)] = mu->result;                    
                }

                theOp = theOp->parent;
            }while(theOp != NULL);         
        }
    }
    
    //And finally track the mappings
    for(set<Register*>::iterator rit = unmappedRegisters.begin(); rit != unmappedRegisters.end(); ++rit){
        Register *current = *rit;
        while(true){
           map<unsigned, Register*>::iterator curMap = partialMappings.find(runtime->registerOffset(current));
           if(curMap == partialMappings.end()) break;
           current = curMap->second;                      
        }
        //New output register
        outputMappings.push_back(pair<Register*, Register*>(*rit, current));            
    }    
}


bool DOperatorTransformer::hasDescendantsOfType(DOperator *dop, DOperator::Type matchType){
    bool result = false;
    
    if(dop->isLeaf())
        return result;
    
    if(dop->type == matchType)
        return true;
    
    for(vector<DOperator*>::iterator chit = dop->children.begin(); chit != dop->children.end(); ++chit){
        if(this->hasDescendantsOfType(*chit, matchType)){
            //We are happy with the first one
            result = true;
            break;
        }
    }
        
    return result;
}

void DOperatorTransformer::getParallelizableJoins(DOperator *op, vector<DOperator*> &output){
    if(op->isEfficientJoin()){
        if( (op->children.at(0)->isMergingOperator() && op->children.at(1)->isAnyIndexScan())
            || (op->children.at(0)->isAnyIndexScan() && op->children.at(1)->isMergingOperator())
            || (op->children.at(0)->isMergingOperator() && op->children.at(1)->isMergingOperator())
         ){
            output.push_back(op);
        }
            
    }
    
    for(vector<DOperator*>::iterator chit = op->children.begin(); chit != op->children.end(); ++chit)
        getParallelizableJoins(*chit, output);    
}

DOperator* DOperatorTransformer::resolveMultipleSourcesLeaves(DOperator *op, DRuntime *runtime){
    if(op->isAnyIndexScan()){
        #if FORCE_MULTIMERGE_UNIONS
        if(op->type == DOperator::TFullyAggregatedIndexScan)
            return addMergeUnionChain(op, runtime);
        else            
            return addMultiMergeUnionChain(op, runtime);

        #else
        if(op->op->getExpectedOutputCardinality() <= MAX_REGISTERS_PER_REMOTE_PAGE)
            return addUnionsAndGroupify(op, runtime);
        else if(op->type == DOperator::TFullyAggregatedIndexScan)
            return addMergeUnionChain(op, runtime);
        else            
            return addMultiMergeUnionChain(op, runtime);
        #endif
    }else{
        if(op->isLeaf()) return NULL;
        vector<unsigned> offsetsToRemove;
        vector<DOperator*> opsToAdd;
        unsigned off = 0;
        for(vector<DOperator*>::iterator dit = op->children.begin(); dit != op->children.end(); ++dit){
            DOperator *newChild = resolveMultipleSourcesLeaves(*dit, runtime);
            if(newChild != NULL && newChild != *dit){
                //Schedule the old child for removal
                unlinkOperators(op->op, (*dit)->op);
                newChild->parent = op;                
                linkWithParent(newChild);
                collectCandidateRegisters(op);
                offsetsToRemove.push_back(off);
                opsToAdd.push_back(newChild);
            }
            ++off;
        }
        
        unsigned j = 0;
        for(vector<unsigned>::iterator offit = offsetsToRemove.begin(); offit != offsetsToRemove.end(); ++offit){
            op->children.insert(op->children.begin() + *offit, opsToAdd[j]);
            op->getChildren().erase(op->getChildren().begin() + *offit + 1);
            ++j;
        }
        
        vector<pair<Register*, Register*> > projectionMappings;        
        findProjectionMappingsForOperator(runtime, op, projectionMappings);
        fixProjections(op, projectionMappings);
        
        return op;
    }
}

DOperator* DOperatorTransformer::addUnionsAndGroupify(DOperator *op, DRuntime *runtime){
    if(op->relevantHosts.size() > 1){
        //Create a Union
        Union *newUnion;
        vector<Operator*> parts;
        vector<vector<Register*> > init, mappings;
        DOperator *dNewUnion;
        vector<pair<Register*,bool> > orderVector;        
        
        this->resolveMultipleSources(op, runtime, parts, init, mappings);
                    
        newUnion = new Union(parts, mappings, init, op->getOperator()->getExpectedOutputCardinality() * parts.size());
        dNewUnion = wrap(newUnion);

        unsigned hostInd = 0;
        for(vector<Operator*>::iterator pit = parts.begin(); pit != parts.end(); ++pit, ++hostInd){
            DOperator *newChild = wrap(*pit);
            newChild->setHost(op->relevantHosts[hostInd].uri);
            dNewUnion->children.push_back(newChild);
            newChild->parent = dNewUnion;
        }

        SimpleGroupify *groupify = new SimpleGroupify(dynamic_cast<Operator*>(newUnion), init.at(0), newUnion->getExpectedOutputCardinality());
        DOperator *dGroupify = wrap(groupify);
        dGroupify->children.push_back(dNewUnion);
        dNewUnion->parent = dGroupify;        
        
        //Sort the registers as they appear in the input to the hash groupify
        for(vector<Register*>::iterator rit = init.at(0).begin(); rit != init.at(0).end(); ++rit)
            orderVector.push_back(pair<Register*, bool>(*rit, false));
        
        dGroupify->outputRegisters = groupify->values;                           
        return dGroupify;        
        
    }else{
        return op;
    }
    
}

void DOperatorTransformer::resolveMultipleSources(DOperator *op, DRuntime *runtime, vector<Operator*> &parts, vector<vector<Register*> > &init, vector<vector<Register*> > &mappings){
    unsigned newRegs;        
    unsigned startReg = runtime->getRegisterCount();        
    mappings.resize(op->relevantHosts.size());
    init.resize(op->relevantHosts.size()); 
    double expectedCardinality = op->op->getExpectedOutputCardinality() / op->relevantHosts.size();
    double expectedCardinalityFirst;
    
    if(op->type == DOperator::TIndexScan){
        IndexScan *is = static_cast<IndexScan*>(op->op);
        newRegs = 3 * op->relevantHosts.size();        

        //Allocate registers for initializations
        runtime->requestExtraAllocation(newRegs);

        //The original index scan is reused
        parts.push_back(op->op);

        //Registering the union registers
        init[0].push_back(runtime->getRegister(startReg));
        init[0].push_back(runtime->getRegister(startReg + 1));
        init[0].push_back(runtime->getRegister(startReg + 2));


        //Add the mappings
        mappings[0].push_back(is->value1);
        mappings[0].push_back(runtime->getRegister(startReg));

        mappings[0].push_back(is->value2);
        mappings[0].push_back(runtime->getRegister(startReg + 1));

        mappings[0].push_back(is->value3);
        mappings[0].push_back(runtime->getRegister(startReg + 2));

        unsigned hostInd = 1;
        unsigned newSizeRuntime = runtime->getRegisterCount();
        for(unsigned i = startReg + 3; i < newSizeRuntime; i = i + 3){            
            bool subBound, predBound, objBound;
            Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
           
            switch(is->order){
                case Database::Order_Object_Predicate_Subject:
                    subBound = is->bound3;
                    predBound = is->bound2;
                    objBound = is->bound1;
                    subReg = runtime->getRegister(i + 2);
                    subReg->value = is->value3->value;
                    predReg = runtime->getRegister(i + 1);
                    predReg->value = is->value2->value;
                    objReg = runtime->getRegister(i);
                    objReg->value = is->value1->value;
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound3?is->value3->value:~0u,is->bound2?is->value2->value:~0u,is->bound1?is->value1->value:~0u, op->relevantHosts[hostInd].id);
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound3?is->value3->value:~0u,is->bound2?is->value2->value:~0u,is->bound1?is->value1->value:~0u, op->relevantHosts[0].id);
                    break;
                case Database::Order_Object_Subject_Predicate:
                    subBound = is->bound2;
                    predBound = is->bound3;
                    objBound = is->bound1;
                    subReg = runtime->getRegister(i + 1);
                    subReg->value = is->value2->value;
                    predReg = runtime->getRegister(i + 2);
                    predReg->value = is->value3->value;
                    objReg = runtime->getRegister(i);
                    objReg->value = is->value1->value;                        
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound2?is->value2->value:~0u,is->bound3?is->value3->value:~0u,is->bound1?is->value1->value:~0u, op->relevantHosts[hostInd].id);                    
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound2?is->value2->value:~0u,is->bound3?is->value3->value:~0u,is->bound1?is->value1->value:~0u, op->relevantHosts[0].id);                    
                    break;
                case Database::Order_Predicate_Object_Subject:
                    subBound = is->bound3;
                    predBound = is->bound1;
                    objBound = is->bound2;
                    subReg = runtime->getRegister(i + 2);
                    subReg->value = is->value3->value;
                    predReg = runtime->getRegister(i);
                    predReg->value = is->value1->value;
                    objReg = runtime->getRegister(i + 1);
                    objReg->value = is->value2->value;                        
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound3?is->value3->value:~0u,is->bound1?is->value1->value:~0u,is->bound2?is->value2->value:~0u, op->relevantHosts[hostInd].id);                    
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound3?is->value3->value:~0u,is->bound1?is->value1->value:~0u,is->bound2?is->value2->value:~0u, op->relevantHosts[0].id);                    
                    break;
                case Database::Order_Predicate_Subject_Object:
                    subBound = is->bound2;
                    predBound = is->bound1;
                    objBound = is->bound3;
                    subReg = runtime->getRegister(i + 1);
                    subReg->value = is->value2->value;
                    predReg = runtime->getRegister(i);
                    predReg->value = is->value1->value;
                    objReg = runtime->getRegister(i + 2);
                    objReg->value = is->value3->value;       
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound2?is->value2->value:~0u,is->bound1?is->value1->value:~0u,is->bound3?is->value3->value:~0u, op->relevantHosts[hostInd].id); 
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound2?is->value2->value:~0u,is->bound1?is->value1->value:~0u,is->bound3?is->value3->value:~0u, op->relevantHosts[0].id);
                    break;
                case Database::Order_Subject_Object_Predicate:
                    subBound = is->bound1;
                    predBound = is->bound3;
                    objBound = is->bound2;
                    subReg = runtime->getRegister(i);
                    subReg->value = is->value1->value;
                    predReg = runtime->getRegister(i + 2);
                    predReg->value = is->value3->value;
                    objReg = runtime->getRegister(i + 1);
                    objReg->value = is->value2->value;   
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound1?is->value1->value:~0u,is->bound3?is->value3->value:~0u,is->bound2?is->value2->value:~0u, op->relevantHosts[hostInd].id);                    
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound1?is->value1->value:~0u,is->bound3?is->value3->value:~0u,is->bound2?is->value2->value:~0u, op->relevantHosts[0].id);                    
                    break;
                case Database::Order_Subject_Predicate_Object:
                    subBound = is->bound1;
                    predBound = is->bound2;
                    objBound = is->bound3;
                    subReg = runtime->getRegister(i);
                    subReg->value = is->value1->value;
                    predReg = runtime->getRegister(i + 1);
                    predReg->value = is->value2->value;
                    objReg = runtime->getRegister(i + 2);
                    objReg->value = is->value3->value;                        
                    expectedCardinality = db->getExactStatistics().getCardinality(is->bound1?is->value1->value:~0u,is->bound2?is->value2->value:~0u,is->bound3?is->value3->value:~0u, op->relevantHosts[hostInd].id);                                        
                    if(hostInd == 1) expectedCardinalityFirst = db->getExactStatistics().getCardinality(is->bound1?is->value1->value:~0u,is->bound3?is->value3->value:~0u,is->bound2?is->value2->value:~0u, op->relevantHosts[0].id);                                        
                    break;
            }
            if(hostInd == 1) is->expectedOutputCardinality = expectedCardinalityFirst;
                
            IndexScan *newIs = IndexScan::create(runtime->getDatabase(), is->order, subReg, subBound, predReg, predBound, objReg, objBound, expectedCardinality);            
            parts.push_back(newIs);

            //Build the mappings
            mappings[hostInd].push_back(newIs->value1);
            mappings[hostInd].push_back(runtime->getRegister(startReg));
            mappings[hostInd].push_back(newIs->value2);
            mappings[hostInd].push_back(runtime->getRegister(startReg + 1));
            mappings[hostInd].push_back(newIs->value3);
            mappings[hostInd].push_back(runtime->getRegister(startReg + 2));

            init[hostInd].push_back(runtime->getRegister(startReg));
            init[hostInd].push_back(runtime->getRegister(startReg + 1));
            init[hostInd].push_back(runtime->getRegister(startReg + 2));

            ++hostInd;
        }
    }else if(op->type == DOperator::TAggregatedIndexScan){
        AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(op->op);
        newRegs = 2 * op->relevantHosts.size();
        ais->expectedOutputCardinality = expectedCardinality;
         //Allocate more registers for initializations
        runtime->requestExtraAllocation(newRegs);

        //The original index scan is reused
        parts.push_back(op->op);
        //Add the mappings
        mappings[0].push_back(ais->value1);
        mappings[0].push_back(runtime->getRegister(startReg));

        mappings[0].push_back(ais->value2);
        mappings[0].push_back(runtime->getRegister(startReg + 1));

        init[0].push_back(runtime->getRegister(startReg));
        init[0].push_back(runtime->getRegister(startReg + 1));

        unsigned hostInd = 1;
        unsigned newSizeRuntime = runtime->getRegisterCount();
        for(unsigned i = startReg + 2; i < newSizeRuntime; i = i + 2){
            bool subBound, predBound, objBound;
            Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
            switch(ais->order){
                case Database::Order_Object_Predicate_Subject:
                    subBound = false;
                    predBound = ais->bound2;
                    objBound = ais->bound1;
                    objReg = runtime->getRegister(i);
                    predReg = runtime->getRegister(i + 1);                        
                    predReg->value = ais->value2->value;
                    objReg->value = ais->value1->value;
                    break;
                case Database::Order_Object_Subject_Predicate:
                    subBound = ais->bound2;
                    predBound = false;
                    objBound = ais->bound1;
                    objReg = runtime->getRegister(i);
                    subReg = runtime->getRegister(i + 1);
                    objReg->value = ais->value2->value;
                    subReg->value = ais->value1->value;
                    break;
                case Database::Order_Predicate_Object_Subject:
                    subBound = false;
                    predBound = ais->bound1;
                    objBound = ais->bound2;
                    predReg = runtime->getRegister(i);
                    objReg = runtime->getRegister(i + 1);
                    predReg->value = ais->value1->value;
                    objReg->value = ais->value2->value;                        
                    break;
                case Database::Order_Predicate_Subject_Object:
                    subBound = ais->bound2;
                    predBound = ais->bound1;
                    objBound = false;
                    predReg = runtime->getRegister(i);                        
                    subReg = runtime->getRegister(i + 1);                        
                    predReg->value = ais->value1->value;
                    subReg->value = ais->value2->value;                        
                    break;
                case Database::Order_Subject_Object_Predicate:
                    subBound = ais->bound1;
                    predBound = false;
                    objBound = ais->bound2;
                    subReg = runtime->getRegister(i);
                    subReg->value = ais->value1->value;
                    objReg = runtime->getRegister(i + 1);
                    objReg->value = ais->value2->value;
                    break;
                case Database::Order_Subject_Predicate_Object:
                    subBound = ais->bound1;
                    predBound = ais->bound2;
                    objBound = false;
                    subReg = runtime->getRegister(i);
                    subReg->value = ais->value1->value;
                    predReg = runtime->getRegister(i + 1);
                    predReg->value = ais->value2->value;
                    break;
            }
            
            AggregatedIndexScan *newAggIs = AggregatedIndexScan::create(runtime->getDatabase(), ais->order, subReg, subBound, predReg, predBound, objReg, objBound, expectedCardinality);
            parts.push_back(newAggIs);

            //Build the mappings
            mappings[hostInd].push_back(newAggIs->value1);
            mappings[hostInd].push_back(runtime->getRegister(startReg));
            mappings[hostInd].push_back(newAggIs->value2);
            mappings[hostInd].push_back(runtime->getRegister(startReg + 1));

            init[hostInd].push_back(runtime->getRegister(startReg));
            init[hostInd].push_back(runtime->getRegister(startReg + 1));

            ++hostInd;
        }

    }else if(op->type == DOperator::TFullyAggregatedIndexScan){
        FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(op->op);
        newRegs = op->relevantHosts.size();
        fais->expectedOutputCardinality = expectedCardinality;            

        runtime->requestExtraAllocation(newRegs);
        parts.push_back(op->op);

        //Add the mappings
        mappings[0].push_back(fais->value1);
        init[0].push_back(runtime->getRegister(startReg));

        mappings[0].push_back(runtime->getRegister(startReg));
        unsigned hostInd = 1;
        unsigned newSizeRuntime = runtime->getRegisterCount();
        for(unsigned i = startReg + 1; i < newSizeRuntime; ++i){
            bool subBound, predBound, objBound;
            Register *subReg = NULL, *predReg =  NULL, *objReg = NULL;
            switch(fais->order){
                case Database::Order_Object_Predicate_Subject:
                    subBound = false;
                    predBound = false;
                    objBound = fais->bound1;
                    objReg = runtime->getRegister(i);
                    objReg->value = fais->value1->value;
                    break;
                case Database::Order_Object_Subject_Predicate:
                    subBound = false;
                    predBound = false;
                    objBound = fais->bound1;
                    objReg = runtime->getRegister(i);
                    objReg->value = fais->value1->value;
                    break;
                case Database::Order_Predicate_Object_Subject:
                    subBound = false;
                    predBound = fais->bound1;
                    objBound = false;
                    predReg = runtime->getRegister(i);
                    predReg->value = fais->value1->value;
                    break;
                case Database::Order_Predicate_Subject_Object:
                    subBound = false;
                    predBound = fais->bound1;
                    objBound = false;
                    predReg = runtime->getRegister(i);
                    predReg->value = fais->value1->value;
                    break;
                case Database::Order_Subject_Object_Predicate:
                    subBound = fais->bound1;
                    predBound = false;
                    objBound = false;
                    subReg = runtime->getRegister(i);
                    subReg->value = fais->value1->value;
                    break;
                case Database::Order_Subject_Predicate_Object:
                    subBound = fais->bound1;
                    predBound = false;
                    objBound = false;
                    subReg = runtime->getRegister(i);
                    subReg->value = fais->value1->value;
                    break;
            }

            FullyAggregatedIndexScan *newFaggIs = FullyAggregatedIndexScan::create(runtime->getDatabase(), fais->order, subReg, subBound, predReg, predBound, objReg, objBound, expectedCardinality);
            parts.push_back(newFaggIs);

            //Build the mappings
            mappings[hostInd].push_back(newFaggIs->value1);
            mappings[hostInd].push_back(runtime->getRegister(startReg));

            init[hostInd].push_back(runtime->getRegister(startReg));

            ++hostInd;
        }

    }

}

void DOperatorTransformer::setLeavesHomeHosts(DOperator *op, set<Host, cmpHosts> &hosts){
    if(!this->db) throw logic_error("drdf::DOperatorTransformer: A database pointer has not been provided!");
    if(op->isLeaf()){
        this->db->getFragmentsDefinitionSegment().setRelevantHosts(op);
        for(vector<Host>::iterator hit = op->relevantHosts.begin(); hit != op->relevantHosts.end(); ++hit){
//            cout << hit->uri << endl;
            hosts.insert(*hit);            
        }
        if(op->relevantHosts.size() == 1)
            op->home = op->relevantHosts[0].uri;
    }else{
        for(vector<DOperator*>::iterator chit = op->children.begin(); chit != op->children.end(); ++chit){
            setLeavesHomeHosts(*chit, hosts);
        }
        op->relevantHosts = vector<Host>(hosts.begin(), hosts.end());        
    }    
}

bool DOperatorTransformer::assignHomeHost(DOperator *op, string host, bool overwrite){
    vector<DOperator*> &children = op->getChildren();
    vector<DOperator*> bmus;
    map<string, double> knownHosts;
    bool finalResult = false;
    
    if(op->isLeaf()){
        return op->getHost().compare(host) == 0;
    }
    
    for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit){
        if((*chit)->home.empty() || overwrite){
            finalResult |= this->assignHomeHost(*chit, host, overwrite);
        }       
        knownHosts.insert(pair<string, double>((*chit)->home, (*chit)->op->getExpectedOutputCardinality()));       
        if((*chit)->type == DOperator::TMultiMergeUnion){
            bmus.push_back(*chit);
        }
            
    }
    
        
    //If I cannot find the host, certainly it is not 
    // within the leaves
    if(knownHosts.find(host) == knownHosts.end()){
        //First check if there is a BMU
        if(!bmus.empty()){
            //Eureka
            if(bmus.size() == 1){
                op->home = bmus[0]->home;
            }else{
                double maxCardinality = -1.0;
                for(vector<DOperator*>::iterator hit = bmus.begin(); hit != bmus.end(); ++hit){
                    if((*hit)->op->getExpectedOutputCardinality() > maxCardinality){
                        op->home = (*hit)->home;
                        maxCardinality = (*hit)->op->getExpectedOutputCardinality();
                    }
                }                
            }
        }else{        
            //Pick the one with the biggest cardinality
            double maxCardinality = -1.0;
            for(map<string, double>::iterator hit = knownHosts.begin(); hit != knownHosts.end(); ++hit){
                if(hit->second > maxCardinality){
                    op->home = hit->first;
                    maxCardinality = hit->second;
                }
            }
        }
        
                    
        finalResult |= false;        
    }else{
        //Then proceed
        op->home = host;
        //Good case
        finalResult |= true;
    }
    
    return finalResult;
}

DOperator * DOperatorTransformer::addTopNetworkSender(DOperator *op, string destinationHost, Runtime *runtime){
    RemoteSend *rs;
    vector<Register*> registers;
    DOperator *newRoot;
    DebugPlanPrinter out(*runtime, false);
    op->print(out);
    assert(op != NULL);
    
    if(op->isAnyIndexScan()){        
        //Retrieve the registers
        if(op->type == DOperator::TAggregatedIndexScan){
            AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(op->op);           
            if(ais->value1 != NULL) registers.push_back(ais->value1);
            if(ais->value2 != NULL) registers.push_back(ais->value2);
        }else if(op->type == DOperator::TFullyAggregatedIndexScan){
            FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(op->op);
            if(fais->value1 != NULL) registers.push_back(fais->value1);
        }else{
            IndexScan *is = static_cast<IndexScan*>(op->op);
            if(is->value1 != NULL) registers.push_back(is->value1);
            if(is->value2 != NULL) registers.push_back(is->value2);
            if(is->value3 != NULL) registers.push_back(is->value3);
        }
        rs = new IndexScanRemoteSend(op->op, op->home, string(""), registers, runtime);        
    }else if(op->isEfficientJoin()){
        Register *left, *right;
        vector<Register*> leftTail, rightTail;
        if(op->type == DOperator::TMergeJoin){
            MergeJoin *mj = static_cast<MergeJoin*>(op->op);                                    
            left = mj->leftValue;
            right = mj->rightValue;
            leftTail = mj->leftTail;
            rightTail = mj->rightTail;            
        }else{
            HashJoin *hj = static_cast<HashJoin*>(op->op);
            left = hj->leftValue;
            right = hj->rightValue;        
            leftTail = hj->leftTail;
            rightTail = hj->rightTail;            
        }        
        rs = new EfficientJoinRemoteSend(op->op, left, leftTail, right, rightTail, op->home, destinationHost, runtime);        
    }else{
        //Use the default remote sender
        vector<Register*> registers;
        if(op->type == DOperator::TSort){
            Sort *sort = static_cast<Sort*>(op->op);
            registers = sort->values;            
        }else if(op->type == DOperator::TUnion){
            Union *un = static_cast<Union*>(op->op);
            registers = un->initializations[0];
        }else if(op->type == DOperator::THashGroupify){
            HashGroupify *hg = static_cast<HashGroupify*>(op->op);
            registers = hg->values;
        }else if(op->type == DOperator::TSimpleGroupify){
            SimpleGroupify *sg = static_cast<SimpleGroupify*>(op->op);
            registers = sg->values;            
        }else if(op->type == DOperator::TMultiMergeUnion){
            MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(op->op);
            registers = mmu->result;
        }
        
        rs = new DefaultRemoteSend(op->op, registers, op->home, destinationHost, runtime);
 
    }
    
    assert(rs != NULL);
    newRoot = wrap(dynamic_cast<Operator*>(rs));
    op->setParent(newRoot);
    newRoot->children.push_back(op);
        
    return newRoot;
}

bool DOperatorTransformer::prepareForRemoteOperations(string &currentHost, DOperator *op, Runtime *runtime, Logger &logger){    
    //Do a depth search until we find a remote edge!!
    queue<DOperator*> theQueue;
    bool success = true;
    map<uint64_t, pair<DOperator*, vector<DOperator*> > > toAdd;

    theQueue.push(op);  
    while(!theQueue.empty()){
        DOperator *current = theQueue.front();
        for(vector<DOperator*>::iterator chit = current->children.begin(); chit != current->children.end(); ++chit){
            //If they are evaluated in different hosts => Instrument
            if(current->home.compare((*chit)->home) != 0){
                assert(currentHost.compare(current->home) == 0);               
                assert(!currentHost.empty());
                assert(!(*chit)->home.empty());
                assert(!current->home.empty());                
                DOperator *dfetcher = this->buildRemoteConnection(current, *chit, runtime);
                RemoteFetch *rf = static_cast<RemoteFetch*>(dfetcher->getOperator());
                string errorMsg;
                if(rf->prepareRequest(errorMsg) < 0){
                    LOG4CPLUS_WARN(logger, "The prepare for execution request to host " << (*chit)->home << " was not successful: " << errorMsg);
                    success = false;
                }
                toAdd[current->id].first = current;
                toAdd[current->id].second.push_back(dfetcher);
            }else{
                //They can be evaluated in the same host, so go deeper
                theQueue.push(*chit);                
            }
        }
        theQueue.pop();
    }    
    
    LOG4CPLUS_INFO(logger, "Linking remote fetch operators " << endl);
    
    for(map<uint64_t, pair<DOperator*, vector<DOperator*> > >::iterator it = toAdd.begin(); it != toAdd.end(); ++it){
        DOperator *parent = it->second.first;
        vector<DOperator*> &children = parent->getChildren();
        for(vector<DOperator*>::iterator indt = it->second.second.begin(); indt != it->second.second.end(); ++indt)
            children.push_back(*indt);
    }
                
    return success;
}

void DOperatorTransformer::unlinkOperators(Operator *parent, Operator *child){
    if(typeid(*parent) == typeid(Filter)){
        Filter *filter = static_cast<Filter*>(parent);
        filter->input = NULL;
    }else if(typeid(*parent) == typeid(HashGroupify)){
        HashGroupify *hash = static_cast<HashGroupify*>(parent);
        hash->input = NULL;
    }else if(typeid(*parent) == typeid(SimpleGroupify)){
        SimpleGroupify *group = static_cast<SimpleGroupify*>(parent);
        group->input = NULL;        
    }else if(typeid(*parent) == typeid(Selection)){
        Selection *sel = static_cast<Selection*>(parent);
        sel->input = NULL;
    }else if(typeid(*parent) == typeid(Sort)){
        Sort *sort = static_cast<Sort*>(parent);
        sort->input = NULL;
    }else if(typeid(*parent) == typeid(TableFunction)){
        TableFunction *tf = static_cast<TableFunction*>(parent);
        tf->input = NULL;
    }else if(typeid(*parent) == typeid(Union)){
        Union *un = static_cast<Union*>(parent);
        unsigned i = 0;
        for(vector<Operator*>::iterator pit = un->parts.begin(); pit != un->parts.end(); ++pit){
            if((*pit) == child){
                un->parts.erase(un->parts.begin() + i);
                break;
            }
            ++i;
        }       
    }else if(typeid(*parent) == typeid(HashJoin)) {
        HashJoin *hj = static_cast<HashJoin*>(parent);
        if(hj->left == child) hj->left = NULL;
        else if(hj->right == child) hj->right = NULL;
    }else if(typeid(*parent) == typeid(MergeJoin)){
        MergeJoin *mj = static_cast<MergeJoin*>(parent);
        if(mj->left == child) mj->left = NULL;
        else if(mj->right == child) mj->right = NULL;
    }else if(typeid(*parent) == typeid(MergeUnion)){
        MergeUnion *mu = static_cast<MergeUnion*>(parent);
        if(mu->left == child) mu->left = NULL;
        else if(mu->right == child) mu->right = NULL;            
    }else if(typeid(*parent) == typeid(NestedLoopFilter)){
        NestedLoopFilter *nlf = static_cast<NestedLoopFilter*>(parent);
        nlf->input = NULL;
    }else if(typeid(*parent) == typeid(NestedLoopJoin)){
        NestedLoopJoin *nlj = static_cast<NestedLoopJoin*>(parent);
        if(nlj->left == child) nlj->left = NULL;
        else if(nlj->right == child) nlj->right = NULL;
    }else if(typeid(*parent) == typeid(ResultsPrinter)){
        ResultsPrinter *rp = static_cast<ResultsPrinter*>(parent);
        rp->input = NULL;
    }else if(typeid(*parent) == typeid(MultiMergeUnion)){
        MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(parent);
        if(mmu->left == child) mmu->left = NULL;
        else if(mmu->right == child) mmu->right = NULL;                    
    }
}

DOperator* DOperatorTransformer::buildRemoteConnection(DOperator *parent, DOperator *child, Runtime *runtime){    
    DOperator *dfetcher;
    Operator *fetcher;

    //Fetch from parent to child
    child->parent = NULL;
    fetcher = new RemoteFetch(child->op, child, parent->home, child->home, runtime);
    //Use the child id
    dfetcher = new DOperator(fetcher, child->id, parent->home);
    dfetcher->parent = parent;          
    this->unlinkOperators(parent->op, child->op);        
    this->linkWithParent(dfetcher);
    
    return dfetcher;
    
}

unsigned DOperatorTransformer::linkWithParent(DOperator *theOp){
    DOperator *parent = theOp->getParent();
    if(!parent) return 0;

    Operator *parentOp = parent->op;
    Operator *op = theOp->op;
    if(typeid(*parentOp) == typeid(Filter)){
        Filter *filter = static_cast<Filter*>(parentOp);
        filter->input = op;
    }else if(typeid(*parentOp) == typeid(HashGroupify)){
        HashGroupify *hash = static_cast<HashGroupify*>(parentOp);
        hash->input = op;
        return 1;
    }else if(typeid(*parentOp) == typeid(SimpleGroupify)){
        SimpleGroupify *group = static_cast<SimpleGroupify*>(parentOp);
        group->input = op;
        return 1;
    }else if(typeid(*parentOp) == typeid(Selection)){
        Selection *sel = static_cast<Selection*>(parentOp);
        sel->input = op;
    }else if(typeid(*parentOp) == typeid(Sort)){
        Sort *sort = static_cast<Sort*>(parentOp);
        sort->input = op;
    }else if(typeid(*parentOp) == typeid(TableFunction)){
        TableFunction *tf = static_cast<TableFunction*>(parentOp);
        tf->input = op;
    }else if(typeid(*parentOp) == typeid(Union)){
        Union *un = static_cast<Union*>(parentOp);
        un->parts.push_back(op);
    }else if(typeid(*parentOp) == typeid(HashJoin)) {
        HashJoin *hj = static_cast<HashJoin*>(parentOp);        
        if(!hj->left) hj->left = op;
        else if(!hj->right) hj->right = op;
    }else if(typeid(*parentOp) == typeid(MergeJoin)){
        MergeJoin *mj = static_cast<MergeJoin*>(parentOp);
        if(!mj->left) mj->left = op;
        else if(!mj->right) mj->right = op;
        //Finalize the merge join construction
        if(mj->left && mj->right) mj->addMergeHint(mj->leftValue, mj->rightValue);
    }else if(typeid(*parentOp) == typeid(MergeUnion)){
        MergeUnion *mu = static_cast<MergeUnion*>(parentOp);
        if(!mu->left) mu->left = op;
        else if(!mu->right) mu->right = op;            
        if(mu->left && mu->right) mu->addMergeHint(mu->leftReg, mu->rightReg);
    }else if(typeid(*parentOp) == typeid(NestedLoopFilter)){
        NestedLoopFilter *nlf = static_cast<NestedLoopFilter*>(parentOp);
        nlf->input = op;
    }else if(typeid(*parentOp) == typeid(NestedLoopJoin)){
        NestedLoopJoin *nlj = static_cast<NestedLoopJoin*>(parentOp);
        if(!nlj->left) nlj->left = op;
        else if(!nlj->right) nlj->right = op;
    }else if(typeid(*parentOp) == typeid(ResultsPrinter)){
        ResultsPrinter *rp = static_cast<ResultsPrinter*>(parentOp);
        rp->input = op;
    }else if(typeid(*parentOp) == typeid(MultiMergeUnion)){
        MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(parentOp);
        if(!mmu->left) mmu->left = op;
        else if(!mmu->right) mmu->right = op;            
    }

    return 1;
}

bool DOperatorTransformer::findDataSourcesForJoin(DOperator *dop, vector<DOperator*> &leftSources, vector<DOperator*> &rightSources){
    if(!dop->isEfficientJoin()) return false;
    bool result = true; 
    
    DOperator *leftChild = dop->children.at(0);
    DOperator* rightChild = dop->children.at(1);    
    if(leftChild->isAnyIndexScan() && rightChild->isAnyIndexScan()){
        return false;
    }else if(leftChild->isMergingOperator() && rightChild->isMergingOperator()){
        if(leftChild->type == DOperator::TMultiMergeUnion){
            result &= findDataSourcesForMMU(leftChild, leftSources);
        }else if(leftChild->type == DOperator::TUnion){
            result &= findDataSourcesSimpleGroupify(leftChild, leftSources);
        }else{
            return false;
        }
        
        if(rightChild->type == DOperator::TMultiMergeUnion){
            result &= findDataSourcesForMMU(rightChild, rightSources);
        }else if(rightChild->type == DOperator::TSimpleGroupify){
            result &= findDataSourcesSimpleGroupify(rightChild, rightSources);
        }else{
            return false;
        }
    }else if(leftChild->isEfficientJoin() && rightChild->isEfficientJoin()){
        return false;
    }else if((leftChild->isMergingOperator() && rightChild->isAnyIndexScan()) || (rightChild->isMergingOperator() && leftChild->isAnyIndexScan()) ){
        if(leftChild->isMergingOperator()){
            if(leftChild->type == DOperator::TMultiMergeUnion){
                result &= findDataSourcesForMMU(leftChild, leftSources);
            }else if(leftChild->type == DOperator::TSimpleGroupify){
                result &= findDataSourcesSimpleGroupify(leftChild, leftSources);
            }else{
                return false;
            }
        }else{
            leftSources.push_back(leftChild);
            result &= true;
        }
        
        if(rightChild->isMergingOperator()){
            if(rightChild->type == DOperator::TMultiMergeUnion){
                result &= findDataSourcesForMMU(rightChild, rightSources);
            }else if(rightChild->type == DOperator::TSimpleGroupify){
                result &= findDataSourcesSimpleGroupify(rightChild, rightSources);
            }else{
                return false;
            }
        }else{
            rightSources.push_back(rightChild);
            result &= true;
        }
        
    }
    
    return result;
            
}

bool DOperatorTransformer::findDataSourcesForMMU(DOperator* child, vector<DOperator*> &sources){
    queue<DOperator*> queue;
    DOperator *current;
    queue.push(child);

    while(!queue.empty()){
        current = queue.front();
        if(current->isAnyIndexScan()){
            sources.push_back(current);
        }else{
            for(vector<DOperator*>::iterator chit = current->children.begin(); chit != current->children.end(); ++chit){
                queue.push(*chit);
            }
        }
        
        queue.pop();
    }
}

bool DOperatorTransformer::findDataSourcesSimpleGroupify(DOperator* child, vector<DOperator*> &sources){
    DOperator *theUnion = child->children.at(0);
    
    unsigned partId = 0;
    for(vector<DOperator*>::iterator chit = theUnion->children.begin(); chit != theUnion->children.end(); ++chit, ++partId){
        sources.push_back(*chit);
    }    
}

DOperator* DOperatorTransformer::buildNewMergeJoin(DRuntime &runtime, DOperator *source, DOperator *newleft, DOperator *newright, double factor, vector<pair<Register*, Register*> > &leftMappings, vector<pair<Register*, Register*> > &rightMappings){
    Register *newLeftReg, *newRightReg;
    vector<Register*> newLeftTail, newRightTail;
    MergeJoin *omj = static_cast<MergeJoin*>(source->op);
          
    for(unsigned i = 0; i < omj->leftTail.size(); ++i){
        Register *newReg = findRegisterMapping2(omj->leftTail[i], leftMappings);
        assert(newReg != NULL);
        newLeftTail.push_back(newReg);        
    }
    
    for(unsigned i = 0; i < omj->rightTail.size(); ++i){
        Register *newReg = findRegisterMapping2(omj->rightTail[i], rightMappings);
        newRightTail.push_back(newReg);                    
    }

    newLeftReg = findRegisterMapping2(omj->leftValue, leftMappings);
    assert(newLeftReg != NULL);
    newRightReg = findRegisterMapping2(omj->rightValue, rightMappings);
    assert(newRightReg != NULL);    
    
    DOperator *result = wrap(new MergeJoin(newleft->op, newLeftReg, newLeftTail, newright->op, newRightReg, newRightTail, omj->getExpectedOutputCardinality() / factor));
    
    result->children.push_back(newleft);
    result->children.push_back(newright);
    newleft->parent = result;
    newright->parent = result;
    result->home = source->home;
    
    return result;
}

DOperator* DOperatorTransformer::buildNewHashJoin(DRuntime &runtime, DOperator *source, DOperator *newleft, DOperator *newright, double factor, vector<pair<Register*, Register*> > &leftMappings, vector<pair<Register*, Register*> > &rightMappings){
    return NULL;
}

DOperator* DOperatorTransformer::buildNewNestedLoopJoin(DOperator *dop, DOperator *newleft, DOperator *newRight, double factor){
    return NULL;
}

DOperator* DOperatorTransformer::copyIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    bool subBound, predBound, objBound;
    Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
    IndexScan *is = static_cast<IndexScan*>(source->op);
    runtime.requestExtraAllocation(3);
    switch(is->order){
        case Database::Order_Object_Predicate_Subject:
            subBound = is->bound3;
            predBound = is->bound2;
            objBound = is->bound1;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            subReg->value = is->value3->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value3, subReg));
            predReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            predReg->value = is->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value2, predReg));            
            objReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            objReg->value = is->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value1, objReg));            
            break;
        case Database::Order_Object_Subject_Predicate:
            subBound = is->bound2;
            predBound = is->bound3;
            objBound = is->bound1;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            subReg->value = is->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value2, subReg));                        
            predReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            predReg->value = is->value3->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value3, predReg));                        
            objReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            objReg->value = is->value1->value;                        
            outputMappings.push_back(pair<Register*, Register*>(is->value1, objReg));                        
            break;
        case Database::Order_Predicate_Object_Subject:
            subBound = is->bound3;
            predBound = is->bound1;
            objBound = is->bound2;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            subReg->value = is->value3->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value3, subReg));                                    
            predReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            predReg->value = is->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value1, predReg));                                    
            objReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            objReg->value = is->value2->value;                        
            outputMappings.push_back(pair<Register*, Register*>(is->value2, objReg));                                    
            break;
        case Database::Order_Predicate_Subject_Object:
            subBound = is->bound2;
            predBound = is->bound1;
            objBound = is->bound3;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            subReg->value = is->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value2, subReg));            
            predReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            predReg->value = is->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value1, predReg));            
            objReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            objReg->value = is->value3->value;                        
            outputMappings.push_back(pair<Register*, Register*>(is->value3, objReg));            
            break;
        case Database::Order_Subject_Object_Predicate:
            subBound = is->bound1;
            predBound = is->bound3;
            objBound = is->bound2;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            subReg->value = is->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value1, subReg));                                                
            predReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            predReg->value = is->value3->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value3, predReg));                                                            
            objReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            objReg->value = is->value2->value;                        
            outputMappings.push_back(pair<Register*, Register*>(is->value2, objReg));            
            break;
        case Database::Order_Subject_Predicate_Object:
            subBound = is->bound1;
            predBound = is->bound2;
            objBound = is->bound3;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 3);
            subReg->value = is->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value1, subReg));            
            predReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            predReg->value = is->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(is->value2, predReg));                                                            
            objReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            objReg->value = is->value3->value;                        
            outputMappings.push_back(pair<Register*, Register*>(is->value3, objReg));            
            break;
    }

    IndexScan *newIs = IndexScan::create(runtime.getDatabase(), is->order, subReg, subBound, predReg, predBound, objReg, objBound, is->getExpectedOutputCardinality());
    return wrap(newIs);
}

DOperator* DOperatorTransformer::copyAggregatedIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    bool subBound, predBound, objBound;
    Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
    AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(source->op);
    runtime.requestExtraAllocation(2);
    switch(ais->order){
        case Database::Order_Object_Predicate_Subject:
            predBound = ais->bound2;
            objBound = ais->bound1;
            predReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            predReg->value = ais->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, predReg));            
            objReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            objReg->value = ais->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, objReg));            
            break;
        case Database::Order_Object_Subject_Predicate:
            subBound = ais->bound2;
            objBound = ais->bound1;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            subReg->value = ais->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, subReg));                        
            objReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            objReg->value = ais->value1->value;                        
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, objReg));                        
            break;
        case Database::Order_Predicate_Object_Subject:
            predBound = ais->bound1;
            objBound = ais->bound2;
            predReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            predReg->value = ais->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, predReg));                                    
            objReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            objReg->value = ais->value2->value;                        
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, objReg));                                    
            break;
        case Database::Order_Predicate_Subject_Object:
            subBound = ais->bound2;
            predBound = ais->bound1;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            subReg->value = ais->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, subReg));            
            predReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            predReg->value = ais->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, predReg));            
            break;
        case Database::Order_Subject_Object_Predicate:
            subBound = ais->bound1;
            objBound = ais->bound2;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            subReg->value = ais->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, subReg));                                                
            objReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            objReg->value = ais->value2->value;                        
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, objReg));            
            break;
        case Database::Order_Subject_Predicate_Object:
            subBound = ais->bound1;
            predBound = ais->bound2;
            subReg = runtime.getRegister(runtime.getRegisterCount() - 2);
            subReg->value = ais->value1->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value1, subReg));            
            predReg = runtime.getRegister(runtime.getRegisterCount() - 1);
            predReg->value = ais->value2->value;
            outputMappings.push_back(pair<Register*, Register*>(ais->value2, predReg));                                                            
            break;
    }

    AggregatedIndexScan *newIs = AggregatedIndexScan::create(runtime.getDatabase(), ais->order, subReg, subBound, predReg, predBound, objReg, objBound, ais->getExpectedOutputCardinality());
    return wrap(newIs);
}

DOperator* DOperatorTransformer::copyFullyAggregatedIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    return NULL;
}               

DOperator* DOperatorTransformer::copySelection(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    Selection *sel = static_cast<Selection*>(source->op);
    Selection::Predicate *newPred = copyPredicate(sel->predicate, outputMappings);
    DOperator *child = copy(*source->children.begin(), runtime, outputMappings);
    DOperator *result = wrap(new Selection(child->op, runtime.getRuntime(), newPred, sel->getExpectedOutputCardinality()));
    result->children.push_back(child);
    return result;
}

Selection::Predicate* DOperatorTransformer::copyPredicate(Selection::Predicate *source, vector<pair<Register*, Register*> > &outputMappings){
    string className(typeid(*source).name());
    
    if(className.find("NotEqual") != string::npos){
        Selection::NotEqual *neq = static_cast<Selection::NotEqual*>(source);
        Selection::Predicate *newLeft, *newRight;
        newLeft = copyPredicate(neq->left, outputMappings);
        newRight = copyPredicate(neq->right, outputMappings);
        Selection::NotEqual *newneq = new Selection::NotEqual(newLeft, newRight);
        return newneq;
    }else if(className.find("Equal") != string::npos){
        Selection::Equal *eq = static_cast<Selection::Equal*>(source);
        Selection::Predicate *newLeft, *newRight;
        newLeft = copyPredicate(eq->left, outputMappings);
        newRight = copyPredicate(eq->right, outputMappings);
        Selection::Equal *neweq = new Selection::Equal(newLeft, newRight);
        return neweq;       
    }else if(className.find("Variable") != string::npos){
        Selection::Variable *var = static_cast<Selection::Variable*>(source);
        Register *newReg = findRegisterMapping2(var->reg, outputMappings);
        Selection::Variable *newVar = new Selection::Variable(newReg);
        return newVar;
    }
    
    return NULL;
}

DOperator * DOperatorTransformer::copyMergeJoin(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    Register *newLeftReg, *newRightReg;
    vector<Register*> newLeftTail, newRightTail;
    MergeJoin *mj = static_cast<MergeJoin*>(source->op);
    vector<DOperator*> newChildren;
    
    newChildren.push_back(copy(source->children[0], runtime, outputMappings));
    newChildren.push_back(copy(source->children[1], runtime, outputMappings));
    
  
    for(unsigned i = 0; i < mj->leftTail.size(); ++i){
        int mapping = findRegisterMapping(mj->leftTail[i], outputMappings);
        assert(mapping != -1);
        newLeftTail.push_back(outputMappings[mapping].second);
    }
    
    for(unsigned i = 0; i < mj->rightTail.size(); ++i){
        int mapping = findRegisterMapping(mj->rightTail[i], outputMappings);
        assert(mapping != -1);
        newRightTail.push_back(outputMappings[mapping].second);
    }
    
    int newLeftRegMap, newRightRegMap; 
    
    newLeftRegMap = findRegisterMapping(mj->leftValue, outputMappings);
    assert(newLeftRegMap != -1);
    newRightRegMap = findRegisterMapping(mj->rightValue, outputMappings);
    assert(newRightRegMap != -1);
    
    newLeftReg = outputMappings[newLeftRegMap].second;
    newRightReg = outputMappings[newRightRegMap].second;
            
    DOperator *result = wrap(new MergeJoin(newChildren[0]->op, newLeftReg, newLeftTail, newChildren[1]->op, newRightReg, newRightTail, mj->getExpectedOutputCardinality()));
    result->children = newChildren;
    newChildren[0]->parent = result;
    newChildren[1]->parent = result;
    
    return result;
    
}        

DOperator * DOperatorTransformer::copyHashJoin(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
}              

DOperator * DOperatorTransformer::copyUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    vector<Operator*> newParts;
    vector<DOperator*> newChildren;
    vector<vector<Register*> > newMappings, newInits;
    vector<pair<Register*, Register*> > fixedMappings;    
    Union *un = static_cast<Union*>(source->op);
    
    for(vector<DOperator*>::iterator chit = source->children.begin(); chit != source->children.end(); ++chit){
        newChildren.push_back(copy(*chit, runtime, outputMappings));
        newParts.push_back(newChildren.back()->op);
    }
    
    newMappings.resize(un->mappings.size());
    newInits.resize(un->initializations.size());
    unsigned i = 0;
    vector<Register*> destinations;
    for(vector<vector<Register*> >::iterator mpit = un->mappings.begin(); mpit != un->mappings.end(); ++mpit, ++i){
        if(i == 0){
            for(unsigned k = 0; k < mpit->size() / 2; ++k){
                runtime.requestExtraAllocation(1);
                destinations.push_back(runtime.getRegister(runtime.getRegisterCount() - 1));
            }
        }
        
        unsigned j = 0;
        for(vector<Register*>::iterator mit = mpit->begin(); mit != mpit->end(); mit = mit + 2, ++j){
            //Search the correct mappings for the origin and allocate more memory for the destination
            int mapping = findRegisterMapping(*mit, outputMappings);
            assert(mapping != -1);
            newMappings[i].push_back(outputMappings[mapping].second);
            newMappings[i].push_back(destinations[j]);            
            fixedMappings.push_back(pair<Register*, Register*>(*(mit + 1), destinations[j]));
        }
        
        newInits[i] = destinations;
    }
    
    outputMappings.insert(outputMappings.end(), fixedMappings.begin(), fixedMappings.end());
    
    DOperator *result = wrap(new Union(newParts, newMappings, newInits, un->getExpectedOutputCardinality()));          
    result->children = newChildren;
    for(vector<DOperator*>::iterator chit = result->children.begin(); chit != result->children.end(); ++chit){
        (*chit)->parent = result;
    }
    
    return result;
}                        

DOperator * DOperatorTransformer::copyMergeUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
}                                

DOperator * DOperatorTransformer::copyMultiMergeUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    DOperator *newleft, *newright;
    MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(source->op);
    vector<Register*> newResult, newLeftRegs, newRightRegs;
    
    newleft = copy(source, runtime, outputMappings);
    newright = copy(source, runtime, outputMappings);
   
    unsigned startPoint = runtime.getRegisterCount();
    runtime.requestExtraAllocation(mmu->result.size());
    unsigned endPoint = runtime.getRegisterCount();
   
    unsigned k = 0;
    for(unsigned i = startPoint; i < endPoint; ++i, ++k){
        outputMappings.push_back(pair<Register*, Register*>(mmu->result[k], runtime.getRegister(i)));
        newResult.push_back(runtime.getRegister(i));
    }
    
    for(unsigned i = 0; i < mmu->leftReg.size(); ++i){
        int mapping = findRegisterMapping(mmu->leftReg[i], outputMappings);
        assert(mapping != -1);
        newLeftRegs.push_back(outputMappings[mapping].second);
        
    }

    for(unsigned i = 0; i < mmu->rightReg.size(); ++i){
        int mapping = findRegisterMapping(mmu->rightReg[i], outputMappings);
        assert(mapping != -1);
        newRightRegs.push_back(outputMappings[mapping].second);
        
    }
    
    DOperator *result = wrap(new MultiMergeUnion(newResult, newleft->op, newLeftRegs, newright->op, newRightRegs, mmu->getExpectedOutputCardinality()));
    result->children.push_back(newleft);
    newleft->parent = result;
    result->children.push_back(newright);
    newright->parent = result;
    return result;
}                                        

DOperator * DOperatorTransformer::copyHashGroupify(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
}

DOperator * DOperatorTransformer::copySort(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
}                                                        

DOperator * DOperatorTransformer::copySimpleGroupify(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    SimpleGroupify *nsg = static_cast<SimpleGroupify*>(source->op);
    DOperator *newInput = copy(source->children.at(0), runtime, outputMappings);
    vector<Register*> newValues;
    
    for(vector<Register*>::iterator rit = nsg->values.begin(); rit != nsg->values.end(); ++rit){
        int mapping = findRegisterMapping(*rit, outputMappings);
        assert(mapping != -1);
        newValues.push_back(outputMappings[mapping].second);
    }
    
    DOperator *result = wrap(new SimpleGroupify(newInput->op, newValues, source->op->getExpectedOutputCardinality()));
    result->children.push_back(newInput);
    newInput->parent = result;
    return result;
}                                                        


DOperator* DOperatorTransformer::copy(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings){
    DOperator *result = NULL;
    if(source->type == DOperator::TIndexScan){
        result = copyIndexScan(source, runtime, outputMappings);
    }else if(source->type == DOperator::TAggregatedIndexScan){
        result = copyAggregatedIndexScan(source, runtime, outputMappings);
    }else if(source->type == DOperator::TFullyAggregatedIndexScan){
        result = copyFullyAggregatedIndexScan(source, runtime, outputMappings);
    }else if(source->type == DOperator::TMergeJoin){
        result = copyMergeJoin(source, runtime, outputMappings);
    }else if(source->type == DOperator::THashJoin){
        result = copyHashJoin(source, runtime, outputMappings);
    }else if(source->type == DOperator::TUnion){
        result = copyUnion(source, runtime, outputMappings);
    }else if(source->type == DOperator::TMergeUnion){
        result = copyMergeUnion(source, runtime, outputMappings);
    }else if(source->type == DOperator::TMultiMergeUnion){
        result = copyMultiMergeUnion(source, runtime, outputMappings);
    }else if(source->type == DOperator::THashGroupify){
        result = copyHashGroupify(source, runtime, outputMappings);
    }else if(source->type == DOperator::TSort){
        result = copySort(source, runtime, outputMappings);
    }else if(source->type == DOperator::TSimpleGroupify){
        result = copySimpleGroupify(source, runtime, outputMappings);
    }
    
    if(result != NULL){
        result->home = source->home;
        result->duplicateHandling = source->duplicateHandling;
        result->id = source->id;
        result->estimatedCost = source->estimatedCost;
        result->outputRegisters = source->outputRegisters;
        result->relevantHosts = source->relevantHosts;
        result->type = source->type;        
    }
    
    return result;
}


void DOperatorTransformer::combineSources(DOperator *dop, vector<DOperator*>& leftSources, vector<DOperator*>& rightSources, vector<DOperator*>& newJoins, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &leftMappings, vector<vector<pair<Register*, Register*> > > &rightMappings, 
        vector<pair<Register*, Register*> > &unionLeftMappings, vector<pair<Register*, Register*> > &unionRightMappings){
    //Build new sources
    double factor = leftSources.size() * rightSources.size();
    unsigned ind = 0;
    DOperator::Type joinType = dop->type;    
    leftMappings.resize((unsigned)factor);
    rightMappings.resize((unsigned)factor);
    for(vector<DOperator*>::iterator lsit = leftSources.begin(); lsit != leftSources.end(); ++lsit){
        for(vector<DOperator*>::iterator rsit = rightSources.begin(); rsit != rightSources.end(); ++rsit, ++ind){            
            DOperator *newLeft, *newRight;
            vector<pair<Register*, Register*> > tmpLeftMaps, tmpRightMaps;
            newLeft = copy(*lsit, runtime, tmpLeftMaps);
            newRight = copy(*rsit, runtime, tmpRightMaps);
            //Solve the mappings
            for(unsigned j = 0; j < tmpLeftMaps.size(); ++j){
                Register *mappedRegister = findRegisterMapping2(tmpLeftMaps[j].first, unionLeftMappings);
                if(mappedRegister != NULL)
                    leftMappings[ind].push_back(pair<Register*, Register*>(mappedRegister, tmpLeftMaps[j].second));
                else
                    leftMappings[ind].push_back(pair<Register*, Register*>(tmpLeftMaps[j].first, tmpLeftMaps[j].second));                    
                
            }
            
            for(unsigned j = 0; j < tmpRightMaps.size(); ++j){
                Register *mappedRegister = findRegisterMapping2(tmpRightMaps[j].first, unionRightMappings);
                if(mappedRegister != NULL)
                    rightMappings[ind].push_back(pair<Register*, Register*>(mappedRegister, tmpRightMaps[j].second));
                else
                    rightMappings[ind].push_back(pair<Register*, Register*>(tmpRightMaps[j].first, tmpRightMaps[j].second));                    
            }
            
            if(joinType == DOperator::TMergeJoin){                
                newJoins.push_back(buildNewMergeJoin(runtime, dop, newLeft, newRight, factor, leftMappings[ind], rightMappings[ind]));
            }else if(joinType == DOperator::THashJoin){
                newJoins.push_back(buildNewHashJoin(runtime, dop, newLeft, newRight, factor, leftMappings[ind], rightMappings[ind]));                
            }
        }
    }
}

void DOperatorTransformer::findProjectionMappingsForOperator2(DOperator *dop, vector<DOperator*> &sources, vector<pair<Register*, Register*> > &unionMappings){
    if(dop->type == DOperator::TMultiMergeUnion){
        MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(dop->op);
        for(vector<DOperator*>::iterator oit = sources.begin(); oit != sources.end(); ++oit){
            vector<Register*> origins;
            if((*oit)->type == DOperator::TFullyAggregatedIndexScan){
                FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>((*oit)->op);
                origins.push_back(fais->value1);
            }else if((*oit)->type == DOperator::TAggregatedIndexScan){
                AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>((*oit)->op);
                origins.push_back(ais->value1);
                origins.push_back(ais->value2);                
            }else if((*oit)->type == DOperator::TIndexScan){
                IndexScan *is = static_cast<IndexScan*>((*oit)->op);
                origins.push_back(is->value1);
                origins.push_back(is->value2);                                
                origins.push_back(is->value3);
            }
                       
            unsigned k = 0;
            for(vector<Register*>::iterator rit = mmu->result.begin(); rit != mmu->result.end(); ++rit, ++k){
                unionMappings.push_back(pair<Register*, Register*>(origins[k], *rit));
            }            
        }
    }else if(dop->type == DOperator::TMergeUnion){
        MergeUnion *mu = static_cast<MergeUnion*>(dop->op);
        for(vector<DOperator*>::iterator oit = sources.begin(); oit != sources.end(); ++oit){            
            if((*oit)->type == DOperator::TFullyAggregatedIndexScan){
                FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>((*oit)->op);
                unionMappings.push_back(pair<Register*, Register*>(fais->value1, mu->result));
            }            
        }
        
    }else if(dop->type == DOperator::TSimpleGroupify){
        SimpleGroupify *sg = static_cast<SimpleGroupify*>(dop->op);
        for(vector<DOperator*>::iterator oit = sources.begin(); oit != sources.end(); ++oit){
            vector<Register*> origins;
            if((*oit)->type == DOperator::TFullyAggregatedIndexScan){
                FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>((*oit)->op);
                origins.push_back(fais->value1);
            }else if((*oit)->type == DOperator::TAggregatedIndexScan){
                AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>((*oit)->op);
                origins.push_back(ais->value1);
                origins.push_back(ais->value2);                
            }else if((*oit)->type == DOperator::TIndexScan){
                IndexScan *is = static_cast<IndexScan*>((*oit)->op);
                origins.push_back(is->value1);
                origins.push_back(is->value2);                                
                origins.push_back(is->value3);
            }
                       
            unsigned k = 0;
            for(vector<Register*>::iterator rit = sg->values.begin(); rit != sg->values.end(); ++rit, ++k){
                unionMappings.push_back(pair<Register*, Register*>(origins[k], *rit));
            }            
        }
    }
    
}

DOperator* DOperatorTransformer::parallelize(DOperator *dop, DRuntime &runtime){
    vector<DOperator *> leftSources, rightSources;
    vector<vector<pair<Register*, Register*> > > rightMappings, leftMappings, newMappings;
    vector<pair<Register*, Register*> > unionLeftMappings, unionRightMappings;
    vector<DOperator*> newJoins; 
    DOperator *newDOperator;
    
    //First determine the data sources for this join
    this->findDataSourcesForJoin(dop, leftSources, rightSources);
    
    this->findProjectionMappingsForOperator2(dop->children.at(0), leftSources, unionLeftMappings);
    this->findProjectionMappingsForOperator2(dop->children.at(1), rightSources, unionRightMappings);
    
    //Combine every source in the left side with every source in the right side
    this->combineSources(dop, leftSources, rightSources, newJoins, runtime, leftMappings, rightMappings, unionLeftMappings, unionRightMappings);
    
    newDOperator = this->addMultiMergeUnionChain(newJoins, runtime, leftMappings, rightMappings, newMappings);        
        
    //Unlink the operator
    if(dop->parent != NULL){
        this->unlinkOperators(dop->parent->op, dop->op);
        vector<DOperator*>::iterator chit;
        for(chit = dop->parent->children.begin(); chit != dop->parent->children.end(); ++chit){
            if(*chit == dop) break;
        }
        //The children must be there
        assert(*chit == dop);
        assert(chit != dop->parent->children.end());
        
        //Link the new operator
        vector<DOperator*>::iterator toInsert = dop->parent->children.erase(chit);                
        newDOperator->parent = dop->parent;        
        newDOperator->parent->children.insert(toInsert, newDOperator);
        dop->parent = NULL;
        this->linkWithParent(newDOperator);
        
        
        //Now fix the mappings
        for(unsigned i = 0; i < newMappings.size(); ++i)
            this->fixProjections(newDOperator->parent, newMappings[i]);
     }else{
        newDOperator->duplicateHandling = dop->duplicateHandling;
        newDOperator->outputRegisters = dop->outputRegisters;
        newDOperator->relevantHosts = dop->relevantHosts;
        //Fix the output mappings
        unsigned solved = 0;
        for(unsigned i = 0; i < newMappings.size(); ++i){
            for(unsigned j = 0; j < newDOperator->outputRegisters.size(); ++j){
                Register *regMapping = findRegisterMapping2(newDOperator->outputRegisters[j], newMappings[i]);
                if(regMapping != NULL){
                    newDOperator->outputRegisters[j] = regMapping;
                    ++solved;
                }                    
            }
                
            if(solved == newDOperator->outputRegisters.size())
                break;
        }
        
    }
    
    return newDOperator;
    
}

DOperator* DOperatorTransformer::addMultiMergeUnionChain(vector<DOperator*> &input, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &leftMappings, 
        vector<vector<pair<Register*, Register*> > > &rightMappings, vector<vector<pair<Register*, Register*> > > &newMappings){

    vector<MultiMergeUnion*> mergeUnions;
    vector<vector<Register *> > unmappedRegisters;
    unsigned nMergeUnions;

    nMergeUnions = input.size() - 1;

    mergeUnions.resize(nMergeUnions);
    MultiMergeUnion *lastMergeUnion = NULL;
    DOperator *lastDOperator = NULL;
    newMappings.resize(input.size());
    unmappedRegisters.resize(input.size());
    
    for(unsigned i = 0; i < nMergeUnions; ++i){                    
        vector<Register*> result;
        if(lastMergeUnion == NULL){
            vector<Register*> leftRegs, rightRegs;
            Operator *join1, *join2;
            if(input[i]->type == DOperator::TMergeJoin){
                MergeJoin *mj1 = static_cast<MergeJoin*>(input[i]->op);
                MergeJoin *mj2 = static_cast<MergeJoin*>(input[i + 1]->op);
                
                leftRegs.push_back(mj1->leftValue);
                leftRegs.push_back(mj1->rightValue);
                leftRegs.insert(leftRegs.end(), mj1->leftTail.begin(), mj1->leftTail.end());
                leftRegs.insert(leftRegs.end(), mj1->rightTail.begin(), mj1->rightTail.end());
                
                rightRegs.push_back(mj2->leftValue);
                rightRegs.push_back(mj2->rightValue);
                rightRegs.insert(rightRegs.end(), mj2->leftTail.begin(), mj2->leftTail.end());
                rightRegs.insert(rightRegs.end(), mj2->rightTail.begin(), mj2->rightTail.end());
                
                join1 = dynamic_cast<Operator*>(mj1);
                join2 = dynamic_cast<Operator*>(mj2);                        
            }else if(input[i]->type == DOperator::THashJoin){
                HashJoin *hj1 = static_cast<HashJoin*>(input[i]->op);
                HashJoin *hj2 = static_cast<HashJoin*>(input[i + 1]->op);
                
                leftRegs.push_back(hj1->leftValue);
                leftRegs.push_back(hj1->rightValue);
                leftRegs.insert(leftRegs.end(), hj1->leftTail.begin(), hj1->leftTail.end());
                leftRegs.insert(leftRegs.end(), hj1->rightTail.begin(), hj1->rightTail.end());
                
                rightRegs.push_back(hj2->leftValue);
                rightRegs.push_back(hj2->rightValue);
                rightRegs.insert(rightRegs.end(), hj2->leftTail.begin(), hj2->leftTail.end());
                rightRegs.insert(rightRegs.end(), hj2->rightTail.begin(), hj2->rightTail.end());

                join1 = dynamic_cast<Operator*>(hj1);
                join2 = dynamic_cast<Operator*>(hj2);                                        
            }else if(input[i]->type == DOperator::TNestedLoopJoin){
                join1 = input[i]->op;
                join2 = input[i + 1]->op;                                                        
            }

            unsigned startPoint = runtime.getRegisterCount();
            runtime.requestExtraAllocation(rightRegs.size());
            unsigned endPoint = runtime.getRegisterCount();
            unsigned j = 0;
            for(unsigned k = startPoint; k < endPoint; ++k, ++j){
                result.push_back(runtime.getRegister(k));
                unmappedRegisters[i].push_back(leftRegs[j]);
                unmappedRegisters[i + 1].push_back(rightRegs[j]);
            }
            
            double totalExCard = join1->getExpectedOutputCardinality() + join2->getExpectedOutputCardinality();
            mergeUnions[i] = new MultiMergeUnion(result, join1, leftRegs, join2, rightRegs, totalExCard);
            lastMergeUnion = mergeUnions[i];                        

            //Linking
            lastDOperator = wrap(mergeUnions[i]);
            lastDOperator->home = input[i]->home;
            input[i]->parent = lastDOperator;
            input[i + 1]->parent = lastDOperator;
            lastDOperator->children.push_back(input[i]);
            lastDOperator->children.push_back(input[i + 1]);                                
        }else{
            Operator *join;
            vector<Register*> leftRegs;
            if(input[i + 1]->type == DOperator::TMergeJoin){
                MergeJoin *mj = static_cast<MergeJoin*>(input[i + 1]->op);
                
                leftRegs.push_back(mj->leftValue);
                leftRegs.push_back(mj->rightValue);
                leftRegs.insert(leftRegs.end(), mj->leftTail.begin(), mj->leftTail.end());
                leftRegs.insert(leftRegs.end(), mj->rightTail.begin(), mj->rightTail.end());
                                
                join = dynamic_cast<Operator*>(mj);
            }else if(input[i + 1]->type == DOperator::THashJoin){
                HashJoin *hj = static_cast<HashJoin*>(input[i + 1]->op);
                
                leftRegs.push_back(hj->leftValue);
                leftRegs.push_back(hj->rightValue);
                leftRegs.insert(leftRegs.end(), hj->leftTail.begin(), hj->leftTail.end());
                leftRegs.insert(leftRegs.end(), hj->rightTail.begin(), hj->rightTail.end());
                
                join = dynamic_cast<Operator*>(hj);
            }else if(input[i + 1]->type == DOperator::TNestedLoopJoin){
                join = input[i + 1]->op;
            }
            
            double totalExCard = join->getExpectedOutputCardinality() + lastMergeUnion->getExpectedOutputCardinality();
            unsigned startPoint = runtime.getRegisterCount();
            runtime.requestExtraAllocation(leftRegs.size());
            unsigned endPoint = runtime.getRegisterCount();
            unsigned j = 0;
            for(unsigned k = startPoint; k < endPoint; ++k, ++j){
                result.push_back(runtime.getRegister(k));       
                unmappedRegisters[i + 1].push_back(leftRegs[j]);                
            }
            mergeUnions[i] = new MultiMergeUnion(result, lastMergeUnion, lastMergeUnion->result, join, leftRegs, totalExCard);

            DOperator *dmmu;                
            dmmu = wrap(mergeUnions[i]);
            input[i + 1]->parent = dmmu;
            dmmu->home = lastDOperator->home;
            lastDOperator->parent = dmmu;
            dmmu->children.push_back(lastDOperator);                
            dmmu->children.push_back(input[i + 1]);

            lastDOperator = dmmu;
            lastMergeUnion = mergeUnions[i];            
        }
    }
        
    for(unsigned i = 0; i < unmappedRegisters.size(); ++i){        
        for(unsigned j = 0; j < unmappedRegisters[i].size(); ++j){
            Register *origin = findRegisterMappingInv2(unmappedRegisters[i][j], leftMappings[i]);
            if(origin == NULL)
                origin = findRegisterMappingInv2(unmappedRegisters[i][j], rightMappings[i]);
            
            assert(origin != NULL);
            newMappings[i].push_back(pair<Register*, Register*>(origin, lastMergeUnion->result[j]));
        }
    }
    
//    for(unsigned m = 0; m < newMappings.size(); ++m){        
//        printRegisterMappings(&runtime, newMappings[m]);
//    }
        
    return lastDOperator;        
}

DOperator* DOperatorTransformer::addUnionsAndGroupify(vector<DOperator*> &input, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &allMappings, vector<vector<pair<Register*, Register*> > > &newMappings){
    Union *newUnion;
    vector<vector<Register*> > init, mappings;
    DOperator *dNewUnion;
    vector<Operator*> parts;
    double totalCardinality = 0;
    unsigned partId = 0;
 
    mappings.resize(input.size());
    init.resize(input.size());
    newMappings.resize(input.size());
    
    for(vector<DOperator*>::iterator dit = input.begin(); dit != input.end(); ++dit){
        parts.push_back((*dit)->op);
        totalCardinality += (*dit)->op->getExpectedOutputCardinality();        
        
        if((*dit)->type == DOperator::TMergeJoin){
            MergeJoin *mj = static_cast<MergeJoin*>((*dit)->op);
            unsigned startPoint = runtime.getRegisterCount();
            runtime.requestExtraAllocation(mj->leftTail.size() + mj->rightTail.size() + 2);
            unsigned endPoint = runtime.getRegisterCount();
            
            Register *mapLeftValue = runtime.getRegister(startPoint++);
            int leftInd;
            int mapping = findRegisterMappingInv(mj->leftValue, allMappings, leftInd);
            assert(mapping != -1);            
            newMappings[partId].push_back(pair<Register*, Register*>(allMappings[leftInd][mapping].first, mapLeftValue));
            mappings[partId].push_back(mj->leftValue);            
            mappings[partId].push_back(mapLeftValue);            
            init[partId].push_back(mapLeftValue);            
            
            int rightInd;
            Register *mapRightValue = runtime.getRegister(startPoint++);
            mapping = findRegisterMappingInv(mj->rightValue, allMappings, rightInd);
            assert(mapping != -1);
            newMappings[partId].push_back(pair<Register*, Register*>(allMappings[rightInd][mapping].first, mapRightValue));            
            mappings[partId].push_back(mj->rightValue);
            mappings[partId].push_back(mapRightValue); 
            init[partId].push_back(mapRightValue);            
            
            for(unsigned i = 0; i < mj->leftTail.size(); ++i){
                int ind;
                Register *theReg = runtime.getRegister(startPoint++);
                int indMapping = findRegisterMappingInv(mj->leftTail[i], allMappings, ind);
                assert(indMapping != -1);
                newMappings[partId].push_back(pair<Register*, Register*>(allMappings[ind][indMapping].first, theReg));                
                mappings[partId].push_back(mj->leftTail[i]);
                mappings[partId].push_back(theReg);            
                init[partId].push_back(theReg);                
            }
            
            for(unsigned i = 0; i < mj->rightTail.size(); ++i){
                int ind;
                Register *theReg = runtime.getRegister(startPoint++);
                int indMapping = findRegisterMappingInv(mj->rightTail[i], allMappings, ind);
                assert(indMapping != -1);
                newMappings[partId].push_back(pair<Register*, Register*>(allMappings[ind][indMapping].first, theReg));                
                mappings[partId].push_back(mj->rightTail[i]);
                mappings[partId].push_back(theReg);            
                init[partId].push_back(theReg);
            }
            
            assert(startPoint == endPoint);
        }
        
        ++partId;
    }
        
    newUnion = new Union(parts, mappings, init, totalCardinality);
    dNewUnion = wrap(newUnion);

    for(vector<DOperator*>::iterator pit = input.begin(); pit != input.end(); ++pit){
        dNewUnion->children.push_back(*pit);
        (*pit)->parent = dNewUnion;
    }
    
    dNewUnion->home = dNewUnion->children.at(0)->home; 

    SimpleGroupify *groupify = new SimpleGroupify(dynamic_cast<Operator*>(newUnion), init.at(0), totalCardinality);
    DOperator *dGroupify = wrap(groupify);
    dGroupify->children.push_back(dNewUnion);
    dNewUnion->parent = dGroupify;        
    dGroupify->home = dNewUnion->home;

    return dGroupify;        

}            

DOperator* DOperatorTransformer::clone(DOperator *input, DRuntime &runtime){
    DOperator *result = NULL;
    assert(this->db != NULL);
    if(input->type == DOperator::TIndexScan){
        result = cloneIndexScan(input, runtime.getDatabase());
    }else if(input->type == DOperator::TAggregatedIndexScan){
        result = cloneAggregatedIndexScan(input, runtime.getDatabase());
    }else if(input->type == DOperator::TFullyAggregatedIndexScan){
        result = cloneFullyAggregatedIndexScan(input, runtime.getDatabase());
    }else if(input->type == DOperator::TMergeJoin){
        result = cloneMergeJoin(input);
    }else if(input->type == DOperator::THashJoin){
        result = cloneHashJoin(input);
    }else if(input->type == DOperator::TUnion){
        result = cloneUnion(input);
    }else if(input->type == DOperator::TMergeUnion){
        result = cloneMergeUnion(input);
    }else if(input->type == DOperator::TMultiMergeUnion){
        result = cloneMultiMergeUnion(input);
    }else if(input->type == DOperator::THashGroupify){
        result = cloneHashGroupify(input);
    }else if(input->type == DOperator::TSort){
        result = cloneSort(input);
    }else if(input->type == DOperator::TSimpleGroupify){
        result = cloneSimpleGroupify(input);
    }else if(input->type == DOperator::TSelection){
        result = cloneSelection(input, runtime);
    }
    
    for(vector<DOperator*>::iterator chit = input->children.begin(); chit != input->children.end(); ++chit){
        DOperator *newChild = clone(*chit, runtime);
        result->children.push_back(newChild);
        newChild->parent = result;
        linkWithParent(newChild);
    }
    
    if(result != NULL){
        result->type = input->type;
        result->duplicateHandling = input->duplicateHandling;
        result->estimatedCost = input->estimatedCost;
        result->id = input->id;
        result->home = input->home;
        result->outputRegisters = input->outputRegisters;
        result->relevantHosts = input->relevantHosts;
    }
    
    return result;    
}

DOperator * DOperatorTransformer::cloneIndexScan(DOperator *source, Database &adb){
    bool subBound, predBound, objBound;
    Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
    IndexScan *is = static_cast<IndexScan*>(source->op);
    switch(is->order){
        case Database::Order_Object_Predicate_Subject:
            subBound = is->bound3;
            predBound = is->bound2;
            objBound = is->bound1;
            subReg = is->value3;
            predReg = is->value2;
            objReg = is->value1;
            break;
        case Database::Order_Object_Subject_Predicate:
            subBound = is->bound2;
            predBound = is->bound3;
            objBound = is->bound1;
            subReg = is->value2;
            predReg = is->value3;
            objReg = is->value1;                        
            break;
        case Database::Order_Predicate_Object_Subject:
            subBound = is->bound3;
            predBound = is->bound1;
            objBound = is->bound2;
            subReg = is->value3;
            predReg = is->value1;
            objReg = is->value2;                        
            break;
        case Database::Order_Predicate_Subject_Object:
            subBound = is->bound2;
            predBound = is->bound1;
            objBound = is->bound3;
            subReg = is->value2;
            predReg = is->value1;
            objReg = is->value3;                        
            break;
        case Database::Order_Subject_Object_Predicate:
            subBound = is->bound1;
            predBound = is->bound3;
            objBound = is->bound2;
            subReg = is->value1;
            predReg = is->value3;
            objReg = is->value2;                        
            break;
        case Database::Order_Subject_Predicate_Object:
            subBound = is->bound1;
            predBound = is->bound2;
            objBound = is->bound3;
            subReg = is->value1;
            predReg = is->value2;
            objReg = is->value3;                        
            break;
    }

    IndexScan *newIs = IndexScan::create(adb, is->order, subReg, subBound, predReg, predBound, objReg, objBound, is->getExpectedOutputCardinality());
    return wrap(newIs);    
}

DOperator * DOperatorTransformer::cloneAggregatedIndexScan(DOperator *source, Database &adb){
    bool subBound, predBound, objBound;
    Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
    AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(source->op);
    switch(ais->order){
        case Database::Order_Object_Predicate_Subject:
            subBound = false;
            predBound = ais->bound2;
            objBound = ais->bound1;
            subReg = NULL;
            predReg = ais->value2;
            objReg = ais->value1;
            break;
        case Database::Order_Object_Subject_Predicate:
            subBound = ais->bound2;
            predBound = false;
            objBound = ais->bound1;
            subReg = ais->value2;
            predReg = NULL;
            objReg = ais->value1;                        
            break;
        case Database::Order_Predicate_Object_Subject:
            subBound = false;
            predBound = ais->bound1;
            objBound = ais->bound2;
            subReg = NULL;
            predReg = ais->value1;
            objReg = ais->value2;                        
            break;
        case Database::Order_Predicate_Subject_Object:
            subBound = ais->bound2;
            predBound = ais->bound1;
            objBound = false;
            subReg = ais->value2;
            predReg = ais->value1;
            objReg = NULL;                        
            break;
        case Database::Order_Subject_Object_Predicate:
            subBound = ais->bound1;
            predBound = false;
            objBound = ais->bound2;
            subReg= ais->value1;
            predReg = NULL;
            objReg = ais->value2;                        
            break;
        case Database::Order_Subject_Predicate_Object:
            subBound = ais->bound1;
            predBound = ais->bound2;
            objBound = false;
            subReg = ais->value1;
            predReg = ais->value2;
            objReg = NULL;                        
            break;
    }

    AggregatedIndexScan *newAis = AggregatedIndexScan::create(adb, ais->order, subReg, subBound, predReg, predBound, objReg, objBound, ais->getExpectedOutputCardinality());
    return wrap(newAis);    
}

DOperator * DOperatorTransformer::cloneFullyAggregatedIndexScan(DOperator *source, Database &adb){
    bool subBound, predBound, objBound;
    Register *subReg = NULL, *predReg = NULL, *objReg = NULL;
    FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(source->op);
    switch(fais->order){
        case Database::Order_Object_Predicate_Subject:
            subBound = false;
            predBound = false;
            objBound = fais->bound1;
            subReg = NULL;
            predReg = NULL;
            objReg = fais->value1;
            break;
        case Database::Order_Object_Subject_Predicate:
            subBound = false;
            predBound = false;
            objBound = fais->bound1;
            subReg = NULL;
            predReg = NULL;
            objReg = fais->value1;                        
            break;
        case Database::Order_Predicate_Object_Subject:
            subBound = false;
            predBound = fais->bound1;
            objBound = false;
            subReg = NULL;
            predReg = fais->value1;
            objReg = NULL;                        
            break;
        case Database::Order_Predicate_Subject_Object:
            subBound = false;
            predBound = fais->bound1;
            objBound = false;
            subReg = NULL;
            predReg = fais->value1;
            objReg = NULL;                        
            break;
        case Database::Order_Subject_Object_Predicate:
            subBound = fais->bound1;
            predBound = false;
            objBound = false;
            subReg = fais->value1;
            predReg = NULL;
            objReg = NULL;                        
            break;
        case Database::Order_Subject_Predicate_Object:
            subBound = fais->bound1;
            predBound = false;
            objBound = false;
            subReg = fais->value1;
            predReg = NULL;
            objReg = NULL;                        
            break;
    }

    FullyAggregatedIndexScan *newFais = FullyAggregatedIndexScan::create(adb, fais->order, subReg, subBound, predReg, predBound, objReg, objBound, fais->getExpectedOutputCardinality());
    return wrap(newFais);
    
}          
DOperator * DOperatorTransformer::cloneMergeJoin(DOperator *source){
    MergeJoin *smj = static_cast<MergeJoin*>(source->op);
    return wrap(new MergeJoin(NULL, smj->leftValue, smj->leftTail, NULL, smj->rightValue, smj->rightTail, smj->getExpectedOutputCardinality()));
}        
DOperator * DOperatorTransformer::cloneHashJoin(DOperator *source){
    HashJoin *shj = static_cast<HashJoin*>(source->op);
    return wrap(new HashJoin(NULL, shj->leftValue, shj->leftTail, NULL, shj->rightValue, shj->rightTail, shj->hashPriority, shj->probePriority, shj->getExpectedOutputCardinality()));
}                

DOperator * DOperatorTransformer::cloneUnion(DOperator *source){
    Union *sun = static_cast<Union*>(source->op);
    vector<Operator*> parts;
    return wrap(new Union(parts, sun->mappings, sun->initializations, sun->getExpectedOutputCardinality()));
}

DOperator * DOperatorTransformer::cloneMergeUnion(DOperator *source){
    MergeUnion *smu = static_cast<MergeUnion*>(source->op);
    return wrap(new MergeUnion(smu->result, NULL, smu->leftReg, NULL, smu->rightReg, smu->getExpectedOutputCardinality()));
}                                

DOperator * DOperatorTransformer::cloneMultiMergeUnion(DOperator *source){
    MultiMergeUnion *smmu = static_cast<MultiMergeUnion*>(source->op);
    return wrap(new MultiMergeUnion(smmu->result, NULL, smmu->leftReg, NULL, smmu->rightReg, smmu->getExpectedOutputCardinality()));
}

DOperator * DOperatorTransformer::cloneHashGroupify(DOperator *source){
    HashGroupify *shg = static_cast<HashGroupify*>(source->op);
    return wrap(new HashGroupify(NULL, shg->values, shg->getExpectedOutputCardinality()));
}                                                

DOperator * DOperatorTransformer::cloneSort(DOperator *source){
    return NULL;
}               

DOperator* DOperatorTransformer::cloneSimpleGroupify(DOperator* source){
    SimpleGroupify *ssg = static_cast<SimpleGroupify*>(source->op);
    return wrap(new SimpleGroupify(ssg->input, ssg->values, ssg->getExpectedOutputCardinality()));
}

DOperator* DOperatorTransformer::cloneSelection(DOperator *source, DRuntime &runtime){
    Selection *sel = static_cast<Selection*>(source->op);
    Selection::Predicate *newPred = clonePredicate(sel->predicate);
    return wrap(new Selection(NULL, runtime.getRuntime(), newPred, sel->getExpectedOutputCardinality()));    
}

Selection::Predicate * DOperatorTransformer::clonePredicate(Selection::Predicate *source){
    string className(typeid(*source).name());
    
    if(className.find("NotEqual") != string::npos){
        Selection::NotEqual *neq = static_cast<Selection::NotEqual*>(source);
        Selection::Predicate *newLeft, *newRight;
        newLeft = clonePredicate(neq->left);
        newRight = clonePredicate(neq->right);
        Selection::NotEqual *newneq = new Selection::NotEqual(newLeft, newRight);
        return newneq;
    }else if(className.find("Equal") != string::npos){
        Selection::Equal *eq = static_cast<Selection::Equal*>(source);
        Selection::Predicate *newLeft, *newRight;
        newLeft = clonePredicate(eq->left);
        newRight = clonePredicate(eq->right);
        Selection::Equal *neweq = new Selection::Equal(newLeft, newRight);
        return neweq;       
    }else if(className.find("Variable") != string::npos){
        Selection::Variable *var = static_cast<Selection::Variable*>(source);
        Selection::Variable *newVar = new Selection::Variable(var->reg);
        return newVar;
    }else if(className.find("Not") != string::npos){
        Selection::Not *notq = static_cast<Selection::Not*>(source);
        Selection::Predicate *newInput = clonePredicate(notq->input);
        Selection::Not *newNot = new Selection::Not(newInput);
        return newNot;        
    }
    
    return NULL;    
}

void DOperatorTransformer::applyHostMappings(DOperator *input, map<unsigned, string> &mappings){
    map<unsigned, string>::iterator it = mappings.find(input->id);
    
    if(it != mappings.end()){
        input->home = it->second;
    }
    
    for(vector<DOperator*>::iterator chit = input->children.begin(); chit != input->children.end(); ++chit){
        applyHostMappings(*chit, mappings);
    }
    
}

void DOperatorTransformer::forceHomeHost(DOperator *input, string host){
    input->setHost(host);
    vector<DOperator*> &children = input->getChildren();
    for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit){
        this->forceHomeHost(*chit, host);
    }
}

