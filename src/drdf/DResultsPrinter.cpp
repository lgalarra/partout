/* 
 * File:   DResultsPrinter.cpp
 * Author: luis
 * Created on September 28, 2011, 11:42 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include <map>
#include <vector>
#include <iostream>
#include <sstream>
#include "drdf/DResultsPrinter.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/DDictionarySegment.hpp"

using namespace drdf;
using namespace std;

const unsigned DResultsPrinter::cacheLimit = 1289;

DResultsPrinter::DResultsPrinter(DRuntime& runtime, drdf::message::ExecResponse &execResp, DOperator *dop, double observedCard, unsigned limit, bool silent): Operator(dop->getOperator()->getExpectedOutputCardinality()), runtime(runtime), 
 dictionary(runtime.getDDatabase().getDictionary()), theOperator(dop), data(execResp), cursor(-1), nRegisters(execResp.registers_size()), observedCard(observedCard), limit(limit), silent(silent){
    hashCache.resize(cacheLimit);
    vector<Register*> &outputRegs = this->theOperator->getOutputRegisters();
    for(vector<Register*>::iterator rit = outputRegs.begin(); rit != outputRegs.end(); ++rit){
        this->outputRegisters.insert(this->runtime.registerOffset(*rit));
        this->outputRegistersUnsorted.push_back(this->runtime.registerOffset(*rit));
    }
}

DResultsPrinter::~DResultsPrinter() {}

//---------------------------------------------------------------------------
static string escapeURI(const char* iter,const char* limit)
{
   string s;
   for (;iter!=limit;++iter) {
      char c=(*iter);
      if ((c=='\\')||(c=='<')||(c=='>')||(c=='\"')||(c=='{')||(c=='}')||(c=='^')||(c=='|')||(c=='`')||((c&0xFF)<0x20))
         s+='\\';
      s+=c;
   }
   return s;
}

//---------------------------------------------------------------------------
static string escape(const char* iter,const char* limit)
{
   string s;
   for (;iter!=limit; ++iter) {
      char c = (*iter);
      switch (c) {
         case '\t': s+= "\\t"; break;
         case '\n': s+= "\\n"; break;
         case '\r': s+= "\\r"; break;
         case '\"': s+= "\\\""; break;
         case '\\': s+= "\\\\"; break;
         default: s+= c; break;
      }
   }
   return s;
}

string DResultsPrinter::output(uint64_t value){
     unsigned slot = value % cacheLimit; 
     if(hashCache[slot].id == value)
         return hashCache[slot].text;
     
     Type::ID type, type2;
     unsigned subtype, subType2;
     string result("??"), result2;
     const char *start, *stop, *start2, *stop2;
          
     if(dictionary.lookupById(value, result, type, subtype)){
        start = result.c_str();
        stop = start + result.size();
        switch (type) {
           case Type::URI: result = "<" + escapeURI(start, stop) + ">";  break;
           case Type::Literal: result = "\"" + escape(start,stop) + "\""; break;
           case Type::String:  result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#string>"; break;
           case Type::Integer: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#integer>"; break;
           case Type::Decimal: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#decimal>"; break;
           case Type::Double:  result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#double>"; break;
           case Type::Boolean: result = "\""+escape(start,stop)+"\"^^<http://www.w3.org/2001/XMLSchema#boolean>"; break;
           case Type::CustomLanguage: 
               result = "\""+escape(start,stop)+"\""; 
               //Lookup language
               if(dictionary.lookupById(subtype, result2, type2, subType2)){
                   start2 = result2.c_str();
                   stop2 = start2 + result2.size();                   
                   result += "@" + escape(start2, stop2);           
               }
               break;
           case Type::CustomType: 
               result2 = "\""+escape(start,stop)+"\""; 
               //Lookup type
               const char* start2, *stop2; Type::ID type2; unsigned subType2;           
               if(dictionary.lookupById(subtype, result2, type2, subType2)){
                   start2 = result2.c_str();
                   stop2 = start2 + result2.size();                                      
                   result += "^^<" + escape(start2, stop2) + ">";           
               }
               break;

        }
         
        hashCache[slot].text = result;
        hashCache[slot].id = value;
     }
     
     return result;
}

//---------------------------------------------------------------------------
unsigned DResultsPrinter::first()
   // Produce the first tuple
{

   observedOutputCardinality = observedCard;
   if(data.data_size() > 0){
       cursor = 0;
       return next();
   }
   
   return 0;

}

//---------------------------------------------------------------------------
unsigned DResultsPrinter::next()
   // Produce the next tuple
{
    if(cursor == data.data_size() || observedOutputCardinality >= limit) return 0;
    
    unsigned tupleCount= (theOperator->getDuplicateHandling() == ResultsPrinter::ShowDuplicates)? 2 : 1;    
    unsigned card = data.data(cursor + nRegisters);   
    
    if(theOperator->getDuplicateHandling() == ResultsPrinter::ExpandDuplicates)
        tupleCount = card;
    
    if(observedOutputCardinality + tupleCount > limit){
        tupleCount = limit - observedOutputCardinality;
    }
    
    map<uint64_t, uint64_t> row;
    for(int i = 0; i < nRegisters; ++i){
        uint64_t receivedReg = data.registers(i);
        //Search if it is in the projection
        if(this->outputRegisters.find(receivedReg) != this->outputRegisters.end()){
            row[receivedReg] = data.data(cursor + i);
        }
    }    

    observedOutputCardinality += tupleCount;    
    if(!silent){    
        for(unsigned i = 0; i < tupleCount; ++i){
            for(vector<uint64_t>::iterator unroit = this->outputRegistersUnsorted.begin(); unroit != this->outputRegistersUnsorted.end(); ++unroit){        
                if(row.find(*unroit) != row.end())
                    cout << output(row[*unroit]) << " ";
                else
                    cout << "?? ";            
            }
            cout << endl;        
        }
    }
        
    cursor += nRegisters + 1;
    
    return tupleCount;
    
}

