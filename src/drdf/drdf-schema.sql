CREATE TABLE `Dictionary`(
    `id` INTEGER PRIMARY KEY,
    `type` INTEGER,
    `subtype` INTEGER,
    `value` TEXT
);

CREATE INDEX `literal_text_idx` ON `Dictionary`(`value`);

CREATE TABLE `Predicate`(
    `id` INTEGER PRIMARY KEY,
    `position` INTEGER,
    `operator` INTEGER
);

CREATE TABLE `Host`(
    `id` INTEGER PRIMARY KEY,
    `uri` TEXT,
    `filename` TEXT
);

CREATE INDEX `host_uri_idx` ON `Host`(`uri`);

CREATE TABLE `EntityHost`(
    `id_literal` INTEGER,
    `id_host` INTEGER,
    PRIMARY KEY(`id_literal`, `id_host`)
);

CREATE TABLE `PredicateHost`(
    `id_predicate` INTEGER, 
    `id_host` INTEGER,
    `flag` INTEGER,
    PRIMARY KEY(`id_predicate`, `id_host`, `flag`)
);

CREATE TABLE `Argument`(
    `id_predicate` INTEGER, 
    `index` INTEGER,
    `id_literal` INTEGER,
    `value` TEXT,
    PRIMARY KEY(`id_predicate`, `index`) 
);
-- AS = Agregated Segment 
CREATE TABLE `ASSubjectPredicate`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

CREATE TABLE `ASSubjectObject`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

CREATE TABLE `ASPredicateObject`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

CREATE TABLE `ASPredicateSubject`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

CREATE TABLE `ASObjectSubject`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

CREATE TABLE `ASObjectPredicate`(
    `value1` INTEGER,
    `value2` INTEGER,
    `card` INTEGER,
    PRIMARY KEY(`value1`, `value2`)
);

-- FAS stands for fully aggregated segment
CREATE TABLE `FASSubject`(
    `value` INTEGER PRIMARY KEY,
    `card` INTEGER
);

CREATE TABLE `FASPredicate`(
    `value` INTEGER PRIMARY KEY,
    `card` INTEGER
);

CREATE TABLE `FASObject`(
    `value` INTEGER PRIMARY KEY,
    `card` INTEGER
);

CREATE TABLE `Statistics`(
    `subject`INTEGER DEFAULT NULL,
    `predicate` INTEGER DEFAULT NULL,
    `object` INTEGER DEFAULT NULL,
    `s_s` INTEGER DEFAULT NULL,
    `s_p` INTEGER DEFAULT NULL,
    `s_o` INTEGER DEFAULT NULL,
    `p_s` INTEGER DEFAULT NULL,
    `p_p` INTEGER DEFAULT NULL,
    `p_o` INTEGER DEFAULT NULL,
    `o_s` INTEGER DEFAULT NULL,
    `o_p` INTEGER DEFAULT NULL,
    `o_o` INTEGER DEFAULT NULL,
    PRIMARY KEY(`subject`, `predicate`, `object`)
);

CREATE TABLE `Metadata`(
    `id` INTEGER PRIMARY KEY,
    `value` TEXT
)