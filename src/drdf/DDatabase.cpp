/* 
 * File:   DDatabase.cpp
 * Author: lgalarra
 * 
 * Created on August 22, 2011, 4:01 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include "drdf/DDatabase.hpp"
#include <iostream>
#include <sstream>

using namespace drdf;
using namespace std;


DDatabase::~DDatabase() {
    delete this->fragmentationSegment;
    delete this->dataWrapper;    
    while(!this->segments.empty()){
        DSegment *seg = this->segments.begin()->second;
        this->segments.erase(this->segments.begin());
        delete seg;
    }        
    delete this->statistics;
    delete this->dictionary;
}

bool DDatabase::close(){
    return sqlite3_close(this->db) == SQLITE_OK;
}

bool DDatabase::createMetadataSegment(){
    string sql("");
    sql += " CREATE TABLE `Metadata`(`id` INTEGER PRIMARY KEY, `value` TEXT)";
    return this->dataWrapper->execute(sql);

}

bool DDatabase::createDictionarySegment(){
    string sql;
    sql = "CREATE TABLE `Dictionary`(`id` INTEGER PRIMARY KEY, `type` INTEGER, `subtype` INTEGER, `value` TEXT);";
    sql += "CREATE INDEX `literal_text_idx` ON `Dictionary`(`value`);";
    return this->dataWrapper->execute(sql);
}

bool DDatabase::createFragmentsDefinition(){
    string sql;
    sql = "CREATE TABLE `Predicate`(`id` INTEGER PRIMARY KEY, `position` INTEGER, `operator` INTEGER);";
    sql += "CREATE TABLE `Host`(`id` INTEGER PRIMARY KEY, `uri` TEXT, `filename` TEXT);";
    sql += "CREATE TABLE `PredicateHost`(`id_predicate` INTEGER, `id_host` INTEGER, `flag` INTEGER, PRIMARY KEY(`id_predicate`, `id_host`, `flag`));";
    sql += "CREATE TABLE `Argument`( `id_predicate` INTEGER, `index` INTEGER, `id_literal` INTEGER, `value` TEXT, PRIMARY KEY(`id_predicate`, `index`));";
    sql += "CREATE TABLE `EntityHost`(`id_literal` INTEGER, `id_host` INTEGER, PRIMARY KEY(`id_literal`, `id_host`));";
    sql += "CREATE INDEX `host_uri_idx` ON `Host`(`uri`);";
    return this->dataWrapper->execute(sql);
}

bool DDatabase::createAggregatedSegments(){
    string sql = "CREATE TABLE `ASSubjectPredicate`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `ASSubjectObject`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `ASPredicateObject`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `ASPredicateSubject`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `ASObjectSubject`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `ASObjectPredicate`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, PRIMARY KEY(`value1`, `value2`));";
    sql += "CREATE TABLE `FASSubject`(`value` INTEGER PRIMARY KEY, `card` INTEGER);";
    sql += "CREATE TABLE `FASPredicate`(`value` INTEGER PRIMARY KEY, `card` INTEGER);";    
    sql += "CREATE TABLE `FASObject`(`value` INTEGER PRIMARY KEY, `card` INTEGER);";
    return this->dataWrapper->execute(sql);    
}

bool DDatabase::createAggregatedSegments2(){
    string sql = "CREATE TABLE `ASSubjectPredicateH`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, `id_host` INTEGER, PRIMARY KEY(`value1`, `value2`, `id_host`));";
    sql += "CREATE TABLE `ASSubjectObjectH`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, `id_host` INTEGER,  PRIMARY KEY(`value1`, `value2`, `id_host`));";
    sql += "CREATE TABLE `ASPredicateObjectH`(`value1` INTEGER, `value2` INTEGER, `card` INTEGER, `id_host` INTEGER, PRIMARY KEY(`value1`, `value2`, `id_host`));";
    sql += "CREATE TABLE `FASSubjectH`(`value` INTEGER, `card` INTEGER, `id_host` INTEGER, PRIMARY KEY(`value`, `id_host`));";
    sql += "CREATE TABLE `FASPredicateH`(`value` INTEGER, `card` INTEGER, `id_host` INTEGER, PRIMARY KEY(`value`, `id_host`));";    
    sql += "CREATE TABLE `FASObjectH`(`value` INTEGER, `card` INTEGER, `id_host` INTEGER, PRIMARY KEY(`value`, `id_host`));";
    sql += "CREATE VIEW `FASObject` AS SELECT value, SUM(card) as card FROM `FASObjectH` GROUP BY `value`;";
    sql += "CREATE VIEW `FASPredicate` AS SELECT value, SUM(card) as card FROM `FASPredicateH` GROUP BY `value`;";
    sql += "CREATE VIEW `FASSubject` AS SELECT value, SUM(card) as card FROM `FASObjectH` GROUP BY `value`;";    
    sql += "CREATE VIEW `ASSubjectPredicate` AS SELECT value1, value2, SUM(card) as card FROM `ASSubjectPredicateH` GROUP BY `value1`, `value2`;";  
    sql += "CREATE VIEW `ASSubjectObject` AS SELECT value1, value2, SUM(card) as card FROM `ASSubjectObjectH` GROUP BY `value1`, `value2`;";      
    sql += "CREATE VIEW `ASPredicateObject` AS SELECT value1, value2, SUM(card) as card FROM `ASPredicateObjectH` GROUP BY `value1`, `value2`;";          
    return this->dataWrapper->execute(sql);    
}


bool DDatabase::createStatisticalSegments(){
    string sql = "CREATE TABLE `Statistics`(`subject`INTEGER DEFAULT NULL, `predicate` INTEGER DEFAULT NULL, `object` INTEGER DEFAULT NULL, ";
    sql += "`s_s` INTEGER DEFAULT NULL, `s_p` INTEGER DEFAULT NULL, `s_o` INTEGER DEFAULT NULL, `p_s` INTEGER DEFAULT NULL, ";
    sql += "`p_p` INTEGER DEFAULT NULL, `p_o` INTEGER DEFAULT NULL, `o_s` INTEGER DEFAULT NULL, `o_p` INTEGER DEFAULT NULL, `o_o` INTEGER DEFAULT NULL, ";
    sql += "PRIMARY KEY(`subject`, `predicate`, `object`));";
        
    return this->dataWrapper->execute(sql);
}

bool DDatabase::create(const char* fileName){
    //Time to create the schema    
    if(!this->init(fileName)){
        return false;
    }
    
    if(!this->createMetadataSegment())
        return false;
    
    if(!this->createDictionarySegment())
        return false;
    
    if(!this->createFragmentsDefinition())
        return false;
    
    if(!this->createAggregatedSegments2())
        return false;
    
    if(!this->createStatisticalSegments())
        return false;
        
    return true;
}


void DDatabase::loadSegments(DictionarySegment *surrogateDict){
    //Load FAS segments
    unsigned _1KB = 1024 / sizeof(Pair);
    unsigned __1KB = 1024 / sizeof(Triple);
        
    this->segments[Tag_S] = DFullyAggregatedFactsSegment::create(*this->dataWrapper, Tag_S, _1KB);
    this->segments[Tag_P] = DFullyAggregatedFactsSegment::create(*this->dataWrapper, Tag_P, _1KB);
    this->segments[Tag_O] = DFullyAggregatedFactsSegment::create(*this->dataWrapper, Tag_O, _1KB);
    
    //Load AS segments
    this->segments[Tag_SP << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_SP, __1KB);
    this->segments[Tag_SO << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_SO, __1KB);
    this->segments[Tag_PS << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_PS, __1KB); 
    this->segments[Tag_PO << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_PO, __1KB);
    this->segments[Tag_OS << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_OS, __1KB);
    this->segments[Tag_OP << 2] = DAggregatedFactsSegment::create(*this->dataWrapper, Tag_OP, __1KB);
    
    //Create some dummies facts segments
    for(unsigned i = 0; i < 6; ++i)
        this->segments[(static_cast<unsigned>(Database::Order_Subject_Predicate_Object) + i + 1) << 4] = DFactsSegment::create(*dataWrapper, static_cast<Database::DataOrder>(Database::Order_Subject_Predicate_Object + i));
    
    
    this->dictionary = DDictionarySegment::create(*this->dataWrapper, surrogateDict);
    this->statistics = DExactStatisticsSegment::create(*this->dataWrapper);
}

DFactsSegment& DDatabase::getFacts(Database::DataOrder order){
    //Change this
    return *static_cast<DFactsSegment*>(this->segments[(order + 1) << 4]);
}

DAggregatedFactsSegment& DDatabase::getAggregatedFacts(Database::DataOrder order){
    return *static_cast<DAggregatedFactsSegment*>(this->segments[(order + 1) << 2]);
}

DFullyAggregatedFactsSegment& DDatabase::getFullyAggregatedFacts(Database::DataOrder order){
    return *static_cast<DFullyAggregatedFactsSegment*>(this->segments[(order >> 1) + 1]);
}

bool DDatabase::open(const char* fileName, bool readonly){
    if(this->init(fileName, readonly)){
        this->loadSegments();
        return true;
    }    
    return false;
}

bool DDatabase::open(const char* fileName, DictionarySegment *surrogateDict, bool readonly){
    if(surrogateDict == NULL){
        return this->open(fileName, readonly);
    }else{
        if(this->init(fileName, readonly)){
            this->loadSegments(surrogateDict);
            return true;
        }    
    }
    return false;
}

bool DDatabase::init(const char* fileName, bool readonly){
    int flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
    if(readonly)
        flags = SQLITE_OPEN_READONLY;
       
    int rc = sqlite3_open_v2(fileName, &this->db, flags, NULL);
    if(rc == SQLITE_OK){
        this->dataWrapper = new DDatabaseWrapper(this->db);
        this->fragmentationSegment = DFragmentDefinitionSegment::create(*this->dataWrapper);        
        return true;
    }
    
    return false;
}


