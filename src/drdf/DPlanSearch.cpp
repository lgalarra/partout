/* 
 * File:   DPlanSearch.cpp
 * Author: lgalarra
 * 
 * Created on September 26, 2011, 12:26 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */
#include <vector>
#include <iostream>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/mersenne_twister.hpp>

#include "drdf/DPlanSearch.hpp"
#include "drdf/CommonTypes.hpp"
#include "drdf/DCodeGen.hpp"
#include "drdf/DOperatorTreePool.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "drdf/DOperatorCostFunction.hpp"

using namespace drdf;
using namespace std;

static DOperator* applyTransformation(DOperatorTransformer &transformer, DOperator *dop, DOperatorTransform &step, DRuntime &runtime){
    DOperator *result;
    if(step.type == DOperatorTransform::AHH){
        if(step.ignoreMappings){
            assert(transformer.assignHomeHost(dop, step.host, true));
        }else{
            transformer.applyHostMappings(dop, step.hostMappings);
        }
        result = dop;
    }else if(step.type == DOperatorTransform::PL){
        DOperator *affected = dop->find(step.affectedOperator);
        assert(affected != NULL);
        DOperator *newSubtree = transformer.parallelize(affected, runtime);
        if(newSubtree->getParent() == NULL){
            result = newSubtree;
        }else{
            result = dop;
        }
        
        delete affected->getOperator();
        delete affected;
        
        result->normalizeIds();
    }
    
    step.hostMappings.clear();
    result->extractHostMappings(step.hostMappings);
    return result;
}

static DOperator* applyTransformations(DOperatorTransformer &transformer, DOperator *dop, TransformSchedule &schedule, DRuntime &runtime){
    DOperator *result = dop;
    for(TransformSchedule::iterator tit = schedule.begin(); tit != schedule.end(); ++tit){
        result = applyTransformation(transformer, result, *tit, runtime);
    }
    
    return result;
}

unsigned DPlanSearch::countJoins(DOperator* op){
    DOperator::Type type = op->getType();
    bool isJoin = (type == DOperator::TMergeJoin || type == DOperator::THashJoin || type == DOperator::TNestedLoopJoin);
    unsigned total = isJoin? 1 : 0;

    if(!op->isLeaf()){
        vector<DOperator*> &children = op->getChildren();
        for(vector<DOperator*>::iterator chit = children.begin(); chit != children.end(); ++chit)
            total += countJoins(*chit);
    }
    
    return total;
}

unsigned DPlanSearch::countHosts(){
    return this->runtime.getDDatabase().getFragmentsDefinitionSegment().countHosts();
}

void DPlanLocalSearch::buildSchedule(DOperator *op, TransformSchedule &schedule){
    DOperatorTransformer transformer(&this->runtime.getDDatabase());
    vector<Host> &hosts = op->getRelevantHosts();
    unsigned steps;
    //Parallelize the join leaves if possible
    vector<DOperator *> potentialPLs;
    transformer.getParallelizableJoins(op, potentialPLs);        
    
    if(hosts.size() <= 1) return;

    vector<Host>::iterator startHost;
    for(startHost = hosts.begin(); startHost != hosts.end(); ++startHost){
        if(startHost->uri.compare(op->getHost()) == 0)
            break;
    }
    
    hosts.erase(startHost);    
    steps = hosts.size();
    
    //There must be at most one
    if(potentialPLs.empty()){       
        schedule.resize(steps);
        for(unsigned i = 0; i < steps; ++i){
            schedule[i].type = DOperatorTransform::AHH;
            schedule[i].affectedOperator = 1; //Always the root               
            schedule[i].host = hosts[i].uri;
            schedule[i].ignoreMappings = true;
        }
    }else{
        schedule.resize(2 * steps + 2);
        for(unsigned i = 0; i < steps; ++i){
            schedule[i].type = DOperatorTransform::AHH;
            schedule[i].affectedOperator = 1;                
            schedule[i].host = hosts[i].uri;
            schedule[i].ignoreMappings = true;            
        }
        
        schedule[steps].type = DOperatorTransform::PL;
        schedule[steps].affectedOperator = potentialPLs[0]->getId();
        schedule[steps].ignoreMappings = true;        
        
        unsigned h = steps + 1;
        for(unsigned i = 0; i < steps ; ++i){
            schedule[h].type = DOperatorTransform::AHH;
            schedule[h].affectedOperator = 1; //Always the root               
            schedule[h].host = hosts[i].uri;
            schedule[h].ignoreMappings = true;            
            ++h;
        }
        
        schedule[h].type = DOperatorTransform::AHH;
        schedule[h].affectedOperator = 1;             
        schedule[h].host = op->getHost();
        schedule[h].ignoreMappings = true;        
    }
    

}        

void DPlanSASearch::buildSchedule(DOperator *op, TransformSchedule &schedule){
    //We will always locate the parallelization at the end
    DOperatorTransformer transformer(&this->runtime.getDDatabase());
    vector<Host> &hosts = op->getRelevantHosts();
    unsigned steps = hosts.size();
    //Find a random join to parallelize
    vector<DOperator *> potentialPLs;
    transformer.getParallelizableJoins(op, potentialPLs);    

    if(potentialPLs.empty()){
        schedule.resize(steps);
    }else{
        schedule.resize(steps + 1);
        schedule[steps].type = DOperatorTransform::PL;
        srand(time(NULL));
        unsigned picked = rand() % potentialPLs.size();
        schedule[steps].affectedOperator = potentialPLs[picked]->getId();       
    }
    
    for(unsigned i = 0; i < steps; ++i){
        schedule[i].type = DOperatorTransform::AHH;
        schedule[i].affectedOperator = 1; //Always the root               
        schedule[i].host = hosts[i].uri;
    }    

}

DOperator* DPlanLocalSearch::findGoodPlan(){
    DOperatorTreePool startUpPool(this->runtime, this->queryGraph, this->plan, 1);
    DOperatorTransformer transformer(&this->runtime.getDDatabase());
    DOperator *startOperator = startUpPool.getCleanDOperator();
    TransformSchedule appliedTransforms, schedule;
    
    cout << "Estimated cost (query response time in ms) for start operator: " <<  DOperatorCostFunction::evaluate(startOperator, this->runtime.getDDatabase()) << endl;  
    DOperator *current, *backup;
    current = startOperator;
    backup = transformer.clone(current, this->runtime);
    double lastCost = DOperatorCostFunction::evaluate(current, this->runtime.getDDatabase());
            
    this->buildSchedule(current, schedule);    
    for(unsigned j = 0; j < schedule.size(); ++j){
        current = applyTransformation(transformer, current, schedule[j], this->runtime);
        //The next time this transformation could be used, it will be applied to the backup operator
        schedule[j].ignoreMappings = false;            
        double newCost = DOperatorCostFunction::evaluate(current, this->runtime.getDDatabase());
        if(newCost < lastCost){
            //We move on
            appliedTransforms.push_back(schedule[j]);
            backup = applyTransformation(transformer, backup, schedule[j], this->runtime);
            lastCost = newCost;
        }else{
            //We reject this bad move
            delete current->getOperator();
            delete current;
            current = backup;
            backup = transformer.clone(current, this->runtime);
            //If the parallelization was not accepted, then do not bother trying AHH again
            if(schedule[j].type == DOperatorTransform::PL) break;
        }
    }
    
    delete backup->getOperator();
    delete backup;
    
    return current;

}

double DPlanSASearch::temperature(double oldCost, double newCost, unsigned iteration){
    assert(iteration > 0);
    return exp(oldCost - newCost) / (double)iteration;
}

DOperator* DPlanSASearch::findGoodPlan(){
    DOperatorTreePool startUpPool(this->runtime, this->queryGraph, this->plan, 1);
    DOperatorTransformer transformer(&this->runtime.getDDatabase());
    DOperator *startOperator = startUpPool.getCleanDOperator();
    unsigned rounds = startOperator->countLeaves();
    unsigned itersPerRound = startOperator->getRelevantHosts().size() + 1; 
    TransformSchedule appliedTransforms;
    
    DOperator *current, *backup;
    current = startOperator;
    backup = transformer.clone(current, this->runtime);
    double lastCost = DOperatorCostFunction::evaluate(current, this->runtime.getDDatabase());
            
    boost::mt19937 rng;
    boost::uniform_real<double> u(0, 1);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > gen(rng, u);
    
    for(unsigned i = 0; i < rounds; ++i){
        TransformSchedule schedule;
        this->buildSchedule(current, schedule);
        for(unsigned j = 0; j < itersPerRound; ++j){
            current = applyTransformation(transformer, current, schedule[j], this->runtime);
            //The next time this transformation could be used, it will be applied to the backup operator
            schedule[j].ignoreMappings = false;            
            double newCost = DOperatorCostFunction::evaluate(current, this->runtime.getDDatabase());
            if(newCost < lastCost || (lastCost >= newCost && temperature(lastCost, newCost, i * itersPerRound + j + 1) > gen())){
                //We move on
                appliedTransforms.push_back(schedule[j]);
                backup = applyTransformation(transformer, backup, schedule[j], this->runtime);
                lastCost = newCost;
            }else{
                //We reject this bad move
                delete current->getOperator();
                delete current;
                current = backup;
                backup = transformer.clone(current, this->runtime);
                backup = applyTransformations(transformer, backup, appliedTransforms, this->runtime);
            }
        }
    }
    
    delete backup->getOperator();
    delete backup;
    
    return current;
}

DOperator* DPlanTrivialSearch::findGoodPlan(){
    DOperatorTreePool pool(this->runtime, this->queryGraph, this->plan, 1);
    return pool.getCleanDOperator();
}
