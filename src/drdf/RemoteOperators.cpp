
/* 
 * File:   RemoteOperators.cpp
 * Author: lgalarra
 * 
 * Created on September 13, 2011, 12:44 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <cassert>
#include <iostream>
#include <vector>
#include <typeinfo>
#include <iosfwd>

#include "drdf/RemoteOperators.hpp"
#include "rts/operator/PlanPrinter.hpp"

#include "drdf/DOperator.hpp"
#include "rts/runtime/Runtime.hpp"

using namespace drdf;
using namespace drdf::message;
using namespace std;


unsigned long getRegisterIndex(Runtime *runtime, Register *reg){
    Register *start = runtime->getRegister(0);
    //The register cannot be outside the bounds of the runtime
    assert(reg >= start && reg <= runtime->getRegister(runtime->getRegisterCount() - 1));
    uint64_t offset = reinterpret_cast<unsigned long>((void*)(reg - start));
    return offset;
}

void RemoteSend::registersOffsets(vector<unsigned long> &offsets){
    for(vector<Register*>::iterator rit = this->registers.begin(); rit != this->registers.end(); ++rit){
        offsets.push_back(getRegisterIndex(this->runtime, *rit));
    }
}

void RemoteSend::print(PlanPrinter& out){
   const char* className = typeid(*this).name();
   ostringstream sstr;   
   out.beginOperator(className, this->expectedOutputCardinality, this->observedOutputCardinality);
   for(vector<Register*>::iterator it = this->registers.begin(); it != this->registers.end(); ++it){
       sstr << out.formatRegister(*it) << " ";
   }   
   out.addArgumentAnnotation(string("Registers: ") + sstr.str());
   out.addArgumentAnnotation(string("From: ") + this->homeHost);   
   out.addArgumentAnnotation(string("To: ") + this->destinationHost);
   this->input->print(out);
   out.endOperator();
}


unsigned RemoteSend::first(){
    started = true;
    this->observedOutputCardinality = 0;
    unsigned count = this->input->first();
    this->observedOutputCardinality += count;
    return count;
}
    
unsigned RemoteSend::next(){
    unsigned count = this->input->next();
    this->observedOutputCardinality += count;
    return count;
}
    
void RemoteSend::addMergeHint(Register* reg1, Register* reg2){
    this->input->addMergeHint(reg1, reg2);
}
    
void RemoteSend::getAsyncInputCandidates(Scheduler& scheduler){
    this->input->getAsyncInputCandidates(scheduler);
}


void IndexScanRemoteSend::values(vector<unsigned>& values){
    for(vector<Register*>::iterator rit = this->registers.begin(); rit != this->registers.end(); ++rit){
        values.push_back((*rit)->value);
    }
}


void EfficientJoinRemoteSend::values(vector<unsigned>& values){
    //They are actually equal
    values.push_back(left->value);
    values.push_back(right->value);

    for(vector<Register*>::iterator rit = this->leftTail.begin(); rit != this->leftTail.end(); ++rit){
        values.push_back( (*rit)->value);        
    }

    for(vector<Register*>::iterator rit = this->rightTail.begin(); rit != this->rightTail.end(); ++rit){
        values.push_back( (*rit)->value);        
    }    
}

void EfficientJoinRemoteSend::registersOffsets(vector<unsigned long> &offsets){
    //Left and right registers
    offsets.push_back(getRegisterIndex(this->runtime, this->left));
    offsets.push_back(getRegisterIndex(this->runtime, this->right));
    
    //Left tail
    for(vector<Register*>::iterator rit = this->leftTail.begin(); rit != this->leftTail.end(); ++rit){
        offsets.push_back(getRegisterIndex(this->runtime, *rit));
    }
    
    //Right tail
    for(vector<Register*>::iterator rit = this->rightTail.begin(); rit != this->rightTail.end(); ++rit){
        offsets.push_back(getRegisterIndex(this->runtime, *rit));
    }
}

void DefaultRemoteSend::values(vector<unsigned> &values){
    for(vector<Register*>::iterator rit = this->registers.begin(); rit != this->registers.end(); ++rit){
        values.push_back((*rit)->value);
    }
}
         

int64_t RemoteFetch::prepareRequest(uint64_t &opid, string &errorMsg){    
    PrepExecutionResponse response;
    bool result = this->client->prepareForExecution(this->sourceHost, this->dop, *this->runtime, response);
    if(result){
        opid = response.opid;
        return response.reqid;
    }else{
        errorMsg = response.errorMsg;
        return -1;
    }
}

int64_t RemoteFetch::prepareRequest(string &errorMsg){
    int64_t tmpReqid;
    if((tmpReqid = this->prepareRequest(this->opid, errorMsg)) > 0){
        this->reqid = static_cast<uint64_t>(tmpReqid);
    }

    return tmpReqid;
}

unsigned RemoteFetch::first(){
    ExecResponse response;
    //Execute the request
    this->observedOutputCardinality = 0;
    this->client->exec(this->sourceHost, this->reqid, this->opid, response);
    if(response.status() == ExecResponse::OK){
        this->dataSource = response.data();
        this->registersSource = response.registers();
        this->morePages = response.has_hasmoreresults() && response.hasmoreresults();
        return next();
    }else if(response.status() == ExecResponse::FORGOTTEN){
        this->morePages = false;
		return 0;
    }else{
        throw RemoteException(response.errormsg());
	}
}

unsigned RemoteFetch::next(){
    int startCursor = this->registerCursor + 1;
    this->registerCursor += this->registersSource.size() + 1;
    if(this->registerCursor >= this->dataSource.size()){
        if(this->morePages) 
            return this->nextPage();
    }else{
        this->observedOutputCardinality += this->dataSource.Get(this->registerCursor);
        //Fill the registers with the appropiate data
        for(int i = 0; i < this->registersSource.size(); ++i){
            Register *currentReg = this->runtime->getRegister(this->registersSource.Get(i));
            currentReg->value = this->dataSource.Get(startCursor + i);
        }
        
        return this->dataSource.Get(this->registerCursor);
    }
    
    return 0;
}

void RemoteFetch::fetchInBg(){
    boost::unique_lock<boost::mutex> lock(this->backDataSourceMutex);
    ExecResponse response;
    this->client->exec(this->sourceHost, this->reqid, this->opid, response);
    this->backDataSource = response.data();
    this->backHasMoreResults = response.has_hasmoreresults() && response.hasmoreresults();
    this->readFromBackup = true;	
    lock.unlock();
}

unsigned RemoteFetch::nextPage(){
//    boost::unique_lock<boost::mutex> lock(this->backDataSourceMutex);
    if(this->readFromBackup){
        this->dataSource = this->backDataSource;
        this->morePages = this->backHasMoreResults;
        this->readFromBackup = false;
    }else{
        ExecResponse response;
        this->client->exec(this->sourceHost, this->reqid, this->opid, response);
        this->dataSource = response.data();
        this->morePages = response.has_hasmoreresults() && response.hasmoreresults();
    }
    this->registerCursor = -1;    
/*   lock.unlock();
   if(this->morePages)
        boost::thread::thread fetchInBg(&RemoteFetch::fetchInBg, this);	*/

    return this->next();
}

void RemoteFetch::registers(map<unsigned, Register*> &row){
    //Fill the registers with the appropiate data
    for(int i = 0; i < this->registersSource.size(); ++i){
        Register *currentReg = this->runtime->getRegister(this->registersSource.Get(i));
        row[this->registersSource.Get(i)] = currentReg;
    }
}

    
void RemoteFetch::addMergeHint(Register* reg1,Register* reg2){
    this->input->addMergeHint(reg1, reg2);
}
    
void RemoteFetch::getAsyncInputCandidates(Scheduler& scheduler){
    this->input->getAsyncInputCandidates(scheduler);
}

void RemoteFetch::print(PlanPrinter& out){
    out.beginOperator("RemoteFetch", this->expectedOutputCardinality, this->observedOutputCardinality);
    out.addArgumentAnnotation(string("From: ") + this->sourceHost);   
    out.addArgumentAnnotation(string("To: ") + this->homeHost); 
	this->input->print(out);  
    out.endOperator();
}    
