/* 
 * File:   FragmentsDefinitionSegment.cpp
 * Author: lgalarra
 * 
 * Created on August 22, 2011, 2:54 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <cassert>
#include <sstream>
#include <typeinfo>
#include <iostream>
#include "rts/runtime/Runtime.hpp"
#include "drdf/FragmentsDefinitionSegment.hpp"
#include "drdf/DOperator.hpp"
#include "rts/operator/AggregatedIndexScan.hpp"
#include "rts/operator/EmptyScan.hpp"
#include "rts/operator/Filter.hpp"
#include "rts/operator/FullyAggregatedIndexScan.hpp"
#include "rts/operator/HashGroupify.hpp"
#include "rts/operator/HashJoin.hpp"
#include "rts/operator/IndexScan.hpp"
#include "rts/operator/MergeJoin.hpp"
#include "rts/operator/MergeUnion.hpp"
#include "rts/operator/NestedLoopFilter.hpp"
#include "rts/operator/NestedLoopJoin.hpp"
#include "rts/operator/ResultsPrinter.hpp"
#include "rts/operator/Selection.hpp"
#include "rts/operator/SingletonScan.hpp"
#include "rts/operator/Sort.hpp"
#include "rts/operator/TableFunction.hpp"
#include "rts/operator/Union.hpp"
#include "drdf/constants.hpp"

using namespace drdf;

DFragmentDefinitionSegment::DFragmentDefinitionSegment(DDatabaseWrapper &dataWrapper): DSegment(dataWrapper), definitionsLoaded(false){}

DFragmentDefinitionSegment::~DFragmentDefinitionSegment(){}

void DFragmentDefinitionSegment::loadHosts(){
    ResultSet hostsResults;
    char *hostsSql = "SELECT * FROM `Host` ORDER BY `id` ASC;";        
    this->dwrapper.execute(hostsSql, hostsResults);    
    while(hostsResults.next()){
        Host newHost;
        int hid;
        hostsResults.getInt("id", hid);
        newHost.id = (unsigned)hid;
        hostsResults.getString("uri", newHost.uri);
        hostsResults.getString("filename", newHost.filename);
        this->hosts[newHost.id] = newHost;
    }
    
}

void DFragmentDefinitionSegment::loadHostMappings(){
    ResultSet mappingsResult;
    char *mappingsSql = "SELECT DISTINCT ph.id_host as host, ph.flag as flag, pre.id as preid, pre.position as preposition, \
    pre.operator as preoperator, arg.id_literal as literal, arg.value as value FROM `PredicateHost` ph JOIN `Predicate` pre \
    LEFT JOIN `Argument` arg ON pre.id = arg.id_predicate WHERE ph.id_predicate = pre.id ORDER BY ph.id_host ASC, pre.id ASC, ph.flag ASC, `arg`.`index` ASC;";
    
    assert(this->dwrapper.execute(mappingsSql, mappingsResult));
    int lastPred = -1;
    int lastHost = -1;
    int lastflag = -1;
    while(mappingsResult.next()){
        int host, pred, flag;
                
        mappingsResult.getInt("host", host);
        mappingsResult.getInt("preid", pred);
        mappingsResult.getInt("flag", flag);
        if(host != lastHost){
            this->mappings[host] = vector<SPARQLPredicate>();
        }        
        
        if(pred != lastPred || (pred == lastPred && lastflag != flag)){
            SPARQLPredicate predSt;
            int flag;
            predSt.id = pred;
            mappingsResult.getInt("preposition", predSt.position);
            mappingsResult.getInt("preoperator", predSt.operator_);
            mappingsResult.getInt("flag", flag);
            predSt.flag = (bool)flag;
            this->mappings[host].push_back(predSt);
        }
        
        //Now time to read the arguments
        SPARQLArgument arg;        
        mappingsResult.getInt("literal", arg.id_literal);
        mappingsResult.getString("value", arg.value);
        arg.id_pred = pred;        
        SPARQLPredicate &lastAddPred = this->mappings[host].back();
        lastAddPred.args.push_back(arg);
        
        lastPred = pred;
        lastHost = host;
        lastflag = flag;
    }      
    

    
}

void DFragmentDefinitionSegment::loadPredicates(){
    ResultSet predsResult;
    char *mappingsSql = "SELECT DISTINCT pre.id as preid, pre.position as preposition, \
    pre.operator as preoperator, arg.id_literal as literal, arg.value as value FROM `Predicate` pre LEFT JOIN `Argument` arg \
    ON pre.id = arg.id_predicate ORDER BY pre.id ASC, `arg`.`index` ASC;";
    
    assert(this->dwrapper.execute(mappingsSql, predsResult));
    int lastPred = -1;
    while(predsResult.next()){
        int pred;
                
        predsResult.getInt("preid", pred);            
        
        if(pred != lastPred){
            SPARQLPredicate predSt;
            predSt.id = pred;
            predsResult.getInt("preposition", predSt.position);
            predsResult.getInt("preoperator", predSt.operator_);
            predSt.flag = true;
            this->predicates[predSt.id] = predSt;
        }
        
        //Now time to read the arguments
        SPARQLArgument arg;        
        predsResult.getInt("literal", arg.id_literal);
        predsResult.getString("value", arg.value);
        arg.id_pred = pred;        
        SPARQLPredicate &lastAddPred = this->predicates[pred];
        lastAddPred.args.push_back(arg);
        
        lastPred = pred;
    }      

}

void DFragmentDefinitionSegment::loadPredicateMappings(){
    //Assuming predicates and hosts are already loaded
    for(map<unsigned, Fragment>::iterator mapit = this->mappings.begin(); mapit != this->mappings.end(); ++mapit){
        for(Fragment::iterator fit = mapit->second.begin(); fit != mapit->second.end(); ++fit){
            this->predicateMappings[fit->id].push_back(this->hosts[mapit->first]);
        }
    }
}

void DFragmentDefinitionSegment::loadPredicateDefinitions(){
    this->loadPredicates();
    this->loadHosts();
    this->loadHostMappings();
    this->loadPredicateMappings();
    this->definitionsLoaded = true;
}

unsigned DFragmentDefinitionSegment::countHosts(){
    if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }
    
    return this->hosts.size();
}

void DFragmentDefinitionSegment::getAllHosts(set<Host, cmpHosts> &output){
     if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }
    
    for(map<unsigned, Host>::iterator it = this->hosts.begin(); it != this->hosts.end(); ++it)
        output.insert(it->second);
}

void DFragmentDefinitionSegment::getAllHosts(vector<Host> &output){
    if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }
    
    for(map<unsigned, Host>::iterator it = this->hosts.begin(); it != this->hosts.end(); ++it)
        output.push_back(it->second);    
}

bool DFragmentDefinitionSegment::getHost(unsigned id, Host &host){
    if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }
    
    map<unsigned, Host>::iterator mit = this->hosts.find(id);
    if(mit != this->hosts.end()){
        host = mit->second;        
        return true;
    }
    
    return false;
}

bool DFragmentDefinitionSegment::getHost(string uri, Host &host){
    ResultSet predsResult;
    string sql = "SELECT * FROM Host WHERE uri = '" + uri + "';";    
    if(!this->dwrapper.execute(sql, predsResult))
        return false;
    
     if(predsResult.next()){
         int hid;
         predsResult.getInt("id", hid);
         host.id = (unsigned)hid;
         host.uri = uri;
         predsResult.getString("filename", host.filename);
     }else{
         return false;
     }
    
    return true;
    
}

bool DFragmentDefinitionSegment::getHostMapping(unsigned id, Fragment &fragment){
    if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }
    
    map<unsigned, Fragment>::iterator mit = this->mappings.find(id);
    if(mit != this->mappings.end()){
        fragment = mit->second;
        return true;
    }    
    
    return false;
}

bool DFragmentDefinitionSegment::getPredicate(unsigned id, SPARQLPredicate& predicate){
    if(!this->definitionsLoaded){
        this->loadPredicateDefinitions();
    }

    map<unsigned, SPARQLPredicate>::iterator fit = this->predicates.find(id);
    if(fit != this->predicates.end()){
        predicate = fit->second;
        return true;
    }
    
    return false;
}

void DFragmentDefinitionSegment::setRelevantHostsAggregatedIndexScan(DOperator *dop){
#if FORCE_ALL_HOSTS == 0   
    AggregatedIndexScan *op = static_cast<AggregatedIndexScan*>(dop->op);
    ostringstream predicateSql;
    Position position1, position2;
    unsigned value1, value2;
    vector<unsigned> relevantPreds;
    
    //First extract the predicates relevant to these values
    switch(op->order){
        case Database::Order_Subject_Object_Predicate: 
            position1 = Subject;
            position2 = Object;
            break;
        case Database::Order_Subject_Predicate_Object:
            position1 = Subject;
            position2 = Predicate;
            break;
        case Database::Order_Predicate_Object_Subject:
            position1 = Predicate;
            position2 = Object;
            break;
        case Database::Order_Predicate_Subject_Object:
            position1 = Predicate;
            position2 = Subject;
            break;
        case Database::Order_Object_Predicate_Subject:
            position1 = Object;
            position2 = Predicate;
            break;
        case Database::Order_Object_Subject_Predicate:
            position1 = Object;
            position2 = Subject;
            break;
    }
    
    //I wonder if this is cheaper with a std::map
    //The only operation where we might touch disk
    if(op->bound1 && op->bound2){
        value1 = op->value1->value;
        value2 = op->value2->value;
        predicateSql << "SELECT DISTINCT p1.id as id FROM Predicate p1 JOIN Argument a1 JOIN Predicate p2 JOIN Argument a2";
        predicateSql << " WHERE p1.operator = " << Equal << " AND p1.id = a1.id_predicate AND p1.position = " << position1 << " AND a1.id_literal = " << value1;
        predicateSql << " AND p2.operator = " << Equal << " AND p2.id = a2.id_predicate AND p2.position = " << position2 << " AND a2.id_literal = " << value2 << ";";
    }else if(op->bound1){
        value1 = op->value1->value;
        value2 = ~0u;        
        predicateSql << "SELECT p.id as id FROM Predicate p JOIN Argument a WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position1 << " AND a.id_literal = " << value1 << ";";
    }else{
        value1 = ~0u;
        value2 = op->value2->value;
        predicateSql << "SELECT p.id as id FROM Predicate p JOIN Argument a WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position2 << " AND a.id_literal = " << value2 << ";";
    }
    
    ResultSet predRS;
    this->dwrapper.execute(predicateSql.str(), predRS);
    while(predRS.next()){
        int predId;
        predRS.getInt("id", predId);
        relevantPreds.push_back((unsigned)predId);
    }
    
    //If we have not heard about this predicate then all hosts are relevant :(
    if(relevantPreds.empty()){
        this->getAllHosts(dop->relevantHosts);
        return;
    }
    
    //Now check the hosts where those predicates are incident
    for(vector<unsigned>::iterator prit = relevantPreds.begin(); prit != relevantPreds.end(); ++prit){        
        SPARQLPredicate pred;
        if(this->getPredicate(*prit, pred)){
            pred.setDataWrapper(&this->dwrapper);
            vector<Host> allHosts;
            this->getAllHosts(allHosts);            
            for(vector<Host>::iterator hit = allHosts.begin(); hit != allHosts.end(); ++hit){
                Fragment definition;                
                this->getHostMapping(hit->id, definition);
                bool relevant = false;
                for(Fragment::iterator fit = definition.begin(); fit != definition.end(); ++fit){
                    if(pred.id == fit->id && pred.flag == fit->flag){
                        relevant = true;
                        break;
                    }
                }                
                //If so, we have found a fragment that might be relevant.. yupi!!!
                if(relevant){
                    dop->relevantHosts.push_back(*hit);
                }
            }
        }
    }
    
    //If it contradicts all fragments definitions, we could check in every host (This could mean the query is empty but in the presence of updates it is uncertain)
    if(dop->relevantHosts.empty()){
        this->getAllHosts(dop->relevantHosts);
    }
   
    //If it contradicts all fragments definitions, we could check in every host (This could mean the query is empty but in the presence of updates it is uncertain)
#else
    this->getAllHosts(dop->relevantHosts);
#endif
}

void DFragmentDefinitionSegment::setRelevantHostsEmptyScan(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsFilter(DOperator *op){}        
void DFragmentDefinitionSegment::setRelevantHostsFullyAggregatedIndexScan(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsHashGroupify(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsHashJoin(DOperator *op){}        

void DFragmentDefinitionSegment::setRelevantHostsIndexScan(DOperator *dop){
#if FORCE_ALL_HOSTS == 0   
    IndexScan *op = static_cast<IndexScan*>(dop->op);
    ostringstream predicateSql;
    Position position1, position2, position3;
    unsigned value1, value2, value3;
    vector<unsigned> relevantPreds;
    
    //First extract the predicates relevant to these values
    switch(op->order){
        case Database::Order_Subject_Object_Predicate: 
            position1 = Subject;
            position2 = Object;
            position3 = Predicate;
            break;
        case Database::Order_Subject_Predicate_Object:
            position1 = Subject;
            position2 = Predicate;
            position3 = Object;
            break;
        case Database::Order_Predicate_Object_Subject:
            position1 = Predicate;
            position2 = Object;
            position3 = Subject;
            break;
        case Database::Order_Predicate_Subject_Object:
            position1 = Predicate;
            position2 = Subject;
            position3 = Object;
            break;
        case Database::Order_Object_Predicate_Subject:
            position1 = Object;
            position2 = Predicate;
            position3 = Subject;
            break;
        case Database::Order_Object_Subject_Predicate:
            position1 = Object;
            position2 = Subject;
            position3 = Predicate;
            break;
    }
    
    //I wonder if this is cheaper with a std::map
    //The only operation where we might touch disk
    if(op->bound1 && op->bound2 && op->bound3){
        value1 = op->value1->value;
        value2 = op->value2->value;
        value3 = op->value3->value;
        predicateSql << "SELECT DISTINCT p.id as id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position1 << " AND a.id_literal = " << value1;
        predicateSql << " UNION SELECT p.id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position2 << " AND a.id_literal = " << value2 << "";
        predicateSql << " UNION SELECT p.id FROM Predicate JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position3 << " AND a.id_literal = " << value3 << ";";        
    }else if(op->bound1 && op->bound2){
        value1 = op->value1->value;
        value2 = op->value2->value;
        value3 = ~0u;
        predicateSql << "SELECT DISTINCT p.id as id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position1 << " AND a.id_literal = " << value1;
        predicateSql << " UNION SELECT p.id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position2 << " AND a.id_literal = " << value2 << ";";
    }else if(op->bound1 && op->bound3){
        value1 = op->value1->value;
        value2 = ~0u;
        value3 = op->value3->value;
        predicateSql << "SELECT DISTINCT p.id as id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position1 << " AND a.id_literal = " << value1;
        predicateSql << " UNION SELECT p.id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position3 << " AND a.id_literal = " << value3 << ";";
    }else if(op->bound2 && op->bound3){
        value1 = ~0u;
        value2 = op->value2->value;
        value3 = op->value3->value;
        predicateSql << "SELECT DISTINCT p.id as id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position2 << " AND a.id_literal = " << value2;
        predicateSql << " UNION SELECT p.id FROM Predicate p JOIN Argument a ";
        predicateSql << " WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position3 << " AND a.id_literal = " << value3 << ";";
    }else if(op->bound1){
        value1 = op->value1->value;
        value2 = value3 = ~0u;
        predicateSql << "SELECT p.id as id FROM Predicate p JOIN Argument a WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position1 << " AND a.id_literal = " << value1 << ";";        
    }else if(op->bound2){
        value2 = op->value2->value;
        value1 = value3 = ~0u;
        predicateSql << "SELECT p.id as id FROM Predicate p JOIN Argument a WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position2 << " AND a.id_literal = " << value2 << ";";        
    }else{
        //op->bound3 must be true
        value3 = op->value3->value;
        value1 = value2 = ~0u;
        predicateSql << "SELECT p.id as id FROM Predicate p JOIN Argument a WHERE p.operator = " << Equal << " AND p.id = a.id_predicate AND p.position = " << position3 << " AND a.id_literal = " << value3 << ";";
        
    }
    
    ResultSet predRS;
    this->dwrapper.execute(predicateSql.str(), predRS);
    while(predRS.next()){
        int predId;
        predRS.getInt("id", predId);
        relevantPreds.push_back((unsigned)predId);
    }
    
    //If we have not heard about this predicate then all hosts are relevant :(
    if(relevantPreds.empty()){
        this->getAllHosts(dop->relevantHosts);
        return;
    }
    
    vector<Host> allHosts;
    this->getAllHosts(allHosts);            
    for(vector<Host>::iterator hit = allHosts.begin(); hit != allHosts.end(); ++hit){
        Fragment definition;                
        this->getHostMapping(hit->id, definition);        
        bool probablyRelevant = true;
        for(vector<unsigned>::iterator prit = relevantPreds.begin(); prit != relevantPreds.end(); ++prit){                
            SPARQLPredicate pred;
            if(this->getPredicate(*prit, pred)){
                pred.setDataWrapper(&this->dwrapper);
                bool definitelyIrrelevant = true;
                for(Fragment::iterator fit = definition.begin(); fit != definition.end(); ++fit){
                    if(pred.id == fit->id && pred.flag == fit->flag){
                        definitelyIrrelevant = false;
                        break;
                    }//else if(pred.id == fit->id && pred.flag != fit->flag){
                       // definitelyIrrelevant = true;
                    //}
                }
                
                if(definitelyIrrelevant){
                    probablyRelevant = false;
                    break;
                }
            }                        
        }
        
        if(probablyRelevant) dop->relevantHosts.push_back(*hit);
    }
        
    //If it contradicts all fragments definitions, we could check in every host (This could mean the query is empty but in the presence of updates it is uncertain)
    if(dop->relevantHosts.empty()){
        this->getAllHosts(dop->relevantHosts);
    }
#else
    this->getAllHosts(dop->relevantHosts);
#endif


}
void DFragmentDefinitionSegment::setRelevantHostsMergeJoin(DOperator *op){}        
void DFragmentDefinitionSegment::setRelevantHostsMergeUnion(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsNestedLoopFilter(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsNestedLoopJoin(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsResultsPrinter(DOperator *op){}        
void DFragmentDefinitionSegment::setRelevantHostsSelection(DOperator *op){}   
void DFragmentDefinitionSegment::setRelevantHostsSingletonScan(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsSort(DOperator *op){}
void DFragmentDefinitionSegment::setRelevantHostsTableFunction(DOperator *op){}        
void DFragmentDefinitionSegment::setRelevantHostsUnion(DOperator *op){}     

void DFragmentDefinitionSegment::setRelevantHosts(DOperator *op){
    string className(typeid(*op->op).name());

    if(className.find("FullyAggregatedIndexScan") != string::npos)
        setRelevantHostsFullyAggregatedIndexScan(op);        
    else if(className.find("AggregatedIndexScan") != string::npos)
        setRelevantHostsAggregatedIndexScan(op);
    else if(typeid(*(op->op)) == typeid(EmptyScan))
        setRelevantHostsEmptyScan(op);
    else if(typeid(*(op->op)) == typeid(Filter))
        setRelevantHostsFilter(op);
    else if(typeid(*(op->op)) == typeid(HashGroupify))
        setRelevantHostsHashGroupify(op);
    else if(typeid(*(op->op)) == typeid(HashJoin))
        setRelevantHosts(op);
    else if(className.find("IndexScan") != string::npos)
        setRelevantHostsIndexScan(op);
    else if(typeid(*(op->op)) == typeid(MergeJoin))
        setRelevantHostsMergeJoin(op);    
    else if(typeid(*(op->op)) == typeid(MergeUnion))
        setRelevantHostsMergeUnion(op);
    else if(typeid(*(op->op)) == typeid(NestedLoopFilter))
        setRelevantHostsNestedLoopFilter(op);
    else if(typeid(*(op->op)) == typeid(NestedLoopJoin))
        setRelevantHostsNestedLoopJoin(op);
    else if(typeid(*(op->op)) == typeid(ResultsPrinter))
        setRelevantHostsResultsPrinter(op);    
    else if(typeid(*(op->op)) == typeid(Selection))
        setRelevantHostsSelection(op);    
    else if(typeid(*(op->op)) == typeid(SingletonScan))
        setRelevantHostsSingletonScan(op);    
    else if(typeid(*(op->op)) == typeid(Sort))
        setRelevantHosts(op);    
    else if(typeid(*(op->op)) == typeid(TableFunction))
        setRelevantHostsTableFunction(op);        
    else if(typeid(*(op->op)) == typeid(Union))
        setRelevantHostsUnion(op);
}
