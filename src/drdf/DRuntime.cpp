#include "drdf/DRuntime.hpp"
#include <cassert>

using namespace drdf;

const unsigned DRuntime::maxExtendedRegisters = 4096;

/// Access a specific register
Register* DRuntime::getRegister(unsigned slot){
    //Then it must be in one of the n
    if(slot >= runtime.getRegisterCount()){
        return &extendedRegisters[slot - runtime.getRegisterCount()];
    }else{
        return runtime.getRegister(slot);
    }

}

PotentialDomainDescription* DRuntime::getDomainDescription(unsigned slot){ 
    assert(slot < runtime.getRegisterCount());
    return runtime.getDomainDescription(slot);       
}

void DRuntime::requestExtraAllocation(unsigned n){
    //Reallocation is not allowed!
    unsigned extendedSize = extendedRegisters.size();
    if(extendedSize + n > maxExtendedRegisters)
        throw std::bad_alloc();
    else{
        extendedRegisters.resize(extendedRegisters.size() + n);
        for(unsigned i = registerCount; i < registerCount + n; ++i)
            extendedRegisters[i].reset();
        
        registerCount += n;
    }
}

/// Get the the index of a register
uint64_t DRuntime::registerOffset(Register *r){
    if(r >= runtime.getRegister(0) && r <= runtime.getRegister(runtime.getRegisterCount() - 1))
        return reinterpret_cast<uint64_t>(((void*)(r - runtime.getRegister(0))));
    else
        return runtime.getRegisterCount() + reinterpret_cast<uint64_t>(((void*)(r - &extendedRegisters[0])));

}

