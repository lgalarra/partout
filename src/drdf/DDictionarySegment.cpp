/* 
 * File:   DDictionarySegment.cpp
 * Author: lgalarra
 * 
 * Created on September 1, 2011, 4:34 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */
#include <iostream>
#include <sstream>

#include "drdf/DDictionarySegment.hpp"
#include "drdf/DRDF3xClient.hpp"
#include "drdf/DRuntime.hpp"

using namespace drdf;
using namespace std;

//Roughly 1 MB
const unsigned DDictionarySegment::stringsCacheSize = 1289;
const unsigned DDictionarySegment::idsCacheSize = 1009433;

DDictionarySegment::DDictionarySegment(DDatabaseWrapper &dataWrapper, DictionarySegment *surrogateDict): DSegment(dataWrapper), surrogateDict(surrogateDict){
    ostringstream sql; 
    ResultSet rs;
    map<string, Host> hosts;
    sql << "SELECT * FROM `Host`;";
    this->dwrapper.execute(sql.str(), rs);

    while(rs.next()){
        int hid;
        string huri, hfile;
        rs.getInt("id", hid);
        rs.getString("uri", huri);
        rs.getString("filename", hfile);
        hosts[huri] = Host((unsigned)hid, huri, hfile);
    }
    
    this->client = new DRDF3xClient<DRuntime>(hosts);    
    this->stringsCache.resize(stringsCacheSize);
    this->idsCache.resize(idsCacheSize);
    
}

DDictionarySegment::~DDictionarySegment() {
    delete this->client;
}

/// Lookup an id for a given string
bool DDictionarySegment::lookup(const std::string& text,::Type::ID type,unsigned subType,unsigned& id){
    unsigned slot=Hash::hash(text,(type<<24)^subType) % stringsCacheSize;
    
    if(this->stringsCache[slot].value.compare(text) == 0 && this->stringsCache[slot].type == type && this->stringsCache[slot].subtype == subType ){
        id = this->stringsCache[slot].id;
        cout << "Dictionary cache hit for text " << text << endl;
    }else{
        //Retrieve the entry from disk
        if(this->surrogateDict != NULL){
            return this->surrogateDict->lookup(text, type, subType, id);
        }
        
        ostringstream sql;
        ResultSet rs;
        int rid, rtype, rsubtype;
        DictionaryEntry de;
        
        sql << "SELECT * FROM `Dictionary` WHERE `value` = '" << text << "' AND `type` = " << type << " AND `subtype` = " << subType << ";";
        this->dwrapper.execute(sql.str(), rs);
        if(!rs.next()){
            return this->broadcastLookup(text, type, subType, id);
        }
        
        rs.getInt("id", rid);
        rs.getInt("type", rtype);
        rs.getInt("subtype", rsubtype);
        
        de.id = (unsigned)rid;
        de.subtype = subType;
        de.type = type;
        de.value = text;        
            
        this->stringsCache[slot] = de;        
        id = de.id;
    }
    
    return true;
}

bool DDictionarySegment::broadcastLookup(const std::string& text,::Type::ID type,unsigned subType,unsigned& id){
    map<string, DictionaryLookupByStringResponse::Status> statuses;
    this->client->broadcastSingleDictionaryLookupByString(text, type, subType, id, statuses);
    for(map<string, DictionaryLookupByStringResponse::Status>::iterator it = statuses.begin(); it != statuses.end(); ++it){
        if(it->second == DictionaryLookupByStringResponse::SUCCESS){                                  
            return true;
        }
    }
    
    return false;
}

bool DDictionarySegment::broadcastLookupById(unsigned id, string &result,::Type::ID& type,unsigned& subType){
    map<string, DictionaryLookupByIdResponse::Status> statuses;
    this->client->broadcastSingleDictionaryLookupById(id, type, subType, result, statuses);
    
    for(map<string, DictionaryLookupByIdResponse::Status>::iterator it = statuses.begin(); it != statuses.end(); ++it){
        if(it->second == DictionaryLookupByIdResponse::SUCCESS){
            return true;
        }
    }
    
    return false;
    
}


/// Lookup a string for a given id: Assume the output result has memory
bool DDictionarySegment::lookupById(unsigned id, string &result, ::Type::ID& type, unsigned& subType){
    unsigned slot = id % idsCacheSize;

    if(this->idsCache[slot].id == id){
        type = this->idsCache[slot].type;
        subType = this->idsCache[slot].subtype;
        result = this->idsCache[slot].value;
    }else{
        
        if(this->surrogateDict != NULL){
            const char *start, *stop;
            bool success = this->surrogateDict->lookupById(id, start, stop, type, subType);
            if(success){
                result = "";
                for(const char *c = start; c != stop; ++c){
                    result.append(*c, 1);
                }
            }
            
            return success;
        }
        
        //Retrieve the entry from disk
        ostringstream sql;
        ResultSet rs;
        int rtype, rsubtype;
        DictionaryEntry de;
        
        sql << "SELECT * FROM `Dictionary` WHERE `id` = '" << id << "';";
        this->dwrapper.execute(sql.str(), rs);
        if(!rs.next()){ 
            return this->broadcastLookupById(id, result, type, subType);
        }
        
        rs.getInt("type", rtype);
        rs.getInt("subtype", rsubtype);
        rs.getString("value", result);
        
        de.id = id;
        de.subtype = (unsigned)rsubtype;
        de.type = static_cast<Type::ID>(rtype);
        de.value = result;                
            
        this->idsCache[slot] = de;
        
        subType = de.subtype;
        type = de.type;        
    }
    
    return true;    
}
