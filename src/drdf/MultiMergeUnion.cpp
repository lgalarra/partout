/* 
 * File:   MultiMergeUnion.cpp
 * Author: luis
 * Created on October 10, 2011, 12:19 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "drdf/MultiMergeUnion.hpp"

MultiMergeUnion::MultiMergeUnion(vector<Register*> &result, Operator* left, vector<Register*> &leftReg, Operator* right,vector<Register*> &rightReg, double expectedOutputCardinality)
:Operator(expectedOutputCardinality), result(result), left(left), leftReg(leftReg), right(right), rightReg(rightReg){
    rightValue.resize(rightReg.size());
    leftValue.resize(leftReg.size());
}

MultiMergeUnion::~MultiMergeUnion() {}

bool MultiMergeUnion::lt(const Tuple &a, const Tuple &b) const{
    for(unsigned i = 0; i < a.size(); ++i){
        if(a[i] < b[i])
            return true;
        else if(a[i] > b[i])
            return false;
    }    
    //They are equal
    return false;
}

bool MultiMergeUnion::gt(const Tuple &a, const Tuple &b) const{
    for(unsigned i = 0; i < a.size(); ++i){
        if(a[i] > b[i])
            return true;
        else if(a[i] < b[i])
            return false;
    }        
    //They are equal
    return false;
}

void MultiMergeUnion::fetchFirstLeft(){
    leftCount = this->left->first();
}

void MultiMergeUnion::fetchFirstRight(){
    rightCount = this->right->first();
}


/// Produce the first tuple
unsigned MultiMergeUnion::first(){
   observedOutputCardinality=0;

   boost::thread::thread threadLeft(&MultiMergeUnion::fetchFirstLeft, this);
   boost::thread::thread threadRight(&MultiMergeUnion::fetchFirstRight, this);
	
   threadLeft.join();
   threadRight.join();	

   // Read the first tuple on the left side
   if (leftCount == 0) {
      if (rightCount == 0) {
         state=done;
         return false;
      }
      //Copy the values
      for(unsigned i = 0; i < rightReg.size(); ++i)
        result[i]->value = rightReg[i]->value;
      
      state=leftEmpty;
      observedOutputCardinality+=rightCount;
      return rightCount;
   }
   
   for(unsigned i = 0; i < leftReg.size(); ++i)
       leftValue[i] = leftReg[i]->value;

   // Read the first tuple on the right side
   if (rightCount == 0) {
       for(unsigned i = 0; i < leftReg.size(); ++i)
           result[i]->value = leftReg[i]->value;
       
      state=rightEmpty;
      observedOutputCardinality+=leftCount;
      return leftCount;
   }
   for(unsigned i = 0; i < rightReg.size(); ++i)
      rightValue[i] = rightReg[i]->value;

   // Handle the cases
   if (lt(leftValue, rightValue)) {
       for(unsigned i = 0; i < result.size(); ++i)
            result[i]->value = leftValue[i];
      state=stepLeft;
      observedOutputCardinality += leftCount;
      return leftCount;
   } else if (gt(leftValue, rightValue)) {
       for(unsigned i = 0; i < result.size(); ++i)
           result[i]->value = rightValue[i];
      state=stepRight;
      observedOutputCardinality+=rightCount;
      return rightCount;
   } else {
      for(unsigned i = 0; i < result.size(); ++i) 
           result[i]->value = leftValue[i];
      state=stepBoth;

      unsigned count=leftCount+rightCount;
      observedOutputCardinality+=count;
      return count;
   }
}
/// Produce the next tuple
unsigned MultiMergeUnion::next(){
   switch (state) {
      case done: return false;
      case stepLeft: case stepBoth:
         if ((leftCount=left->next())==0) {
            for(unsigned i = 0; i < result.size(); ++i)
                result[i]->value = rightValue[i];
            
            state=leftEmpty;
            observedOutputCardinality+=rightCount;
            return rightCount;
         }
         for(unsigned i = 0; i < leftReg.size(); ++i)
            leftValue[i] = leftReg[i]->value;
         // Fallthrough
      case stepRight:
         if (state!=stepLeft) {
            if ((rightCount=right->next())==0) {
               for(unsigned i = 0; i < result.size(); ++i)
                    result[i]->value = leftValue[i];
               state=rightEmpty;               
               observedOutputCardinality+=leftCount;
               return leftCount;
            }
            for(unsigned i = 0; i < rightReg.size(); ++i)
                rightValue[i] = rightReg[i]->value;
         }
         // Handle the cases
         if (lt(leftValue, rightValue)) {
            for(unsigned i = 0; i < result.size(); ++i)
                result[i]->value = leftValue[i];
            state=stepLeft;
            observedOutputCardinality+=leftCount;
            return leftCount;
         } else if (gt(leftValue, rightValue)) {
            for(unsigned i = 0; i < result.size(); ++i) 
                result[i]->value = rightValue[i];
            state=stepRight;
            observedOutputCardinality+=rightCount;
            return rightCount;
         } else {
            for(unsigned i = 0; i < result.size(); ++i) 
                result[i]->value = leftValue[i];
            state=stepBoth;

            unsigned count=leftCount+rightCount;
            observedOutputCardinality+=count;
            return count;
         }
      case leftEmpty:
         if ((rightCount=right->next())==0) {
            state=done;
            return false;
         }
         for(unsigned i = 0; i < rightReg.size(); ++i)
            result[i]->value = rightReg[i]->value;
         observedOutputCardinality+=rightCount;
         return rightCount;
      case rightEmpty:
         if ((leftCount=left->next())==0) {
            state=done;
            return false;
         }
         for(unsigned i = 0; i < leftReg.size(); ++i)
            result[i]->value=leftReg[i]->value;
         observedOutputCardinality+=leftCount;
         return leftCount;
   }
   return false;
    
}

/// Print the operator tree. Debugging only.
void MultiMergeUnion::print(PlanPrinter& out){
   out.beginOperator("MultiMergeUnion",expectedOutputCardinality,observedOutputCardinality);
   for(unsigned i = 0; i < result.size(); ++i)
       out.addEqualPredicateAnnotation(result[i], leftReg[i]);
   for(unsigned i = 0; i < result.size(); ++i)
       out.addEqualPredicateAnnotation(result[i], rightReg[i]);
   left->print(out);
   right->print(out);
   out.endOperator();
}

/// Add a merge join hint
void MultiMergeUnion::addMergeHint(Register* reg1,Register* reg2){
   if(left != NULL && right != NULL){
     left->addMergeHint(reg1,reg2);
     right->addMergeHint(reg1,reg2);
   }
}


void MultiMergeUnion::getAsyncInputCandidates(Scheduler& scheduler){
    if(left != NULL && right != NULL){
       left->getAsyncInputCandidates(scheduler);
       right->getAsyncInputCandidates(scheduler);
    }
}
