/* 
 * File:   SimpleGroupify.cpp
 * Author: luis
 * Created on October 9, 2011, 5:38 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include <vector>
#include <map>
#include <iostream>

#include "drdf/SimpleGroupify.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/PlanPrinter.hpp"

using namespace std;

SimpleGroupify::SimpleGroupify(Operator* input,const std::vector<Register*>& values,double expectedOutputCardinality):
Operator(expectedOutputCardinality), input(input), values(values){
}

SimpleGroupify::~SimpleGroupify() {
    delete input;
}

/// Produce the first tuple
unsigned SimpleGroupify::first(){
    if(this->input->first()){
        do{
            //Add the registers to the sorted data source (this breaks pipelining)
            Tuple newTuple;
            for(vector<Register*>::iterator it = values.begin(); it != values.end(); ++it){
                newTuple.push_back(pair<unsigned, Register*>((*it)->value, *it));
            }
            cout << endl;
            map<Tuple, unsigned, cmpTuple>::iterator tit = dataSource.find(newTuple);
            if(tit == dataSource.end())
                dataSource[newTuple] = 1;
            else
                dataSource[newTuple] += 1;                        
        }while(this->input->next());
       
        cursor = dataSource.begin();
        this->observedOutputCardinality = 0;

        return next();
    }else{
        return 0;
    }        
}

/// Produce the next tuple
unsigned SimpleGroupify::next(){
    if(cursor == dataSource.end())
        return 0;
    
    unsigned result = cursor->second;
    this->observedOutputCardinality += result;
    
    for(Tuple::const_iterator tit = cursor->first.begin(); tit != cursor->first.end(); ++tit)
        tit->second->value = tit->first;
               
    ++cursor;
    
    return result;
}

/// Print the operator tree. Debugging only.
void SimpleGroupify::print(PlanPrinter& out){
   out.beginOperator("SimpleGroupify",expectedOutputCardinality,observedOutputCardinality);
   out.addMaterializationAnnotation(values);
   input->print(out);
   out.endOperator();
}

