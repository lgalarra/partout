/* 
 * File:   DOperator.cpp
 * Author: lgalarra
 * 
 * Created on September 3, 2011, 5:18 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <typeinfo>
#include <sstream>
#include "drdf/DOperator.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "drdf/RemoteOperators.hpp"

using namespace drdf;

unsigned DOperator::opid = 0;

void DOperator::setType(){
    string theType(typeid(*op).name());
    if(theType.find("FullyAggregatedIndexScan") != string::npos)
        type = TFullyAggregatedIndexScan;
    else if(typeid(*op) == typeid(EmptyScan))
        type = TEmptyScan;
    else if(typeid(*op) == typeid(Filter))
        type = TFilter;
    else if(theType.find("AggregatedIndexScan") != string::npos)
        type = TAggregatedIndexScan;
    else if(typeid(*op) == typeid(HashGroupify))
        type = THashGroupify;
    else if(typeid(*op) == typeid(HashJoin))
        type = THashJoin;
    else if(theType.find("IndexScan") != string::npos)
        type = TIndexScan;
    else if(typeid(*op) == typeid(MergeJoin))
        type = TMergeJoin;
    else if(typeid(*op) == typeid(MergeUnion))
        type = TMergeUnion;
    else if(typeid(*op) == typeid(NestedLoopFilter))
        type = TNestedLoopFilter;
    else if(typeid(*op) == typeid(NestedLoopJoin))
        type = TNestedLoopJoin;
    else if(typeid(*op) == typeid(ResultsPrinter))
        type = TResultsPrinter;
    else if(typeid(*op) == typeid(Selection))
        type = TSelection;
    else if(typeid(*op) == typeid(SingletonScan))
        type = TSingletonScan;
    else if(typeid(*op) == typeid(Sort))
        type = TSort;
    else if(typeid(*op) == typeid(TableFunction))
        type = TTableFunction;
    else if(typeid(*op) == typeid(Union))
        type = TUnion;
    else if(typeid(*op) == typeid(RemoteSend))
        type = TRemoteSend;
    else if(theType.find("RemoteFetch") != string::npos)
        type = TRemoteFetch;
    else if(typeid(*op) == typeid(SimpleGroupify))
        type = TSimpleGroupify;
    else if(typeid(*op) == typeid(MultiMergeUnion))
        type = TMultiMergeUnion;
}

void DOperator::print(DebugPlanPrinter &out){
    stringstream sstr;
    string name(typeid(*this->op).name());
    out.beginOperator(name, 0, 0);
    sstr << "Host " << this->home << " Id " << this->id << "Type " << this->type << " Parent ";
    if(parent) sstr << parent->id << endl;
    out.addArgumentAnnotation(sstr.str());    
    out.endOperator();
 
}

unsigned DOperator::countLeaves(){
    unsigned nleaves = 0;
    
    if(this->isLeaf())
        return 1;
    
    for(vector<DOperator*>::iterator chit = this->children.begin(); chit != this->children.end(); ++chit)
        nleaves += (*chit)->countLeaves();
    
    return nleaves;
        
}

void DOperator::getLeaves(vector<DOperator*> &leaves){    
    if(this->isLeaf()){
        leaves.push_back(this);
        return;
    }
    
    for(vector<DOperator*>::iterator chit = this->children.begin(); chit != this->children.end(); ++chit)
        (*chit)->getLeaves(leaves);
}

void DOperator::normalizeIds(){
    queue<DOperator*> theQueue;
    unsigned currentId = 1;
    theQueue.push(this);
        
    while(!theQueue.empty()){
        DOperator *current = theQueue.front();
        current->id = currentId;
        for(vector<DOperator*>::iterator chit = current->children.begin(); chit != current->children.end(); ++chit)
            theQueue.push(*chit);
        
        theQueue.pop();
        ++currentId;
    }    
}

void DOperator::extractHostMappings(map<unsigned, string> &mappings){
    mappings[this->id] = this->home;
    
    for(vector<DOperator*>::iterator chit = this->children.begin(); chit != this->children.end(); ++chit){
        assert(*chit != NULL);
        (*chit)->extractHostMappings(mappings);
    }
}

DOperator * DOperator::find(unsigned id){
    if(this->id == id)
        return this;

   
    for(vector<DOperator*>::iterator chit = this->children.begin(); chit != this->children.end(); ++chit){
        DOperator* found = (*chit)->find(id);
        if(found != NULL){
            return found;
        }
    }
        
    return NULL;
}