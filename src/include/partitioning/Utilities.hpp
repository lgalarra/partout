#ifndef partitioning_utilities_hpp
#define	partitioning_utilities_hpp

/* 
 * File:   Utilities.hpp
 * Author: luis
 * Created on May 24, 2011, 12:49 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "rts/database/Database.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "infra/util/Type.hpp"

using namespace std;

inline bool isLeafOperator(QueryGraph::Filter &aOperator){
    return aOperator.type >= QueryGraph::Filter::Variable && aOperator.type <= QueryGraph::Filter::Null;    
}

inline bool isDiscardedOperator(QueryGraph::Filter aOperator){
    return aOperator.type == QueryGraph::Filter::Builtin_bound  
                  || aOperator.type == QueryGraph::Filter::Builtin_sameterm || aOperator.type == QueryGraph::Filter::Function
                  || aOperator.type == QueryGraph::Filter::ArgumentList || aOperator.type == QueryGraph::Filter::Builtin_in;    
}

inline bool isNotBooleanOperator(QueryGraph::Filter aOperator){
    return aOperator.type == QueryGraph::Filter::Builtin_str || aOperator.type == QueryGraph::Filter::Builtin_datatype  
              || (aOperator.type >= QueryGraph::Filter::Plus && aOperator.type <= QueryGraph::Filter::Div) || aOperator.type == QueryGraph::Filter::UnaryPlus
              || aOperator.type == QueryGraph::Filter::UnaryMinus;
    
}

inline bool isBooleanAtomicOperator(QueryGraph::Filter aOperator){
    return (aOperator.type >= QueryGraph::Filter::Equal && aOperator.type <= QueryGraph::Filter::GreaterOrEqual) ||
          aOperator.type == QueryGraph::Filter::Builtin_langmatches || aOperator.type == QueryGraph::Filter::Builtin_regex
          || (aOperator.type >= QueryGraph::Filter::Builtin_isiri && aOperator.type <= QueryGraph::Filter::Builtin_isliteral);     
}

inline bool isBooleanConnector(QueryGraph::Filter aOperator){
    return aOperator.type == QueryGraph::Filter::Or || aOperator.type == QueryGraph::Filter::And || aOperator.type == QueryGraph::Filter::Not;
}

//Determines the string representation of a value by its id in the database.
bool lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result);

//Runs a query against the database
bool runQuery(Database& db, const string& query, vector<vector<string> > &values, bool showCardinalities = false);


//Determines whether the query has results
bool queryHasResults(Database& db, const string& query, unsigned int &card);

//Check the type of an entity given its database id
QueryGraph::Filter::Type determineEntityType(Database &db, unsigned int id, Type::ID &type, unsigned &subType);

//Extract independent subqueries
unsigned int extractIndependentSubqueries(QueryGraph::SubQuery &subquery, vector<QueryGraph::SubQuery> &subqueries);

//Escape an entity for output
string escapeURI(const char* iter,const char* limit);
string escape(const char* iter,const char* limit);

//Escape an entity for output
bool lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result, string &result2);

#endif	/* UTILITIES_HPP */
