/* 
 * File:   PredicateExtractor.hpp
 * Author: luis
 *
 * Created on May 21, 2011, 9:43 PM
 */

#ifndef PREDICATEEXTRACTOR_HPP
#define	PREDICATEEXTRACTOR_HPP

#include "rts/database/Database.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "partitioning/CommonTypes.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include <vector>

//using namespace qload;

namespace stdpartitioning{
    class QueryGraphPredicateExtractor {
    public:
        QueryGraphPredicateExtractor(Database&);
        unsigned int extractPredicates(QueryGraph &query, vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg);
    private:
        Database &db;
        map<string, SPARQLPredicate> specialPredicates;
        map<string, bool> specialPredicatesAddedFlag;
        bool specialPredicatesLoaded;
        
        void initializeSpecialPredicatesMap(Database &db){
            //For URIS in objects
            SPARQLPredicate uriObject, literalObject;
            map<string, string> dataTypes;
            map<string, unsigned int> typeIds; 
                        
            uriObject.position = SPARQLPredicate::Object;
            uriObject.predicate.type = QueryGraph::Filter::Builtin_isiri;
            uriObject.predicate.arg1 = new QueryGraph::Filter();
            uriObject.predicate.arg1->type = QueryGraph::Filter::Variable;
            specialPredicates["isuri-object"] = uriObject;
            specialPredicatesAddedFlag["isuri-object"] = false;
            
            literalObject.position = SPARQLPredicate::Object;
            literalObject.predicate.type = QueryGraph::Filter::Builtin_isliteral;
            literalObject.predicate.arg1 = new QueryGraph::Filter();
            literalObject.predicate.arg1->type = QueryGraph::Filter::Variable;
            specialPredicates["isliteral-object"] = literalObject;
            specialPredicatesAddedFlag["isliteral-object"] = false;            
            
            dataTypes["string"] = "http://www.w3.org/2001/XMLSchema#string";
            dataTypes["integer"] = "http://www.w3.org/2001/XMLSchema#integer";
            dataTypes["boolean"] = "http://www.w3.org/2001/XMLSchema#boolean";            
            dataTypes["decimal"] = "http://www.w3.org/2001/XMLSchema#decimal";            
            dataTypes["double"] = "http://www.w3.org/2001/XMLSchema#double";    
            dataTypes["decimal"] = "http://www.w3.org/2001/XMLSchema#decimal";
            for(map<string, string>::iterator sit = dataTypes.begin(); sit != dataTypes.end(); ++sit){
                Type::ID type; 
                unsigned int subType, typeId;
                
                if(db.getDictionary().lookup(sit->second, type, subType, typeId))
                    typeIds[sit->first + "-type"] = typeId;                
            }
            
            for(map<string, string>::iterator it = dataTypes.begin(); it != dataTypes.end(); ++it){
                SPARQLPredicate pred;
                pred.position = SPARQLPredicate::Object;
                pred.predicate.type = QueryGraph::Filter::Equal;
                pred.predicate.arg1 = new QueryGraph::Filter();
                pred.predicate.arg1->type = QueryGraph::Filter::Builtin_datatype;
                pred.predicate.arg1->arg1 = new QueryGraph::Filter;
                pred.predicate.arg1->arg1->type = QueryGraph::Filter::Variable;
                pred.predicate.arg2 = new QueryGraph::Filter();
                pred.predicate.arg2->type = QueryGraph::Filter::Literal;
                string key = it->first + "-type";
                if(typeIds.find(key) != typeIds.end())
                    pred.predicate.arg2->id = typeIds[key];
                else                    
                    pred.predicate.arg2->value = it->second;
                
                specialPredicates[key + "-object"] = pred;
                specialPredicatesAddedFlag[key + "-object"] = false;                
            }
            
        };
        /**
         * There are certain predicates that will be added to the partitioning algorithm
         * in some scenarios.
         * @param key
         */
        bool lookupSpecialPredicate(Database &db, string key, SPARQLPredicate &predicate){
            if(!specialPredicatesLoaded){
                initializeSpecialPredicatesMap(db);
                specialPredicatesLoaded = true;
            }
            
            map<string, SPARQLPredicate>::iterator it = specialPredicates.find(key);
            if(it != specialPredicates.end()){
                specialPredicatesAddedFlag[key] = true;
                predicate = it->second;
                return true;
            }
            
            return false;
        };
        
        bool specialPredicateAlreadyAdded(string key){
            map<string, bool>::iterator spit = specialPredicatesAddedFlag.find(key);
            return (spit != specialPredicatesAddedFlag.end() && specialPredicatesAddedFlag[key]);
        };
        
        void addSpecialLanguagePredicate(unsigned id, string langTag, SPARQLPredicate &langMatches){            
            langMatches.position = SPARQLPredicate::Object;
            langMatches.predicate.type = QueryGraph::Filter::Builtin_langmatches;
            langMatches.predicate.arg1 = new QueryGraph::Filter();
            langMatches.predicate.arg1->type = QueryGraph::Filter::Builtin_lang;
            langMatches.predicate.arg1->arg1 = new QueryGraph::Filter();
            langMatches.predicate.arg1->arg1->type = QueryGraph::Filter::Variable;
            langMatches.predicate.arg2 = new QueryGraph::Filter();
            langMatches.predicate.arg2->type = QueryGraph::Filter::Literal;
            langMatches.predicate.arg2->value = langTag;
            langMatches.predicate.arg2->id = id;
            specialPredicates["langMatches-" + langTag + "-object"] = langMatches;
            specialPredicatesAddedFlag["langMatches-" + langTag + "-object"] = false;
        };
        
        void addSpecialTypePredicate(unsigned id, string typeTag, SPARQLPredicate &typeMatches){
            typeMatches.position = SPARQLPredicate::Object;
            typeMatches.predicate.type = QueryGraph::Filter::Equal;
            typeMatches.predicate.arg1 = new QueryGraph::Filter();
            typeMatches.predicate.arg1->type = QueryGraph::Filter::Builtin_datatype;
            typeMatches.predicate.arg1->arg1 = new QueryGraph::Filter();
            typeMatches.predicate.arg1->arg1->type = QueryGraph::Filter::Variable;
            typeMatches.predicate.arg2 = new QueryGraph::Filter();
            typeMatches.predicate.arg2->type = QueryGraph::Filter::Literal;
            typeMatches.predicate.arg2->value = typeTag;
            typeMatches.predicate.arg2->id = id;
            specialPredicates["type-" + typeTag + "-object"] = typeMatches;
            specialPredicatesAddedFlag["type-" + typeTag + "-object"] = false;
            
        };
        
        /**
         * Given a query graph, extracts a list of atomic predicates.
         * @param query 
         * @param predicates
         * @return int Number of extracted predicates
         */
        unsigned int extractPredicatesFromFilters(QueryGraph &query, vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg);        

        /**
         * Generates equality predicates from the literals in the graph patterns
         * @param query
         * @param predicates
         * @return 
         */
        unsigned int extractPredicatesFromLiterals(QueryGraph &query, vector<SPARQLPredicate> &predicates, PredicateDependencyGraph &pdg);
        
        /*
        * Gets all independent subqueries stored recursively in the query graph.
        * Equivalent to extract all the leaves in the subquery hierarchy.
        * @param query
        * @param subqueries
        * @return 
         **/
        unsigned int extractIndependentSubqueries(QueryGraph::SubQuery&, vector<QueryGraph::SubQuery> &);

        
        
        /**
         * Given a filter condition represented as an operator tree, returns a vector
         * of all atomic predicates
         * @param QueryGraph::Filter filter
         * @param QueryGraph::SubQuery query An independent minimal subquery (it does not contain other independent subqueries).
         * @param predicates
         * @return 
         */
        unsigned int extractAtomicPredicates(QueryGraph::Filter &, QueryGraph::SubQuery &, vector<SPARQLPredicate> &, PredicateDependencyGraph &);
        
        /**
         * Determines under which positions (subject, object or predicate) a 
         * filter condition applies.
         * @param filter
         * @param query An independent minimal subquery (it does not contain other independent subqueries).
         * @param positions
         */
        void determinePredicatePositions(QueryGraph::Filter &filter, QueryGraph::SubQuery &query, bitset<3> &positions);
        
        /**
         * Extracts all the variables present in the filter condition.
         * @param filter
         * @param output
         * @return int Number of variables in the filter condition
         */
        unsigned int extractVariables(QueryGraph::Filter &filter, vector<QueryGraph::Filter> &output);
        
        /**
         * Given a string id, determines whether it is a IRI or a literal.
         */
        QueryGraph::Filter::Type determineEntityType(unsigned int, Type::ID &, unsigned &);
        
        /**
         * Determines whether there exist an entry in the predicates dependency graph
         * equivalent to the node sent as argument. A node is said to be equivalent in this context
         * if the values for their constant components are the same (as well as the components which are
         * constant. 
         * @param result The equivalent pattern
         * @param pdg The predicates dependency graph
         * @return bool If there was a equivalent node, the method returns true and locates the pattern id
         * in the second argument.
         */
        bool findEquivalentPattern(QueryGraph::Node &, PatternId &, PredicateDependencyGraph &);
        
        /**
         * Determines which query patterns share a variable with the filter and adds the relation 
         * to the predicate dependency graph
         * @param SPARQLPredicate
         * @param QueryGraph::Subquery The subquery the filter has scope over
         * @param PredicateDependencyGraph
         */
        void identifyAffectedPatterns(SPARQLPredicate &pred, QueryGraph::SubQuery &, PredicateDependencyGraph &);        
        
        
        void addPredicate(QueryGraph::Node &, QueryGraph::Filter &, SPARQLPredicate::Position, vector<SPARQLPredicate> &, PredicateDependencyGraph &);        
        
        bool predicateAlreadyAdded(SPARQLPredicate&, vector<SPARQLPredicate>&);
        
    };
}
#endif	/* PREDICATEEXTRACTOR_HPP */