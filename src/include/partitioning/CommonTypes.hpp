/* 
 * File:   CommonTypes.hpp
 * Author: luis
 * Created on May 22, 2011, 1:34 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 **/

#ifndef partitioning_common_types
#define	partitioning_common_types

#include <utility>
#include <vector>
#include <ostream>
#include <string>
#include <stdio.h>
#include <map>
#include <string.h>
#include <libgen.h>
#include "cts/parser/SPARQLParser.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "partitioning/Utilities.hpp"
#include "boost/dynamic_bitset.hpp"
#include "rts/database/Database.hpp"
#include <boost/thread.hpp>

using namespace std;
using namespace boost;

//Some operations need a context database
void setDefaultDatabase(Database *aDb);

Database* getDefaultDatabase();

bool negatePredicate(QueryGraph::Filter::Type f, QueryGraph::Filter::Type &output);
bool nodeIsEquivalent(QueryGraph::Node &, QueryGraph::Node &);

inline std::ostream& operator<<(std::ostream& str, const QueryGraph::Filter& filter){
    Type::ID type; unsigned int subType; string result;            
    Database *ddb = getDefaultDatabase();
    
    switch(filter.type){
        case QueryGraph::Filter::And:
            str << "(And " << *(filter.arg1) << ", " << *(filter.arg2) << ")";
            break;
        case QueryGraph::Filter::Or:
            str << "(Or " << *(filter.arg1) << ", " << *(filter.arg2) << ")";
            break;
        case QueryGraph::Filter::Variable:
            str << "Variable (id): " << filter.id;
            break;
        case QueryGraph::Filter::Literal:
            str << "Literal: " << filter.id << " (value): ";
            lookupById(*ddb, filter.id, type, subType, result);
            str << result;            
            break;
        case QueryGraph::Filter::Equal:
            str << "(" << *(filter.arg1) << " == " << *(filter.arg2) << ")" ;            
            break;
        case QueryGraph::Filter::IRI:
            str << "URI (id): " << filter.id << " (value): ";
            lookupById(*ddb, filter.id, type, subType, result);
            str << result;
            break;
        case QueryGraph::Filter::Builtin_langmatches:
            str << "langMatches: " << *(filter.arg1);
            break;
        case QueryGraph::Filter::Builtin_lang:
            str << "lang: " << *(filter.arg1);            
            break;            
        case QueryGraph::Filter::Builtin_bound:
            str << "bound: " << *(filter.arg1);
            break;
        case QueryGraph::Filter::Builtin_in:
            break;
        case QueryGraph::Filter::Builtin_isblank:
            str << "isBlank: " << *(filter.arg1);
            break;            
        case QueryGraph::Filter::Builtin_isiri:
            str << "isIRI: " << *(filter.arg1);
            break;
        case QueryGraph::Filter::Builtin_isliteral:
            str << "isLiteral: " << *(filter.arg1);
            break;            
        case QueryGraph::Filter::Builtin_regex:
            str << "regex: " << *(filter.arg1) << ", " << *(filter.arg2);
            if(filter.arg3) str << ", " << *(filter.arg3);
            break;
        case QueryGraph::Filter::Builtin_str:    
            str << "str: " << *(filter.arg1);
            break;            
        case QueryGraph::Filter::NotEqual:
            str << "(" << *(filter.arg1) << " != " << *(filter.arg2) << ")" ;            
            break;            
        case QueryGraph::Filter::Less:
            str << "(" << *(filter.arg1) << " < " << *(filter.arg2) << ")" ;            
            break;
        case QueryGraph::Filter::LessOrEqual:
            str << "(" << *(filter.arg1) << " <= " << *(filter.arg2) << ")" ;            
            break;            
        case QueryGraph::Filter::Greater :
            str << "(" << *(filter.arg1) << " > " << *(filter.arg2) << ")" ;            
            break;                        
        case QueryGraph::Filter::GreaterOrEqual :
            str << "(" << *(filter.arg1) << " >= " << *(filter.arg2) << ")" ;            
            break;                                    
        default: 
            str << filter.type; 
    }

    return str;
};

namespace stdpartitioning{
    
    struct DatabaseDescriptor{
        Database *db;
        Database *smdb;
        string filename;
        float sfactor;
        DatabaseDescriptor(): db(NULL), filename(""), smdb(NULL), sfactor(0.0){};
        DatabaseDescriptor(Database *db, string filename): db(db), filename(filename), smdb(NULL), sfactor(0.0){};
        DatabaseDescriptor(Database *db, Database *sampleDb, float sampleFactor, string filename): db(db), filename(filename), smdb(sampleDb), sfactor(sampleFactor){};        
        string dbBasename(){
            char *dbname = new char[filename.length() + 1];
            string strdbbasename;
            strcpy(dbname, this->filename.c_str());
            strdbbasename = string(basename(dbname));
            delete[] dbname;
            return strdbbasename;
        };
    };
    
    typedef pair<SPARQLParser::Pattern, SPARQLParser::Pattern> JoinPatternOccurrence;

   
   /**
    * A very simple dictionary wrapper with a 1024 entries cache to avoid expensive dictionary
    * lookups as much as possible.
    */
   class DictionaryWrapper{
   private:
      struct CacheEntry{
           Type::ID type;
           unsigned id;
           string text;
           unsigned subtype;
       };

       
       Database *db;
       vector<CacheEntry> dictHashCache;
       boost::mutex dictMutex;
       static const unsigned lookupSize = 409;
       
   public:
       DictionaryWrapper(){
           dictHashCache.resize(lookupSize);
       }
       
       ~DictionaryWrapper(){ this->db = NULL; }
       
       DictionaryWrapper& operator=(const DictionaryWrapper &otherWrapper){
           this->db = otherWrapper.db;
           dictHashCache = otherWrapper.dictHashCache;
           return *this;
       } 
       
       void setDatabase(Database *db){ this->db = db; }
       
       bool lookupById(unsigned id, Type::ID &type, unsigned &subtype, string &result);

   };

    
   /**
    * Represents a simple predicate for a standard partitioning algorithm by considering
    * RDF triples as rows from a big relation. The filter conditions are expressed respect to
    * the columns subject, predicate and object. Predicates over more than one column are not
    * supported.
    * 
    */
   struct SPARQLPredicate {
      //In order to assign every predicate a unique id 
   private:
      static unsigned int pid; 
      
   public:
      
      unsigned int id; 
       
      enum Position{Subject = 2, Predicate = 1, Object = 0};
      
      QueryGraph::Filter predicate;
      
      Position position;
      
      SPARQLPredicate();      
      
      SPARQLPredicate(const QueryGraph::Filter&, Position);
     
      string toString() const;
      
      bool isEquivalent(const SPARQLPredicate &anotherPredicate);
      
      void negated(SPARQLPredicate &negated);
      
      unsigned argumentsCount();
      
      //List of plain arguments (applicable only to atomic binary predicates like equal, greater,...)
      void argumentsList(vector<pair<unsigned, string> > &args);
      
      bool meets(unsigned subject, unsigned predicate, unsigned object, bool flag, DictionaryWrapper &dictWrapper);
      
   };
   
   typedef QueryGraph::Filter::Type SPARQLOperator;
   
   
   struct SPARQLPattern{
   private:  
     static unsigned int pid;
   public:
     //Global id  
     unsigned int id;
     //The number of times it has appeared in the query load
     unsigned int freq;
     //The id of the query it was extracted from
     unsigned int qid;
       
     //Taken from an isolated query (contains the literals basically)
     QueryGraph::Node localNode;  
     
     SPARQLPattern();     
     
   };
   
   struct SPARQLEdge{
       //SPARQLPattern ids
       unsigned int from;
       unsigned int to;
       // The number of times these patterns join in the query load
       unsigned int weight; 
   };
   
   typedef unsigned int PatternId;   
   typedef unsigned int PredicateId;
   typedef map<PatternId, pair<SPARQLPattern, vector<SPARQLPredicate> > > PredicateDependencyGraph;
   typedef map<PatternId, vector<SPARQLEdge> > PatternDependencyGraph;
   typedef map<PredicateId, pair<SPARQLPredicate, vector<SPARQLPattern> > > ReversePredicateDependencyGraph;
   typedef vector<string> Row;
   typedef vector<Row> ResultsTable;
   
   bool ptdgExistsEdge(PatternDependencyGraph &prdg, PatternId from, PatternId to, SPARQLEdge &theEdge);   
   
   /**
    * Represents the conjunction of a minimal set of predicates
    */
   struct Minterm{
       //The predicates whose conjunction (of negated versions) defines the minterm
       vector<SPARQLPredicate*> predicates;
       
       //TRUE in the ith position means the predicate is in normal form, otherwise it is negated.
       dynamic_bitset<> flags;
       
       bool isSatisfiable();
       
       //Builds aimed to return all triples matching the minterm
       string buildQuery();
       
       //Query to know the number of items matching the minterm
       string buildCountQuery();       
       
       //String representation of the flags
       string flagsString() const;
       
       //Check whether two minterms have the same predicates in the same order
       bool samePredicates(Minterm &);
       
       //Gets the id with all strings
       string idsString() const;
       
       //Checks if all the predicates in this minterm are expressed in affirmative form
       bool isPositive();
       
       //Check whether all predicates are negated equalities (the most expensive type of query)
       bool isNegative();
              
       //The same minterm with the last predicate out
       Minterm prefixMinterm();
       
       //The same minterm but with the last flag flipped
       Minterm complementMinterm();
       
       //The same minterm with first predicate out
       Minterm suffixMinterm();
       
       //Build a new minterm with an additional predicate
       Minterm extendedMinterm(SPARQLPredicate*, bool);
       
       //Sorts predicates by id
       void sortPredicatesById();
       
       //Check if it is the left-over partition
       bool isZeroMinterm();
              
       
   private:
       //Quick sort implementation for predicates
       void mintermsQuickSort(unsigned int start, unsigned int end);
   };
  
   inline std::ostream& operator<<(std::ostream& str, const SPARQLPredicate& pred){
       str << "Pid " << pred.id << ", " << pred.toString();
       return str;
   };   

   
   inline std::ostream& operator<<(std::ostream& str, const Minterm& min){
       str << min.flagsString() << " ids " << min.idsString();
       for(vector<SPARQLPredicate*>::const_iterator pit = min.predicates.begin(); pit != min.predicates.end(); ++pit)
            str << **pit << "; ";
       return str;
   }

   
   class Exception: public std::exception{   
   public:
       enum Code{INVALID_STATE, DATABASE_ERROR, PARTITIONING_ERROR};       
       Exception(Code code, string message = "") throw();
       Exception(const Exception &e) throw();
       virtual ~Exception() throw();       
       virtual const char* what() const throw();
   private:
       string message;
       Code code;       
   };
      
   struct Host{
        string name;
        double weight;

        Host(): name(""), weight(0.0){};
        Host(string name, double weight): name(name), weight(weight){};
    };
}



#endif	/* COMMONTYPES_H */