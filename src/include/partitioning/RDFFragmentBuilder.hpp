/* 
 * File:   RDFFragmentBuilder.hpp
 * Author: luis
 *
 * Created on June 18, 2011, 8:30 PM
 */

#ifndef RDFFRAGMENTBUILDER_HPP
#define	RDFFRAGMENTBUILDER_HPP

#include <vector>
#include <string>
#include <map>
#include <queue>
#include <boost/thread.hpp>

#include "partitioning/Fragment.hpp"
#include "partitioning/CommonTypes.hpp"
#include "partitioning/PartitioningDescription.hpp"
#include "partitioning/TempFile.hpp"

namespace stdpartitioning{
    class RDFFragmentBuilder {        
    private:
        class DictionaryLookupAndBackup;
        
        boost::mutex filesMapMutex;
        boost::mutex allocMapMutex;
        string binPrefix;
        string tmpPrefix;
        queue<unsigned> properties;
        boost::mutex propertiesMutex;
        
    public:
        RDFFragmentBuilder(DatabaseDescriptor &, string &, string &);
        
        virtual ~RDFFragmentBuilder();
        
        void buildFragments(FragmentsAllocationMap &, FragmentFilesAllocationMap &);
        void buildFragments2(FragmentsAllocationMap &, FragmentFilesAllocationMap &);
        void buildFragments3(FragmentsAllocationMap &, FragmentFilesAllocationMap &);
        void buildFragments4(FragmentsAllocationMap &); 
        void buildFragments5(FragmentsAllocationMap &);
    private:
        DatabaseDescriptor &dbds;

        void buildSingleFragment(vector<FragmentAllocationAssign> &, FragmentFilesAllocationMap *);   
        void buildSinglePartition(string &, string &, FragmentsAllocationMap *);
        void buildSingleFragment3(unsigned key, map<string, fstream*> *, map<string, DictionaryLookupAndBackup*> *, map<string, boost::mutex*> *, FragmentsAllocationMap *);
        void buildSingleFragment4(unsigned key, map<string, fstream*> *, map<string, DictionaryLookupAndBackup*> *, map<string, boost::mutex*> *, PartitionsSummaryMap *, Fragment *);        
        void buildSingleFragment5(map<string, fstream*> *, map<string, DictionaryLookupAndBackup*> *, map<string, boost::mutex*> *, PartitionsSummaryMap *, Fragment *);        

        
        //Runs a query against the database and stores the dictionary entries in a temporal file
        bool runQuery(Database& db, const string& query, vector<vector<string> > &values, DictionaryLookupAndBackup &lookupByIdAndStore, bool showCardinalities);
                       
        class DictionaryLookupAndBackup{
        private:
            //Maximum number of entries allowed
            unsigned backupLimit;
            unsigned storedEntries;
        public:
            struct DictionaryEntry{
                Type::ID type;
                unsigned subtype;
                string value;                
            };
            
            static const unsigned cacheMax;
            static const unsigned defaultBackupLimit;
            map<unsigned, DictionaryEntry> cache;
            TempFile *tempFile;                        
            
            DictionaryLookupAndBackup(string backupFile, unsigned backupLimit = 128);
            ~DictionaryLookupAndBackup();
            void store(unsigned int id, Type::ID &type, unsigned &subType, string &result);
            bool operator()(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result);            
            void flush();
            bool lookupById(Database &db, unsigned int id, Type::ID &type, unsigned &subType, string &result, string &result2);

        };
    };
}
#endif	/* RDFFRAGMENTBUILDER_HPP */

