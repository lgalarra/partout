/* 
 * File:   JoinOccurrenceExtractor.hpp
 * Author: luis
 *
 * Created on May 22, 2011, 1:05 AM
 */

#ifndef JOINOCCURRENCEEXTRACTOR_HPP
#define	JOINOCCURRENCEEXTRACTOR_HPP

#include <vector>
#include "qload/CommonTypes.hpp"

using namespace std;

namespace qload{
    class JoinOccurrenceExtractor {
    public:
        JoinOccurrenceExtractor();
        virtual ~JoinOccurrenceExtractor();
        void extractJoinCoocurrences(vector<SPARQLIndependentSubquery>&, vector<JoinPatternOccurrence>&);
        void extractJoinCoocurrences(SPARQLIndependentSubquery &, vector<JoinPatternOccurrence>&);
    private:

    };
}

#endif	/* JOINOCCURRENCEEXTRACTOR_HPP */

