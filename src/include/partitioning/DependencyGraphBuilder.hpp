/* 
 * File:   DependencyGraphBuilder.hpp
 * Author: luis
 *
 * Created on June 7, 2011, 1:38 PM
 */

#ifndef partitioning_dependencygraphbuilder_hpp
#define	partitioning_dependencygraphbuilder_hpp

#include "partitioning/CommonTypes.hpp"
#include "rts/database/Database.hpp"

namespace stdpartitioning{
    class DependencyGraphBuilder {
    public:
        DependencyGraphBuilder();
        
        /**
         * Extracts all existing dependencies between triple patterns and predicates.
         * @param QueryGraph query
         * @param PredicateDependencyGraph pdg Predicate-Pattern relationships
         * @param PatternDependencyGraph pdtg Pattern-Pattern relationships
         */
        void extractDependencies(QueryGraph &, PredicateDependencyGraph &, PatternDependencyGraph &);
        
        /*
         * Given a predicate dependency graph with pattern ids as keys, it reverses it by building a 
         * new map where predicate ids are the keys
         * @param PredicateDependencyGraph input
         * @param ReversePredicateDependencyGraph output
         */
        void reversePredicateDependencyGraph(PredicateDependencyGraph &, ReversePredicateDependencyGraph &);
    private:
        /*
        * Gets all independent subqueries stored recursively in the query graph.
        * Equivalent to extract all the leaves in the subquery hierarchy.
        * @param query
        * @param subqueries
        * @return 
         **/
        unsigned int extractIndependentSubqueries(QueryGraph::SubQuery&, vector<QueryGraph::SubQuery> &);
        
        
        bool existsEquivalentPattern(QueryGraph::Node &, PredicateDependencyGraph &, PatternId &);
        bool joinsEquivalentPattern(QueryGraph::Node &, QueryGraph::SubQuery &, PredicateDependencyGraph &, vector<PatternId> &);        
    };
}

#endif	/* DEPENDENCYGRAPHBUILDER_HPP */

