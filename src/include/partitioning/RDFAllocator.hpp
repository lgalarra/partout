
#ifndef RDFALLOCATOR_HPP
#define	RDFALLOCATOR_HPP

#include <vector>
#include "partitioning/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"
#include "partitioning/PartitioningDescription.hpp"

using namespace std;

namespace stdpartitioning{
    
    struct BenefitDescriptor{
        Fragment *fragment;
        double benefit;
        string node;
    };
        
    class BenefitSelector{
    public:
        virtual void selectBenefitial(vector<BenefitDescriptor> &benefitVector, vector<unsigned int> &positions) = 0;
    };
    
    class OneTopBenefitSelector: public BenefitSelector{
    public:
        virtual void selectBenefitial(vector<BenefitDescriptor> &benefitVector, vector<unsigned int> &positions){
            unsigned int greater = -1;
            double greaterBenefit = -1.0;
            
            for(unsigned int i = 0; i < benefitVector.size(); ++i){
                if(benefitVector[i].benefit > greaterBenefit){
                    greater = i;
                    greaterBenefit = benefitVector[i].benefit;
                }
            }
            
            positions.push_back(greater);
            
        };
    };
        
    class RDFAllocator {
        BenefitSelector *benefitSelector;
    public:
        RDFAllocator(BenefitSelector *);
        void assignFragmentsToNodes(FragmentationScheme &, PartitioningDescription &, 
        PatternDependencyGraph &, FragmentsAllocationMap &);
        
    private:
        void sortDescendentlyByLoad(FragmentationScheme &, vector<Fragment*> &);
        
        void calculateBenefitVectorForFragment(Fragment *, FragmentationScheme &, PartitioningDescription &, 
        PatternDependencyGraph &, vector<BenefitDescriptor> &, FragmentsAllocationMap &);        

        void quickSort(vector<Fragment*>&, unsigned int, unsigned int);
        double aggregateEdges(vector<SPARQLPattern*> &, vector<SPARQLPattern*> &, PatternDependencyGraph &);
        
    };
}
#endif	/* RDFALLOCATOR_H */