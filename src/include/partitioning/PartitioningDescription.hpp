/* 
 * File:   PartitioningDescription.hpp
 * Author: luis
 *
 * Created on June 8, 2011, 2:56 PM
 */

#ifndef partitioning_partitioningdescription_hpp
#define	partitioning_partitioningdescription_hpp

#include <string>
#include <stdio.h>
#include "partitioning/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"

using namespace std;

namespace stdpartitioning{
    class PartitioningDescription {        
    public:                
        PartitioningDescription();
        PartitioningDescription(map<string, Host> &);
        virtual ~PartitioningDescription();
        void addNode(string name, double weight);
        void removeNode(string name);
        const map<string, Host>& getNodes() const;
        Host& getMostCapable();
        
        static void sampleDescription(PartitioningDescription &description, unsigned nNodes){            
            unsigned port = 2000;
            double percentage = 1.0 / (double)nNodes;
            unsigned host = 1;
            for(unsigned k = 0; k < nNodes; ++k){
                if(k % 2 == 0){
                    port = 2000;
                    ++host;
                    if(host == 3) ++host;
                }else port = 2001;
                
                char uri[20];
                sprintf(uri, "everlast0%d:%d", host, port);
                description.addNode(string(uri), percentage);
            }
          
        }
    private:
        map<string, Host> nodes;        

    };    
}
#endif	/* PARTITIONINGDESCRIPTION_HPP */

