/* 
 * File:   RDFStandardHorizontalPartitioner.hpp
 * Author: luis
 *
 * Created on May 22, 2011, 1:17 AM
 */

#ifndef RDFSTANDARDHORIZONTALPARTITIONER_HPP
#define	RDFSTANDARDHORIZONTALPARTITIONER_HPP

#include "partitioning/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"
#include "rts/database/Database.hpp"
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace boost;

namespace stdpartitioning{
    class RDFStandardHorizontalPartitioner {
    public:
        RDFStandardHorizontalPartitioner(Database &);
        RDFStandardHorizontalPartitioner(Database &, Database *, float);
        virtual ~RDFStandardHorizontalPartitioner();
        void optimalHorizontalFragmentation(vector<SPARQLPredicate> &, FragmentationScheme &, ReversePredicateDependencyGraph &);        
    private:
        
        Database &db;
        
        //Sample database
        Database *sdb;
        
        float samplingFactor;
        
        /**
         * Given a set of input predicates, it determines a complete and minimal set of predicates.
         * A set of predicates is complete if it partitions the relation into a set of 
         * mutually disjoint fragments such that the access frequency of all tuples 
         * within a fragment is uniform for all queries. A set of predicates is minimal 
         * if the resulting partitioning is obtained by minimal number of predicates.
         * 
         * @param vector Initial set of predicates
         * @param vector Minimal and complete set of predicates
         * @return int Number of predicates in the output set
         */
        unsigned int minimalSetOfPredicates(vector<SPARQLPredicate>&, vector<SPARQLPredicate*>&, ReversePredicateDependencyGraph &);
        
        /**
         * Determines if a predicate is present in his normal or negated form 
         * within a set of predicates. This function does not consider logical equivalence, therefore
         * it expects negated predicates are simply preceded by a NOT operator.
         * @param vector Set of predicates 
         * @param SPARQLPredicate
         * @return bool
         */
        bool isPresentOrNegated(vector<SPARQLPredicate*>&, SPARQLPredicate&);
        
        /**
         * Determines if a new predicate is redundant to a set of predicates. This happens is
         * the introduction of the new predicate does not alter the fragmentation defined by
         * the existing set of predicates.
         * @param vector Input set
         * @param SPARQLPredicate Evaluated predicate
         * @return 
         */
        bool isRedundant(vector<SPARQLPredicate*>&, SPARQLPredicate&, ReversePredicateDependencyGraph &);                
        
        /**
         * Removes all predicates that might become redundant due to the addition of
         * the last predicate in the input vector
         * @param vector Input
         * @param vector Removed predicates
         * @return int Number of removed predicates
         */
        unsigned int removeRedundantPredicates(vector<SPARQLPredicate*>&, SPARQLPredicate &, ReversePredicateDependencyGraph &);
        
        /**
         * 
         * @param 
         * @param 
         */
        void buildFragmentationScheme(vector<SPARQLPredicate*>&, FragmentationScheme&, ReversePredicateDependencyGraph &);
        
        /**
         * 
         * @param 
         * @param 
         */
        void buildRelevantPredicateCombinations(vector<SPARQLPredicate*>& pred, vector<Minterm> &output);

    };
    
    inline void operator++(dynamic_bitset<> &bitset){
        if(bitset.count() == bitset.size())
            return;

        size_t firstOne = bitset.find_first();

        //There are 0 before, nice case
        if(firstOne == bitset.npos || firstOne > 0){
            bitset.set(0, true);
        }else{
            //Bad case.. search for the next 0
            unsigned int nextCero = firstOne + 1;
            while(bitset[nextCero]) nextCero++;

            //I found my cero, now turn it into 1 and make the others 0
            bitset.set(nextCero, true);
            for(unsigned int i = 0; i < nextCero; ++i)
                bitset.set(i, false);
        }

    }
}
#endif	/* RDFSTANDARDHORIZONTALPARTITIONER_HPP */