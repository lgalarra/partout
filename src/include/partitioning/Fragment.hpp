/* 
 * File:   Fragment.hpp
 * Author: luis
 *
 * Created on June 10, 2011, 6:44 PM
 */

#ifndef partitioning_fragment_hpp
#define	partitioning_fragment_hpp

#include "partitioning/CommonTypes.hpp"
#include "rts/database/Database.hpp"
#include <boost/thread.hpp>

using namespace std;

#define ENABLE_SAMPLING 1

namespace stdpartitioning{
   typedef map<string, vector<pair<Minterm, unsigned int> > > StatisticsMap;    
   
   class MintermStatistics{
   private:
       Database *db;
       float samplingFactor;
       
       vector<pair<Minterm, unsigned int> >::iterator findMinterm(Minterm &, StatisticsMap::iterator &);
       
       //Cache for statistics that can be calculated based on redundant calculations
       static StatisticsMap statisticsCache;
       bool lookupCardinalityInMap(Minterm &, unsigned int &);
   public:
       
       MintermStatistics();
       MintermStatistics(Database *);
       MintermStatistics(Database *, float);
       bool lookupCardinality(Minterm &, unsigned int &);
       void setCardinality(Minterm &, unsigned int);
       unsigned int getDatabaseSize();
       
       //Update the statistics cache directly
       static void addCardinality(Minterm &minterm, unsigned int cardinality){
           string flagsString = minterm.flagsString();
           StatisticsMap::iterator stit = statisticsCache.find(flagsString);
           //If the flags string is not there, create a new entry
           if(stit == statisticsCache.end()){
               statisticsCache[flagsString].push_back(pair<Minterm, unsigned int>(minterm, cardinality));
           }else{
               //Check whether it refers to the same minterm (using the same predicates)
               vector<pair<Minterm, unsigned int> >::iterator it;
               for(it = stit->second.begin(); it != stit->second.end(); ++it){
                   //Update it
                   if(it->first.samePredicates(minterm)){
                       it->second = cardinality;
                       return;
                   }
               }

               //If we reached this state, we must add the entry
               stit->second.push_back(pair<Minterm, unsigned int>(minterm, cardinality));                    
           }           
       };
   };

    
   class Fragment{
   private:
       //Database
       Database *db;
       
       //Sample database
       Database *sdb;
       
       //The minterm that explains the fragment
       Minterm minterm;
       
       //Statistics extractor
       MintermStatistics statsCalculator;
       
       //The number of tuples in the database
       unsigned int size;
       
       //The number of times this fragment has been touched by the query load
       unsigned int frequency;
       
       //To know if the size has to be calculated (this is expensive)
       bool frequencyUpdated; 
       
       //Calculate the size of the fragments
       void calculateSize();
       
       //Estimate the access frequency based on the dependency graph
       void calculateFrequency();      
       
       DictionaryWrapper dictWrapper;
       
       boost::mutex cardMutex;
       
   public:  
       bool sizeUpdated;
       
       unsigned observedCardinality;
       
       //Global dependency graph: implemented as a adjancency list
       ReversePredicateDependencyGraph *dependencyGraph;
       
       Fragment();
       
       Fragment(Minterm, Database *, ReversePredicateDependencyGraph*);
       
       Fragment(Minterm, Database *, Database *, float, ReversePredicateDependencyGraph*);
       
       Fragment(const Fragment&);
       
       Fragment& operator=(Fragment &anotherFragment);
       
       virtual ~Fragment();
       
       Minterm& getMinterm();
       
       const Minterm& getMinterm() const;       
       
       unsigned int getSize();    
       
       unsigned int getFrequency();
       
       double getRatio();
       
       unsigned int getLoad();
       
       vector<SPARQLPattern*> getRelatedPatterns();
       
       unsigned int onesCount() const;
       
       bool meets(unsigned, unsigned, unsigned);
       
       unsigned hit(){ 
           boost::mutex::scoped_lock(this->cardMutex);
           observedCardinality++; 
           return observedCardinality; 
       }
       
       unsigned getObservedCardinality(){ return observedCardinality; }
       
       bool isLeftOverFragment();
       
       Database* getDatabase(){ return this->db; }
       
   };
      
   struct FragmentationScheme{
   public:
       vector<Fragment> fragments;
       //Fragmentation signature: array of ratios: size*frequency
       vector<double> freqSizeVector;
       
       FragmentationScheme(): loadUpdated(false), sizeUpdated(false){};
       
       bool sameSignature(const FragmentationScheme &) const;
       
       unsigned int getTotalLoad();
       
       bool empty();
       
       void calculateRatio();

       void pushFragment(Fragment &);
       
       void cleanup(vector<unsigned int> &);
       
   private:
       
       bool loadUpdated;
       
       bool sizeUpdated;
       
       unsigned int totalLoad;
       
       void sortFragments();
       
       void nOnesQuickSort(vector<Fragment> &, unsigned int, unsigned int);
       
   };

   inline std::ostream& operator<<(std::ostream& str, const Fragment& frag){
       vector<SPARQLPredicate*>::const_iterator pid;
       const Minterm &m = frag.getMinterm();
              
       str << m.flags;
       return str;
   };
   
   inline std::ostream& operator<<(std::ostream& str, const FragmentationScheme& scheme){
       vector<Fragment>::const_iterator fit;
       vector<double>::const_iterator rit;
       
       str << "Fragments: ";
       for(fit = scheme.fragments.begin(); fit != scheme.fragments.end(); ++fit){
           str << *fit << ", ";           
       }
       
       str << endl;
       
       str << "Ratio vector: ";
       for(rit = scheme.freqSizeVector.begin(); rit != scheme.freqSizeVector.end(); ++rit){
           str << *rit << ", ";
       }
       str << endl;       
       
       return str;       
   };
   
   struct Assignment{
        string host;
        Fragment *fragment;
    };
    
    typedef Assignment FragmentAllocationAssign;
    typedef map<string, vector<FragmentAllocationAssign> > AllocationMapT;
    typedef AllocationMapT FragmentsAllocationMap;
    typedef map<string, string> FragmentFilesAllocationMap; 

    class PartitionSummary;
    typedef map<string, PartitionSummary> PartitionsSummaryMap;
    
    unsigned numberOfUniqueAffirmativePredicates(vector<FragmentAllocationAssign> &assignments);
    
    enum PredicateEntryStatus{Positive, Negative, Ignore};
    
    class PartitionSummary{
    private:
        DictionaryWrapper dictWrapper;
        
    public:
        vector<SPARQLPredicate*> predicates;
        
        vector<PredicateEntryStatus> status;
        
        vector<FragmentAllocationAssign> fragments;
        
        
        PartitionSummary(){}
        
        PartitionSummary(const PartitionSummary &otherSummary){
            this->predicates = otherSummary.predicates;
            this->status = otherSummary.status;          
            this->dictWrapper = otherSummary.dictWrapper;
        }
        
        PartitionSummary(vector<SPARQLPredicate*> &preds, vector<PredicateEntryStatus> &stat, Database *db): 
                predicates(preds), status(stat){
            dictWrapper.setDatabase(db);
        };
        
        PartitionSummary& operator=(const PartitionSummary &otherSummary);

        bool meets(unsigned, unsigned, unsigned);        
                
        void setFragments(vector<FragmentAllocationAssign> &frags){ this->fragments = frags; }
                
        static PartitionSummary summarize(vector<FragmentAllocationAssign> &);
        
        static void summarize(FragmentsAllocationMap &, PartitionsSummaryMap &);
    };
    
   inline std::ostream& operator<<(std::ostream& str, const PartitionSummary& summary){
       str << "Predicates: ";
       for(vector<SPARQLPredicate*>::const_iterator pit = summary.predicates.begin(); pit != summary.predicates.end(); ++pit){
           str << **pit << ", ";           
       }
       
       unsigned size = summary.status.size();
       for(int i = size - 1 ; i >= 0; --i){
           if(summary.status[i] == Positive)
               str << "1";
           else if(summary.status[i] == Negative)
               str << "0";
           else
               str << "I";           
       }
       str << endl;       
       
       return str;       
   };

   inline std::ostream& operator<<(std::ostream& str, const PartitionsSummaryMap& summary){
       for(PartitionsSummaryMap::const_iterator psit = summary.begin(); psit != summary.end(); ++psit)
           str << "Host " << psit->first << " " << psit->second << endl;
       
       return str;
   }
    
}
#endif	/* FRAGMENT_HPP */
