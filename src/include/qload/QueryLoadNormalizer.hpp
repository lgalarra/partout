#ifndef QUERYLOADNORMALIZER_HPP
#define	QUERYLOADNORMALIZER_HPP
/* 
 * File:   QueryLoadNormalizer.hpp
 * Author: lgalarra
 *
 * Created on May 14, 2011, 2:44 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 **/

#include "rts/database/Database.hpp"
#include "cts/parser/SPARQLParser.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "qload/CommonTypes.hpp"
#include <vector>

using namespace std;

namespace qload{
    class QueryLoadNormalizer {        
    public:
        QueryLoadNormalizer(Database &db, map<string, FrequentElementDescriptor> &frequentElements);
        virtual ~QueryLoadNormalizer();
        
        /**
         * Returns a vector of normalized independent subqueries.
         * @param query
         * @param subqueries
         */
        void normalizeQuery(string query, QueryGraph &output);
    private:
         /**
         * Normalize a graph pattern 
         * @param SPARQLParser::Pattern in
         * @param int initialId -> Minimum integer that can be used as id for 
         * for the new introduced variables
         * @return Number of normalized components
         */
        unsigned int normalizePattern(SPARQLParser::Pattern&, unsigned int);       
        //Frequent Elements from query load
        map<string, FrequentElementDescriptor> &frequentElements;        
        //Extract groups of patterns that could be processed in parallel        
        void extractAllPatterns(const SPARQLParser::PatternGroup&, vector<SPARQLParser::Pattern> &);
        //Extract pattern groups
        void extractPatternGroups(SPARQLParser::PatternGroup *, vector<SPARQLParser::PatternGroup*> &output);        
        //Remove filters with unfrequent literals
        void normalizeFilters(SPARQLParser::PatternGroup *);
        //Check whether a filter condition should remain
        void containsUnfrequentConstant(SPARQLParser::Filter *, bool &result);
        //Check whether an entity is in the frequent elements map
        //Equality is tested according to http://www.w3.org/TR/rdf-sparql-query/#matchingRDFLiterals
        bool isFrequentEntity(SPARQLParser::Element&);
        //Determines whether a pattern is compounded of three variables (useless)
        bool allVariables(SPARQLParser::Pattern &);
        //Generates a random variable name
        string randomVariableName();
        
        
        Database &db;
        
    };
}
#endif	/* QUERYLOADNORMALIZER_HPP */

