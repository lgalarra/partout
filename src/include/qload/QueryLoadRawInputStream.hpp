#ifndef QUERYLOADINPUTSTREAM_H_
#define QUERYLOADINPUTSTREAM_H_

using namespace std;
#include <istream>
#include "qload/QueryLoadInputStream.hpp"

namespace qload{

    /**
     * Class for reading raw query logs.
     */
    class QueryLoadRawInputStream: public qload::QueryLoadInputStream
    {
    private: 
       ifstream stream;
       string filename;

    public:
       QueryLoadRawInputStream(string filename);
       virtual ~QueryLoadRawInputStream();
        /**
         * Represents a HTTP log with a SPARQL query 
         */        
       struct SPARQLLogQuery{
           string date;
           string type;
           string graphUri;
           string query;
           string format;
       };
        //Returns the next query in the query load
        qload::QueryLoadRawInputStream::SPARQLLogQuery getQuery(unsigned int &, bool &);
        QueryLoadRawInputStream& operator>>(QueryLoadRawInputStream::SPARQLLogQuery&);
        string fixSelectClause(string);        
    };
}

#endif /*QUERYLOADINPUTSTREAM_H_*/