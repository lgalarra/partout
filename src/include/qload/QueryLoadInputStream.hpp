#ifndef QUERYLOADINPUTSTREAM_HPP
#define	QUERYLOADINPUTSTREAM_HPP

/* File:   QueryLoadInputStream.hpp
 * Author: lgalarra
 *
 * Created on May 13, 2011, 1:59 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 **/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

namespace qload{
    class QueryLoadInputStream{
    public:
        QueryLoadInputStream(string filename);
        virtual ~QueryLoadInputStream();
        bool open();
        bool close();
        bool eof();
        bool good();
        std::string getFilename();
        //Returns the next query in the query load
        virtual string get(unsigned int &);
        virtual QueryLoadInputStream& operator>> (string &);        
    protected:        
        fstream stream;
        string filename;        
                
    };
}

#endif	/* QUERYLOADINPUTSTREAM_HPP */