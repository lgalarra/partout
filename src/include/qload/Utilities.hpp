#ifndef UTILITIES_H
#define	UTILITIES_H

/* 
 * File:   Utilities.h
 * Author: Luis Galárraga
 * Created on May 19, 2011, 7:15 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */
#include <string>

using namespace std;


//Utility to decode queries taken from log files
string urldecode(string text);

//Remove trailing spaces and line ends
void trim(std::string &s, char c);

#endif	/* NEWFILE_H */
