#ifndef QUERYLOADOUTPUTSTREAM_HPP
#define	QUERYLOADOUTPUTSTREAM_HPP

/** 
 * File:   QueryLoadOutputStream.cpp
 * Author: luis
 * Created on May 11, 2011, 1:28 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#include "qload/QueryLoadRawInputStream.hpp"
using namespace std;

namespace qload{
    class QueryLoadOutputStream {
    public:
        QueryLoadOutputStream(std::string filename);
        bool open();
        bool close();
        virtual bool put(const string&);
        bool good();
        virtual ~QueryLoadOutputStream();
        virtual QueryLoadOutputStream& operator<<(const string&);
    protected:
        std::string filename;
        std::ofstream stream;

    };
}

#endif	/* QUERYLOADOUTPUTSTREAM_HPP */