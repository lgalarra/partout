#ifndef SAMPLER_H_
#define SAMPLER_H_

//---------------------------------------------------------------------------
// DRDF-3X
// (c) 2011 Luis Galarraga
//
// This work is licensed under the Creative Commons
// Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
// or send a letter to Creative Commons, 171 Second Street, Suite 300,
// San Francisco, California, 94105, USA.
//---------------------------------------------------------------------------

using namespace std;

namespace qload{
    template <class InputStream, class OutputStream>
    class Sampler{
    public:
	Sampler();
	virtual int sample(InputStream &in, OutputStream &out, unsigned int sampleSize) = 0;
	virtual ~Sampler();
    };

}
#endif /*SAMPLER_H_*/
