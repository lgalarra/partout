#ifndef QUERYLOADEXTRACTOR_HPP
#define	QUERYLOADEXTRACTOR_HPP
/* 
 * File:   QueryLoadExtractor.hpp
 * Author: lgalarra
 *
 * Created on May 13, 2011, 4:46 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. * 
 */

#include <bitset>
#include "qload/QueryLoadRawInputStream.hpp"
#include "qload/QueryLoadOutputStream.hpp"

using namespace std;

namespace qload{
    class QueryLoadExtractor {
    public:
        QueryLoadExtractor();
        virtual ~QueryLoadExtractor();
        int extract(QueryLoadRawInputStream &in, QueryLoadOutputStream &out, bitset<5> features);
    private:

    };
}

#endif	/* QUERYLOADEXTRACTOR_HPP */

