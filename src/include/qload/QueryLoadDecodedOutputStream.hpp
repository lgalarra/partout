/* 
 * File:   QueryLoadDecodedOutputStream.hpp
 * Author: luis
 *
 * Created on June 15, 2011, 6:35 PM
 */

#ifndef QUERYLOADDECODEDOUTPUTSTREAM_HPP
#define	QUERYLOADDECODEDOUTPUTSTREAM_HPP

#include "qload/QueryLoadOutputStream.hpp"

using namespace qload;

namespace qload{
    class QueryLoadDecodedOutputStream: public QueryLoadOutputStream {
    public:
        QueryLoadDecodedOutputStream(string filename);
        virtual ~QueryLoadDecodedOutputStream();
        virtual bool put(const string &s);
    };
}

#endif	/* QUERYLOADDECODEDOUTPUTSTREAM_HPP */

