#ifndef QueryLoadDecodedInputStream_HPP
#define	QueryLoadDecodedInputStream_HPP

/* File:   QueryLoadDecodedInputStream.hpp
 * Author: lgalarra
 *
 * Created on May 13, 2011, 1:59 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 **/

#include <iostream>
#include <fstream>
#include <string>
#include "qload/QueryLoadInputStream.hpp"

using namespace std;

namespace qload{
    class QueryLoadDecodedInputStream: public QueryLoadInputStream{
    public:
        QueryLoadDecodedInputStream(string filename);
        virtual ~QueryLoadDecodedInputStream();
        virtual string get(unsigned int &);
        
    };
}

#endif	/* QueryLoadDecodedInputStream_HPP */
