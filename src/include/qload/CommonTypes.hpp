/* 
 * File:   newfile.hpp
 * Author: luis
 * Created on May 21, 2011, 9:22 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#ifndef NEWFILE_HPP
#define	NEWFILE_HPP
#include <utility>
#include <iostream>
#include "cts/parser/SPARQLParser.hpp"

using namespace std;

inline std::ostream& operator<<(std::ostream& str, const SPARQLParser::Element& e){
    string output;
    output += "Value: " + e.value;
    output += "Type: ";
    switch(e.type){
        case SPARQLParser::Element::IRI:
            output += "URI, " + e.value;
            break;
        case SPARQLParser::Element::Variable:    
            output += "Variable";
            break;            
        case SPARQLParser::Element::Literal:
            switch(e.subType){
                case SPARQLParser::Element::CustomType:
                    output += "Custom type (" + e.value + "^^" + e.subTypeValue + ")";
                    break;
                case SPARQLParser::Element::CustomLanguage:
                    output += "Custom type (" + e.value + "@" + e.subTypeValue + ")";
                    break;
                case SPARQLParser::Element::None:
                    output += "Literal (" + e.value + ")";
                    break;

            }
            break;
    }

    str << output;
    return str;
}    


namespace qload{

    typedef struct {
        vector<SPARQLParser::Pattern> patterns;
        vector<SPARQLParser::Filter> conditions;
    }SPARQLIndependentSubquery;
    
    typedef pair<double, SPARQLParser::Element> FrequentElementDescriptor;        

    inline std::ostream& operator<<(std::ostream& str, const qload::FrequentElementDescriptor& fd){
        str << "<" << fd.first << ", " << fd.second << ">";
        return str;
    }
            
}

#endif	/* NEWFILE_HPP */
