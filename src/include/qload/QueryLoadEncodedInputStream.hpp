/* 
 * File:   QueryLoadEncodedInputStream.hpp
 * Author: luis
 *
 * Created on June 15, 2011, 6:40 PM
 */

#ifndef QUERYLOADENCODEDINPUTSTREAM_HPP
#define	QUERYLOADENCODEDINPUTSTREAM_HPP

#include "qload/QueryLoadInputStream.hpp"

namespace qload{
    class QueryLoadEncodedInputStream: public QueryLoadInputStream {
    public:
        QueryLoadEncodedInputStream(string filename);
        virtual ~QueryLoadEncodedInputStream();
        //Returns the next query in the query load
        virtual string get(unsigned int &);
    };
}
#endif	/* QUERYLOADENCODEDINPUTSTREAM_HPP */

