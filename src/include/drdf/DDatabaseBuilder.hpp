/* 
 * File:   DDatabaseBuilder.hpp
 * Author: lgalarra
 *
 * Created on August 22, 2011, 5:33 PM
 */

#ifndef drdf_ddatabasebuilder_hpp
#define	drdf_ddatabasebuilder_hpp

#include "qload/CommonTypes.hpp"
#include "partitioning/CommonTypes.hpp"
#include "partitioning/Fragment.hpp"
#include "drdf/DDatabase.hpp"

using namespace stdpartitioning;

namespace drdf{
    class DDatabaseBuilder {        
    private:       
        //The out file
        DDatabase out;
        //The file name
        const char *file;   
        //Prefix for temporal files
        string tmpPrefix;
        
        
        /**
         * Executes a bunch of SQL instructions
         * @param sql
         * @return bool
         */
        bool execute(string sql);
        bool execute(string sql, int &rc);
        
    public:
        DDatabaseBuilder(const char *fileName, string &tmpPrefix);

        virtual ~DDatabaseBuilder();
        
        /**
         * Opens the underlying storage for future insertions
         */
        bool start();
        
        /**
         * Writes the id->string mappings for literals and URIs
         * 
         */
        bool writeDictionaryEntries(FragmentFilesAllocationMap&, map<string, drdf::Host>&, map<string, qload::FrequentElementDescriptor> &, Database &);
        
        /**
         * Writes the fragments definition in the distributed database file.
         */
        bool writePredicatesDefinition(vector<stdpartitioning::SPARQLPredicate> &);
        
        /**
         * Writes the host mappings: id -> host/file
         */
        bool writeHostMappings(FragmentFilesAllocationMap&, map<string, drdf::Host> &);

        /**
         * Writes the mappings id host -> predicate
         */
        bool writePredicateHostMappings(FragmentsAllocationMap&, map<string, drdf::Host> &);
        
        /**
         * Writes the information for the aggregated facts segments (projections over one and two columns)
         * for the triple patterns that appeared in the query load which are contained in the 
         * predicate dependency graph.
         * @param PredicateDependencyGraph: map patterns -> predicates
         */
        bool writeAggAndFullyAggFactsSegments(PredicateDependencyGraph &, stdpartitioning::DatabaseDescriptor &, FragmentFilesAllocationMap &, map<string, drdf::Host> &);
                
                
        /**
         * Writes the information about the exact statistics used by RDF-3x: histograms with join
         * cardinalities, frequent join paths and characteristic sets.
         */
        bool writeExactStatisticsSegment(Database &, PatternDependencyGraph &, PredicateDependencyGraph &);
        
        /**
         * Writes information about the sizes of segments, number of disk pages, etc.
         * @param 
         * @return 
         */
        bool writeSegmentsMetadata(Database &);
        
        /**
         * Closes the output database.
         */
        void close();

    };
}
#endif	/* DDATABASEBUILDER_HPP */

