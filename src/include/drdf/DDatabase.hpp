/* 
 * File:   DDatabase.hpp
 * Author: lgalarra
 *
 * Created on August 22, 2011, 4:01 PM
 */

#ifndef drdf_ddatabase_hpp
#define	drdf_ddatabase_hpp

#include <sqlite3.h>
#include <map>

#include "rts/database/Database.hpp"
#include "rts/segment/DictionarySegment.hpp"
#include "drdf/FragmentsDefinitionSegment.hpp"
#include "drdf/DDictionarySegment.hpp"
#include "drdf/DExactStatisticsSegment.hpp"
#include "drdf/DSegments.hpp"

using namespace std;

namespace drdf{
    
    class DDatabaseBuilder;
    
    /*
     * Class to handle a distributed database
     */
    class DDatabase : public Database{
    private:
        DFragmentDefinitionSegment *fragmentationSegment;
        DDictionarySegment *dictionary;
        DExactStatisticsSegment *statistics;
        sqlite3 *db;
        DDatabaseWrapper *dataWrapper;        
        map<unsigned, DSegment*> segments; 

        /**
         * Creates the metadata table
         * @return bool
         */

        bool createMetadataSegment();
        
        /**
         * Creates the schema structure to store the fragments definition:
         * predicates -> hosts
         * @return bool
         */
        bool createFragmentsDefinition();
        
        /**
         * Creates the schema structure to store the mappings: id -> strings
         * @return bool
         */
        bool createDictionarySegment();
        
        /**
         * Schema to store aggregated information
         * @return bool 
         */
        bool createAggregatedSegments();
        bool createAggregatedSegments2();
        
        /**
         * Schema to store statistical information about joins
         */
        bool createStatisticalSegments();
        
        /**
         * Instantiates all data segments
         */
        void loadSegments(DictionarySegment *surrogateDict = NULL);
        
        /**
         * Inits member objects
         */
        bool init(const char* fileName, bool readonly = false);    
    public:
        DDatabase(): Database(), dataWrapper(0), db(0), fragmentationSegment(0), dictionary(0), statistics(0){};
        ~DDatabase();
        
        bool create(const char *filename);
        bool open(const char *fileName, bool readonly = false);
        bool open(const char* fileName, DictionarySegment *surrogateDict = NULL, bool readonly = true);
        bool close();
        
        DFragmentDefinitionSegment& getFragmentsDefinitionSegment(){ return *fragmentationSegment; }
        /// Try to keep the interface as similar as possible
        DAggregatedFactsSegment& getAggregatedFacts(Database::DataOrder order);
        /// Get fully aggregated fcats
        DFullyAggregatedFactsSegment& getFullyAggregatedFacts(Database::DataOrder order);
        //Get the dictionary
        DDictionarySegment& getDictionary(){ return *dictionary; }
        /// Get the exact statistics
        DExactStatisticsSegment& getExactStatistics(){ return *statistics; }                
        //Get the dummy facts segment
        DFactsSegment& getFacts(Database::DataOrder order);
        
        DDatabaseWrapper *getDataWrapper(){ return dataWrapper; }


        friend class DDatabaseBuilder;
    };


}

#endif	/* DDATABASE_HPP */

