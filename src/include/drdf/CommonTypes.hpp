/* 
 * File:   CommonTypes.hpp
 * Author: luis
 * Created on May 22, 2011, 1:34 AM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 **/

#ifndef drdf_common_types
#define	drdf_common_types

#include <string>
#include <string.h>
#include <vector>
#include "infra/util/Type.hpp"

using namespace std;

namespace drdf{
    class DDatabase;
    class DDatabaseWrapper;
    
    struct DatabaseDescriptor{
        DDatabase *ddb;
        string filename;
    };
    
    struct Host{
        static unsigned hostId;

        unsigned id;
        // IP:port
        string uri;
        // Relative path to the database file
        string filename;

        Host(): uri("drdf3x://localhost:5678/"), filename(""), id(++hostId){};
        Host(string uri, string filename): uri(uri), filename(filename), id(++hostId){};
        Host(unsigned id, string uri, string filename): id(id), uri(uri), filename(filename){};
        bool operator==(const Host &other){
            return (this->id = other.id && this->uri.compare(other.uri) && this->filename.compare(other.filename) == 0);
        }
        
    };
    
    struct SPARQLArgument{
        int id_pred;
        int id_literal;
        string value;
        
        bool equivalent(const SPARQLArgument &);
    };
    
    struct SPARQLPredicate{
    public:
        int id;
        int position;
        int operator_;
        bool flag;
        vector<SPARQLArgument> args;        

        SPARQLPredicate(): wrapper(0){}
        void setDataWrapper(DDatabaseWrapper *wrapper){ this->wrapper = wrapper; }        
        bool contradicts(const SPARQLPredicate &);
    private:
        DDatabaseWrapper *wrapper; //Useful for lookup queries
        bool getType(unsigned, Type::ID &);        
        bool lookupSubtype(unsigned, unsigned &);
    };
    
    typedef vector<drdf::SPARQLPredicate> Fragment;
    
    //A tuple of 4 elements
    struct Quad{
        unsigned value1, value2, value3, value4;
        
        Quad(){}
        Quad(unsigned value1, unsigned value2, unsigned value3, unsigned value4): value1(value1), value2(value2), value3(value3), value4(value4){}
        
       inline bool operator==(const Quad &other) const{
           return value1 == other.value1 && value2 == other.value2 && value3 == other.value3 && value4 == other.value4;
       }
       
       inline bool operator<(const Quad &other) const{
           if(this->value1 < other.value1){
               return true;
           }else if(this->value1 == other.value1){
               if(this->value2 < other.value2){
                   return true;
               }else if(this->value2 == other.value2){
                   if(this->value3 < other.value3){
                       return true;
                   }else if(this->value3 == other.value3){
                       if(this->value4 < other.value4)
                           return true;
                       else
                           return false;
                   }else{
                       return false;
                   } 
               }else{
                   return false;
               }
           }else{
               return false;
           }
       }
       
       inline bool operator>(const Quad &other) const{
           return !(*this < other) && !(*this == other);
       }
       
       inline bool operator<=(const Quad &other) const{
           return (*this < other) || (*this == other);
       }
       
       inline bool operator>=(const Quad &other) const{
           return (*this > other) || (*this == other);
       }


    };
    
    /// A triple    
    struct Triple {
       unsigned value1,value2, value3;
              
       Triple(){}
       Triple(unsigned value1, unsigned value2, unsigned value3): value1(value1), value2(value2), value3(value3){}
       
       inline bool operator==(const Triple &other) const{
           return value1 == other.value1 && value2 == other.value2 && value3 == other.value3;
       };
       
       inline bool operator<(const Triple &other) const{
           if(this->value1 < other.value1){
               return true;
           }else if(this->value1 == other.value1){
               if(this->value2 < other.value2){
                   return true;
               }else if(this->value2 == other.value2){
                   if(this->value3 < other.value3){
                       return true;
                   }else{
                       return false;
                   } 
               }else{
                   return false;
               }
           }else{
               return false;
           }
           
       }
       
       inline bool operator>(const Triple &other) const{
           return !(*this < other) && !(*this == other);
       }
       
       inline bool operator<=(const Triple &other) const{
           return (*this < other) || (*this == other);
       }
       
       inline bool operator>=(const Triple &other) const{
           return (*this > other) || (*this == other);
       }
    };
    
    struct Pair{
        unsigned value1, value2;
        
        Pair(){}
        Pair(unsigned value1, unsigned value2): value1(value1), value2(value2){}

        inline bool operator==(const Pair &other) const{
            return value1 == other.value1 && value2 == other.value2;
        }

        inline bool operator<(const Pair &other) const{
           if(this->value1 < other.value1){
               return true;
           }else if(this->value1 == other.value1){
               if(this->value2 < other.value2){
                   return true;
               }else{
                   return false;
               }
           }else{
               return false;
           }
        }
       
        inline bool operator>(const Pair &other) const{
           return !(*this < other) && !(*this == other);
        }

        inline bool operator<=(const Pair &other) const{
           return (*this < other) || (*this == other);
        }

        inline bool operator>=(const Pair &other) const{
           return (*this > other) || (*this == other);
        }

        
    };
    
    
    class cmpPair{
    public:
       inline bool operator()(const Pair &a, const Pair&b) const{
           if(a.value1 < b.value1)
                return true;
            else if(a.value1 > b.value1){
                return false;
            }else{
                return a.value2 < b.value2;
            }
       }
    };
    
    class cmpTriple{
    public:
        bool operator()(const Triple &a, const Triple &b) const{
            if(a.value1 < b.value1){
                return true;
            }else if(a.value1 == b.value1){
                if(a.value2 < b.value2){
                    return true;
                }else if(a.value2 == b.value2){
                    if(a.value3 < b.value3){
                        return true;
                    }else{
                        return false;
                    } 
                }else{
                    return false;
                }
            }else{
                return false;
            }
        };        
    };
    
    class cmpQuad{
    public:
        bool operator()(const Quad &a, const Quad &b) const{
           if(a.value1 < b.value1){
               return true;
           }else if(a.value1 == b.value1){
               if(a.value2 < b.value2){
                   return true;
               }else if(a.value2 == b.value2){
                   if(a.value3 < b.value3){
                       return true;
                   }else if(a.value3 == b.value3){
                       if(a.value4 < b.value4)
                           return true;
                       else
                           return false;
                   }else{
                       return false;
                   } 
               }else{
                   return false;
               }
           }else{
               return false;
           }

        };        
    };

    
    class cmpHosts{
    public:
        bool operator()(const Host &a, const Host &b){
            return a.id < b.id;
        }
    };
    
    
    typedef vector<Triple> TripleSet;
    typedef vector<Pair> PairSet;

    struct DictionaryEntry{
        unsigned id;
        Type::ID type;
        unsigned subtype;
        string value;
    };
    
    struct StatisticsEntry{
        unsigned subject;
        unsigned predicate;
        unsigned object;
        unsigned long long s_s;
        unsigned long long s_p;
        unsigned long long s_o;
        unsigned long long p_s;
        unsigned long long p_p;
        unsigned long long p_o;
        unsigned long long o_s;
        unsigned long long o_p;
        unsigned long long o_o;
    };
    
}

#endif	/* COMMONTYPES_H */