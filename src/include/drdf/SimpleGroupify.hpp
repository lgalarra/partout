/* 
 * File:   SimpleGroupify.hpp
 * Author: luis
 *
 * Created on October 9, 2011, 5:38 PM
 */

#ifndef drdf_simplegroupify_hpp
#define	drdf_simplegroupify_hpp

#include "rts/operator/Operator.hpp"
#include "rts/runtime/Runtime.hpp"
#include <vector>
#include <map>

using namespace std;

class PlanPrinter;

namespace drdf{
    class DOperatorTransformer;
    class DOperator;
    class DRuntime;
    template <class Env>
    class DRDF3xClient;
}

class SimpleGroupify: public Operator {
public:
    SimpleGroupify(Operator* input,const std::vector<Register*>& values,double expectedOutputCardinality);
    ~SimpleGroupify();
   /// Produce the first tuple
   unsigned first();
   /// Produce the next tuple
   unsigned next();

   /// Print the operator tree. Debugging only.
   void print(PlanPrinter& out);
   /// Add a merge join hint
   void addMergeHint(Register* reg1,Register* reg2){};
   /// Register parts of the tree that can be executed asynchronous
   void getAsyncInputCandidates(Scheduler& scheduler){};
        
private:
    Operator *input;
    vector<Register*> values;
        
    typedef vector<pair<unsigned, Register*> > Tuple;
    
    class cmpTuple{
    public:
       bool operator()(const Tuple &a, const Tuple&b) const{
           for(unsigned i = 0; i < a.size(); ++i){
               if(a[i] < b[i])
                   return true;
               else if(a[i] > b[i])
                   return false;                   
           }
           
           //They are equal
           return false;
       }

    };
    
   map<Tuple, unsigned, cmpTuple> dataSource;
   map<Tuple, unsigned, cmpTuple>::iterator cursor;
   
   friend class drdf::DOperatorTransformer;
   friend class drdf::DOperator;
   friend class drdf::DRDF3xClient<drdf::DRuntime>;
   friend class drdf::DRDF3xClient<Runtime>;
    
    
};

#endif	/* SIMPLEGROUPIFY_HPP */

