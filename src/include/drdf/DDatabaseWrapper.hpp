/* 
 * File:   DDatabaseWrapper.hpp
 * Author: lgalarra
 *
 * Created on August 31, 2011, 1:18 PM
 */

#ifndef drdf_ddatabasewrapper_hpp
#define	drdf_ddatabasewrapper_hpp

#include <vector>
#include <string>
#include <map>
#include <sqlite3.h>

using namespace std;

namespace drdf{
    typedef map<string, string> Row; 
    
    class DDatabaseWrapper;
    
    class ResultSet{
    private:            
        DDatabaseWrapper *dp;
        unsigned nResults;
        vector<Row> table;
        vector<Row>::iterator cursor; 
        bool first;

        int processRow(char** data, char** columns);
        
        static int processRow(void *object, int nResults, char** data, char** columns){
            ResultSet *obj = (ResultSet*)object;
            obj->nResults = nResults;
            return obj->processRow(data, columns);
        };
        
    public:
        ResultSet(): dp(0), first(true){};        
        bool next();
        bool getInt(const string &, int &);
        bool getString(const string&, string &);
        bool getInt(const char*, int &);
        bool getString(const char*, string &);        
        unsigned getNResults(){ return nResults; }  
        
        friend class DDatabaseWrapper;
    };

    
    class DDatabaseWrapper{        
    public: 
        enum MetadataTag{            
            IS_CARD, AS_SP_G1, AS_SO_G1, AS_PS_G1, AS_PO_G1, AS_OS_G1, AS_OP_G1,
            AS_SP_G2, AS_SO_G2, AS_PS_G2, AS_PO_G2, AS_OS_G2, AS_OP_G2,
            FAS_S_G1, FAS_P_G1, FAS_O_G1, AS_SP_PAGES, AS_SO_PAGES, AS_PS_PAGES,
            AS_PO_PAGES, AS_OS_PAGES, AS_OP_PAGES, FAS_S_PAGES, FAS_P_PAGES, FAS_O_PAGES,
            IS_SPO_PAGES, IS_SOP_PAGES, IS_PSO_PAGES, IS_POS_PAGES, IS_OSP_PAGES, IS_OPS_PAGES, 
            IS_SPO_G1, IS_SOP_G1, IS_PSO_G1, IS_POS_G1, IS_OPS_G1, IS_OSP_G1,
            IS_SPO_G2, IS_SOP_G2, IS_PSO_G2, IS_POS_G2, IS_OPS_G2, IS_OSP_G2
        };
        
        static const int NULL_INT;
        static const string NULL_STRING;
    private:
        sqlite3 *db;
        std::map<MetadataTag, int> intMetadata;
                
    public:              
        static const int ARG_ERROR = -1;
        
        DDatabaseWrapper(sqlite3 *db): db(db){};        
        bool execute(const string &query, ResultSet &set);
        bool execute(const char *query, ResultSet &set);
        bool execute(const string &sql);
        bool execute(const char *sql);
        int getIntMetadata(MetadataTag key);
    };
}

#endif	/* DDATABASEWRAPPER_HPP */

