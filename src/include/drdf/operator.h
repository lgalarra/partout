/* 
 * File:   operator.h
 * Author: luis
 * Created on September 12, 2011, 11:41 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA.
 * 
 */

#ifndef drdf_operator_h
#define	drdf_operator_h

#include "rts/operator/Operator.hpp"
#include "rts/operator/AggregatedIndexScan.hpp"
#include "rts/operator/EmptyScan.hpp"
#include "rts/operator/Filter.hpp"
#include "rts/operator/FullyAggregatedIndexScan.hpp"
#include "rts/operator/HashGroupify.hpp"
#include "rts/operator/HashJoin.hpp"
#include "rts/operator/IndexScan.hpp"
#include "rts/operator/MergeJoin.hpp"
#include "rts/operator/MergeUnion.hpp"
#include "rts/operator/NestedLoopFilter.hpp"
#include "rts/operator/NestedLoopJoin.hpp"
#include "rts/operator/ResultsPrinter.hpp"
#include "rts/operator/Selection.hpp"
#include "rts/operator/SingletonScan.hpp"
#include "rts/operator/Sort.hpp"
#include "rts/operator/TableFunction.hpp"
#include "rts/operator/Union.hpp"

#include "drdf/SimpleGroupify.hpp"
#include "drdf/MultiMergeUnion.hpp"

#endif	/* OPERATOR_H */
