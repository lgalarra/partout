/* 
 * File:   DOperatorCostFunction.hpp
 * Author: luis
 *
 * Created on September 22, 2011, 10:23 PM
 */

#ifndef drdf_doperatorcostfunction_hpp
#define	drdf_doperatorcostfunction_hpp

#include "drdf/DOperator.hpp"
#include "drdf/DDatabase.hpp"


namespace drdf{
    class DOperatorCostFunction {
    public:
        /// Costs for a seek in 1/10ms
        static const unsigned seekCosts;
        /// Costs for a sequential page read in 1/10ms
        static const unsigned scanCosts;
        /// Number of CPU operations per 1/10ms
        static const unsigned cpuSpeed;
        /// Cost for sending and receiving a execRequest
        static const unsigned execRequestCost;
        /// Cost for sending and receiving a prepareForExecRequest
        static const unsigned prepareForExecRequestCost;
        /// Maximum size allowed for transmited pages
        static const unsigned remoteDataPageSize;

        static double evaluate(DOperator *, DDatabase &);
        
        static double evaluateCommunicationCost(DOperator *, DDatabase &);
        
        static double evaluateTotalCommunicationCost(DOperator *, DDatabase &);

    };
}
#endif	/* DOPERATORCOSTFUNCTION_HPP */