/* 
 * File:   DSegment.hpp
 * Author: lgalarra
 *
 * Created on August 31, 2011, 1:27 PM
 */

#ifndef drdf_dsegment_hpp
#define	drdf_dsegment_hpp

#include "drdf/DDatabaseWrapper.hpp"
#include "drdf/CommonTypes.hpp"
#include <vector>

using namespace std;

namespace drdf{
    enum ATag {          
      Tag_SP = 1,Tag_SO,Tag_OP,Tag_OS,Tag_PS,Tag_PO
   };
   
   enum FATag{
       Tag_S = 1, Tag_P, Tag_O
   };
    
    class DSegment{
    protected:
        DDatabaseWrapper &dwrapper;
        
        int pages;

        DSegment(DDatabaseWrapper &dataWrapper): dwrapper(dataWrapper), pages(-1){}
        
    public:
        
        int getPages(){ return this->pages; }
        
        virtual ~DSegment(){}
    };
}

#endif	/* DSEGMENT_HPP */

