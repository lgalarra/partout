/* 
 * File:   DRDF3xClient.hpp
 * Author: lgalarra
 *
 * Created on September 10, 2011, 5:54 PM
 */

#ifndef server_drdf3xclient_hpp
#define	server_drdf3xclient_hpp

#include <map>
#include <string>
#include <vector>
#include <string.h>
#include <iostream>
#include <queue>
#include <boost/thread.hpp>
#include <boost/thread/thread_time.hpp>
#include <typeinfo>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio.hpp>
#include <unistd.h>
#include <sys/utsname.h>
#include <exception>

#include "infra/util/Hash.hpp"
#include "infra/util/Type.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/Operator.hpp"

#include "drdf/CommonTypes.hpp"
#include "drdf/DOperatorTransformer.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/operator.h"
#include "server/RDF3xServerLookupMessages.h"
#include "server/RDF3xServerExecMessages.h"
#include "server/RDF3xServerMessages.h"

using boost::asio::ip::tcp;
using namespace std;
using namespace drdf::message;

namespace drdf{
    class DOperator;
    
    struct LookupRequest{
        unsigned id;
        string text;
        Type::ID type;
        unsigned subtype;
    };

    struct LookupResponse{
        unsigned id;
        string text;
        Type::ID type;
        unsigned subtype;      
        bool success;           
    };

    struct PrepExecutionResponse{
        bool success;            
        uint64_t reqid;
        uint64_t opid;
        string errorMsg;
    };

    
    /**
     * A simple synchronous client
     */
    template <class Env>
    class DRDF3xClient {        
    public:
        DRDF3xClient();
        DRDF3xClient(map<string, drdf::Host> &);
        DRDF3xClient(unsigned, map<string, drdf::Host> &);
        DRDF3xClient(unsigned);
        DRDF3xClient(unsigned, string &);
        DRDF3xClient(unsigned, map<string, drdf::Host> &, string &);
        ~DRDF3xClient();

        void dictionaryLookupById(string, vector<LookupRequest> &, vector<LookupResponse> &, DictionaryLookupByIdResponse::Status&);
        void dictionaryLookupByString(string, vector<LookupRequest> &, vector<LookupResponse> &, DictionaryLookupByStringResponse::Status &);        
        bool singleDictionaryLookupById(string, unsigned, ::Type::ID&, unsigned&, string& , DictionaryLookupByIdResponse::Status&);
        bool singleDictionaryLookupByString(string, string, ::Type::ID, unsigned, unsigned&, DictionaryLookupByStringResponse::Status &);        
        
        void broadcastSingleDictionaryLookupById(unsigned, ::Type::ID&, unsigned&, string& , map<string, DictionaryLookupByIdResponse::Status>&);
        void broadcastSingleDictionaryLookupByString(string, ::Type::ID, unsigned, unsigned&, map<string, DictionaryLookupByStringResponse::Status> &);
        void broadcastDictionaryLookupById(vector<LookupRequest> &, vector<LookupResponse> &, map<string, DictionaryLookupByIdResponse::Status> &);
        void broadcastDictionaryLookupByString(vector<LookupRequest> &, vector<LookupResponse> &, map<string, DictionaryLookupByStringResponse::Status> &);        
                
        bool prepareForExecution(string, DOperator *, Env &, PrepExecutionResponse &);           
        bool prepareForExecution(string, Operator *, Env &, PrepExecutionResponse &);     
        void explainDistributedPlan(DOperator *, Env &);
        
        bool exec(string, uint64_t, uint64_t, ExecResponse&);
        
    private:
        const static unsigned lookupRequestMaxSize;
        const static unsigned lookupResponseMaxSize;        
        const static unsigned prepareForExecRequestMaxSize;
        const static unsigned prepareForExecResponseMaxSize;
        const static unsigned execRequestMaxSize;
        const static unsigned execResponseMaxSize;
        const static unsigned headerSize;
        static unsigned predid;
        
        string senderName;
        
        bool connect(string &, tcp::socket *s);
        void createRequest(LookupRequest &, DictionaryLookupByIdRequest::SingleRequest &);
        void createRequest(LookupRequest&, DictionaryLookupByStringRequest::SingleRequest &);
        void createResponse(const DictionaryLookupByIdResponse::SingleResponse &, LookupResponse &);
        void createResponse(const DictionaryLookupByStringResponse::SingleResponse &, LookupResponse &);
        void createPrepareForExecRequest(DOperator *, PrepareForExecRequest &, Env &);

        unsigned generateId();
        string generateName();

        google::protobuf::uint64 getRegisterIndex(Runtime &, Register *);        
        google::protobuf::uint64 getRegisterIndex(DRuntime &, Register *);                
        void buildOperatorMessage(OperatorMessage *, DOperator *, Env &);
        
        void buildOperatorMessageFromIndexScan(OperatorMessage *, Operator *, Env &);
        void buildOperatorMessageFromAggregatedIndexScan(OperatorMessage *, Operator *, Env &);        
        void buildOperatorMessageFromFullyAggregatedIndexScan(OperatorMessage *, Operator *, Env &);
        void buildOperatorMessageFromMergeJoin(OperatorMessage *, Operator *, Env &);      
        void buildOperatorMessageFromSort(OperatorMessage*, Operator *, Env &);
        void buildOperatorMessageFromUnion(OperatorMessage*, Operator *, Env &);        
        void buildOperatorMessageFromHashGroupify(OperatorMessage*, Operator *, Env &);
        void buildOperatorMessageFromSimpleGroupify(OperatorMessage*, Operator *, Env &);        
        void buildOperatorMessageFromMergeUnion(OperatorMessage *, Operator *, Env &);
        void buildOperatorMessageFromMultiMergeUnion(OperatorMessage *, Operator *, Env &);
        void buildOperatorMessageFromHashJoin(OperatorMessage *, Operator *, Env &);
        void buildOperatorMessageFromSelection(OperatorMessage *, Operator *, Env &);
        
        OperatorMessage::PredicateMessage * buildPredicateMessage(Selection::Predicate *, OperatorMessage::PredicateTree *, Env &);
        OperatorMessage::PredicateType getPredicateType(Selection::Predicate *);
        void buildPredicateMessageFromBinaryPredicate(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &);
    	void buildPredicateMessageFromUnaryPredicate(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &runtime);
        void buildPredicateMessageFromVariable(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &);
        
        bool request(string &, RDF3xServerRemoteRequest &, DictionaryLookupByStringResponse &);        
        bool request(string &, RDF3xServerRemoteRequest &, DictionaryLookupByIdResponse &);        
        bool request(string &, RDF3xServerRemoteRequest &, PrepareForExecResponse &);        
        bool request(string &, RDF3xServerRemoteRequest &, ExecResponse &);        
        
        map<string, Host> servers;
        boost::asio::io_service *io_service;
        tcp::resolver *resolver;
        unsigned cid; //Client id

    };
    
    template <class Env>
    const unsigned DRDF3xClient<Env>::lookupRequestMaxSize = 8192; //8KB 
    template <class Env>
    const unsigned DRDF3xClient<Env>::lookupResponseMaxSize = 8192;        
    template <class Env>
    const unsigned DRDF3xClient<Env>::prepareForExecRequestMaxSize = 8192 * 4;
    template <class Env>
    const unsigned DRDF3xClient<Env>::prepareForExecResponseMaxSize = 1024;
    template <class Env>
    const unsigned DRDF3xClient<Env>::execRequestMaxSize  = 8192; // 32 KB
    template <class Env>
    const unsigned DRDF3xClient<Env>::execResponseMaxSize = 8 * 1024 * 1024; // 32 MB
    template <class Env>
    unsigned DRDF3xClient<Env>::predid = 0; // 32 MB
    template <class Env>
    const unsigned DRDF3xClient<Env>::headerSize = 5;


    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(): cid(generateId()), senderName(generateName()){
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);
    }
    
    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(map<string, drdf::Host> &hosts): cid(generateId()), senderName(generateName())
    , servers(hosts) {
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);
    }

    template <class Env>
    unsigned DRDF3xClient<Env>::generateId(){
       //Get a random id for the client
       struct utsname machineName;
       uname(&machineName);
       string fullname, hrname;
       hrname = this->generateName();
       fullname = hrname  + string(machineName.release);
       fullname += string(machineName.sysname) + string(machineName.version);
       return (((uint64_t)getpid()) << 32) | Hash::hash(fullname, getpid());
    }
    
    template <class Env>
    string DRDF3xClient<Env>::generateName(){
       struct utsname machineName;
       uname(&machineName);
       return string(machineName.domainname) + " " + string(machineName.machine) + " " + string(machineName.nodename);        
    }

    
    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(unsigned cid, map<string, drdf::Host> &servers): cid(cid), servers(servers){
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);
    }

    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(unsigned cid): cid(cid){
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);
    }
    
    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(unsigned cid, string &sender):cid(cid), senderName(sender){
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);        
    }

    template <class Env>
    DRDF3xClient<Env>::DRDF3xClient(unsigned cid, map<string, drdf::Host> &servers, string &sender):
    cid(cid), servers(servers), senderName(sender){
        this->io_service = new boost::asio::io_service;
        this->resolver = new tcp::resolver(*this->io_service);        
    }

    template <class Env>
    DRDF3xClient<Env>::~DRDF3xClient() {
        delete this->io_service;
        delete this->resolver;
    }

    template <class Env>
    bool DRDF3xClient<Env>::connect(string &serverUri, tcp::socket *s){
        //Create a socket
        boost::system::error_code error;
        size_t colonPosition = serverUri.rfind(':');
        //Get host and port
//        cout << serverUri << endl;
        string hostString = serverUri.substr(0, colonPosition);
        string portString = serverUri.substr(colonPosition + 1, string::npos);
        const char* host = hostString.c_str();
        const char* port = portString.c_str();
        assert(host != NULL);
        assert(port != NULL);
//        cout << "host " << host << " port " << port << endl;
        tcp::resolver::query query(tcp::v4(), host , port);
        tcp::resolver::iterator iterator = this->resolver->resolve(query);
        s->connect(*iterator, error);
        return error? false: true;
    }

    template <class Env>
    bool DRDF3xClient<Env>::request(string &server, RDF3xServerRemoteRequest &request, DictionaryLookupByStringResponse &response){
        tcp::socket s(*this->io_service);
        bool success = false;
        if(this->connect(server, &s)){
            //Time to write
            string content = request.SerializeAsString();
            size_t request_length = content.size();
            try{
                boost::asio::write(s, boost::asio::buffer(content.c_str(), request_length));
            }catch(std::exception &e){
                response.set_status(DictionaryLookupByStringResponse::ERROR);
                response.set_errormsg(e.what());                
		s.close();
                return false;
            }            
            char* responseString = new char[lookupResponseMaxSize];        
            boost::system::error_code error;
            size_t reply_length;
            
            try{
                reply_length = boost::asio::read(s, boost::asio::buffer(responseString, lookupResponseMaxSize), 
                            boost::asio::transfer_at_least(1), error);
            }catch(std::exception &e){
                response.set_status(DictionaryLookupByStringResponse::ERROR);     
                response.set_errormsg(e.what());                
                delete[] responseString;
                s.close();
                return false;
            }
            
            if(!error){
                response.ParseFromArray((const void*)responseString, reply_length);
                success = true;                
            }else{
                response.set_status(DictionaryLookupByStringResponse::ERROR);                
            }

            delete[] responseString;
        }
        //Close the connection
        s.close();    
        return success;
    }      

    template <class Env>
    bool DRDF3xClient<Env>::request(string &server, RDF3xServerRemoteRequest &request, DictionaryLookupByIdResponse &response){
        tcp::socket s(*this->io_service);
        bool success = false;
        if(this->connect(server, &s)){
            //Time to write
            string content = request.SerializeAsString();
            size_t request_length = content.size();
            try{
                boost::asio::write(s, boost::asio::buffer(content.c_str(), request_length));
            }catch(std::exception &e){
                response.set_status(DictionaryLookupByIdResponse::ERROR);                
                response.set_errormsg(e.what());                
		s.close();
                return false;
            }
            
            char* responseString = new char[lookupResponseMaxSize];        
            boost::system::error_code error;
            size_t reply_length;
            try{
                reply_length = boost::asio::read(s, boost::asio::buffer(responseString, lookupResponseMaxSize), 
                            boost::asio::transfer_at_least(1), error);
            }catch(std::exception &e){
                response.set_status(DictionaryLookupByIdResponse::ERROR);        
                response.set_errormsg(e.what());                
                delete[] responseString;
		s.close();
                return false;                
            }
            
            if(!error){
                response.ParseFromArray((const void*)responseString, reply_length);
                success = true;                
            }else{
                response.set_status(DictionaryLookupByIdResponse::ERROR);                
            }

            delete[] responseString;
        }
        //Close the connection
        s.close();    
        return success;
    }

    template <class Env>
    bool DRDF3xClient<Env>::request(string &server, RDF3xServerRemoteRequest &request, PrepareForExecResponse &response){
        tcp::socket s(*this->io_service);
        bool success = false;
        boost::system::error_code rerror, werror;        
        if(this->connect(server, &s)){
            //Time to write
            string content = request.SerializeAsString();
            size_t request_length = content.size();
            LengthMessage lmess;
            string lengthString;
            
            //Write the length of the message
            try{
                lmess.set_length(request_length);
                lengthString = lmess.SerializeAsString();
                boost::asio::write(s, boost::asio::buffer(lengthString.c_str(), lengthString.size()), boost::asio::transfer_all(), werror);
            }catch(std::exception &e){
                response.set_success(false);   
                response.set_errormsg(e.what());         
		s.close();       
                return false;                
            }
            
                        
            if(werror){
                response.set_success(false);
		s.close();
                return false;
            }
            
            try{
                boost::asio::write(s, boost::asio::buffer(content.c_str(), request_length), boost::asio::transfer_all(), werror);
            }catch(std::exception &e){
                response.set_success(false);   
                response.set_errormsg(e.what());                
                s.close();
                return false;                
            }
            
            if(werror){
                response.set_success(false);
                s.close();
                return false;
            }

            
            char* responseString = new char[prepareForExecResponseMaxSize];        
            size_t reply_length;
            
            try{
                reply_length = boost::asio::read(s, boost::asio::buffer(responseString, prepareForExecResponseMaxSize), 
                            boost::asio::transfer_at_least(1), rerror);
            }catch(std::exception &e){
                response.set_success(false);
                response.set_errormsg(e.what());
                delete[] responseString;
		s.close();
                return false;                                
            }
            
            if(!rerror){
                response.ParseFromArray((const void*)responseString, reply_length);
                success = true;                
            }else{
                response.set_success(false);                
                s.close();
            }

            delete[] responseString;
        }
        //Close the connection
        s.close();    
        return success;
    }        

    template <class Env>
    bool DRDF3xClient<Env>::request(string &server, RDF3xServerRemoteRequest &request, ExecResponse &response){
        tcp::socket s(*this->io_service);
        bool success = false;
        boost::system::error_code rerror, werror;        
        if(this->connect(server, &s)){
            //Time to write
            string content = request.SerializeAsString();
            size_t request_length = content.size();            
            string lengthString;
                        
            //Write the length of the message
            try{
                LengthMessage lwmess;
                lwmess.set_length(request_length);
                lengthString = lwmess.SerializeAsString();
                boost::asio::write(s, boost::asio::buffer(lengthString.c_str(), lengthString.size()), boost::asio::transfer_all(), werror);
            }catch(std::exception &e){
                response.set_status(ExecResponse::ERROR);   
                response.set_errormsg(e.what());         
		s.close();       
                return false;                
            }

            if(werror){
                response.set_status(ExecResponse::ERROR);
		s.close();
                return false;
            }

            
            try{
                boost::asio::write(s, boost::asio::buffer(content.c_str(), request_length), boost::asio::transfer_all(), werror);
            }catch(std::exception &e){
                response.set_status(ExecResponse::ERROR);
                response.set_errormsg(e.what());
		s.close();
                return false;                                
            }
            
            if(werror){
                response.set_status(ExecResponse::ERROR);
		s.close();
                return false;                                
            }

            char *sizeMessage = new char[headerSize];
            LengthMessage lmess;            
            try{
                size_t br = boost::asio::read(s, boost::asio::buffer(sizeMessage, headerSize), boost::asio::transfer_all(), rerror);
            }catch(std::exception &e){
                response.set_status(ExecResponse::ERROR);
                response.set_errormsg(e.what());
		s.close();
                return false;                                
            }
            
            if(rerror){
                response.set_status(ExecResponse::ERROR);
		s.close();	
                return false;                                
            }

            lmess.ParseFromArray((void*)sizeMessage, headerSize);            
            uint64_t bufferSize = lmess.length();
            char* responseString = new char[bufferSize];        
            size_t reply_length;            
            
            delete[] sizeMessage;
            
            try{
                reply_length = boost::asio::read(s, boost::asio::buffer(responseString, bufferSize), 
                            boost::asio::transfer_all(), rerror);
            }catch(std::exception &e){
                response.set_status(ExecResponse::ERROR);
                response.set_errormsg(e.what());
                delete[] responseString;
		s.close();
                return false;                                                
            }
            
            if(!rerror){
                response.ParseFromArray((const void*)responseString, reply_length);
                success = true;                
            }else{
                response.set_status(ExecResponse::ERROR);                
                s.close();
            }
            
            delete[] responseString;
        }
        //Close the connection
        s.close();    
        return success;
    }  

    template <class Env>
    bool DRDF3xClient<Env>::singleDictionaryLookupById(string server, unsigned id, ::Type::ID &type, unsigned &subtype, string &text, DictionaryLookupByIdResponse::Status &status){
        LookupRequest req;
        RDF3xServerRemoteRequest request;
        DictionaryLookupByIdResponse response;

        request.set_cid(static_cast<google::protobuf::uint64>(this->cid ));
        DictionaryLookupByIdRequest *out = request.mutable_lookupidrequest();
        DictionaryLookupByIdRequest::SingleRequest *singleReq = out->add_requests();
        req.id = id;
        this->createRequest(req, *singleReq);
        bool result = this->request(server, request, response);
        status = response.status();
        response.PrintDebugString();
        if(status == DictionaryLookupByIdResponse::SUCCESS){
            id = static_cast<unsigned>(response.responses(0).lid());
            type = static_cast<Type::ID>(response.responses(0).type());
            subtype = static_cast<unsigned>(response.responses(0).subtype());
            text = response.responses(0).result();
            result = false;
        }

        return result;
    }

    template <class Env>
    bool DRDF3xClient<Env>::singleDictionaryLookupByString(string server, string text, ::Type::ID type, unsigned subtype, unsigned &id, DictionaryLookupByStringResponse::Status &status){
        LookupRequest req;
        RDF3xServerRemoteRequest request;
        DictionaryLookupByStringResponse response;

        request.set_cid(static_cast<google::protobuf::uint64>(this->cid ));
        DictionaryLookupByStringRequest *out = request.mutable_lookupstringrequest();       
        DictionaryLookupByStringRequest::SingleRequest *singleReq = out->add_requests();
        req.text = text;
        req.type = type;
        req.subtype = subtype;

        this->createRequest(req, *singleReq);    
        request.PrintDebugString();
        bool result = this->request(server, request, response);   
        status = response.status();
        response.PrintDebugString();
        if(status == DictionaryLookupByStringResponse::SUCCESS){
            id = static_cast<unsigned>(response.responses(0).result());
            result = false;
        }

        return result;    
    }

    template <class Env>
    void DRDF3xClient<Env>::dictionaryLookupById(string server, vector<LookupRequest> &requests, vector<LookupResponse> &responses, DictionaryLookupByIdResponse::Status &status){
        vector<LookupRequest>::iterator it;
        RDF3xServerRemoteRequest request;  
        DictionaryLookupByIdResponse response;
        DictionaryLookupByIdRequest *out = request.mutable_lookupidrequest();
        for(it = requests.begin(); it != requests.end(); ++it){
            DictionaryLookupByIdRequest::SingleRequest *singleReq = out->add_requests();
            this->createRequest(*it, *singleReq);        
        }

        request.PrintDebugString();
        this->request(server, request, response);
        response.PrintDebugString();

        for(int i = 0; i < response.responses_size(); ++i){
            const DictionaryLookupByIdResponse::SingleResponse &singleResponse = response.responses(i);
            LookupResponse lookupSingleResponse;
            this->createResponse(singleResponse, lookupSingleResponse);
            responses.push_back(lookupSingleResponse);
        }

        status = response.status();
    }

    template <class Env>
    void DRDF3xClient<Env>::createRequest(LookupRequest &in, DictionaryLookupByIdRequest::SingleRequest &out){
        out.set_lid(static_cast<google::protobuf::uint64>(in.id));    
    }

    template <class Env>
    void DRDF3xClient<Env>::createRequest(LookupRequest &in, DictionaryLookupByStringRequest::SingleRequest &out){
        out.set_literal(in.text);
        out.set_subtype(static_cast<google::protobuf::uint64>(in.subtype));
        out.set_type(static_cast<drdf::message::EntityType>(in.type));    
    }

    template <class Env>
    void DRDF3xClient<Env>::createResponse(const DictionaryLookupByIdResponse::SingleResponse &in, LookupResponse &out){
        out.success = in.success();
        if(out.success){
            if(in.has_lid()) out.id = static_cast<unsigned>(in.lid());
            if(in.has_subtype()) out.subtype = static_cast<unsigned>(in.subtype());
            if(in.has_result()) out.text = in.result();
            if(in.has_type()) out.type = static_cast<Type::ID>(in.type());
        }
    }

    template <class Env>
    void DRDF3xClient<Env>::createResponse(const DictionaryLookupByStringResponse::SingleResponse &in, LookupResponse &out){
        out.success = in.success();
        if(out.success){
            if(in.has_result()) out.id = static_cast<unsigned>(in.result());
            if(in.has_subtype()) out.subtype = static_cast<unsigned>(in.subtype());
            if(in.has_literal()) out.text = in.literal();
            if(in.has_type()) out.type = static_cast<Type::ID>(in.type());
        }
    }

    template <class Env>
    void DRDF3xClient<Env>::dictionaryLookupByString(string server, vector<LookupRequest> &requests, vector<LookupResponse> &responses, DictionaryLookupByStringResponse::Status &status){
        vector<LookupRequest>::iterator it;
        RDF3xServerRemoteRequest request;  
        DictionaryLookupByStringResponse response;
        DictionaryLookupByStringRequest *out = request.mutable_lookupstringrequest();
        for(it = requests.begin(); it != requests.end(); ++it){
            DictionaryLookupByStringRequest::SingleRequest *singleReq = out->add_requests();
            this->createRequest(*it, *singleReq);        
        }

        request.PrintDebugString();
        this->request(server, request, response);
        response.PrintDebugString();

        for(int i = 0; i < response.responses_size(); ++i){
            const DictionaryLookupByStringResponse::SingleResponse &singleResponse = response.responses(i);
            LookupResponse lookupSingleResponse;
            this->createResponse(singleResponse, lookupSingleResponse);
            responses.push_back(lookupSingleResponse);
        }

        status = response.status();
    }

    template <class Env>
    void DRDF3xClient<Env>::broadcastSingleDictionaryLookupById(unsigned id, ::Type::ID &type, unsigned &subtype, string &text , map<string, DictionaryLookupByIdResponse::Status> &statusMsgs){
        boost::thread **works = new boost::thread*[this->servers.size()];           
        unsigned i = 0;    

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            statusMsgs[sit->first] = DictionaryLookupByIdResponse::ERROR;        
        }


        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            works[i] = new boost::thread(boost::bind(&DRDF3xClient<Env>::singleDictionaryLookupById, this, sit->first, id, type, subtype, text, statusMsgs.at(sit->first)));
            ++i;
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            works[j]->join();
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            delete works[j];
        }

        delete[] works;
    }

    template <class Env>
    void DRDF3xClient<Env>::broadcastSingleDictionaryLookupByString(string key, ::Type::ID type, unsigned subtype, unsigned &id, map<string, DictionaryLookupByStringResponse::Status> &statusMsgs){
        boost::thread **works = new boost::thread*[this->servers.size()];    

        unsigned i = 0;    
        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            statusMsgs[sit->first] = DictionaryLookupByStringResponse::ERROR;        
        }

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            works[i] = new boost::thread(boost::bind(&DRDF3xClient<Env>::singleDictionaryLookupByString, this, sit->first, key, type, subtype, id, statusMsgs.at(sit->first)));
            ++i;
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            works[j]->join();
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            delete works[j];
        }

        delete[] works;
    }

    template <class Env>
    void DRDF3xClient<Env>::broadcastDictionaryLookupById(vector<LookupRequest> &requests, vector<LookupResponse> &responses, map<string, DictionaryLookupByIdResponse::Status>&statusMsgs){
        boost::thread **works = new boost::thread*[this->servers.size()];    

        unsigned i = 0;    
        vector<vector<LookupResponse> > output;

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            statusMsgs[sit->first] = DictionaryLookupByIdResponse::ERROR;                
        }

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            vector<LookupResponse> partialOutput;
            output.push_back(partialOutput);
            works[i] = new boost::thread(boost::bind(&DRDF3xClient<Env>::dictionaryLookupById, this, sit->first, requests, output.back(), statusMsgs.at(sit->first)));
            ++i;
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            works[j]->join();
        }

        //Merge the responses
        for(vector<vector<LookupResponse> >::iterator vit = output.begin(); vit != output.end(); ++vit){
            for(vector<LookupResponse>::iterator vitit = vit->begin(); vitit != vit->end(); ++vitit){
                responses.push_back(*vitit);
            }
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            delete works[j];
        }

        delete[] works;

    }

    template <class Env>
    void DRDF3xClient<Env>::broadcastDictionaryLookupByString(vector<LookupRequest> &requests, vector<LookupResponse> &responses, map<string, DictionaryLookupByStringResponse::Status> &statusMsgs){
        boost::thread **works = new boost::thread*[this->servers.size()];    

        unsigned i = 0;    
        vector<vector<LookupResponse> > output;

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            statusMsgs[sit->first] = DictionaryLookupByStringResponse::ERROR;                
        }

        for(map<string, Host>::iterator sit = this->servers.begin(); sit != this->servers.end(); ++sit){
            vector<LookupResponse> partialOutput;
            output.push_back(partialOutput);
            works[i] = new boost::thread(boost::bind(&DRDF3xClient<Env>::dictionaryLookupByString, this, sit->first, requests, output.back(), statusMsgs.at(sit->first)));
            ++i;
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            works[j]->join();
        }

        //Merge the responses
        for(vector<vector<LookupResponse> >::iterator vit = output.begin(); vit != output.end(); ++vit){
            for(vector<LookupResponse>::iterator vitit = vit->begin(); vitit != vit->end(); ++vitit){
                responses.push_back(*vitit);
            }
        }

        for(unsigned j = 0; j < this->servers.size(); ++j){
            delete works[j];
        }

        delete[] works;

    }

    template <class Env>
    bool DRDF3xClient<Env>::prepareForExecution(string server, DOperator *op, Env &runtime, PrepExecutionResponse &resp){
        RDF3xServerRemoteRequest request;    
        PrepareForExecResponse response;
        request.set_cid(static_cast<google::protobuf::uint64>(this->cid));
        PrepareForExecRequest *execRequest = request.mutable_prepforexrequest();
        this->createPrepareForExecRequest(op, *execRequest, runtime);
		execRequest->PrintDebugString();
        this->request(server, request, response);

        resp.errorMsg = response.errormsg();
        resp.reqid = response.reqid();
        resp.success = response.success();    
        resp.opid = response.opid();

        return response.success();
    }
    
    template <class Env>
    void DRDF3xClient<Env>::explainDistributedPlan(DOperator *op, Env &runtime){
        RDF3xServerRemoteRequest request;    
        request.set_cid(static_cast<google::protobuf::uint64>(this->cid));
        PrepareForExecRequest *execRequest = request.mutable_prepforexrequest();
        this->createPrepareForExecRequest(op, *execRequest, runtime);
        execRequest->PrintDebugString();
    }

    template <class Env>
    bool DRDF3xClient<Env>::exec(string server, uint64_t reqid, uint64_t opid, ExecResponse &response){
        RDF3xServerRemoteRequest request;    
        request.set_cid(static_cast<google::protobuf::uint64>(this->cid));
        ExecRequest *execRequest = request.mutable_execrequest();
        execRequest->set_prepareforexecid(reqid);
        execRequest->set_opid(opid);
        this->request(server, request, response);
        return response.status() == ExecResponse::OK;
    }

    template <class Env>
    bool DRDF3xClient<Env>::prepareForExecution(string server, Operator *op, Env &runtime, PrepExecutionResponse &resp){
        DOperatorTransformer transformer;
        DOperator *dop = transformer.buildDOperator(op);
        bool result = this->prepareForExecution(server, dop, runtime, resp);
        delete dop;
        return result;

    }

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromIndexScan(OperatorMessage *opmsg, Operator *op, Env &runtime){
        IndexScan *is = static_cast<IndexScan*>(op);

        opmsg->set_dataorder(static_cast<OperatorMessage::DataOrder>(is->order));
        opmsg->set_type(OperatorMessage::IndexScan);    

        if(is->bound1){
            if(is->order == Database::Order_Subject_Predicate_Object || is->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(is->value1->value));
            }else if(is->order == Database::Order_Predicate_Object_Subject || is->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(is->value1->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(is->value1->value));
            }
        }

        if(is->bound2){
            if(is->order == Database::Order_Object_Subject_Predicate || is->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(is->value2->value));
            }else if(is->order == Database::Order_Subject_Predicate_Object || is->order == Database::Order_Object_Predicate_Subject){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(is->value2->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(is->value2->value));
            }
        }

        if(is->bound3){
            if(is->order == Database::Order_Predicate_Object_Subject || is->order == Database::Order_Object_Predicate_Subject){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(is->value3->value));
            }else if(is->order == Database::Order_Object_Subject_Predicate || is->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(is->value3->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(is->value3->value));
            }
        }

        if(is->value1){
            if(is->order == Database::Order_Subject_Predicate_Object || is->order == Database::Order_Subject_Object_Predicate){
                 opmsg->set_regsubject(this->getRegisterIndex(runtime, is->value1));
            }else if(is->order == Database::Order_Predicate_Object_Subject || is->order == Database::Order_Predicate_Subject_Object){
                 opmsg->set_regpredicate(this->getRegisterIndex(runtime, is->value1));
            }else{
                 opmsg->set_regobject(this->getRegisterIndex(runtime, is->value1));
            }
        }

        if(is->value2){
            if(is->order == Database::Order_Object_Subject_Predicate || is->order == Database::Order_Predicate_Subject_Object){
                 opmsg->set_regsubject(this->getRegisterIndex(runtime, is->value2));
            }else if(is->order == Database::Order_Subject_Predicate_Object || is->order == Database::Order_Object_Predicate_Subject){
                 opmsg->set_regpredicate(this->getRegisterIndex(runtime, is->value2));
            }else{
                 opmsg->set_regobject(this->getRegisterIndex(runtime, is->value2));
            }
        }

        if(is->value3){
            if(is->order == Database::Order_Predicate_Object_Subject || is->order == Database::Order_Object_Predicate_Subject){
                 opmsg->set_regsubject(this->getRegisterIndex(runtime, is->value3));
            }else if(is->order == Database::Order_Object_Subject_Predicate || is->order == Database::Order_Subject_Object_Predicate){
                 opmsg->set_regpredicate(this->getRegisterIndex(runtime, is->value3));
            }else{
                 opmsg->set_regobject(this->getRegisterIndex(runtime, is->value3));
            }        
        }

    }

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromAggregatedIndexScan(OperatorMessage *opmsg, Operator *op, Env &runtime){
        AggregatedIndexScan *ais = static_cast<AggregatedIndexScan*>(op);

        opmsg->set_dataorder(static_cast<OperatorMessage::DataOrder>(ais->order));
        opmsg->set_type(OperatorMessage::AggregatedIndexScan);    

        if(ais->bound1){
            assert(ais->value1); //This register should be known
            if(ais->order == Database::Order_Subject_Predicate_Object || ais->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(ais->value1->value));
            }else if(ais->order == Database::Order_Predicate_Object_Subject || ais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(ais->value1->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(ais->value1->value));
            }
        }

        if(ais->bound2){
            if(ais->order == Database::Order_Object_Subject_Predicate || ais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(ais->value2->value));
            }else if(ais->order == Database::Order_Subject_Predicate_Object || ais->order == Database::Order_Object_Predicate_Subject){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(ais->value2->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(ais->value2->value));
            }
        }

        if(ais->value1){
            if(ais->order == Database::Order_Subject_Predicate_Object || ais->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_regsubject(this->getRegisterIndex(runtime, ais->value1));
            }else if(ais->order == Database::Order_Predicate_Object_Subject || ais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_regpredicate(static_cast<google::protobuf::uint64>(this->getRegisterIndex(runtime, ais->value1)));
            }else{
                opmsg->set_regobject(static_cast<google::protobuf::uint64>(this->getRegisterIndex(runtime, ais->value1)));
            }
        }

        if(ais->value2){
            if(ais->order == Database::Order_Object_Subject_Predicate || ais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_regsubject(static_cast<google::protobuf::uint64>(this->getRegisterIndex(runtime, ais->value2)));
            }else if(ais->order == Database::Order_Subject_Predicate_Object || ais->order == Database::Order_Object_Predicate_Subject){
                opmsg->set_regpredicate(static_cast<google::protobuf::uint64>(this->getRegisterIndex(runtime, ais->value2)));
            }else{
                opmsg->set_regobject(static_cast<google::protobuf::uint64>(this->getRegisterIndex(runtime, ais->value2)));
            }
        }



    }

    template <class Env>
    google::protobuf::uint64 DRDF3xClient<Env>::getRegisterIndex(Runtime &runtime, Register *reg){
        Register *start = runtime.getRegister(0);
        //The register cannot be outside the bounds of the runtime
        assert(reg >= start && reg <= runtime.getRegister(runtime.getRegisterCount() - 1));
        uint64_t offset = reinterpret_cast<google::protobuf::uint64>((void*)(reg - start));
        return offset;
    }

    template <class Env>
    google::protobuf::uint64 DRDF3xClient<Env>::getRegisterIndex(DRuntime &runtime, Register *reg){
        return runtime.registerOffset(reg);
    }


    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromFullyAggregatedIndexScan(OperatorMessage *opmsg, Operator *op, Env &runtime){
        FullyAggregatedIndexScan *fais = static_cast<FullyAggregatedIndexScan*>(op);

        opmsg->set_dataorder(static_cast<OperatorMessage::DataOrder>(fais->order));
        opmsg->set_type(OperatorMessage::FullyAggregatedIndexScan);    

        //There is no option actually
        if(fais->bound1){
             if(fais->order == Database::Order_Subject_Predicate_Object || fais->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_subjectbound(static_cast<google::protobuf::uint64>(fais->value1->value));
            }else if(fais->order == Database::Order_Predicate_Object_Subject || fais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_predicatebound(static_cast<google::protobuf::uint64>(fais->value1->value));
            }else{
                opmsg->set_objectbound(static_cast<google::protobuf::uint64>(fais->value1->value));
            }
        }

        if(fais->value1){
            if(fais->order == Database::Order_Subject_Predicate_Object || fais->order == Database::Order_Subject_Object_Predicate){
                opmsg->set_regsubject(this->getRegisterIndex(runtime, fais->value1));            
            }else if(fais->order == Database::Order_Predicate_Object_Subject || fais->order == Database::Order_Predicate_Subject_Object){
                opmsg->set_regpredicate(this->getRegisterIndex(runtime, fais->value1));
            }else{
                opmsg->set_regobject(this->getRegisterIndex(runtime, fais->value1));
            }
        }

    }

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromSort(OperatorMessage *opmsg, Operator *op, Env &runtime){
        Sort *sort = static_cast<Sort*>(op);

        opmsg->set_type(OperatorMessage::Sort);

        //The registers to sort
        google::protobuf::RepeatedField<google::protobuf::uint64> *regToSort = opmsg->mutable_regvalues();
        for(vector<Register*>::iterator ltit = sort->values.begin(); ltit != sort->values.end(); ++ltit){
            regToSort->Add(this->getRegisterIndex(runtime, *ltit));
        }

        //The orders of the registers
        google::protobuf::RepeatedField<google::protobuf::uint64> *orderSlots = opmsg->mutable_orderreqisters();
        google::protobuf::RepeatedField<bool> *descending = opmsg->mutable_orderflags();
        for(vector<Sort::Order>::iterator orit = sort->order.begin(); orit != sort->order.end(); ++orit){
            orderSlots->Add(orit->slot);
            descending->Add(orit->descending);
        }    
    }
    
    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromHashGroupify(OperatorMessage *opmsg, Operator *op, Env &runtime){
        HashGroupify *hg = static_cast<HashGroupify*>(op);
        
        opmsg->set_type(OperatorMessage::HashGroupify);
        
        for(vector<Register*>::iterator rit = hg->values.begin(); rit != hg->values.end(); ++rit){
            google::protobuf::RepeatedField<google::protobuf::uint64> *regs = opmsg->mutable_regshashjoin();
            regs->Add(this->getRegisterIndex(runtime, *rit));
        }
    }        
    
    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromSimpleGroupify(OperatorMessage *opmsg, Operator *op, Env &runtime){
        SimpleGroupify *sg = static_cast<SimpleGroupify*>(op);
        
        opmsg->set_type(OperatorMessage::SimpleGroupify);
        
        for(vector<Register*>::iterator rit = sg->values.begin(); rit != sg->values.end(); ++rit){
            google::protobuf::RepeatedField<google::protobuf::uint64> *regs = opmsg->mutable_regshashjoin();
            regs->Add(this->getRegisterIndex(runtime, *rit));
        }
    }        


    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromUnion(OperatorMessage *opmsg, Operator *op, Env &runtime){
        Union *un = static_cast<Union*>(op);

        opmsg->set_type(OperatorMessage::Union);
        
        //The initialization registers
        google::protobuf::RepeatedPtrField<OperatorMessage::UnionInitialization> *inits = opmsg->mutable_unioninits();
        for(vector<vector<Register*> >::iterator initIt = un->initializations.begin(); initIt != un->initializations.end(); ++initIt){
            OperatorMessage::UnionInitialization *newInit = inits->Add();
            for(vector<Register*>::iterator regit = initIt->begin(); regit != initIt->end(); ++regit){
                newInit->add_registers(this->getRegisterIndex(runtime, *regit));
            }
        }

        google::protobuf::RepeatedPtrField<OperatorMessage::UnionMapping> *mappings = opmsg->mutable_unionmappings();
        for(vector<vector<Register*> >::iterator mit = un->mappings.begin(); mit != un->mappings.end(); ++mit){
            OperatorMessage::UnionMapping *newMap = mappings->Add();
            for(vector<Register*>::iterator regit = mit->begin(); regit != mit->end(); ++regit){
                newMap->add_registers(this->getRegisterIndex(runtime, *regit));
            }

        }

    }

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromMergeUnion(OperatorMessage *opmsg, Operator *op, Env &runtime){
        MergeUnion *mu = static_cast<MergeUnion*>(op);
        //Set the type
        opmsg->set_type(OperatorMessage::MergeUnion);

        //Set the registers offsets
        opmsg->set_regmuleft(this->getRegisterIndex(runtime, mu->leftReg));
        opmsg->set_regmuright(this->getRegisterIndex(runtime, mu->rightReg));
        opmsg->set_regmuresult(this->getRegisterIndex(runtime, mu->result));

    }
    
    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromMergeJoin(OperatorMessage *opmsg, Operator *op, Env &runtime){
        MergeJoin *mj = static_cast<MergeJoin*>(op);
        //Set the type
        opmsg->set_type(OperatorMessage::MergeJoin);

        //Set the registers offsets
        opmsg->set_regleftvalue(this->getRegisterIndex(runtime, mj->leftValue));
        opmsg->set_regrightvalue(this->getRegisterIndex(runtime, mj->rightValue));

        //Set the extra bindings offsets
        google::protobuf::RepeatedField<google::protobuf::uint64> *leftTail = opmsg->mutable_regslefttail();
        for(vector<Register*>::iterator ltit = mj->leftTail.begin(); ltit != mj->leftTail.end(); ++ltit){
            leftTail->Add(this->getRegisterIndex(runtime, *ltit));
        }

        google::protobuf::RepeatedField<google::protobuf::uint64> *rightTail = opmsg->mutable_regsrighttail();
        for(vector<Register*>::iterator rtit = mj->rightTail.begin(); rtit != mj->rightTail.end(); ++rtit){
            rightTail->Add(this->getRegisterIndex(runtime, *rtit));
        }
    }

    

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromHashJoin(OperatorMessage *opmsg, Operator *op, Env &runtime){
        HashJoin *hj = static_cast<HashJoin*>(op);
        //Set the type
        opmsg->set_type(OperatorMessage::HashJoin);

        //Set the registers offsets
        opmsg->set_regleftvalue(this->getRegisterIndex(runtime, hj->leftValue));
        opmsg->set_regrightvalue(this->getRegisterIndex(runtime, hj->rightValue));

        //Set the extra bindings offsets
        google::protobuf::RepeatedField<google::protobuf::uint64> *leftTail = opmsg->mutable_regslefttail();
        for(vector<Register*>::iterator ltit = hj->leftTail.begin(); ltit != hj->leftTail.end(); ++ltit){
            leftTail->Add(this->getRegisterIndex(runtime, *ltit));
        }

        google::protobuf::RepeatedField<google::protobuf::uint64> *rightTail = opmsg->mutable_regsrighttail();
        for(vector<Register*>::iterator rtit = hj->rightTail.begin(); rtit != hj->rightTail.end(); ++rtit){
            rightTail->Add(this->getRegisterIndex(runtime, *rtit));
        }
        
        opmsg->set_probepriority(hj->probePriority);
        opmsg->set_hashpriority(hj->hashPriority);

    }
    
    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromMultiMergeUnion(OperatorMessage *opmsg, Operator *op, Env &runtime){
        MultiMergeUnion *mmu = static_cast<MultiMergeUnion*>(op);
        //Set the type
        opmsg->set_type(OperatorMessage::MultiMergeUnion);

        google::protobuf::RepeatedField<google::protobuf::uint64> *result = opmsg->mutable_regmmuresult();
                
        //Set the registers offsets
        for(vector<Register*>::iterator rit = mmu->result.begin(); rit != mmu->result.end(); ++rit)
            result->Add(this->getRegisterIndex(runtime, *rit));

        google::protobuf::RepeatedField<google::protobuf::uint64> *regLeft = opmsg->mutable_regmmuleft();
        
        for(vector<Register*>::iterator rit = mmu->leftReg.begin(); rit != mmu->leftReg.end(); ++rit)            
            regLeft->Add(this->getRegisterIndex(runtime, *rit));

        google::protobuf::RepeatedField<google::protobuf::uint64> *regRight = opmsg->mutable_regmmuright();
        
        for(vector<Register*>::iterator rit = mmu->rightReg.begin(); rit != mmu->rightReg.end(); ++rit)
            regRight->Add(this->getRegisterIndex(runtime, *rit));
    }
    
    template <class Env>
    OperatorMessage::PredicateMessage * DRDF3xClient<Env>::buildPredicateMessage(Selection::Predicate *pred, OperatorMessage::PredicateTree *predmsg, Env &runtime){
        OperatorMessage::PredicateMessage *newPredMsg = predmsg->mutable_tree()->Add();
        OperatorMessage::PredicateType predType = this->getPredicateType(pred);
        newPredMsg->set_type(predType);
        newPredMsg->set_pid(++predid);        
        switch(predType){
            case OperatorMessage::Equal: case OperatorMessage::Less : case OperatorMessage::LessOrEqual: case OperatorMessage::NotEqual: 
            case OperatorMessage::And: case OperatorMessage::Or: case OperatorMessage::Plus: case OperatorMessage::Minus: case OperatorMessage::Mul:
            case OperatorMessage::Div:
                buildPredicateMessageFromBinaryPredicate(pred, predmsg, newPredMsg, runtime);           
                break;
            case OperatorMessage::Variable:
                buildPredicateMessageFromVariable(pred, predmsg, newPredMsg, runtime);
                break;
			case OperatorMessage::Not:
                buildPredicateMessageFromUnaryPredicate(pred, predmsg, newPredMsg, runtime);
				break;
        }
                
        return newPredMsg;
    }
    
    template <class Env>
    OperatorMessage::PredicateType DRDF3xClient<Env>::getPredicateType(Selection::Predicate *pred){
        string theType(typeid(*pred).name());
        
        cout << theType << endl;
        if(theType.find("NotEqual") != string::npos){
            return OperatorMessage::NotEqual;            
        }else if(theType.find("Equal") != string::npos){
            return OperatorMessage::Equal;
        }else if(theType.find("Variable") != string::npos){
            return OperatorMessage::Variable;
        }else if(theType.find("Not") != string::npos){
			return OperatorMessage::Not;
		}
        
        return OperatorMessage::Null;
    }
    
    template <class Env>
    void DRDF3xClient<Env>::buildPredicateMessageFromBinaryPredicate(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &runtime){
        Selection::BinaryPredicate *bp = static_cast<Selection::BinaryPredicate*>(pred);
        OperatorMessage::PredicateMessage *leftmsg = buildPredicateMessage(bp->left, predTree, runtime);
        OperatorMessage::PredicateMessage *rightmsg = buildPredicateMessage(bp->right, predTree, runtime);
        leftmsg->set_ppid(predmsg->pid());
        rightmsg->set_ppid(predmsg->pid());        
    }

    template <class Env>
    void DRDF3xClient<Env>::buildPredicateMessageFromUnaryPredicate(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &runtime){
        Selection::UnaryPredicate *bp = static_cast<Selection::UnaryPredicate*>(pred);
        OperatorMessage::PredicateMessage *inputmsg = buildPredicateMessage(bp->input, predTree, runtime);
        inputmsg->set_ppid(predmsg->pid());
    }


    template <class Env>
    void DRDF3xClient<Env>::buildPredicateMessageFromVariable(Selection::Predicate *pred, OperatorMessage::PredicateTree *predTree, OperatorMessage::PredicateMessage *predmsg, Env &runtime){        
        Selection::Variable *vr = static_cast<Selection::Variable*>(pred);
        predmsg->set_regvar(this->getRegisterIndex(runtime, vr->reg));        
    }

    
    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessageFromSelection(OperatorMessage *opmsg, Operator *op, Env &runtime){
        Selection *sel = static_cast<Selection*>(op);
        //Set the type
        opmsg->set_type(OperatorMessage::Selection);
        OperatorMessage::PredicateTree *predicateMsg = opmsg->mutable_predicate();
        buildPredicateMessage(sel->predicate, predicateMsg, runtime);
    }
    
    

    template <class Env>
    void DRDF3xClient<Env>::buildOperatorMessage(OperatorMessage *operatorMessage, DOperator *dop, Env &runtime){
        Operator *theOp = dop->getOperator();    
        string operatorClass(typeid(*theOp).name());
        if(operatorClass.find("FullyAggregatedIndexScan") != string::npos){
            buildOperatorMessageFromFullyAggregatedIndexScan(operatorMessage, theOp, runtime);
        }else if(operatorClass.find("AggregatedIndexScan") != string::npos){
            buildOperatorMessageFromAggregatedIndexScan(operatorMessage, theOp, runtime);
        }else if(operatorClass.find("IndexScan") != string::npos){
            buildOperatorMessageFromIndexScan(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(MergeJoin)){
            buildOperatorMessageFromMergeJoin(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(Sort)){
            buildOperatorMessageFromSort(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(Union)){
            buildOperatorMessageFromUnion(operatorMessage, theOp, runtime);        
        }else if(typeid(*theOp) == typeid(HashGroupify)){
            buildOperatorMessageFromHashGroupify(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(MergeUnion)){
            buildOperatorMessageFromMergeUnion(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(SimpleGroupify)){
            buildOperatorMessageFromSimpleGroupify(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(MultiMergeUnion)){
            buildOperatorMessageFromMultiMergeUnion(operatorMessage, theOp, runtime);
        }else if(typeid(*theOp) == typeid(HashJoin)){
            buildOperatorMessageFromHashJoin(operatorMessage, theOp, runtime);            
        }else if(typeid(*theOp) == typeid(Selection)){
            buildOperatorMessageFromSelection(operatorMessage, theOp, runtime);
        }
    }

    template <class Env>
    void DRDF3xClient<Env>::createPrepareForExecRequest(DOperator *op, PrepareForExecRequest &request, Env &runtime){
        //Let's apply breath search
        queue<DOperator*> queue;
        queue.push(op);
        OperatorTree *tree = request.mutable_tree();
        tree->set_runtimesize(static_cast<google::protobuf::uint64>(runtime.getRegisterCount()));
        assert(runtime.getRegisterCount() > 0);
        google::protobuf::RepeatedPtrField<OperatorMessage> *messages =  tree->mutable_optree();        
        DOperator *current;
        while(!queue.empty()){
            current = queue.front();
            OperatorMessage *newMessage = messages->Add();
            newMessage->set_opid(static_cast<uint64_t>(current->getId()));
            newMessage->set_expectedcardinality(current->getOperator()->getExpectedOutputCardinality());            
            DOperator *parent = current->getParent();
            if(parent != NULL) 
                newMessage->set_parent(static_cast<google::protobuf::uint64>(parent->getId()));        
            else
                newMessage->set_endpointhost(this->senderName);

            newMessage->set_host(current->getHost());
            this->buildOperatorMessage(newMessage, current, runtime);

            vector<DOperator*> &children = current->getChildren();
            for(vector<DOperator*>::iterator it = children.begin(); it != children.end(); ++it){
                queue.push(*it);
            }

            queue.pop();
        }    
    }
    
}
#endif	/* DRDF3XCLIENT_HPP */

