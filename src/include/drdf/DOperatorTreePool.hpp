/* 
 * File:   DOperatorTreePool.hpp
 * Author: luis
 *
 * Created on September 25, 2011, 3:44 PM
 */

#ifndef drdf_doperatortreepool_hpp
#define	drdf_doperatortreepool_hpp

#include <queue>

#include "cts/plangen/Plan.hpp"
#include "cts/infra/QueryGraph.hpp"
#include "drdf/DRuntime.hpp"

using namespace std;

namespace drdf{

    /**
     * A class to manage a pool of operator trees
     */
    class DOperatorTreePool {
    public:
        DOperatorTreePool(DRuntime& runtime, const QueryGraph& query, Plan* plan, unsigned initialSize = 10);
        virtual ~DOperatorTreePool();        
        DOperator* getCleanDOperator();
        vector<Register*> getOutputRegisters(){ return this->outputRegisters; }
        
    private:
        DRuntime &runtime;
        const QueryGraph &queryGraph;
        Plan *plan;
        unsigned size;
        vector<Register*> outputRegisters;
        queue<DOperator*> pool;
        
        void populatePool();
        
    };
}
#endif	/* DOPERATORTREEPOOL_HPP */

