/* 
 * File:   FragmentsDefinitionSegment.hpp
 * Author: lgalarra
 *
 * Created on August 22, 2011, 2:54 PM
 */

#ifndef drdf_fragmentsdefinitionsegment_hpp
#define	drdf_fragmentsdefinitionsegment_hpp

#include <sqlite3.h>
#include <map>
#include <vector>
#include <set>

#include "drdf/operator.h"
#include "drdf/CommonTypes.hpp"
#include "drdf/DSegment.hpp"
#include "drdf/DDatabaseWrapper.hpp"
#include "drdf/DOperator.hpp"

using namespace std;

namespace drdf{
    class DFragmentDefinitionSegment: public DSegment {
    private:        
        map<unsigned, Fragment> mappings;
        map<unsigned, Host> hosts;
        map<unsigned, SPARQLPredicate> predicates;
        map<unsigned, vector<Host> > predicateMappings;
        bool definitionsLoaded;
                
        void loadPredicateDefinitions();
        void loadHosts();
        void loadHostMappings();
        void loadPredicateMappings();
        void loadPredicates();
        
        void setRelevantHostsAggregatedIndexScan(DOperator *op);
        void setRelevantHostsEmptyScan(DOperator *op);
        void setRelevantHostsFilter(DOperator *op);        
        void setRelevantHostsFullyAggregatedIndexScan(DOperator *op);
        void setRelevantHostsHashGroupify(DOperator *op);
        void setRelevantHostsHashJoin(DOperator *op);        
        void setRelevantHostsIndexScan(DOperator *op);
        void setRelevantHostsMergeJoin(DOperator *op);        
        void setRelevantHostsMergeUnion(DOperator *op);
        void setRelevantHostsNestedLoopFilter(DOperator *op);
        void setRelevantHostsNestedLoopJoin(DOperator *op);
        void setRelevantHostsResultsPrinter(DOperator *op);        
        void setRelevantHostsSelection(DOperator *op);   
        void setRelevantHostsSingletonScan(DOperator *op);
        void setRelevantHostsSort(DOperator *op);
        void setRelevantHostsTableFunction(DOperator *op);        
        void setRelevantHostsUnion(DOperator *op);
        
    protected:
        DFragmentDefinitionSegment(DDatabaseWrapper &dataWrapper);        
                
    public:
        enum Type {
             Or, And, Equal, NotEqual, Less, LessOrEqual, Greater, GreaterOrEqual, Plus, Minus, Mul, Div,
             Not, UnaryPlus, UnaryMinus, Literal, Variable, IRI, Null, Function, ArgumentList,
             Builtin_str, Builtin_lang, Builtin_langmatches, Builtin_datatype, Builtin_bound, Builtin_sameterm,
             Builtin_isiri, Builtin_isblank, Builtin_isliteral, Builtin_regex, Builtin_in
        };

        enum Position{Subject = 2, Predicate = 1, Object = 0};       

        
        static DFragmentDefinitionSegment* create(DDatabaseWrapper &wrapper){
            DFragmentDefinitionSegment *newSegment = new DFragmentDefinitionSegment(wrapper);
            return newSegment;
        };                
        ~DFragmentDefinitionSegment();        
        
        /**
         * Returns all hosts registered in the system
         * @return 
         */
        void getAllHosts(vector<Host>&);
        void getAllHosts(set<Host, cmpHosts>&);
        
        /**
         * Returns the number of available hosts in the setup
         * @param 
         * @param 
         * @return 
         */
        unsigned countHosts();
        
        /**
         * Retrieve host by id
         */
        bool getHost(unsigned, Host&);
        
        /**
         * Retrieve host by uri
         */
        bool getHost(string, Host&);
        
        /**
         * Retrieves the information about a predicate given its id
         */
        bool getPredicate(unsigned, SPARQLPredicate&);
        
        /**
         * Return a mapping host -> predicates
         * @return 
         */
        bool getHostMapping(unsigned, Fragment &);        
        
        /**
         * Sets the relevant (those who might contain triples required by the operator) 
         * hosts to the specified operator.
         */
        void setRelevantHosts(DOperator *op);
    };
}

#endif	/* FRAGMENTSDEFINITIONSEGMENT_HPP */

