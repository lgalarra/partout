/* 
 * File:   DDictionarySegment.hpp
 * Author: lgalarra
 *
 * Created on September 1, 2011, 4:34 PM
 */

#ifndef drdf_ddictionarysegment_hpp
#define	drdf_ddictionarysegment_hpp

#include "drdf/DSegment.hpp"
#include "rts/segment/DictionarySegment.hpp"

namespace drdf{
    template <class Env>
    class DRDF3xClient;
    class DRuntime;

    class DDictionarySegment: public DSegment {
    private:        
        static const unsigned stringsCacheSize;
        static const unsigned idsCacheSize;
        vector<DictionaryEntry> stringsCache;
        vector<DictionaryEntry> idsCache;
        DRDF3xClient<DRuntime> *client;
        DictionarySegment *surrogateDict;
                
        bool broadcastLookup(const std::string& text,::Type::ID type,unsigned subType,unsigned& id);
        bool broadcastLookupById(unsigned id, string &result,::Type::ID& type, unsigned& subType);
        
    protected:
        DDictionarySegment(DDatabaseWrapper &dataWrapper, DictionarySegment *segment);

    public:
        static DDictionarySegment* create(DDatabaseWrapper &dataWrapper, DictionarySegment *dict){
            return new DDictionarySegment(dataWrapper, dict);
        }
        
        DDictionarySegment(DDictionarySegment &otherDictionary);
        ~DDictionarySegment();        
        /// Lookup an id for a given string
        bool lookup(const std::string& text,::Type::ID type,unsigned subType,unsigned& id);
        /// Lookup a string for a given id
        bool lookupById(unsigned id, string &result,::Type::ID& type,unsigned& subType);

    };
}

#endif	/* DDICTIONARYSEGMENT_HPP */

