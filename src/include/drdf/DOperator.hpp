/* 
 * File:   DOperator.hpp
 * Author: lgalarra
 *
 * Created on September 5, 2011, 5:47 PM
 */

#ifndef drdf_doperator_hpp
#define	drdf_doperator_hpp

#include <string>
#include <vector>
#include <map>
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "rts/operator/ResultsPrinter.hpp"
#include "infra/osdep/Timestamp.hpp"
#include "drdf/CommonTypes.hpp"
#include "drdf/operator.h"


using namespace std;

namespace drdf{
     class DOperatorTransformer;
     class DFragmentDefinitionSegment;
     class DOperatorTreePool;
    /**
     * Information about a physical operator and its
     * the relevant hosts.
     */
    class DOperator {
    public:
        enum Type{
            TAggregatedIndexScan, TEmptyScan, TFilter, TFullyAggregatedIndexScan, THashGroupify, THashJoin, TIndexScan, TMergeJoin, TMergeUnion, TNestedLoopFilter,
            TNestedLoopJoin, TResultsPrinter, TSelection, TSingletonScan, TSort, TTableFunction, TUnion, TRemoteFetch, TRemoteSend, TSimpleGroupify, TMultiMergeUnion };        
    private:        
        static unsigned opid;
        
        Type type;
        
        Operator *op;
        //Host that might contain information relevant to the operator
        //It is only used by leaves like index scans.
        vector<Host> relevantHosts;
        //Where the operator will be actually evaluated
        string home;
        //Children operators
        vector<DOperator*> children;
        //Parent
        DOperator *parent;
        //Unique identifier
        unsigned id;
        //Estimated cost given an estimator
        double estimatedCost;
        //Important registers
        vector<Register*> outputRegisters;
        //Duplicate handling (for the top printer operator)
        ResultsPrinter::DuplicateHandling duplicateHandling;
        
        
        void setType();
    public:
        DOperator(Operator *op): op(op), home(""), id(++opid), parent(0), estimatedCost(-1){
            setType();
        }
        
        DOperator(Operator *op, unsigned id, string home): op(op), id(id), home(home), parent(0), estimatedCost(-1){
            setType();
        }
        
        DOperator(Operator *op, string home): op(op), id(++opid), home(home), parent(0), estimatedCost(-1){
            setType();
        }

        ~DOperator(){
            while(!children.empty()){
                DOperator *toRemove = *children.begin();
                children.erase(children.begin());
                delete toRemove;
            }
        }
        
        Timestamp start, end;
        bool isLeaf(){ return children.empty(); }
        bool isRoot(){ return parent == NULL; }
        unsigned getId(){ return id; } 
        vector<DOperator*>& getChildren(){ return children; }
        DOperator * getParent() { return parent; }
        void setParent(DOperator *parent){ this->parent = parent; }                
        Type getType(){ return type; }
        string getHost(){ return home; }       
        void setHost(string host){ this->home = host; }
        Operator* getOperator(){ return op; }
        bool isAnyIndexScan(){ return type == TAggregatedIndexScan || type == TFullyAggregatedIndexScan || type == TIndexScan; }
        bool isEfficientJoin(){ return type == TMergeJoin || type == THashJoin; }              
        void print(DebugPlanPrinter &dpp);        
        void addRelevantHost(Host host){ this->relevantHosts.push_back(host); } 
        DOperator* getChild(unsigned i){ return this->children[i]; }
        double getEstimatedCost(){ return this->estimatedCost; }
        void setEstimatedCost(double estimate){ this->estimatedCost = estimate; }
        vector<Host>& getRelevantHosts(){ return this->relevantHosts; }
        vector<Register*>& getOutputRegisters(){ return this->outputRegisters; }
        void setOutputRegisters(vector<Register*> &registers){ this->outputRegisters = registers; }
        
        unsigned countLeaves();
        void getLeaves(vector<DOperator*> &leaves);

        void setDuplicateHandling(ResultsPrinter::DuplicateHandling dh){this->duplicateHandling = dh; }
        ResultsPrinter::DuplicateHandling getDuplicateHandling(){ return this->duplicateHandling; }
        void normalizeIds();
        void extractHostMappings(map<unsigned, string> &mappings);       
        bool isMergingOperator(){ return this->type == TSimpleGroupify || this->type == TMergeUnion || this->type == TMultiMergeUnion; }
        DOperator *find(unsigned id);

        friend class DOperatorTransformer;
        friend class DFragmentDefinitionSegment;
        friend class DOperatorTreePool;
    };
    
    
}



#endif	/* DOPERATOR_HPP */

