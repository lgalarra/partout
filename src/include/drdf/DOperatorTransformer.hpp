/* 
 * File:   DTransformer.hpp
 * Author: lgalarra
 *
 * Created on September 5, 2011, 5:56 PM
 */

#ifndef drdf_doperatortransformer_hpp
#define	drdf_doperatortransformer_hpp

#include "drdf/DOperator.hpp"
#include "drdf/DDatabase.hpp"
#include "drdf/CommonTypes.hpp"
#include "drdf/DRuntime.hpp"
#include <set>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <iomanip>


class AggregatedIndexScan;
class EmptyScan;
class Filter;
class FullyAggregatedIndexScan;
class HashGroupify;
class HashJoin;
class IndexScan;
class MergeJoin;
class MergeUnion;
class NestedLoopFilter;
class NestedLoopJoin;
class ResultsPrinter;
class Selection;
class SingletonScan;
class Sort;
class TableFunction;
class Union;

using namespace std;
using namespace log4cplus;

namespace drdf{
    
    typedef vector<pair<Register*, Register*> > RegMap;
    /**
     * This class implements transformations to an existing DOperator object
     */
    class DOperatorTransformer {        
    private:
        DDatabase* db;
        
        //Build operators
        DOperator* buildDOperator(AggregatedIndexScan *op);
        DOperator* buildDOperator(EmptyScan *op);
        DOperator* buildDOperator(Filter *op);        
        DOperator* buildDOperator(FullyAggregatedIndexScan *op);
        DOperator* buildDOperator(HashGroupify *op);
        DOperator* buildDOperator(SimpleGroupify *op);
        DOperator* buildDOperator(HashJoin *op);        
        DOperator* buildDOperator(IndexScan *op);
        DOperator* buildDOperator(MergeJoin *op);        
        DOperator* buildDOperator(MergeUnion *op);
        DOperator* buildDOperator(MultiMergeUnion *op);
        DOperator* buildDOperator(NestedLoopFilter *op);
        DOperator* buildDOperator(NestedLoopJoin *op);
        DOperator* buildDOperator(ResultsPrinter *op);        
        DOperator* buildDOperator(Selection *op);   
        DOperator* buildDOperator(SingletonScan *op);
        DOperator* buildDOperator(Sort *op);
        DOperator* buildDOperator(TableFunction *op);        
        DOperator* buildDOperator(Union *op);  
        
        /**
         * Builds an operator for every relevant host for the first argument and stores them in 
         * the vector parts. The other arguments are required in case they are used to build a union
         * @param op
         * @param parts
         * @param init
         * @param mappings
         */
        void resolveMultipleSources(DOperator *op, DRuntime *runtime, vector<Operator*> &parts, vector<vector<Register*> > &init, vector<vector<Register*> > &mappings);
        
        /**
         * If the operator has more than one relevant source (which is only applicable to leafs like index scans), 
         * it creates a union + a hash groupify + a sort gathering all the results
         * @param parent
         * @param child
         * @result DOperator* The original operator if nothing must be done, otherwise a point to the new operator
         * (a sort)
         */
        DOperator* addUnionsAndGroupify(DOperator *op, DRuntime *runtime);    
        DOperator* addUnionsAndGroupify(vector<DOperator*> &input, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &allMappings, vector<vector<pair<Register*, Register*> > > &newMappings);            
        
        /**
         * If the operator has more than one relevant source (which is only applicable to leafs like index scans), 
         * it creates a chain of merge unions to retrieve the results
         * @param parent
         * @param child
         * @result DOperator* The original operator if nothing must be done, otherwise a point to the new operator
         * (a sort)
         */
        
        DOperator* addMergeUnionChain(DOperator *op, DRuntime *runtime);
        DOperator* addMultiMergeUnionChain(DOperator *op, DRuntime *runtime);
        DOperator* addMultiMergeUnionChain(vector<DOperator*> &input, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &leftMappings, 
        vector<vector<pair<Register*, Register*> > > &rightMappings, vector<vector<pair<Register*, Register*> > > &newMappings);
        
                
        /**
         * Allocates a single instance of DOperator wrapping the argument.
         * @param Operator
         * @return DOperator
         */
        inline DOperator* wrap(Operator *op){
            return new DOperator(op);
        }
               
        /**
         * Determines whether a join operator has descendants which are joins (merge, hash or nested loop)
         * and they match its type.
         */
        bool hasDescendantsOfType(DOperator *dop, DOperator::Type matchType);
        
        /**
         * Removes a parent-child link
         * @param parent
         * @param child
         */
        void unlinkOperators(Operator *parent, Operator *child);
        
        /**
         * 
         * @param dop
         * @param inputMappings
         */
        void fixProjectionsMergeJoin(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);
        void fixProjectionsHashJoin(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);        
        void fixProjectionsHashGroupify(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);        
        void fixProjectionsSimpleGroupify(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);     
        void fixProjectionsSelection(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);        
        void collectCandidateRegisters(DOperator *dop);
        
        bool findDataSourcesForJoin(DOperator *dop, vector<DOperator*> &leftSources, vector<DOperator*> &rightSources);        
        bool findDataSourcesForMMU(DOperator* child, vector<DOperator*> &sources);
        bool findDataSourcesSimpleGroupify(DOperator* child, vector<DOperator*> &sources);

        /**
         * Combines the given sources using the join type provided as argument.
         * 
         * @param leftSources
         * @param rightSources
         * @param newJoins
         */
        void combineSources(DOperator *dop, vector<DOperator*>& leftSources, vector<DOperator*>& rightSources, vector<DOperator*>& newJoins, DRuntime &runtime, vector<vector<pair<Register*, Register*> > > &leftMappings, vector<vector<pair<Register*, Register*> > > &rightMappings, 
        vector<pair<Register*, Register*> > &unionLeftMappings, vector<pair<Register*, Register*> > &unionRightMappings);
        
        DOperator * buildNewMergeJoin(DRuntime &runtime, DOperator *source, DOperator *newleft, DOperator *newright, double factor, vector<pair<Register*, Register*> > &leftMappings, vector<pair<Register*, Register*> > &rightMappings);
        DOperator * buildNewHashJoin(DRuntime &runtime, DOperator *source, DOperator *newleft, DOperator *newright, double factor, vector<pair<Register*, Register*> > &leftMappings, vector<pair<Register*, Register*> > &rightMappings);
        DOperator * buildNewNestedLoopJoin(DOperator *dop, DOperator *newleft, DOperator *newRight, double factor);
        DOperator * copy(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);
        DOperator * copyIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);
        DOperator * copyAggregatedIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);        
        DOperator * copyFullyAggregatedIndexScan(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);          
        DOperator * copyMergeJoin(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);        
        DOperator * copySelection(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);   
        Selection::Predicate * copyPredicate(Selection::Predicate *source, vector<pair<Register*, Register*> > &outputMappings);        
        DOperator * copyHashJoin(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);                
        DOperator * copyUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);                        
        DOperator * copyMergeUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);                                
        DOperator * copyMultiMergeUnion(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);                                        
        DOperator * copyHashGroupify(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);                                                
        DOperator * copySort(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);        
        DOperator * copySimpleGroupify(DOperator *source, DRuntime &runtime, vector<pair<Register*, Register*> > &outputMappings);        

        DOperator * cloneIndexScan(DOperator *source, Database &adb);
        DOperator * cloneAggregatedIndexScan(DOperator *source, Database &adb);        
        DOperator * cloneFullyAggregatedIndexScan(DOperator *source, Database &adb);          
        DOperator * cloneMergeJoin(DOperator *source);        
        DOperator * cloneHashJoin(DOperator *source);                
        DOperator * cloneUnion(DOperator *source);                        
        DOperator * cloneMergeUnion(DOperator *source);                                
        DOperator * cloneMultiMergeUnion(DOperator *source);                                        
        DOperator * cloneHashGroupify(DOperator *source);                                                
        DOperator * cloneSort(DOperator *source);       
        DOperator * cloneSimpleGroupify(DOperator* source);      
        DOperator * cloneSelection(DOperator *source, DRuntime &runtime);        
        Selection::Predicate * clonePredicate(Selection::Predicate *source);
        
        
    public:
        DOperatorTransformer();
        
        DOperatorTransformer(DDatabase *db);
                        
        ~DOperatorTransformer();
        
        enum MultipleSourcesSolve{ UseUnionAndHashGroupify, UseMergeUnionChain };
               
        /**
         * Links the physical operator contain in op to the physical operator
         * of the parent
         * @param op
         * @return 
         */
        unsigned linkWithParent(DOperator *op);

        
        /**
         * Wraps an operator to include information about its relevant hosts. It recursively applies the function
         * to the children
         * @param op
         * @return 
         */
        DOperator * buildDOperator(Operator *op);
        
        
        /**
         * Creates a pair of RemoteFetch and RemoteSender operators 
         * to encapsulate network communication.
         * @param parent
         * @param child
         */
        DOperator* buildRemoteConnection(DOperator *parent, DOperator *child, Runtime *runtime);
        
        /**
         * If any pair of parent-child operators are evaluated in different
         * hosts, then it creates the respective operator wrappers for the transmission
         * and reception over the network.
         */
        bool prepareForRemoteOperations(string &currentHost, DOperator *op, Runtime *runtime, Logger &logger);
        
        /**
         * Determines the hosts that are relevant to the leaves operators of the tree
         * sent as argument. The method sets the hosts structures in the operator tree
         * and returns a vector of pointers to them.
         */
        void setLeavesHomeHosts(DOperator *, set<Host, cmpHosts> &);
        
        /**
         * Assigns the home hosts (host where the evaluation of the operator takes place)
         * to the provided operator. If the host is not included in the list of relevant hosts for
         * the leaves of the tree, the method returns false, otherwise returns true.
         * @param op
         * @param host
         * @param overwrite If the operator has already a home host, overwrite it!
         */        
        bool assignHomeHost(DOperator *op, string host, bool overwrite = false);
        
        /**
         * Instruments the operator for sending results through the network
         * @param op
         */
        DOperator * addTopNetworkSender(DOperator *op, string destinationHost, Runtime *runtime);

        /*
         * Operators with more than one relevant source are merged using an union
         */
        DOperator* resolveMultipleSourcesLeaves(DOperator *op, DRuntime *runtime);
        
        /**
         * Returns a vector with all joins which have potential to be parallelize. A join can be parallelize if
         * one of its descendants is another join (we will consider joins of the same type for simplicity).
         */
        void getParallelizableJoins(DOperator *op, vector<DOperator*> &output);
        
        /**
         * If the DOperator has been modified to support distributed transactions, fixes the projection registers
         * based on the mappings (it tracks the original register based on the mappings created in unions and merge unions)
         */
        void findProjectionMappingsForOperator(DRuntime* runtime, DOperator *dop, vector<pair<Register*, Register*> > &outputMappings);
        void findProjectionMappingsForOperator2(DOperator *dop, vector<DOperator*> &sources, vector<pair<Register*, Register*> > &unionMappings);        
        
        /*
         * Redirects the registers pointers from the given operator up to the root
         */
        void fixProjections(DOperator *dop, vector<pair<Register*, Register*> > &inputMappings);
        
        /**
         * Required for searches. Given the pointer to a join, it sends it down in the hierarchy
         * making the operation even more parallelizable.
         *
         */
        DOperator* parallelize(DOperator *dop, DRuntime &runtime);
        
        /**
         * Clones an operator tree
         */
        DOperator* clone(DOperator *input, DRuntime &runtime);
        
        /**
         * Applies the operator -> host mappings to an input operator
         * @param input
         * @param mappings
         * @return 
         */
        void applyHostMappings(DOperator *input, map<unsigned, string> &mappings);

        /**
         * Assigns the provided hosts as the home hosts for all the operators in the operator
         * tree rooted at input.
         * @param input
         * @param host
         */
        void forceHomeHost(DOperator *input, string host);
    };
}
#endif	/* DTRANSFORMER_HPP */
