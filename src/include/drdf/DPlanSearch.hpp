/* 
 * File:   DPlanSearch.hpp
 * Author: lgalarra
 *
 * Created on September 26, 2011, 12:26 PM
 */

#ifndef drdf_dplansearch_hpp
#define	drdf_dplansearch_hpp

#include "cts/infra/QueryGraph.hpp"
#include "cts/plangen/Plan.hpp"
#include "drdf/DRuntime.hpp"
#include "drdf/DOperator.hpp"

namespace drdf{

    /**
     * This defines a single transformation within 
     */
    struct DOperatorTransform{
        enum Type{ AHH, PL };
        Type type;
        //Relative id based on location in a breath search
        unsigned affectedOperator;
        //The host assigned to the operator (in case of a AHH)
        string host;
        map<unsigned, string> hostMappings;
        //If the type == AHH then just apply the mappings
        bool ignoreMappings;
        
    };
    
    typedef vector<DOperatorTransform> TransformSchedule;
    
    class DPlanSearch {
    protected:
        DRuntime &runtime;
        const QueryGraph &queryGraph;
        Plan *plan;
        DPlanSearch(DRuntime& runtime, const QueryGraph& query, Plan* plan): runtime(runtime), queryGraph(query), plan(plan){} 
        virtual void buildSchedule(DOperator *op, TransformSchedule &schedule) = 0;
        unsigned countJoins(DOperator* op);
        unsigned countHosts();

    public:
        virtual DOperator* findGoodPlan() = 0;
        virtual ~DPlanSearch(){}
    };
    
    /**
     * Finds a good plan applying a simple local search
     * @return 
     */
    class DPlanLocalSearch: public DPlanSearch{
    private:
        virtual void buildSchedule(DOperator *op, TransformSchedule &schedule);
    public:
        DPlanLocalSearch(DRuntime& runtime, const QueryGraph& query, Plan* plan): DPlanSearch(runtime, query, plan){}         
        DOperator* findGoodPlan();
        
    };
    
    class DPlanSASearch: public DPlanSearch{
    private:
        virtual void buildSchedule(DOperator *op, TransformSchedule &schedule);
        virtual double temperature(double oldCost, double newCost, unsigned iteration);
    public:        
        DPlanSASearch(DRuntime& runtime, const QueryGraph& query, Plan* plan): DPlanSearch(runtime, query, plan){}         
        DOperator* findGoodPlan();
    };
    
    /**
     * Does nothing, simply returns the first plan
     * generated by RDF3x cost model and query processor
     */
    class DPlanTrivialSearch: public DPlanSearch{
    private:
        void buildSchedule(DOperator *op, TransformSchedule &schedule){}
    public:
        DPlanTrivialSearch(DRuntime& runtime, const QueryGraph& query, Plan* plan): DPlanSearch(runtime, query, plan){}         
        DOperator* findGoodPlan();
    };    
    
}
#endif	

