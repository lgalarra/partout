/* 
 * File:   DResultsPrinter.hpp
 * Author: luis
 *
 * Created on September 28, 2011, 11:42 PM
 */

#ifndef drdf_dresultsprinter_hpp
#define	drdf_dresultsprinter_hpp

#include "drdf/DOperator.hpp"
#include "server/RDF3xServerExecMessages.h"
#include "drdf/DRuntime.hpp"
#include <set>

namespace drdf{
    class DResultsPrinter: public Operator {        
    public:
        DResultsPrinter(DRuntime& runtime, drdf::message::ExecResponse &execResp, DOperator *dop, double observedCard, unsigned limit, bool silent);
        ~DResultsPrinter();
        
       /// Produce the first tuple
       unsigned first();
       /// Produce the next tuple
       unsigned next();
       
       /// Print the operator tree. Debugging only.
       void print(PlanPrinter& out){}
       /// Add a merge join hint
       void addMergeHint(Register* reg1,Register* reg2){}
       /// Register parts of the tree that can be executed asynchronous
       virtual void getAsyncInputCandidates(Scheduler& scheduler){}

        
    private:
        
        struct DictionaryEntry{
            unsigned id;
            string text;
        };
                
        DRuntime &runtime;
        drdf::message::ExecResponse &data;
        DDictionarySegment &dictionary;
        int cursor;
        int nRegisters;
        vector<DictionaryEntry> hashCache;
        static const unsigned cacheLimit;
        DOperator *theOperator;
        multiset<uint64_t> outputRegisters;
        vector<uint64_t> outputRegistersUnsorted;
        double observedCard;
        unsigned limit;        
        bool silent;
        
        string output(uint64_t value);
    };
}
#endif	/* DRESULTSPRINTER_HPP */

