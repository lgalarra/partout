#ifndef drdf_dexactstatisticssegment_hpp
#define	drdf_dexactstatisticssegment_hpp

/* 
 * File:   DExactStatisticsSegment.hpp
 * Author: lgalarra
 *
 * Created on September 3, 2011, 5:18 PM
 */

#include "drdf/DDatabaseWrapper.hpp"
#include "drdf/CommonTypes.hpp"
#include "drdf/DSegment.hpp"
#include <map>

using namespace std;

namespace drdf{
    class DExactStatisticsSegment: public DSegment {        
    private:
        map<Triple, StatisticsEntry> cacheJoins;
        map<Triple, unsigned> cacheCards;
        static const unsigned cacheJoinSize;
        static const unsigned cacheCardsSize;
        unsigned int totalCardinality;
        static const unsigned total_cardinality_key;
        
        string getCardinalityQuery(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant);        
        string getCardinalityQuery(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, string host);      
        string getCardinalityQuery(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, unsigned id_host);              
        void freeCacheCardsSpace();
        void freeCacheJoinsSpace();
        bool getJoinInfo(unsigned long long* joinInfo,unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant);    
        unsigned getHostId(string host);
    protected:
        DExactStatisticsSegment(DDatabaseWrapper &dataWrapper);

    public:        
        static DExactStatisticsSegment* create(DDatabaseWrapper &dataWrapper){
            return new DExactStatisticsSegment(dataWrapper);
        }
                
        DExactStatisticsSegment(const DExactStatisticsSegment& orig);
        ~DExactStatisticsSegment();
        
        /// Compute the cardinality of a single pattern
        unsigned getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant);     
        unsigned getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, string host);
        unsigned getCardinality(unsigned subjectConstant,unsigned predicateConstant,unsigned objectConstant, unsigned host);
        /// Compute the join selectivity
        double getJoinSelectivity(bool s1c,unsigned s1,bool p1c,unsigned p1,bool o1c,unsigned o1,bool s2c,unsigned s2,bool p2c,unsigned p2,bool o2c,unsigned o2);

    };
}

#endif