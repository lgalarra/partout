#ifndef drdf_dsegments_hpp
#define	drdf_dsegments_hpp

/* 
 * File:   DSegments.hpp
 * Author: lgalarra
 *
 * Created on August 27, 2011, 3:38 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. *  * 
 */
#include <math.h>
#include "rts/database/Database.hpp"
#include "drdf/DSegment.hpp"
#include "drdf/DDatabaseWrapper.hpp"

using namespace std;

namespace drdf{
    class DAggregatedFactsSegment: public DSegment{
    private:
        ///How many entries are loaded in memory
        unsigned limit;
        //Cursor
        unsigned limitStart;
        //Level 1 and 2 cardinalities
        unsigned groups1, groups2;       
        //Table name
        string table;        
        //Tag (table with the information)
        ATag tag;
        //Metadata tag for the full cardinality
        DDatabaseWrapper::MetadataTag g1MetaTag, g2MetaTag;
        //Metadata tag for the number of pages
        DDatabaseWrapper::MetadataTag pagesMetaTag;
     protected:
        explicit DAggregatedFactsSegment(DDatabaseWrapper &dpartition, ATag tag, unsigned pageSize); 
       
     public:
        static DAggregatedFactsSegment* create(DDatabaseWrapper &ddp, ATag tag, unsigned pageSize){
            DAggregatedFactsSegment *newSegment = new DAggregatedFactsSegment(ddp, tag, pageSize);
            return newSegment;
        }
        
        unsigned getLevel1Groups() const { return groups1; }
        unsigned getLevel2Groups() const { return groups2; }

        /// A scan over the facts segment
        class Scan {
        private:

            DAggregatedFactsSegment* seg;
            /// The decompressed triples
            TripleSet triples;
            TripleSet::iterator cursor;

            Scan(const Scan&);
            void operator=(const Scan&);

            /// Perform a binary search
            bool find(unsigned value1,unsigned value2);
            /// Read the next page
            bool readNextPage();
            /// Read first item of the page
            bool first();
          
        public:
            /// Destructor
            ~Scan();

            /// Start a new scan over the whole segment and reads the first entry
            bool first(DAggregatedFactsSegment& segment);
            /// Start a new scan starting from the first entry >= the start condition and reads the first entry
            bool first(DAggregatedFactsSegment& segment,unsigned start1,unsigned start2);

            /// Read the next entry
            bool next();
            /// Get the first value
            unsigned getValue1(){ return cursor->value1; }
            /// Get the second value
            unsigned getValue2(){ return cursor->value2; }
            /// Get the count
            unsigned getCount(){ return cursor->value3; }

            /// Close the scan
            void close();
        };
       
        friend class Scan;
    };
    
    class DFullyAggregatedFactsSegment: public DSegment{
    private:
       ///How many entries are loaded in memory
        unsigned limit;
        //Cursor
        unsigned limitStart;
        //Total entries
        unsigned groups1;
        //Tag (table with the information)
        FATag tag;
        //Table name
        string table;
        //Number of items fetched
        unsigned pageSize;
        //Cardinality of the segment        
        DDatabaseWrapper::MetadataTag gMetatag;
        //Number of pages in the segment 
        DDatabaseWrapper::MetadataTag pagesMetatag;


        DFullyAggregatedFactsSegment(const DFullyAggregatedFactsSegment&);
        void operator=(const DFullyAggregatedFactsSegment&);
        
        friend class DatabaseBuilder;

    protected:
        /// Constructor
        explicit DFullyAggregatedFactsSegment(DDatabaseWrapper &ddp, FATag tag, unsigned pageSize);

        
    public:
         static DFullyAggregatedFactsSegment* create(DDatabaseWrapper &wrapper, FATag tag, unsigned pageSize){
            DFullyAggregatedFactsSegment *newSegment = new DFullyAggregatedFactsSegment(wrapper, tag, pageSize);
            return newSegment;
         }
        
         unsigned getLevel1Groups() const { return this->groups1; }
         
         /// A scan over the facts segment
         class Scan {
         private:              
            /// The segment
            DFullyAggregatedFactsSegment* seg;
            /// The decompressed triples
            PairSet pairs;
            /// Current cursor of the scan
            PairSet::iterator cursor;

            Scan(const Scan&);
            void operator=(const Scan&);

            /// Perform a binary search
            bool find(unsigned value1);
            /// Read the next page
            bool readNextPage();
            /// Read the first pair
            bool first();
          
        public:
            /// Destructor
            ~Scan();

            /// Start a new scan over the whole segment and reads the first entry
            bool first(DFullyAggregatedFactsSegment& segment);
            /// Start a new scan starting from the first entry >= the start condition and reads the first entry
            bool first(DFullyAggregatedFactsSegment& segment, unsigned start1);

            /// Read the next entry
            bool next();
            /// Get the first value
            unsigned getValue1() const { return this->cursor->value1; }
            /// Get the count
            unsigned getCount() const { return this->cursor->value2; }

            /// Close the scan
            void close();
       };
       
       friend class Scan;        
    };
    
     /**
      * Proxy class to simulate we have the data
      */
     class DFactsSegment: public DSegment{
     private:
        unsigned totalCardinality;
        //Have to figure out!
        unsigned groups1, groups2;
        
        Database::DataOrder order;
        
        DFactsSegment(const DFactsSegment &otherSegment);
        void operator=(const DFactsSegment &otherSegment);

        void loadMetadata();
        
        friend class DDatabaseBuilder;
        
     protected:
        /// Constructor
        explicit DFactsSegment(DDatabaseWrapper& wrapper, Database::DataOrder order);

     public:
        static const unsigned pageSize;

        ~DFactsSegment(){}
       
        static DFactsSegment* create(DDatabaseWrapper &wrapper, Database::DataOrder order){
            DFactsSegment *newSegment = new DFactsSegment(wrapper, order);
            return newSegment;
        }
              
        /// Get the number of level 1 groups
        unsigned getLevel1Groups() const { return groups1; }
        /// Get the number of level 2 groups
        unsigned getLevel2Groups() const { return groups2; }
        /// Get the total cardinality
        unsigned getCardinality() const { return totalCardinality; }        

       
    };
}

#endif	/* DSEGMENTS_HPP */

