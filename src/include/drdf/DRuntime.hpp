/* 
 * File:   DRuntime.hpp
 * Author: luis
 *
 * Created on September 5, 2011, 10:57 PM
 */

#ifndef drdf_druntime_hpp
#define	drdf_druntime_hpp

#include <vector>
#include "rts/runtime/Runtime.hpp"
#include "drdf/DDatabase.hpp"

using namespace std;

namespace drdf{
    class DRuntime{
    private:
        
        Runtime &runtime;
        
        vector<Register> extendedRegisters;
        
        DDatabase &ddb;
        
        size_t registerCount;

        std::vector<PotentialDomainDescription> domainDescriptions;

    public:
        
        static const unsigned maxExtendedRegisters;
        
        /// Destructor
        ~DRuntime(){}
        
        ///Constructor
        DRuntime(DDatabase &ddb, Runtime &r): runtime(r), ddb(ddb), registerCount(r.getRegisterCount()){
            extendedRegisters.reserve(maxExtendedRegisters);
            domainDescriptions.reserve(maxExtendedRegisters);
        }        

        Database& getDatabase() const { return runtime.getDatabase(); }
        /// Get the database
        DDatabase& getDDatabase() const { return ddb; }
        /// Has a differential index?
        bool hasDifferentialIndex() const { return runtime.hasDifferentialIndex(); }
        /// Get the differential index
        DifferentialIndex& getDifferentialIndex() const { return runtime.getDifferentialIndex(); }
        /// Has a temporary dictionary?
        bool hasTemporaryDictionary() const { return runtime.hasTemporaryDictionary(); }
        /// Get the temporary dictionary
        TemporaryDictionary& getTemporaryDictionary() const { return runtime.getTemporaryDictionary(); }
        /// Set the number of registers
        void allocateRegisters(unsigned count){ 
            runtime.allocateRegisters(count); 
            extendedRegisters.clear();
            registerCount = runtime.getRegisterCount(); 
        }
        /// Extend runtime
        void requestExtraAllocation(unsigned n);
        /// Get the number of registers
        unsigned getRegisterCount() const { return registerCount; }
        /// Access a specific register
        Register* getRegister(unsigned slot);
        /// Set the number of domain descriptions
        void allocateDomainDescriptions(unsigned count){ 
            runtime.allocateDomainDescriptions(count); 
            domainDescriptions.clear();            
        }
        /// Access a specific domain description
        PotentialDomainDescription* getDomainDescription(unsigned slot);
        /// Get the the index of a register
        uint64_t registerOffset(Register *r);
        //Get a runtime object
        Runtime & getRuntime(){ return runtime; }
        
        
    };

}

#endif