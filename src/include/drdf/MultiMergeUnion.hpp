/* 
 * File:   MultiMergeUnion.hpp
 * Author: luis
 *
 * Created on October 10, 2011, 12:20 PM
 */

#ifndef drdf_multimergeunion_hpp
#define	drdf_multimergeunion_hpp

#include <vector>
#include "rts/operator/Operator.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include <boost/thread.hpp>

using namespace std;

namespace drdf{
    class DOperatorTransformer;
    class DOperator;
    template <class Env>
    class DRDF3xClient;
    class DRuntime;
}

class MultiMergeUnion: public Operator {    
public:
    MultiMergeUnion(vector<Register*> &result, Operator* left, vector<Register*> &leftReg, Operator* right,vector<Register*> &rightReg, double expectedOutputCardinality);
    
    ~MultiMergeUnion();
    
   /// Produce the first tuple
   unsigned first();
   /// Produce the next tuple
   unsigned next();

   /// Print the operator tree. Debugging only.
   void print(PlanPrinter& out);
   /// Add a merge join hint
   void addMergeHint(Register* reg1,Register* reg2);
   /// Register parts of the tree that can be executed asynchronous
   void getAsyncInputCandidates(Scheduler& scheduler);
   
   unsigned degree(){ return result.size(); }
   
   friend class drdf::DOperatorTransformer;
   friend class drdf::DOperator;
   friend class drdf::DRDF3xClient<Runtime>;   
   friend class drdf::DRDF3xClient<drdf::DRuntime>;      

    
private:
   /// Possible states
   enum State { done, stepLeft, stepRight, stepBoth, leftEmpty, rightEmpty };

   typedef vector<unsigned> Tuple;
   
   bool lt(const Tuple &a, const Tuple &b) const;
   bool gt(const Tuple &a, const Tuple &b) const;
   /// The input
   Operator* left,*right;
   /// The input registers
   vector<Register*> leftReg, rightReg;
   /// The result register
   vector<Register*> result;
   /// The values
   Tuple leftValue,rightValue;
   /// The counts
   unsigned leftCount,rightCount;
   /// The state
   State state;
   
   void fetchFirstRight();
   void fetchFirstLeft();
      
};

#endif	/* MULTIMERGEUNION_HPP */

