/* 
 * File:   DCodeGen.hpp
 * Author: luis
 *
 * Created on September 6, 2011, 11:41 PM
 */

#ifndef drdf_dcodegen_hpp
#define	drdf_dcodegen_hpp

#include "infra/osdep/Timestamp.hpp"

class Operator;
class Plan;
class Register;
class Runtime;
class QueryGraph;

namespace drdf{
    class DRuntime;
    struct Host;
    
    struct SAStatistics{
        //Start time for issuing the query
        Timestamp startTime;
        //Size of the first, original operator tree
        Timestamp originalOpTreeSize;
        //The depth of the first, original operator tree
        Timestamp originalOpTreeDepth;
        //Number of different hosts relevant to this query
        unsigned relevantHosts;
        //How many different partialOpTrees were used in the search
        Timestamp partialOpTrees;
        //Timestamp before sending the prepare for exec request
        Timestamp prepareForExecStartStamp;
        //Timestamp after receiving a reply for prepare for exec request
        Timestamp prepareForExecEndStamp;
        //Timestamp before sending exec request
        Timestamp execStartStamp;
        //Timestamp after sending exec request
        Timestamp execEndStamp;
        //Timestamp after retrieving all results
        Timestamp endTime;
        //Component of the response time estimated in network transmissions
        double estimatedCommunicationCost, estimatedTotalCommunicationCost, estimatedResponseTime;
        //Query Cardinality
        double cardinality;
        
    };
    
    class DCodeGen {
    public:
        /// Collect all variables contained in a plan
        static void collectVariables(std::set<unsigned>& variables,Plan* plan);
        /// Translate an execution plan into an operator tree without output generation
        static Operator* translateIntern(DRuntime& runtime,const QueryGraph& query,Plan* plan,std::vector<Register*>& output);
        /// Translate an execution plan into an operator tree
        static Operator* translate(DRuntime& runtime,const QueryGraph& query,Plan* plan, std::vector<Register*> &output,bool silent=false);
    };
}
#endif	/* DCODEGEN_HPP */
