#ifndef drdf_constants_hpp
#define	drdf_constants_hpp

/* 
 * File:   constants.hpp
 * Author: lgalarra
 *
 * Created on October 11, 2011, 1:20 PM
 * Project: DRDF-3X
 * Copyright: 2011 Luis Galarraga
 * 
 * This work is licensed under the Creative Commons
 * Attribution-Noncommercial-Share Alike 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300,
 * San Francisco, California, 94105, USA. *  * 
 */

#define MAX_REGISTERS_PER_REMOTE_PAGE 1024 //In number of registers
#define FORCE_MULTIMERGE_UNIONS 1
#define FORCE_ALL_HOSTS 0




#endif	/* CONSTANTS_HPP */

