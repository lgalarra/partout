/* 
 * File:   DDebugPlanPrinter.hpp
 * Author: luis
 *
 * Created on September 7, 2011, 12:09 AM
 */

#ifndef drdf_ddebugplanprinter_hpp
#define	drdf_ddebugplanprinter_hpp

#include "drdf/DRuntime.hpp"
#include "rts/database/Database.hpp"
#include "rts/runtime/Runtime.hpp"
#include "rts/operator/PlanPrinter.hpp"
#include "rts/operator/Operator.hpp"

namespace drdf{
    class DDebugPlanPrinter: public DebugPlanPrinter {
    protected:
    
        DDebugPlanPrinter(DRuntime &druntime, Runtime& runtime, bool showObserved): DebugPlanPrinter(runtime, showObserved), druntime(druntime){}        
        
        DDebugPlanPrinter(DRuntime &druntime, std::ostream& out,Runtime& runtime,bool showObserved): DebugPlanPrinter(out, runtime, showObserved), druntime(druntime){}        

    public:
        
        static DDebugPlanPrinter* create(DRuntime &runtime, bool showObserved){
            Database dummyDb;
            Runtime dummy(dummyDb);
            return new DDebugPlanPrinter(runtime, dummy, showObserved);
            
        }

        /// Format a register (for generic annotations)
        std::string formatRegister(const Register* reg);
        
        /// Format a constant value (for generic annotations)
        std::string formatValue(unsigned value);
        
        ~DDebugPlanPrinter(){}
        
    private:
        DRuntime &druntime;        
    };
}
#endif	/* DDEBUGPLANPRINTER_HPP */

